﻿using System;
using System.Collections.Generic;
using UnityEngine;
using SCamera.Type;
using SCamera.State;
using Object.Movement;
using Object.MovementInput;
using System.Linq;
using Object.MovementRestriction;
using Object.Type;
using Object.FSM;
using Object.CameraZoomInput;
using Object.CameraObject.ActionInput;
using Object.ControlSetup;

public class InteractionManager : ImmotralMonobehaviour {

    [Header("Manage Input")]

    // This variable can enable/disableall interaction from the user throught this manager
    public bool allowUserInput = true;

    // This variable can enable/disable all interaction from the ai throught this manager
    public bool allowAIInput = true;

    // This variable can enable/disable the object interaction throught this manager
    public bool allowSubjectControl = true;

    // This variable can enable/disable the camera interaction throught this manager
    public bool allowCameraControl = true;


    [Header("Manage Cameras/Perspectives and Controlls")]

    // List of relevant cameraTypes
    public List<CameraTypeEnum> relevantCameraTypes;

    // List of relevant cameraTypes
    public List<ObjectTypeEnum> relevantInteractableObjectTypes;

    // List of all cameras in the scene, with the relevant cameraType
    private List<GameObject> cameras;

    // current camera perspective
    private GameObject currentCamera;

    // current camera perspective
    private GameObject currentSubject;

    // string that can be: "[Platformname]" or ""
    [SerializeField] private string currentCameraPlatformConversion = "";
    [SerializeField] private string currentSubjectPlatformConversion = "";

    // string that can be: "default", "custom" or ""
    [SerializeField] private string currentCameraControlSetup = "";
    [SerializeField] private string currentSubjectControlSetup = "";

    // Use this for pre-initialization
    protected override void Awake()
    {
        // inheritted Awake executions
        base.Awake();

        if(relevantCameraTypes == null)
            relevantCameraTypes = new List<CameraTypeEnum>();

        if(relevantInteractableObjectTypes == null)
            relevantInteractableObjectTypes = new List<ObjectTypeEnum>();

        if(cameras == null)
            cameras = new List<GameObject>();

    }

    // Use this for initialization
    protected virtual void Start()
    {
        // Gathers all cameras from the GameManager that have the relevant cameraType
        //FindCameras();

        // Fills an array with the deafult inputcontrols for each cameraMovement
        //setDefaultCameraMovementInput();

        // Sets first of cameraPerspectiveList as default camera perspective
        //if (cameras.Count > 0) SetCurrentCamera(cameras[0]);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (currentCamera != null)
            InteractCamera();

        if (currentSubject != null)
            InteractSubject();
    }

    // Initializes and fills the list of cameraperspective GameObjects, these are found in the list of all Camera's in the GameManager
    protected virtual void FindCameras()
    {
        cameras.Clear();

        // Get all camera's from GameManger
        CameraView[] allFoundCameras = Resources.FindObjectsOfTypeAll<CameraView>();

        foreach (CameraTypeEnum cameraType in relevantCameraTypes)
        {
            foreach (CameraView camera in allFoundCameras)
                if (camera.cameraControlSettings.cameraType == cameraType)
                    cameras.Add(camera.currentGameObject);
        }
    }

    // Sets the current camera to interact
    protected virtual void SetCurrentCamera(GameObject controllableCamera)
    {
        // Update all possible cameras
        FindCameras();

        if (cameras.Contains(controllableCamera))
        {
            // Disable all camera's except for this one
            foreach (GameObject camera in cameras)
                if(camera != controllableCamera)
                    camera.gameObject.SetActive(false);

            currentCamera = controllableCamera;
            currentCamera.SetActive(true);
        }
    }

    // Sets the current camera to interact
    protected virtual void SetCurrentSubject(GameObject controllableSubject, bool UseSubjectCamera)
    {
        if (controllableSubject.GetComponent<ObjectControlView>() != null)
        {
            if (controllableSubject.GetComponent<ObjectControlView>())
                currentSubject = controllableSubject;

            if (currentSubject != null)
            {
                // Look for camera
                if (UseSubjectCamera == true)
                    if (currentSubject.GetComponentsInChildren<CameraView>(true) != null && currentSubject.GetComponentsInChildren<CameraView>(true).Length >= 1)
                        SetCurrentCamera(currentSubject.GetComponentsInChildren<CameraView>(true)[0].gameObject);
            }
        }


    }

    public GameObject GetCurrentSubject()
    {
        return currentSubject;
    }

    // Rturns the current camera to interact
    public GameObject GetCurrentCamera()
    {
        return currentCamera;
    }

    // Sets the next camera from the "camara" list as the current camera to interact
    protected virtual void SetNextCamera()
    {
        SetCurrentCamera(cameras[(cameras.IndexOf(currentCamera) + 1) == cameras.Count ? 0 : (cameras.IndexOf(currentCamera) + 1)]);
    }

    // Execute the subject interaction based on player's inputcontrol input, if allowed 
    protected virtual void InteractCamera()
    {
        if (currentCamera.GetComponent<Camera>() != null)
            currentCamera.GetComponent<Camera>().enabled = true;

        if (currentCamera.GetComponent<CameraView>() != null && currentCamera.GetComponent<CameraView>().ControlSettings != null)
        {
            if (currentCamera.GetComponent<CameraView>().ControlSettings.controlSetup.ToString() != currentCameraControlSetup)
                SetAvailableCameraControls();

            // Make convert Array to list (For ease)
            ObjectMovementRestrictionEnum[] movementRestrictions = currentCamera.GetComponent<CameraView>().cameraControlSettings.movementRestrictions;

            // If camera is movement is locked, do nothing, else send camera move commands
            if (movementRestrictions == null || Array.IndexOf(movementRestrictions, ObjectMovementRestrictionEnum.LOCKEDPOSITION) < 0)
                MoveCamera();

            // If camera is rotation is locked, do nothing, else send camera rotate commands
            if (movementRestrictions == null || Array.IndexOf(movementRestrictions, ObjectMovementRestrictionEnum.LOCKEDROTATION) < 0)
                RotateCamera();

            // If camera is rotation around is locked, do nothing, else send camera rotatearound commands
            if (movementRestrictions == null || Array.IndexOf(movementRestrictions, ObjectMovementRestrictionEnum.LOCKEDROTATIONAROUND) < 0)
                RotateAroundCamera();

            // If camera is rotation around is locked, do nothing, else send camera rotatearound commands
            if (movementRestrictions == null || Array.IndexOf(movementRestrictions, ObjectMovementRestrictionEnum.LOCKEDZOOM) < 0)
                ZoomCamera();
        }
    }

    // Execute the subject interaction based on player's inputcontrol input, if allowed 
    protected virtual void InteractSubject()
    {
        if (currentSubject.GetComponent<ComplexObjectView>() != null)
            currentSubject.GetComponent<ComplexObjectView>().enabled = true;

        if (currentSubject.GetComponent<ComplexObjectView>() != null && currentSubject.GetComponent<ComplexObjectView>().ControlSettings != null)
        {
            if (currentSubject.GetComponent<ComplexObjectView>().ControlSettings.controlSetup.ToString() != currentSubjectControlSetup)
                SetAvailableSubjectControls();

            // Make convert Array to list (For ease)
            ObjectMovementRestrictionEnum[] movementRestrictions = currentSubject.GetComponent<ComplexObjectView>().ControlSettings.movementRestrictions;

                // If camera is movement is locked, do nothing, else send camera move commands
                if (movementRestrictions == null || Array.IndexOf(movementRestrictions, ObjectMovementRestrictionEnum.LOCKEDPOSITION) < 0)
                    MoveSubject();

                // If camera is rotation is locked, do nothing, else send camera rotate commands
                if (movementRestrictions == null || Array.IndexOf(movementRestrictions, ObjectMovementRestrictionEnum.LOCKEDROTATION) < 0)
                    RotateSubject();

                // If camera is rotation around is locked, do nothing, else send camera rotatearound commands
                if (movementRestrictions == null || Array.IndexOf(movementRestrictions, ObjectMovementRestrictionEnum.LOCKEDROTATIONAROUND) < 0)
                    RotateAroundSubject();
        }
    }


    // Chechks if the currentCamera is an FPS or omniscent
    //protected virtual bool CheckIfCameraIsValid()
    //{
    //    List<GameObject> listValidCameras = cameras.FindAll(c => c.GetComponent<CameraSettingsController>().cameraType == this.cameraType);

    //    if (listValidCameras != null || listValidCameras.Count > 0)
    //        return true;
    //    else
    //        return false;
    //}

    // Moves the camera 
    protected virtual void MoveCamera()
    {
        // complexObject controlSettings
        CameraControlModel cameraControlSettings = GetCurrentCamera().GetComponent<CameraView>().cameraControlSettings;

        //
        if (cameraControlSettings.availableObjectMovementInput == null)
            SetAvailableCameraControls();

        if (cameraControlSettings.availableObjectMovementInput == null)
            return;

        // Moves using AI instructions
        if (cameraControlSettings.cameraState == CameraStateEnum.AUTO)
        {
            //TODO
        }

        // Moves using User inputcontrols
        if (cameraControlSettings.cameraState == CameraStateEnum.MANUAL)
        {
            foreach (ObjectMovementInput movementInput in cameraControlSettings.availableObjectMovementInput)
            {
                switch (movementInput.movement)
                {
                    case ObjectMovementEnum.MOVELATERAL:
                        float xTranslation;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                xTranslation = Input.GetAxis(input) * cameraControlSettings.lateralMovementSpeed;
                                xTranslation *= Time.deltaTime;
                                currentCamera.transform.Translate(new Vector3(xTranslation, 0, 0));
                            }
                        }
                        break;
                    case ObjectMovementEnum.MOVEVERTICAL:
                        float yTranslation;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                yTranslation = Input.GetAxis(input) * cameraControlSettings.verticalMovementSpeed;
                                yTranslation *= Time.deltaTime;
                                currentCamera.transform.Translate(new Vector3(0, yTranslation, 0));
                            }
                        }
                        break;
                    case ObjectMovementEnum.MOVELONGITUDINAL:
                        float zTranslation;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                zTranslation = Input.GetAxis(input) * cameraControlSettings.longitudinalMovementSpeed;
                                zTranslation *= Time.deltaTime;
                                currentCamera.transform.Translate(new Vector3(0, 0, zTranslation));
                            }
                        }
                        break;
                    default:
                        // if nothing else matches, do the default
                        break;
                }
            }
        }
    }

    // Moves the object 
    protected virtual void MoveSubject()
    {
        // complexObject controlSettings
        ObjectControlModel objectControlSettings = currentSubject.GetComponent<ComplexObjectView>().ControlSettings;

        //
        if (objectControlSettings.availableObjectMovementInput == null)
            SetAvailableSubjectControls();

        // Moves using AI instructions
        if (currentSubject.GetComponent<ComplexObjectView>().MachineState == ObjectFSMEnum.AIINTERACTIVE || currentSubject.GetComponent<ComplexObjectView>().MachineState == ObjectFSMEnum.AINONINTERACTIVE)
        {
            //TODO
        }

        // Moves using User inputcontrols
        if (currentSubject.GetComponent<ComplexObjectView>().MachineState == ObjectFSMEnum.PLAYERCONTROLLED)
        {
            foreach (ObjectMovementInput movementInput in objectControlSettings.availableObjectMovementInput)
            {
                switch (movementInput.movement)
                {
                    case ObjectMovementEnum.MOVELATERAL:
                        float xTranslation;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                xTranslation = Input.GetAxis(input) * objectControlSettings.lateralMovementSpeed;
                                xTranslation *= Time.deltaTime;
                                currentSubject.transform.Translate(new Vector3(xTranslation, 0, 0));
                            }
                        }
                        break;
                    case ObjectMovementEnum.MOVEVERTICAL:
                        float yTranslation;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                yTranslation = Input.GetAxis(input) * objectControlSettings.verticalMovementSpeed;
                                yTranslation *= Time.deltaTime;
                                currentSubject.transform.Translate(new Vector3(0, yTranslation, 0));
                            }
                        }
                        break;
                    case ObjectMovementEnum.MOVELONGITUDINAL:
                        float zTranslation;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                zTranslation = Input.GetAxis(input) * objectControlSettings.longitudinalMovementSpeed;
                                zTranslation *= Time.deltaTime;
                                currentSubject.transform.Translate(new Vector3(0, 0, zTranslation));
                            }
                        }
                        break;
                    default:
                        // if nothing else matches, do the default
                        break;
                }
            }
        }
    }

    // Rotates the camera
    protected virtual void RotateCamera()
    {
        // complexObject controlSettings
        CameraControlModel cameraControlSettings = currentCamera.GetComponent<CameraView>().cameraControlSettings;

        // Rotates using AI instructions
        if (cameraControlSettings.cameraState == CameraStateEnum.AUTO)
        {
            //TODO
        }

        // Roates using User inputcontrols
        if (cameraControlSettings.cameraState == CameraStateEnum.MANUAL)
        {
            foreach (ObjectMovementInput movementInput in cameraControlSettings.availableObjectMovementInput)
            {
                switch (movementInput.movement)
                {
                    case ObjectMovementEnum.ROTATEYAW:
                        float yRotation;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                yRotation = Input.GetAxis(input) * cameraControlSettings.yawRotationSpeed;
                                yRotation *= Time.deltaTime;
                                currentCamera.transform.Rotate(new Vector3(0, yRotation, 0));
                            }
                        }
                        break;
                    case ObjectMovementEnum.ROTATEPITCH:
                        float xRotation;
                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                xRotation = Input.GetAxis(input) * cameraControlSettings.pitchRotationSpeed;
                                xRotation *= Time.deltaTime;
                                currentCamera.transform.Rotate(new Vector3(xRotation, 0, 0));
                            }
                        }
                        break;
                    case ObjectMovementEnum.ROTATEROLL:
                        float zRotation;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                zRotation = Input.GetAxis(input) * cameraControlSettings.rollRotationSpeed;
                                zRotation *= Time.deltaTime;
                                currentCamera.transform.Rotate(new Vector3(0, 0, zRotation));
                            }
                        }
                        break;
                    default:
                        // if nothing else matches, do the default
                        break;
                }
            }
        }
    }

    // Rotates the object
    protected virtual void RotateSubject()
    {
        // complexObject controlSettings
        ObjectControlModel objectControlSettings = currentSubject.GetComponent<ComplexObjectView>().ControlSettings;

        // Moves using AI instructions
        if (currentSubject.GetComponent<ComplexObjectView>().MachineState == ObjectFSMEnum.AIINTERACTIVE || currentSubject.GetComponent<ComplexObjectView>().MachineState == ObjectFSMEnum.AINONINTERACTIVE)
        {
            //TODO
        }

        // Moves using User inputcontrols
        if (currentSubject.GetComponent<ComplexObjectView>().MachineState == ObjectFSMEnum.PLAYERCONTROLLED)
        {
            foreach (ObjectMovementInput movementInput in objectControlSettings.availableObjectMovementInput)
            {
                switch (movementInput.movement)
                {
                    case ObjectMovementEnum.ROTATEYAW:
                        float yRotation;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                yRotation = Input.GetAxis(input) * objectControlSettings.yawRotationSpeed;
                                yRotation *= Time.deltaTime;
                                currentSubject.transform.Rotate(new Vector3(0, yRotation, 0));
                            }
                        }
                        break;
                    case ObjectMovementEnum.ROTATEPITCH:
                        float xRotation;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                xRotation = Input.GetAxis(input) * objectControlSettings.pitchRotationSpeed;
                                xRotation *= Time.deltaTime;
                                currentSubject.transform.Rotate(new Vector3(xRotation, 0, 0));
                            }
                        }
                        break;
                    case ObjectMovementEnum.ROTATEROLL:
                        float zRotation;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                zRotation = Input.GetAxis(input) * objectControlSettings.rollRotationSpeed;
                                zRotation *= Time.deltaTime;
                                currentSubject.transform.Rotate(new Vector3(0, 0, zRotation));
                            }
                        }
                        break;
                    default:
                        // if nothing else matches, do the default
                        break;
                }
            }
        }
    }

    // Rotates the camera around the subject
    protected virtual void RotateAroundCamera()
    {
        // complexObject controlSettings
        CameraControlModel cameraControlSettings = currentCamera.GetComponent<CameraView>().cameraControlSettings;

        if (cameraControlSettings.subject == null)
            return;

        // Rotates using AI instructions
        if (cameraControlSettings.cameraState == CameraStateEnum.AUTO)
        {
            //TODO
        }

        // Roates using User inputcontrols
        if (cameraControlSettings.cameraState == CameraStateEnum.MANUAL)
        {
            foreach (ObjectMovementInput movementInput in cameraControlSettings.availableObjectMovementInput)
            {
                switch (movementInput.movement)
                {
                    case ObjectMovementEnum.ROTATEAROUNDAZIMUTHAL:
                        float yRotationAround;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                yRotationAround = Input.GetAxis(input) * cameraControlSettings.azimuthalAngleCoordinatesChangeSpeed;
                                yRotationAround *= Time.deltaTime;
                                currentCamera.transform.RotateAround(cameraControlSettings.subject.Position, Vector3.up, yRotationAround);
                            }
                        }
                        break;
                    case ObjectMovementEnum.ROTATEAROUNDPOLAR:
                        float xRotationAround;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                xRotationAround = Input.GetAxis(input) * cameraControlSettings.polarAngleCoordinatesChangeSpeed;
                                xRotationAround *= Time.deltaTime;
                                currentCamera.transform.RotateAround(cameraControlSettings.subject.Position, Vector3.right, xRotationAround);
                            }
                        }
                        break;
                    default:
                        // if nothing else matches, do the default
                        break;
                }
            }
        }
    }

    // Rotates the camera around the subject
    protected virtual void RotateAroundSubject()
    { 
        // complexObject controlSettings
        ObjectControlModel objectControlSettings = currentSubject.GetComponent<ComplexObjectView>().ControlSettings;

        // Moves using AI instructions
        if (currentSubject.GetComponent<ComplexObjectView>().MachineState == ObjectFSMEnum.AIINTERACTIVE || currentSubject.GetComponent<ComplexObjectView>().MachineState == ObjectFSMEnum.AINONINTERACTIVE)
        {
            //TODO
        }

        // Moves using User inputcontrols
        if (currentSubject.GetComponent<ComplexObjectView>().MachineState == ObjectFSMEnum.PLAYERCONTROLLED)
        {
            foreach (ObjectMovementInput movementInput in objectControlSettings.availableObjectMovementInput)
            {
                switch (movementInput.movement)
                {
                    case ObjectMovementEnum.ROTATEAROUNDAZIMUTHAL:
                        float yRotationAround;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                yRotationAround = Input.GetAxis(input) * objectControlSettings.azimuthalAngleCoordinatesChangeSpeed;
                                yRotationAround *= Time.deltaTime;
                                currentSubject.transform.RotateAround(objectControlSettings.subject.Position, Vector3.up, yRotationAround);
                            }
                        }
                        break;
                    case ObjectMovementEnum.ROTATEAROUNDPOLAR:
                        float xRotationAround;

                        foreach (string input in movementInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                xRotationAround = Input.GetAxis(input) * objectControlSettings.polarAngleCoordinatesChangeSpeed;
                                xRotationAround *= Time.deltaTime;
                                currentSubject.transform.RotateAround(objectControlSettings.subject.Position, Vector3.right, xRotationAround);
                            }
                        }
                        break;
                    default:
                        // if nothing else matches, do the default
                        break;
                }
            }
        }
    }

    // Zooms the camera
    protected virtual void ZoomCamera()
    {
        // complexObject controlSettings
        CameraControlModel cameraControlSettings = currentCamera.GetComponent<CameraView>().cameraControlSettings;

        // Rotates using AI instructions
        if (cameraControlSettings.cameraState == CameraStateEnum.AUTO)
        {
            //TODO
        }

        // Roates using User inputcontrols
        if (cameraControlSettings.cameraState == CameraStateEnum.MANUAL)
        {
            foreach (CameraActionInput cameraActionInput in cameraControlSettings.availableCameraActionInput)
            {
                switch (cameraActionInput.actionInput)
                {
                    case CameraActionInputEnum.ZOOMING:
                        float maxZoom = 20.0f; //cameraControlSettings.maxZoom;
                        float normal = cameraControlSettings.startingZoomFactor;
                        float smooth = cameraActionInput.amountOfSmooothing;

                        foreach (string input in cameraActionInput.inputKeyAxes)
                        {
                            if (IsAxisAvailable(input))
                            {
                                GetCurrentCamera().GetComponent<Camera>().fieldOfView = Mathf.Lerp(GetCurrentCamera().GetComponent<Camera>().fieldOfView, Input.GetAxis(input) * maxZoom, Time.deltaTime * smooth);
                            }
                        }
                        break;
                    default:
                        // if nothing else matches, do the default
                        break;
                }
            }
        }
    }  


    // Converts the defined inputcontrol axe, define in the availableCameraMovement so that it works for the current currentplatform
    protected virtual void ConvertSubjectInputToPlatformSpecific()
    {
        string inputPlatformExtension = "";

        if (Application.platform == RuntimePlatform.Android)
            inputPlatformExtension = "_Andriod";

        else if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer ||
            Application.platform == RuntimePlatform.OSXEditor ||
            Application.platform == RuntimePlatform.OSXDashboardPlayer)
            inputPlatformExtension += "_IOS";

        else if (Application.platform == RuntimePlatform.LinuxEditor ||
            Application.platform == RuntimePlatform.LinuxPlayer)
            inputPlatformExtension = "_Linux";

        else if (Application.platform == RuntimePlatform.WebGLPlayer)
            inputPlatformExtension = "_WebGL";

        else if (Application.platform == RuntimePlatform.WindowsPlayer ||
            Application.platform == RuntimePlatform.WindowsEditor)
            inputPlatformExtension = "_Windows";
        else
            inputPlatformExtension = "";

        ObjectControlModel currentSubjectControls = null;
        if (GetCurrentSubject() != null && GetCurrentSubject().GetComponent<ComplexObjectView>() != null && GetCurrentSubject().GetComponent<ComplexObjectView>().ControlSettings != null)
            currentSubjectControls = GetCurrentSubject().GetComponent<ComplexObjectView>().ControlSettings;

        // Convert input for object movement
        if (currentSubjectControls != null && currentSubjectControls.availableObjectMovementInput != null && currentSubjectControls.availableObjectMovementInput.Length > 0)
            foreach (ObjectMovementInput objectMovementInput in currentSubjectControls.availableObjectMovementInput)
                if (objectMovementInput.inputKeyAxes != null)
                {
                    for (int i = 0; i < objectMovementInput.inputKeyAxes.Count; i++)
                    {
                        objectMovementInput.inputKeyAxes[i] += inputPlatformExtension;
                    }
                }

          currentSubjectPlatformConversion = inputPlatformExtension;
    }

    // Converts the defined inputcontrol axe, define in the availableCameraMovement so that it works for the current currentplatform
    protected virtual void ConvertCameraInputToPlatformSpecific()
    {
        string inputPlatformExtension = "";

        if (Application.platform == RuntimePlatform.Android)
            inputPlatformExtension = "_Andriod";

        else if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer ||
            Application.platform == RuntimePlatform.OSXEditor ||
            Application.platform == RuntimePlatform.OSXDashboardPlayer)
            inputPlatformExtension += "_IOS";

        else if (Application.platform == RuntimePlatform.LinuxEditor ||
            Application.platform == RuntimePlatform.LinuxPlayer)
            inputPlatformExtension = "_Linux";

        else if (Application.platform == RuntimePlatform.WebGLPlayer)
            inputPlatformExtension = "_WebGL";

        else if (Application.platform == RuntimePlatform.WindowsPlayer ||
            Application.platform == RuntimePlatform.WindowsEditor)
            inputPlatformExtension = "_Windows";
        else
            inputPlatformExtension = "";

        CameraControlModel currentCameraControls = null;
        if (GetCurrentCamera() != null && GetCurrentCamera().GetComponent<CameraView>() != null && GetCurrentCamera().GetComponent<CameraView>().cameraControlSettings != null)
            currentCameraControls = GetCurrentCamera().GetComponent<CameraView>().cameraControlSettings;

        // Convert input for camera movement
        if (currentCameraControls != null && currentCameraControls.availableObjectMovementInput != null && currentCameraControls.availableObjectMovementInput.Length > 0)
            foreach (ObjectMovementInput cameraMovementInput in currentCamera.GetComponent<CameraView>().cameraControlSettings.availableObjectMovementInput)
                if (cameraMovementInput.inputKeyAxes != null)
                {
                    for (int i = 0; i < cameraMovementInput.inputKeyAxes.Count; i++)
                        cameraMovementInput.inputKeyAxes[i] += inputPlatformExtension;
                }

        // Convert input for camera actions
        if (currentCameraControls != null && currentCameraControls.availableCameraActionInput != null && currentCameraControls.availableCameraActionInput.Length > 0)
            foreach (CameraActionInput cameraActionInput in currentCameraControls.availableCameraActionInput)
                if (cameraActionInput.inputKeyAxes != null)
                {
                    for (int i = 0; i < cameraActionInput.inputKeyAxes.Count; i++)
                        cameraActionInput.inputKeyAxes[i] += inputPlatformExtension;
                }


        currentCameraPlatformConversion = "test";//inputPlatformExtension;
    }

    // Set the available movement controls for the object, this is based on the defined movement in the cameracontroller
    protected virtual void SetAvailableSubjectControls()
    {
        ObjectControlModel currentSubjectControls = GetCurrentSubject().GetComponent<ComplexObjectView>().ControlSettings;

        // New array availableCameraMovementInput for defining 
        ObjectMovementInput[] newAvailableObjectMovementInput = new ObjectMovementInput[0];

        // Fill availableCameraMovementInput array 
        if (currentSubjectControls.controlSetup == ControlSetupEnum.CUSTOM)
        {
            if (currentSubjectControls.customObjectMovementInput != null && currentSubjectControls.customObjectMovementInput.Length >= 1)
                newAvailableObjectMovementInput = (ObjectMovementInput[])currentSubject.GetComponent<ComplexObjectView>().ControlSettings.customObjectMovementInput.Clone();

            currentSubjectControlSetup = "CUSTOM";
        }
        else
        {
            if (currentSubjectControls.defaultObjectMovementInput != null && currentSubjectControls.defaultObjectMovementInput.Length >= 1)
                newAvailableObjectMovementInput = (ObjectMovementInput[])currentSubject.GetComponent<ComplexObjectView>().ControlSettings.defaultObjectMovementInput.Clone();

            currentSubjectControlSetup = "DEFAULT";
        }

        if (newAvailableObjectMovementInput != null)
            if (currentSubjectControls.movementRestrictions != null)
                // Strip availableObjectMovementInput so it holds only, currentObejct, allowed defined movement
                foreach (ObjectMovementEnum movement in currentSubjectControls.movementRestrictions)
                    newAvailableObjectMovementInput = newAvailableObjectMovementInput.Where(m => m.movement != movement).ToArray();

        // Set filtered array as new availible camera movement input
        currentSubjectControls.availableObjectMovementInput = newAvailableObjectMovementInput;

        // Convert availibleinput to platformspecific input axes
        if (currentSubjectControls.availableObjectMovementInput != null)
            ConvertSubjectInputToPlatformSpecific();
    }

    // Set the available movement controls for the camera, this is based on the defined movement in the cameracontroller
    protected virtual void SetAvailableCameraControls()
    {
        CameraControlModel currentCameraControls = GetCurrentCamera().GetComponent<CameraView>().cameraControlSettings;

        // New array availableCameraMovementInput for defining 
        ObjectMovementInput[] newAvailableObjectMovementInput = new ObjectMovementInput[0];

        // Fill availableCameraMovementInput array 
        if (currentCameraControls.controlSetup == ControlSetupEnum.CUSTOM)
        {
            if (currentCameraControls.customObjectMovementInput != null && currentCameraControls.customObjectMovementInput.Length >= 1)
            {
                newAvailableObjectMovementInput = (ObjectMovementInput[])currentCameraControls.customObjectMovementInput.Clone();
                currentCameraControlSetup = "CUSTOM";
            }
            else
            {
                if (currentCameraControls.defaultObjectMovementInput != null && currentCameraControls.defaultObjectMovementInput.Length >= 1)
                {
                    newAvailableObjectMovementInput = (ObjectMovementInput[])currentCameraControls.defaultObjectMovementInput.Clone();
                    currentCameraControlSetup = "DEFAULT";
                }

            }
        }
        else
        {
            if (currentCameraControls.defaultObjectMovementInput != null && currentCameraControls.defaultObjectMovementInput.Length >= 1)
            {
                newAvailableObjectMovementInput = (ObjectMovementInput[])currentCameraControls.defaultObjectMovementInput.Clone();
                currentCameraControlSetup = "DEFAULT";
            }
        }

        if (newAvailableObjectMovementInput != null)
        {
            if (currentCameraControls.movementRestrictions != null)
            {
                // Strip availableObjectMovementInput so it holds only, currentObejct, allowed defined movement
                foreach (ObjectMovementEnum movement in currentCameraControls.movementRestrictions)
                {
                    newAvailableObjectMovementInput = newAvailableObjectMovementInput.Where(m => m.movement != movement).ToArray();
                }
            }

            // Set filtered array as new availible camera movement input
            currentCameraControls.availableObjectMovementInput = newAvailableObjectMovementInput;


            // New array availableCameraMovementInput for defining 
            CameraActionInput[] newAvailableCameraActionInput = new CameraActionInput[0];

            // Fill availableCameraMovementInput array 
            if (currentCameraControls.customCameraActionInput != null && currentCameraControls.customCameraActionInput.Length >= 1)
                newAvailableCameraActionInput = (CameraActionInput[])currentCameraControls.customCameraActionInput.Clone();

            if (currentCameraControls.defaultCameraActionInput != null && currentCameraControls.defaultCameraActionInput.Length >= 1)
                newAvailableCameraActionInput = (CameraActionInput[])currentCameraControls.defaultCameraActionInput.Clone();

            if (currentCameraControls.actionRestrictions != null)
            {
                // Strip availableCameraActionInput so it holds only, currentCamera, allowed defined actions
                foreach (CameraActionInputEnum action in currentSubject.GetComponent<CameraView>().cameraControlSettings.actionRestrictions)
                {
                    newAvailableCameraActionInput = newAvailableCameraActionInput.Where(m => m.actionInput != action).ToArray();
                }
            }

            // Set filtered array as new availible camera movement input
            currentCameraControls.availableCameraActionInput = newAvailableCameraActionInput;

            // Convert availibleinput to platformspecific input axes
            if (currentCameraControls.availableObjectMovementInput != null || currentCameraControls.availableCameraActionInput != null)
                ConvertCameraInputToPlatformSpecific();
        }
    }

    // Checks if an input is possible
    private bool IsAxisAvailable(string axisName)
    {
        try
        {
            Input.GetAxis(axisName);
            return true;
        }
        catch (UnityException exc)
        {
            return false;
        }
    }

}
