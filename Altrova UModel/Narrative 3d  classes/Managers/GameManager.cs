﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using VDS.RDF;
using Object.Type;
using System.Linq;
using Dimensions;
using ObjectControl.Type;
using System.Xml;

public class GameManager : ImmotralMonobehaviour
{
    // THIS DESIDES WHAT PROJECTS TO LOAD (true = PROJECTFOLDER OR false = ASSETBUNDLE)
    [SerializeField] public bool development = true;


    // Static instance of a class of this type
    [SerializeField] public static GameManager instance;

    // Ref to the Worlddimension
    [SerializeField] public GameObject worldDimensionTransform;

    // Ref to the Worlddimension
    [SerializeField] public GameObject navDimensionTransform;


    // List of ontology graphs representing the semantic files from the currentproject 
    [HideInInspector] private Dictionary<string, IGraph> ontologyIndex;


    // A cache holding all found ObjectViews in te scene 
    [HideInInspector] private ObjectView objectViews;


    // A dictionairy of containing ObjectType (enums) keys and specific dictionairies as values. These dictionaries have EntityID as keys and hold ObjectViews as value. This provides a single way quick reference to specific object's views
    [HideInInspector] public Dictionary<ObjectTypeEnum, Dictionary<string, ObjectView>> viewsByEntityID;

    // A dictionairy of containing ObjectTypeEnum (enums) keys and specific dictionairies as values. These dictionaries have sementicURIs as keys and hold SemanticObjectViews as value. This provides a single way quick reference to semanticObject's views
    [HideInInspector] public Dictionary<ObjectTypeEnum, Dictionary<string, SemanticObjectView>> viewsBySementicURI;

    // A dictionairy of containing entityID keys and specific dictionairies as values. These dictionaries have controlType (enum) as keys and hold ObjectControlView as value. This provides a single way quick reference to object's ObjectControlViews    
    [HideInInspector] public Dictionary<ControlTypeEnum, Dictionary<string, ObjectControlView>> controlViewsByEntityID;


    //// A dictionairy of containing ObjectType (enums) keys and specific dictionairies as values. These dictionaries have EntityID as keys and hold ObjectControllers as value. This provides a single way quick reference to specific object's controllers
    //[HideInInspector] public Dictionary<ObjectTypeEnum, Dictionary<string, ObjectController>> controllersByEntityID;

    //// A dictionairy of containing ObjectTypeEnum (enums) keys and specific dictionairies as values. These dictionaries have sementicURIs as keys and hold SemanticObjectControllers as value. This provides a single way quick reference to semanticObject's controllers
    //[HideInInspector] public Dictionary<ObjectTypeEnum, Dictionary<string, SemanticObjectController>> controllersBySementicURI;

    //// A dictionairy of containing entityID keys and specific dictionairies as values. These dictionaries have controlType (enum) as keys and hold ObjectControlController as value. This provides a single way quick reference to object's ObjectControlControllers    
    //[HideInInspector] public Dictionary<ControlTypeEnum, Dictionary<string, ObjectControlController>> controlControllersByEntityID;


    //// A dictionairy of containing ObjectType (enums) keys and specific dictionairies as values. These dictionaries have EntityID as keys and hold ObjectModels as value. This provides a single way quick reference to specific object's models
    //[HideInInspector] public Dictionary<ObjectTypeEnum, Dictionary<string, ObjectModel>> modelsByEntityID;

    //// A dictionairy of containing ObjectTypeEnum (enums) keys and specific dictionairies as values. These dictionaries have sementicURIs as keys and hold SemanticObjectModels as value. This provides a single way quick reference to semanticObject's models
    //[HideInInspector] public Dictionary<ObjectTypeEnum, Dictionary<string, SemanticObjectModel>> modelsBySementicURI;

    //// A dictionairy of containing entityID keys and specific dictionairies as values. These dictionaries have controlType (enum) as keys and hold ObjectControlModel as value. This provides a single way quick reference to object's ObjectControlModels    
    //[HideInInspector] public Dictionary<ControlTypeEnum, Dictionary<string, ObjectControlModel>> controlModelsByEntityID;

    //private  loadedProjectAssetBundles;

    // The current interactable Dimensions
    [HideInInspector] public DimensionEnum currentDimension = DimensionEnum.UNASSIGNED;

    // Use this for pre-initialization (A Unity MonodevelopEvent)
    protected override void Awake()
    {
        // inheritted Awake executions
        base.Awake();

        // Instantiate this singleton manager object
        SetGameManager();

        // Initialise ontologyIndex
        ontologyIndex = new Dictionary<string, IGraph>();

        // Set the starting dimension (enable first found camera as default)
        if (currentDimension == DimensionEnum.UNASSIGNED)
        {
            // Disable cameras from both dimensions
            NavManager.instance.DisableAllNavCameras();
            WorldManager.instance.DisableAllWorldCameras();

            // Set the default starting dimension (the World dimensions) and enable one camera
            currentDimension = DimensionEnum.WORLD;
        }
        else if (currentDimension == DimensionEnum.WORLD)
        {
            // Disable cameras from both dimensions
            NavManager.instance.DisableAllNavCameras();
        }
        else if (currentDimension == DimensionEnum.NAV)
        {
            // Disable cameras from both dimensions
            WorldManager.instance.DisableAllWorldCameras();
        }
    }

    // Use this for initialization (A Unity MonodevelopEvent)
    public void Start() { }

    // Update is called once per frame (A Unity MonodevelopEvent)
    public void Update() { }
    
    // Loads a new project
    public void WipeOntologyIndexAndCreateNew()
    {
        // Reload RDF graphs for currentproject
        ontologyIndex.Clear();

        foreach (string path in ProjectManager.instance.transform.GetComponent<ProjectManager>().currentProjectSemanticDataNamesAndPaths.Values)
        {
            LoadRDFFileInToOntologyIndex(path);
        }
    }

    // Load the graph for the RDF file
    private void LoadRDFFileInToOntologyIndex(string pathToRDF)
    {
        if (pathToRDF != null && File.Exists(pathToRDF))
        {
            IGraph wikiDataGraph = SMWParser.Instance.LoadGraphFromRDFPath(pathToRDF);
            this.ontologyIndex.Add(pathToRDF, wikiDataGraph);
        }
        else
        {
            return;
        }
    }

    // Set the dimension
    public void setCurrentDimension(DimensionEnum dimensionEnum)
    {
    if (dimensionEnum != currentDimension && dimensionEnum == DimensionEnum.WORLD)
        {
            NavManager.instance.DisableAllNavCameras();

            if (!WorldManager.instance.worldIsLoaded && 
                (WorldManager.instance.GetCurrentWorldObject() == null 
                || WorldManager.instance.GetCurrentWorldObject() == this.worldDimensionTransform)
                )

                    if (NarrativeManager.instance.GetProtagonist() != null)
                        WorldManager.instance.CreateNewWorldAndWipeOldWorld(NarrativeManager.instance.GetProtagonist(), true, null, true, false, false);
                    else
                        WorldManager.instance.CreateNewWorldAndWipeOldWorld(null, true, null, true, false, false);

        }
        else if (dimensionEnum == DimensionEnum.NAV)
        {
            WorldManager.instance.DisableAllWorldCameras();

            if (!NavManager.instance.navIsLoaded)
                NavManager.instance.LoadNavWorld(null);
        }
    }

    // Set the dimension
    public void toggleCurrentDimension()
    {
        if (currentDimension == DimensionEnum.NAV)
            setCurrentDimension(DimensionEnum.WORLD);

        else if (currentDimension == DimensionEnum.WORLD)
            setCurrentDimension(DimensionEnum.NAV);

    }

    // Generates and index of all ObjectViewIndexes in the world dimension and nav dimension
    private void GenerateObjectViewIndexes() {

        // Clear in world Existing object views index
        viewsByEntityID.Clear();
        viewsBySementicURI.Clear();

        // Temporary list of all existing transforms
        List<Transform> viewTransforms = new List<Transform>();

        // Fill viewTransforms with worlddimension views 
        if (worldDimensionTransform != null)
            viewTransforms.AddRange(worldDimensionTransform.GetComponentsInChildren<Transform>().ToList());

        // Fill viewTransforms with navdimension views 
        if (navDimensionTransform != null)
            viewTransforms.AddRange(navDimensionTransform.GetComponentsInChildren<Transform>().ToList());

        // Index the views in the "viewsByEntityID" and "viewsBySementicURI" list
        foreach (Transform transform in viewTransforms)
        {
            AddObjectViewToRightIndex(transform);
        }
    }

    // Checks if a given transform contains a ObjectView component that is not added to the View index. Then tries to add it
    private void AddObjectViewToRightIndex(Transform transform)
    {
        ObjectView transformAssignedObjectView = transform.GetComponent<ObjectView>();

        if (transformAssignedObjectView != null && !ObjectFactory.Instance.GetAllViews().Contains(transformAssignedObjectView))
            ObjectFactory.Instance.AddToIndex(transformAssignedObjectView);
    }

    // Part of initialisation
    private void SetGameManager()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        //DontDestroyOnLoad(gameObject);
    }
}
