﻿using Object.Type;
using SMW.Relation.Activity;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VDS.RDF;
using System;

public class NarrativeManager : ImmotralMonobehaviour
{
    // Static instance of a class of this type
    public static NarrativeManager instance;

    // The protagonist is an actor or a roll
    ComplexObjectModel protagonist = null;
    
    // The protagonist is an actor or a roll
    ObjectModel setting = null;

    // The protagonists mood
    int mood = 0;

    // currentPractices to follow
    List<PracticeModel> relevantPractices = new List<PracticeModel>();

    // Sequence of explorered Contexts and internal sequence of executed actions
    private List<ActivityModel> executedActivities = new List<ActivityModel>();

    // Allowed activities to preform
    private List<IntentionalElementModel> allowedIntentionalElement = new List<IntentionalElementModel>();

    // Use this for pre-initialization
    protected override void Awake()
    {
        // inheritted Awake executions
        base.Awake();

        setActionManager();
    }

    public void Start() { }

    public void Update() { }

    // Setup the narrative
    public bool SetupNewNarrative(ComplexObjectModel protagonist, bool allowCreateNewProtagonist, ObjectModel setting, bool allowCreateNewSetting, bool keepTheCurrentNarrative)
    {
        if (this.protagonist != protagonist)
            if(keepTheCurrentNarrative)
                return SetupNewNarrative(protagonist, allowCreateNewProtagonist, setting, allowCreateNewSetting, this.relevantPractices, this.executedActivities);
            else
                return SetupNewNarrative(protagonist, allowCreateNewProtagonist, setting, allowCreateNewSetting, null, null);
        else
            return false;
    }
    
    // Setup the narrative
    public bool SetupNewNarrative(ComplexObjectModel protagonist, bool allowCreateNewProtagonist, ObjectModel setting, bool allowCreateNewSetting, List<PracticeModel> relevantPractices, List<ActivityModel> executedActivities)
    {       
        // Stop this methode if it is not possible or required to change the narrative by a logical way
        if ((protagonist == null && setting == null) || (this.protagonist == protagonist && this.setting == setting))
            return false;

        // Set a new protagonist, but stop this methode if not it was not possible (returns a "false")
        if (!SetProtagonist(protagonist, allowCreateNewProtagonist))
            return false;

        // Set a new setting, but stop this methode if not it was not possible (returns a "false")
        if (!SetSetting(setting, allowCreateNewSetting))
            return false;

        // Set the relevant practives
        SetRelevantPracticesForTheNarrativeAndWipePrevious(protagonist, relevantPractices);

        // Set the relevant executed activities
        SetExecutedActivitiesForTheNarrativeAndWipePrevious(executedActivities);

        // Set default allowed intentional elements
        SetDefaultAllowedIntentionalElements();

        // Succes
        return true;
    }

    // Set all relevant practices and experiences, and wipe the previous set
    private void SetExecutedActivitiesForTheNarrativeAndWipePrevious(List<ActivityModel> executedActivities)
    {
        if (executedActivities != null)
            this.executedActivities = executedActivities;
        else
            this.executedActivities.Clear();
    }

    // Set executed activities, and wipe the previous set
    public void SetRelevantPracticesForTheNarrativeAndWipePrevious(ComplexObjectModel protagonist, List<PracticeModel> relevantPractices)
    {
        if (relevantPractices != null)
            this.relevantPractices = relevantPractices;
        else
        {
            if (protagonist is ContextModel)
                this.relevantPractices = ((ContextModel)protagonist).RelatedPractices;

            else if (protagonist is ActorModel)
                this.relevantPractices = ((ActorModel)protagonist).RelatedPractices;

            else this.relevantPractices.Clear();
        }
    }

    // Sets the default allowed intentionalELements
    public List<IntentionalElementModel> SetDefaultAllowedIntentionalElements()
    {
        // Find and set base category for a intentional element

        SMWCategory baseSmwCategoriesForIntentionalElements = null;

        SMWParser.Instance.GetObjectTypeBaseCategory(((SemanticObjectModel)protagonist).SemanticNode.Graph, ObjectTypeEnum.INTENTIONALELEMENT);

        if (baseSmwCategoriesForIntentionalElements == null)
            baseSmwCategoriesForIntentionalElements = SMWParser.Instance.GetObjectTypeBaseCategory(((SemanticObjectModel)protagonist).SemanticNode.Graph, ObjectTypeEnum.ACTIVITY);

        // Find all availible intentional elements 

        List<IntentionalElementModel> availableIntentionalElement = new List<IntentionalElementModel>();

        if (baseSmwCategoriesForIntentionalElements != null)
        {
            foreach (IGraph g in SMWParser.Instance.GetLoadedGraphs())
                availableIntentionalElement.AddRange(baseSmwCategoriesForIntentionalElements.GetAllnstanceModelsFromThisCategoryAndSubCategoriesUnOrdered().Where(smw => smw is IntentionalElementModel).Cast<IntentionalElementModel>().ToList());
        }

        // Find the allowed intentional elements by there met Dependecy, Connects and Contributes

        List<IntentionalElementModel> tempAllowedIntentionalElement = availableIntentionalElement;

        tempAllowedIntentionalElement = GetAllAllowedIntentionalElementsByMetDependency(tempAllowedIntentionalElement);
        tempAllowedIntentionalElement = GetAllAllowedActivitiesByMetConnects(tempAllowedIntentionalElement);

        // Set and return the found allowed intentional elements as new allowed intentional elements

        return this.allowedIntentionalElement = tempAllowedIntentionalElement;
    }

    // Returns all intentional element from the given list that have met their connects requirement
    private List<IntentionalElementModel> GetAllAllowedActivitiesByMetConnects(List<IntentionalElementModel> intentionalElementToCheck)
    {
        List<IntentionalElementModel> activitiesThatRequireAConnectsSEQ = new List<IntentionalElementModel>();
        List<IntentionalElementModel> activitiesThatRequireNoConnectsSEQ = new List<IntentionalElementModel>();
        List<IntentionalElementModel> activitiesThatMetThereConnects = new List<IntentionalElementModel>();

        // Check for ie that require connect

        foreach (ActivityModel potentialActivity in intentionalElementToCheck.Where(pIe => pIe is ActivityModel).Cast<ActivityModel>())
        {
            if (potentialActivity.Connects != null && potentialActivity.Connects.Length >= 1)
            {
                foreach (Connects con in potentialActivity.Connects)
                {
                    if (activitiesThatRequireAConnectsSEQ.Contains(con.GetElementLink()) == false)
                        activitiesThatRequireAConnectsSEQ.Add(con.GetElementLink());
                }
            }
        }
        
        // Check for ie that require no connect
        foreach (IntentionalElementModel pIe in intentionalElementToCheck)
        {
            if (activitiesThatRequireAConnectsSEQ.Contains(pIe) == false)
                if (activitiesThatRequireNoConnectsSEQ.Contains(pIe) == false)
                    activitiesThatRequireNoConnectsSEQ.Add(pIe);
        }

        // Check for allowed by connect
        foreach (ActivityModel executedActivity in executedActivities)
        {
            if (executedActivity.Connects != null && executedActivity.Connects.Length >= 1)
            {
                foreach (Connects con in executedActivity.Connects)
                {
                    if (activitiesThatMetThereConnects.Contains(con.GetElementLink()) == false)
                        activitiesThatMetThereConnects.Add(con.GetElementLink());
                }
            }
        }
        
        // Return a list of allowed activities
        List<IntentionalElementModel> allowedActivities = new List<IntentionalElementModel>();

        allowedActivities.AddRange(activitiesThatRequireNoConnectsSEQ);
        allowedActivities.AddRange(activitiesThatMetThereConnects);

        return allowedActivities;
    }

    // Returns all intentional element from the given list that have met their dependency requirement
    private List<IntentionalElementModel> GetAllAllowedIntentionalElementsByMetDependency(List<IntentionalElementModel> intentionalElementToCheck)
    {
        List<IntentionalElementModel> intentionalElementsThatMetThereDepends = new List<IntentionalElementModel>();

        foreach (IntentionalElementModel ie in intentionalElementToCheck)
        {
            if (ie.Depends != null && ie.Depends.Length >= 1)
            {
                foreach (IntentionalElementModel executedIe in executedActivities)
                {
                    if (ie.Depends.ToList().Find(dep => dep.GetElementLink() == executedIe) != null)
                        intentionalElementsThatMetThereDepends.Add(ie);
                }
            }
            else
                intentionalElementsThatMetThereDepends.Add(ie);
        }

        return intentionalElementsThatMetThereDepends;
    }

    // Returns all allowed activities
    public List<ActivityModel> GetAllowedActivities()
    {
        if(allowedIntentionalElement == null)
            SetDefaultAllowedIntentionalElements();

        return allowedIntentionalElement.Where(ie => ie is ActivityModel).Cast<ActivityModel>().ToList();
    }

    // Returns all executed activities
    public List<ActivityModel> GetExecutedActivities()
    {
        return executedActivities;
    }

    // Returns the protagonist of the narrative
    public ComplexObjectModel GetProtagonist()
    {
        return protagonist;
    }

    // Returns the setting of the narrative
    public ObjectModel GetSetting()
    {
        return setting;
    }

    // Set the protagonist for the narrative
    private bool SetProtagonist(ComplexObjectModel protagonist, bool allowCreateNewProtagonist)
    {
        if (protagonist == null)
        {
            if (allowCreateNewProtagonist == true)
            {
                this.protagonist = (ComplexObjectModel)ObjectFactory.Instance.modelFactory.createForComplexObject("", "Non-Semantic Protaganist");
                return true;
            }
        }
        else if (CheckIfProtagonistIsAllowed(protagonist))
        {
            this.protagonist = protagonist;
            return true;
        }

        return false;
    }

    // Set the setting for the narrative
    private bool SetSetting(ObjectModel setting, bool allowCreateNewSetting)
    {
        // if the setting is noet allowed then return false
        if (setting == null)
        {
            if (allowCreateNewSetting == true)
            {
                this.setting = (ComplexObjectModel)ObjectFactory.Instance.modelFactory.createForComplexObject("", "Non-Semantic Setting");
                return true;
            }
        }
        else if (CheckIfSettingIsAllowed(setting))
        { 
            this.setting = setting;
            return true;
        }

        return false;
    }

    // Check is the combination of the protagonist and setting
    private bool CheckIfProtagonistIsAllowed(ObjectModel protagonist)
    {
        if (this.setting != null && this.setting is SMWObjectModel)
        {
            if ((protagonist is ActivityModel) && ((ActivityModel)protagonist).Context == this.setting)
                return true;

            else if ((protagonist is ContextModel) && ((ContextModel)protagonist).SuperContext == this.setting)
                return true;
            else
                return false;
        }

        return true;
    }

    // Check is the combination of the protagonist and setting
    private bool CheckIfSettingIsAllowed(ObjectModel setting)
    {
        if (this.protagonist != null && this.protagonist is SMWObjectModel)
        {
            if ((this.protagonist is ActivityModel) && ((ActivityModel)this.protagonist).Context == setting)
                return true;

            else if ((this.protagonist is ContextModel) && ((ContextModel)this.protagonist).SuperContext == setting)
                return true;

            else
                return false;
        }

        return false;
    }

    // Updates the list of allowed intentional elements based on the executed activity
    private void UpdateAllowedIntentionalElementsByExecutedActivity(ActivityModel executedActivity)
    {
        if (executedActivity.Connects != null && executedActivity.Connects.Count() >= 1)
        {
            foreach (Connects con in executedActivity.Connects)
            {
                IntentionalElementModel connectedIE = con.GetElementLink();

                if (con.elementConnectionType == ConnectionTypeEnum.SEQ && allowedIntentionalElement.Contains((ActivityModel)connectedIE) == false)
                    allowedIntentionalElement.Add((ActivityModel)connectedIE);
            }
        }
        
    }

    // Registers an executed activity
    public bool RegisterExcetutedAction(ActivityModel executedActivity)
    {
        if (CheckIfActivityIsAllowed(executedActivity) == true && executedActivities.Contains(executedActivity) == false)
        {
            executedActivities.Add(executedActivity);

            // Add new allowed activities
            UpdateAllowedIntentionalElementsByExecutedActivity(executedActivity);

            return true;
        }

        return false;
    }

    // Returns true is it is allowed, else returns false
    public bool CheckIfActivityIsAllowed(ActivityModel activity)
    {
        if (GetAllowedActivities().Contains(activity))
            return true;
        else
            return false;
    }

    // Creates a singleton object of this class, if this did not exist yet
    private void setActionManager()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a ActionManager.
            Destroy(gameObject);
    }
}
