﻿// This manager class handles the view and the interaction with specific camera perspective and it's subject of intressed. (This is meant only to work with cameraTypes: FISRTPERSON, THIRDPERSON and OMNISCENT)
using System;
using System.Collections.Generic;
using UnityEngine;

public class PerspectiveManager : InteractionManager
{
    // Static instance of a class of this type
    public static PerspectiveManager instance;

    // Use this for pre-initialization (A Unity MonodevelopEvent)
    protected override void Awake()
    {
        // inheritted Awake executions
        base.Awake();

        // Set static accessable singleton instance of the manager
        setPerspectiveManager();
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
      
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    // Set new perspectice
    public bool SetNewPerspective(ComplexObjectView gameobjectView, bool spectate, bool allowObjectControl, bool allowCameraControl)
    {
        // Return false when there is no given gameobjectView
        if (gameobjectView == null || gameobjectView.gameObject == null)
            return false;

        // Set the given object as new Object of intressed
        if (this.GetCurrentSubject() != gameobjectView.gameObject)
            this.SetCurrentSubject(gameobjectView.gameObject, true);

        // If not spectate change the narrative to the specified
        if (spectate == false)
            if (gameobjectView.FindCreateSetController().FindSetModel() != null)
                if (NarrativeManager.instance.GetProtagonist() != this.GetCurrentSubject().GetComponent<ComplexObjectView>().FindCreateSetController().FindSetModel())
                {
                    if (gameobjectView is ActorView || gameobjectView is ContextView)
                        NarrativeManager.instance.SetupNewNarrative((SMWObjectModel)this.GetCurrentSubject().GetComponent<ComplexObjectView>().FindCreateSetController().FindSetModel(), true, (ObjectModel) WorldManager.instance.GetCurrentWorldObject().FindCreateSetController().FindSetModel(), true, true);

                    else return false;
                }

        // Set allowed controls
        this.allowSubjectControl = allowObjectControl;
        this.allowCameraControl = allowCameraControl;

        // Succes
        return true;
    }

    // Set perspective tot current protagonist
    public bool SetPerspectiveToProtagonist(bool allowObjectControl, bool allowCameraControl)
    {
        return SetNewPerspective((ComplexObjectView) NarrativeManager.instance.GetProtagonist().FindCreateSetController().FindSetView(), false, allowObjectControl, allowCameraControl);
    }

    private void setPerspectiveManager()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
    }
}

