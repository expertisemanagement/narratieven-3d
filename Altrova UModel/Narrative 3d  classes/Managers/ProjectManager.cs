﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using System;
using SMW.Relation.Practice;

public class ProjectManager : ImmotralMonobehaviour {

    // Static instance of a class of this type
    [HideInInspector] public static ProjectManager instance;

    // The projectbrowser gameobject
    [SerializeField] public GameObject projectbrowser;

    // Projectfolder settings        
    [HideInInspector] public readonly string resourceFolderName = @"ProjectResources";
    [HideInInspector] public readonly string assetBundleFolderName = @"_AssetBundles";
    [HideInInspector] public readonly string semanticDataFolderName = @"SemanticData";
    [HideInInspector] public readonly string worldFolderName = @"Worlds";
    [HideInInspector] public readonly string savedataFolderName = @"Savedata";
    [HideInInspector] public readonly string actionRecordsFolderName = @"Savedata/Recordings/Actions";

    [HideInInspector] public readonly string stateRecordsFolderName = @"Savedata/Recordings/States";

    [HideInInspector] public string projectResourceFolder
    {
        get
        {
            if (GameManager.instance.development == false) return Application.dataPath + resourceFolderName + "/" + assetBundleFolderName;
            else return Application.dataPath + "/" + resourceFolderName;
        }
    }

    [HideInInspector] public string currentProjectName;
    [HideInInspector] public string currentProjectPath
    {
        get
        {
            if (GameManager.instance.development == false)
            {
                if (Application.platform == RuntimePlatform.Android)
                    return projectResourceFolder + "/" + currentProjectName + "/Andriod";

                else if (Application.platform == RuntimePlatform.IPhonePlayer ||
                    Application.platform == RuntimePlatform.OSXPlayer ||
                    Application.platform == RuntimePlatform.OSXEditor ||
                    Application.platform == RuntimePlatform.OSXDashboardPlayer)
                    return projectResourceFolder + "/" + currentProjectName + "/IOS";

                else if (Application.platform == RuntimePlatform.LinuxEditor ||
                    Application.platform == RuntimePlatform.LinuxPlayer)
                    return projectResourceFolder + "/" + currentProjectName + "/Linux";

                else if (Application.platform == RuntimePlatform.WebGLPlayer)
                    return projectResourceFolder + "/" + currentProjectName + "/WebGL";

                else if (Application.platform == RuntimePlatform.WindowsPlayer ||
                    Application.platform == RuntimePlatform.WindowsEditor)
                    return projectResourceFolder + "/" + currentProjectName + "/Windows";
                else
                    return projectResourceFolder + "/" + currentProjectName;
            }
            else
            {
                return projectResourceFolder + "/" + currentProjectName;
            }
        }
    }

    // Project names and paths
    [HideInInspector] public Dictionary<string, string> projectNamesAndPaths
    {
        get
        {
            Dictionary<string, string> projectNamesAndPaths = new Dictionary<string, string>();

            foreach (string path in Directory.GetDirectories(projectResourceFolder))
            {
                projectNamesAndPaths.Add(new FileInfo(path).Name, path);
            }

            return projectNamesAndPaths;
        }
    }

    // Current project semanticdata names and paths
    [HideInInspector] public Dictionary<string, string> currentProjectSemanticDataNamesAndPaths
    {
        get
        {
            Dictionary<string, string> projectSemanticDataNamesAndPaths = new Dictionary<string, string>();

            foreach (string path in Directory.GetFiles(currentProjectSubFolderSemanticData))
            {
                if (Path.GetExtension(path).ToUpper() == semanticDataExtension.ToUpper())
                    projectSemanticDataNamesAndPaths.Add(new FileInfo(path).Name, path);
            }

            return projectSemanticDataNamesAndPaths;
        }
    }

    // Current project world names and paths
    [HideInInspector] public Dictionary<string, string> currentProjectWorldsNamesAndPaths
    {
        get
        {
            Dictionary<string, string> projectWorldsNamesAndPaths = new Dictionary<string, string>();

            foreach (string path in Directory.GetFiles(currentProjectSubFolderWorlds))
            {
                if (Path.GetExtension(path).ToUpper() == worldExtension.ToUpper())
                    projectWorldsNamesAndPaths.Add(new FileInfo(path).Name, path);
            }

            return projectWorldsNamesAndPaths;
        }
    }

    // Current project state names and paths
    [HideInInspector] public Dictionary<string, string> currentProjectStateRecordNamesAndPaths
    {
        get
        {
            Dictionary<string, string> projectProjectStateRecordNamesAndPaths = new Dictionary<string, string>();

            foreach (string path in Directory.GetFiles(currentProjectSubFolderStateRecords))
                if (Path.GetExtension(path).ToUpper() == stateRecordExtension.ToUpper())
                    projectProjectStateRecordNamesAndPaths.Add(new FileInfo(path).Name, path);

            return projectProjectStateRecordNamesAndPaths;
        }
    }

    // Current project action names and paths
    [HideInInspector] public Dictionary<string, string> currentProjectActionRecordNamesAndPaths
    {
        get
        {
            Dictionary<string, string> projectActionRecordNamesAndPaths = new Dictionary<string, string>();

            foreach (string path in Directory.GetFiles(currentProjectSubFolderActionRecords))
                if (Path.GetExtension(path).ToUpper() == actionRecordExtension.ToUpper())
                    projectActionRecordNamesAndPaths.Add(new FileInfo(path).Name, path);

            return projectActionRecordNamesAndPaths;
        }
    }

    [HideInInspector] public string currentProjectSubFolderSemanticData { get { return currentProjectPath + "/" + semanticDataFolderName; }  private set { } }
    [HideInInspector] public string currentProjectSubFolderWorlds { get { return currentProjectPath + "/" + worldFolderName; } private set { } }
    [HideInInspector] public string currentProjectSubFolderSavedata { get { return currentProjectPath + "/" + savedataFolderName; } private set { } }
    [HideInInspector] public string currentProjectSubFolderActionRecords { get { return currentProjectPath + "/" + actionRecordsFolderName; } private set { } }
    [HideInInspector] public string currentProjectSubFolderStateRecords { get { return currentProjectPath + "/" + stateRecordsFolderName; } private set { } }

    [HideInInspector] private string semanticDataExtension = ".xml";
    [HideInInspector] private string worldExtension = ".scene";
    [HideInInspector] private string stateRecordExtension = ".staterecord";
    [HideInInspector] private string actionRecordExtension = ".actionrecord";

    public AssetBundle GetDesignedLevelAssets()
    {
        return null; //TODO
    }

    // Use this for pre-initialization (A Unity MonodevelopEvent)
    protected override void Awake()
    {
        // inheritted Awake executions
        base.Awake();

        SetProjectManager();
    }

    // Use this for initialization
    void Start ()
    {
        //Load all projects
        GetFirstProject();

        // Opens the projectBrowser
        InterfaceManager.instance.ToggleProjectBrowser();
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    // Registers all existing projectPaths
    public string GetFirstProject()
    {
        return currentProjectName = Path.GetFileName(projectNamesAndPaths.Values.First());
    }

    private void SetProjectManager()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        //DontDestroyOnLoad(gameObject);
    }
}
