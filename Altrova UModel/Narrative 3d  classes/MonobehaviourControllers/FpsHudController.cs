﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FpsHudController : MonoBehaviour {

    public Canvas FpsHudCanvas;
    public Image redical;
    public Text targetedNameText;
    public Text targetedActionText;

    private GameObject targetedObject;

    // Use this for initialization
    void Start () {
        if (FpsHudCanvas == null)
            FpsHudCanvas = this.transform.GetComponent<Canvas>();

        if (redical == null)
            redical = this.transform.GetComponent<Image>();

        if (targetedNameText == null)
            targetedNameText = this.transform.GetComponent<Text>();

        if (targetedActionText == null)
            targetedActionText = this.transform.GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {

        // Shoot raycast

        RaycastHit hit;

        Physics.Raycast(redical.transform.position, redical.transform.TransformDirection(Vector3.forward), out hit, 200);

        if (hit.transform != null && hit.transform.gameObject != null)
            targetedObject = hit.transform.gameObject;

        if (targetedObject != null)
        {
            ObjectView objectView = targetedObject.GetComponent<ObjectView>();

            if (objectView != null)
                objectView.LookedAt(this.gameObject, null);

            if (Input.GetAxis("Interact") > 0)
            {
                if(objectView != null)
                    objectView.Interact(this.gameObject, null);
            }
        }

        UpdateHud();
    }

    private void UpdateHud()
    {
        if (targetedObject != null)
        {
            ObjectView objectView = targetedObject.GetComponent<ObjectView>();

            if (objectView != null)
            {
                SetHudPositive();

                if (objectView is SemanticObjectView)
                    targetedNameText.text = ((SemanticObjectModel)objectView.FindCreateSetController().FindCreateSetModel()).ObjectName;
                else
                    targetedNameText.text = targetedObject.name;
            }
            else
            {
                SetHudNeutral();
                targetedNameText.text = targetedObject.name;
            }
        }
        else
        {
            SetHudNeutral();
        }
    }

    private void SetHudPositive()
    {
        targetedNameText.color = new Color32(0, 255, 0, 255);
        targetedActionText.text = "[Interactable]";
        targetedActionText.color = new Color32(0, 255, 0, 255);
    }

    private void SetHudNegative()
    {

    }

    private void SetHudNeutral()
    {
        redical.color = new Color32(255, 255, 255, 255);
        targetedNameText.text = "";
        targetedNameText.color = new Color32(255, 255, 255, 255);
        targetedActionText.text = "";
        targetedActionText.color = new Color32(255, 255, 255, 255);
    }
}
