﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using VDS.RDF;
using Object.Type;
using SMW.EMM.Context;
using System.Linq;

public class NarrativeMenu : InterfaceController
{
   
    public GameObject b_stepOneDropdown;
    private GameObject stepOneDropdown
    {
        get
        {
            if (b_stepOneDropdown != null) return b_stepOneDropdown;
            else return gameObject.transform.Find("StepOne").Find("Dropdown").gameObject;
        }
        set { stepOneDropdown = value; }
    }

    public GameObject b_stepTwoDropdown;
    private GameObject stepTwoDropdown
    {
        get
        {
            if (b_stepTwoDropdown != null) return b_stepTwoDropdown;
            else return gameObject.transform.Find("StepTwo").Find("Dropdown").gameObject;
        }
        set { stepTwoDropdown = value; }
    }

    public GameObject b_stepThreeDropdown;
    private GameObject stepThreeDropdown
    {
        get
        {
            if (b_stepThreeDropdown != null) return b_stepThreeDropdown;
            else return gameObject.transform.Find("StepThree").Find("Dropdown").gameObject;
        }
        set { stepTwoDropdown = value; }
    }

    public GameObject b_stepFourButton;
    private GameObject stepFourButton
    {
        get
        {
            if (b_stepFourButton != null) return b_stepFourButton;
            else return gameObject.transform.Find("StepFour").Find("Button").gameObject;
        }
        set { stepFourButton = value; }
    }

    // Loaded Graphs
    List<IGraph> listGraphs = new List<IGraph>();

    // Existing Contexts
    List<SMWObjectModel> smwObjectModels = new List<SMWObjectModel>();

    // List of all possible Actors/Rolls for the selected Context
    List<SMWObjectModel> knowCharacters = new List<SMWObjectModel>();
    
    void Awake()
    {
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateChoices()
    {
        SetGraphChoicesDropdown();
    }

    // Sets the options of the project dorpdown
    public void SetGraphChoicesDropdown()
    {
        //  Clear the current dropdown list
        stepOneDropdown.GetComponent<Dropdown>().ClearOptions();

        // Create a list of save names and add these to the options 
        List<string> graphNames = new List<string>();

        listGraphs = SMWParser.Instance.GetLoadedGraphs();
        foreach (IGraph g in listGraphs)
        {
            graphNames.Add(g.ToString());
        }

        if (graphNames.Count <= 0)
        {
            stepOneDropdown.GetComponent<Dropdown>().interactable = false;
            graphNames.Add("No Graphs found");
        }
        else
        {
            stepOneDropdown.GetComponent<Dropdown>().interactable = true;
            graphNames.Add("All Graphs");
        }

        stepOneDropdown.GetComponent<Dropdown>().AddOptions(graphNames);

        // Update other choices from next step
        SetContextDropdown();
    }

    // Sets the options of the project dorpdown
    public void SetContextDropdown()
    {
        //  Clear the current dropdown list
        stepTwoDropdown.GetComponent<Dropdown>().ClearOptions();

        // Create a list of save names and add these to the options 
        List<SMWObjectModel> listOfSMWObjectModels = new List<SMWObjectModel>();
        string stepTwoChoice = stepOneDropdown.GetComponent<Dropdown>().options[stepOneDropdown.GetComponent<Dropdown>().value].text;
        if (stepTwoChoice != "All Graphs")
        {
            smwObjectModels = SMWParser.Instance.GetObjectTypeBaseCategory(listGraphs.Find(g => g.ToString() == stepTwoChoice), ObjectTypeEnum.CONTEXT).GetAllnstanceModelsFromThisCategoryAndSubCategoriesUnOrdered();
        }
        else
        {
            foreach(IGraph g in listGraphs)
                if(listGraphs.Contains(g) == true)
                smwObjectModels = SMWParser.Instance.GetObjectTypeBaseCategory(listGraphs[listGraphs.IndexOf(g)], ObjectTypeEnum.CONTEXT).GetAllnstanceModelsFromThisCategoryAndSubCategoriesUnOrdered();
        }

        
        List<string> listSMWObjectNames = new List<string>();
        foreach (SMWObjectModel smwObjectModel in smwObjectModels)
            listSMWObjectNames.Add(smwObjectModel.ObjectName); // listSMWObjectNames.Add(smwObjectModel.objectName);

        if (listSMWObjectNames.Count <= 0)
        {
            stepTwoDropdown.GetComponent<Dropdown>().interactable = false;
            listSMWObjectNames.Add("No Contexts found");
        }
        else
        {
            stepTwoDropdown.GetComponent<Dropdown>().interactable = true;
        }

        stepTwoDropdown.GetComponent<Dropdown>().AddOptions(listSMWObjectNames);

        // Update other choices from next step
        SetProtagonistDropdown();
    }

    // Sets the options of the project dorpdown
    public void SetProtagonistDropdown()
    {
        //  Clear the current dropdown list
        stepThreeDropdown.GetComponent<Dropdown>().ClearOptions();

        // Step Two choice
        string stepOneChoice = stepOneDropdown.GetComponent<Dropdown>().options[stepOneDropdown.GetComponent<Dropdown>().value].text;

        knowCharacters = new List<SMWObjectModel>();

        if (stepOneChoice != "All Graphs")
        {
            // Gather Actors
            knowCharacters.AddRange(SMWParser.Instance.GetAllInstancesByObjectType(listGraphs.Find(g => g.ToString() == stepOneChoice), ObjectTypeEnum.ACTOR, true));

            // Gather Rolls
            foreach (SMWObjectModel potentialRoll in SMWParser.Instance.GetAllInstancesByObjectType(listGraphs.Find(g => g.ToString() == stepOneChoice), ObjectTypeEnum.CONTEXT, true))
            {
                if (potentialRoll is ContextModel)
                    if (((ContextModel)potentialRoll).ContextType == ContextTypeEnum.ROLE)
                        knowCharacters.Add(potentialRoll);
            }
        }
        else
        {
            foreach (IGraph g in listGraphs)
                if (listGraphs.Contains(g) == true)
                {
                    // Gather Actors
                    knowCharacters.AddRange(SMWParser.Instance.GetAllInstancesByObjectType(listGraphs[listGraphs.IndexOf(g)], ObjectTypeEnum.ACTOR, true));
                    
                    // Gather Rolls
                    foreach (SMWObjectModel potentialRoll in SMWParser.Instance.GetAllInstancesByObjectType(listGraphs[listGraphs.IndexOf(g)], ObjectTypeEnum.CONTEXT, true))
                    {
                        if (potentialRoll is ContextModel)
                        {
                            Debug.Log(((ContextModel)potentialRoll).ContextType);

                            if (((ContextModel)potentialRoll).ContextType == ContextTypeEnum.ROLE)
                                knowCharacters.Add(potentialRoll);
                        }
                    }
                }
        }

        // Step two choice
        SMWObjectModel stepTwoChoiceObject = smwObjectModels.Find(smw => smw.ObjectName == stepTwoDropdown.GetComponent<Dropdown>().options[stepTwoDropdown.GetComponent<Dropdown>().value].text);



        // Limit the found Actors and Rolls to only the selected Context
        List<ActorModel> actorsFromSelectedContext = knowCharacters.FindAll(smw => smw.GetType() == typeof(ActorModel)).Cast<ActorModel>().ToList().FindAll(a => a.Context == stepTwoChoiceObject).ToList();

        List<ContextModel> rollsFromSelectedContext = knowCharacters.FindAll(smw => smw.GetType() == typeof(ContextModel)).Cast<ContextModel>().ToList().FindAll(a => a.SuperContext == stepTwoChoiceObject).ToList();

        knowCharacters.Clear();
        knowCharacters.AddRange(actorsFromSelectedContext.Cast<SMWObjectModel>().ToList());
        knowCharacters.AddRange(rollsFromSelectedContext.Cast<SMWObjectModel>().ToList());

        // Create a list of save names and add these to the options 
        List<string> listProtagonistNames = new List<string>();

        foreach (SMWObjectModel smwObjectModel in knowCharacters)
                listProtagonistNames.Add(smwObjectModel.ObjectName);

        if (listProtagonistNames.Count <= 0)
        {
            stepThreeDropdown.GetComponent<Dropdown>().interactable = false;
            listProtagonistNames.Add("No Actors/Rolls found");
        }
        else
        {
            stepThreeDropdown.GetComponent<Dropdown>().interactable = true;
        }

        stepThreeDropdown.GetComponent<Dropdown>().AddOptions(listProtagonistNames);

        // Update button from next step
        ToggleStartButton();
    }

    // Toggle interaction state of the start/initiate button. This will enable when the required variables for levelloading are compleet.
    public void ToggleStartButton()
    {
        if (stepOneDropdown.GetComponent<Dropdown>().options.Count > 0 && stepTwoDropdown.GetComponent<Dropdown>().options.Count > 0 && stepThreeDropdown.GetComponent<Dropdown>().options.Count > 0)
            stepFourButton.GetComponent<Button>().interactable = true;
        else
            stepFourButton.GetComponent<Button>().interactable = false;
    }

    public void OnGraphDropdownValueChanged()
    {
        SetContextDropdown();
    }

    public void OnContextDropdownValueChanged()
    {
        SetProtagonistDropdown();
    }

    public void OnProtagonistDropdownValueChanged()
    {
        ToggleStartButton();
    }

    public void OnStartButtonClick()
    {
        // Get step 2 choice (= the Context)
        SMWObjectModel worldSMWObject = smwObjectModels.Find(smw => smw.ObjectName == stepTwoDropdown.GetComponent<Dropdown>().options[stepTwoDropdown.GetComponent<Dropdown>().value].text);

        // Get step 3 choice (= the protagonist)
        SMWObjectModel protagonist = knowCharacters.Find(c => c.ObjectName == stepThreeDropdown.GetComponent<Dropdown>().options[stepThreeDropdown.GetComponent<Dropdown>().value].text);

        // load world
        WorldManager.instance.CreateNewWorldAndWipeOldWorld(protagonist, false, worldSMWObject, false, true, false);

        // Hide this interface
        this.ToggleVisablity();
    }

    public void OnEnable()
    {
        UpdateChoices();
    }

}