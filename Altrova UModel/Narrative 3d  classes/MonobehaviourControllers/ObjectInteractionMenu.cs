﻿using Object.FSM;
using Object.GUIstate;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ObjectInteractionMenu : MonoBehaviour {

    // subject of this interaction menu
    [SerializeField] public GameObject directObject;

    // The interacting protagonist
    [HideInInspector] public GameObject subject;

    // The object used for interaction
    [HideInInspector] public GameObject indirectObject;

    [SerializeField] public Canvas menuCanvas;
    [SerializeField] public Text titleText;
    [SerializeField] public Button interactButton;
    [SerializeField] public Button spectateButton;
    [SerializeField] public Button diveInToButton;

    // Use this for initialization
    private void Start () {
		
	}
	
	// Update is called once per frame
	private void Update () {
		
	}

    private void OnEnable()
    {
        if (subject != null && menuCanvas != null && titleText != null && interactButton != null && spectateButton != null && diveInToButton != null)
        {
            menuCanvas.gameObject.SetActive(true);
            titleText.gameObject.SetActive(true);
            interactButton.gameObject.SetActive(true);
            spectateButton.gameObject.SetActive(true);
            diveInToButton.gameObject.SetActive(true);
            
            menuCanvas.worldCamera = PerspectiveManager.instance.GetCurrentCamera().GetComponent<Camera>();

            UpdateCanvas();
        }
        else
        {
            this.enabled = false;
        }
    }

    private void OnDisable()
    {
        menuCanvas.gameObject.SetActive(false);
        titleText.gameObject.SetActive(false);
        interactButton.gameObject.SetActive(false);
        spectateButton.gameObject.SetActive(false);
        diveInToButton.gameObject.SetActive(false);

    }

    private void UpdateCanvas()
    {
        if (subject != null)
        {
            interactButton.interactable = false;
            spectateButton.interactable = false;
            diveInToButton.interactable = false;

            ObjectView foundObjectView = directObject.GetComponent<ObjectView>();

            if (foundObjectView is ComplexObjectView)
            {
                if (((ComplexObjectView)foundObjectView).MachineState != ObjectFSMEnum.STATICNONINTERACTIVE || ((ComplexObjectView)foundObjectView).MachineState != ObjectFSMEnum.HIDDEN || ((ComplexObjectView)foundObjectView).MachineState != ObjectFSMEnum.AINONINTERACTIVE)
                    interactButton.interactable = true;

                if (subject.GetComponent<SemanticObjectView>())
                {
                    titleText.text = ((SemanticObjectModel)directObject.GetComponent<SemanticObjectView>().FindSetController().FindCreateSetModel()).ObjectName;

                    diveInToButton.interactable = true;

                    spectateButton.interactable = true;
                }
            }
            else
            {
                titleText.text = directObject.name;
            }
        }

        // Update location to stand between the subject and the directObject
        if (subject != null && directObject != null)
        {
            this.gameObject.transform.position = (subject.transform.position + directObject.transform.position) / 2;

            transform.LookAt(directObject.transform.position);
        }
        
    }

    public void InteractDirectObjectWithSubject()
    {
        ObjectView foundObjectView = directObject.GetComponent<ObjectView>();
        //foundObjectView.Interact(this.gameObject, indirectObject);


        if (foundObjectView is ActivityView && NarrativeManager.instance.RegisterExcetutedAction((ActivityModel)foundObjectView.FindCreateSetController().FindCreateSetModel()) == true)
            {
                WorldManager.instance.UpdateActivitiyVisuals();

                ActivityModel nextActivity = (ActivityModel) GameObject.FindObjectsOfType<RelationController>().ToList().FindAll(rc => rc.subjectElement == (IntentionalElementModel)foundObjectView.FindCreateSetController().FindCreateSetModel()).ToList().Find(rc => rc.relatedElement is ActivityModel && rc.relatedElementAllowsExecution).relatedElement;

                if (nextActivity != null && nextActivity.Context != NarrativeManager.instance.GetProtagonist())
                {
                    this.gameObject.transform.SetParent(null);

                    PerspectiveManager.instance.SetNewPerspective((ContextView) nextActivity.Context.FindCreateSetController().FindSetView(), false, true, true);
                    //WorldManager.instance.CreateNewWorldAndWipeOldWorld(nextActivity.Context, false, nextActivity.Context.SuperContext, false, true, false);
                    Destroy(this.gameObject);
                    return;
                }

                WorldManager.instance.UpdateActivitiyVisuals();
            }

        this.enabled = false;
    }

    public void SpectateDirectObject()
    {
        ComplexObjectView foundObjectView = subject.GetComponent<ComplexObjectView>();
        if(foundObjectView != null)
            PerspectiveManager.instance.SetNewPerspective(foundObjectView, true, false, false);

        this.enabled = false;
    }

    public void DiveInToDirectObject()
    {
        SMWObjectView foundObjectView = subject.GetComponent<SMWObjectView>();
        if (foundObjectView != null)
            WorldManager.instance.CreateNewWorldAndWipeOldWorld(NarrativeManager.instance.GetProtagonist(), false, (SMWObjectModel) foundObjectView.FindCreateSetController().FindCreateSetModel(), false, false, true);

        this.enabled = false;
    }

    public ObjectInteractionMenu Initialise(GameObject subject, GameObject directObject)
    {
        this.subject = subject;
        this.indirectObject = indirectObject;

        return this;
    }
}
