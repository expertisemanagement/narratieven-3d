﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ProjectMenu : InterfaceController
{
   
    public GameObject b_stepOneDropdown;
    public GameObject StepOneDropdown
    {
        get
        {
            if (b_stepOneDropdown != null) return b_stepOneDropdown;
            else return gameObject.transform.Find("StepOne").Find("Dropdown").gameObject;
        }
        set { StepOneDropdown = value; }
    }

    public GameObject b_stepTwoDropdown;
    public GameObject StepTwoDropdown
    {
        get
        {
            if (b_stepTwoDropdown != null) return b_stepTwoDropdown;
            else return gameObject.transform.Find("StepTwo").Find("Dropdown").gameObject;
        }
        set { StepTwoDropdown = value; }
    }

    public GameObject b_stepThreeButton;
    public GameObject StepThreeButton
    {
        get
        {
            if (b_stepThreeButton != null) return b_stepThreeButton;
            else return gameObject.transform.Find("StepThree").Find("Button").gameObject;
        }
        set { StepThreeButton = value; }
    }

    // Use this for pre-initialization
    void Awake()
    {
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Updates the choices of the dropdowns
    public void UpdateChoices()
    {
        SetProjectDropdown();
        // SetSaveDropdown(); Disabled during incompleet working
    }

    // Sets the options of the project dorpdown
    public void SetProjectDropdown()
    {

        //  Clear the current dropdown list
        StepOneDropdown.GetComponent<Dropdown>().ClearOptions();

        // Create a list of save names and add these to the options 
        List<string> listProjectNames = new List<string>();

        foreach (string projectName in ProjectManager.instance.projectNamesAndPaths.Keys)
            if (projectName != ProjectManager.instance.assetBundleFolderName)
                listProjectNames.Add(Path.GetFileName(projectName));

        if (listProjectNames.Count <= 0)
        {
            StepOneDropdown.GetComponent<Dropdown>().interactable = false;
            listProjectNames.Add("No projects found");
        }
        else
        {
            StepOneDropdown.GetComponent<Dropdown>().interactable = true;
        }

        StepOneDropdown.GetComponent<Dropdown>().AddOptions(listProjectNames);

        // Toggle interaction state of the start/initiate button
        SetSaveDropdown();
    }

    // EventHandler for when the project dropdown changes
    public void OnProjectDropdownValueChanged()
    {
        //get the selected index
        int optionIndex = StepOneDropdown.GetComponent<Dropdown>().value;

        //get all options available within this dropdown menu
        List<Dropdown.OptionData> menuOptions = StepOneDropdown.GetComponent<Dropdown>().options;

        //get the string value of the selected index
        string value = menuOptions[optionIndex].text;

        ProjectManager.instance.currentProjectName = ProjectManager.instance.projectNamesAndPaths[value];

        ToggleStartButton();
    }

    // EventHandler for when the startbutton is clicked
    public void OnStartButtonClick()
    {
        GameManager.instance.WipeOntologyIndexAndCreateNew();
        InterfaceManager.instance.HideAllGUI();
        InterfaceManager.instance.ToggleProtagonistBrowser();
    }

    // Sets the options of the saves dorpdown
    public void SetSaveDropdown()
    {
        //  Clear the current dropdown list
        StepTwoDropdown.GetComponent<Dropdown>().ClearOptions();

        // Create a list of save names and add these to the options 
        List<string> listProjectNames = new List<string>();

        foreach (string saveName in ProjectManager.instance.currentProjectStateRecordNamesAndPaths.Keys)
            if (saveName != ProjectManager.instance.assetBundleFolderName) listProjectNames.Add(saveName);

        if (listProjectNames.Count <= 0)
        {
           StepTwoDropdown.GetComponent<Dropdown>().interactable = false;
           listProjectNames.Add("No saves found");
        }
        else
        {
            StepTwoDropdown.GetComponent<Dropdown>().interactable = true;
        }

        StepTwoDropdown.GetComponent<Dropdown>().AddOptions(listProjectNames);

        // Toggle interaction state of the start/initiate button
        ToggleStartButton();
    }

    // Toggle interaction state of the start/initiate button. This will enable when the required variables for levelloading are compleet.
    public void ToggleStartButton()
    {
        if (StepOneDropdown.GetComponent<Dropdown>().options.Count > 0)
            StepThreeButton.GetComponent<Button>().interactable = true;
        else
            StepThreeButton.GetComponent<Button>().interactable = false;
    }

    public void OnEnable()
    {
        UpdateChoices();
    }
}