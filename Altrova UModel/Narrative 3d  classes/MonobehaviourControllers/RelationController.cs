﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RelationController : MonoBehaviour {

    public bool isVisable;
    public LineRenderer ln;
    public Canvas relationCanvas;
    public Text relationText;
    public bool relationAllowed;

    public Relation relation;
    public IntentionalElementModel subjectElement;
    public bool subjectElementIsExecuted;
    public bool subjectElementAllowsExecution;
    public IntentionalElementModel relatedElement;
    public bool relatedElementIsExecuted;
    public bool relatedElementAllowsExecution;
    public string relationType;

    // Use this for initialization
    void Start()
    {
        if (this.ln == null)
        {
            ln = this.gameObject.GetComponentInChildren<LineRenderer>();

            if (this.ln == null)
                ln = this.gameObject.AddComponent<LineRenderer>();
        }

        if (this.relationCanvas == null)
        {
            relationCanvas = this.gameObject.GetComponentInChildren<Canvas>();

            if (this.relationCanvas == null)
                relationCanvas = this.gameObject.AddComponent<Canvas>();
        }

        if (relationText == null)
        {
            relationText = this.gameObject.GetComponentInChildren<Text>();

            if (this.relationText == null)
                relationText = this.gameObject.AddComponent<Text>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        isVisable = WorldManager.instance.showRelations;

        if (relation != null)
        {
            UpdateLineRenderer();
            UpdateRelationCanvas();
            UpdateRelationText();
        }
    }

    public Vector3 GetStartingPoint()
    {
        if (ln.positionCount >= 1)
            return ln.GetPosition(0);
        else
            return new Vector3();
    }

    public void SetStartingPoint(Vector3 startingPoint)
    {
        this.transform.position = startingPoint;
        ln.SetPosition(0, startingPoint);
    }

    public Vector3 GetEndingPoint()
    {
        if (ln.positionCount >= 1)
            return ln.GetPosition(ln.positionCount - 1);
        else
            return new Vector3();
    }

    public void SetEndPoint(Vector3 endPoint)
    {
        ln.SetPosition(ln.positionCount - 1, endPoint);
    }

    public void UpdateLineRenderer()
    {
        if (this.ln != null)
        {
            this.ln.enabled = isVisable;

            if (isVisable == true)
            {

                if (subjectElement.FindCreateSetController().FindSetView() != null)
                    SetStartingPoint(subjectElement.FindCreateSetController().FindSetView().transform.position);

                if (relatedElement.FindCreateSetController().FindSetView() != null)
                    SetEndPoint(relatedElement.FindCreateSetController().FindSetView().transform.position);
            }
        }

        // Set color
        if (subjectElementIsExecuted && relatedElementIsExecuted)
        {
            this.ln.startColor = new Color32(0, 255, 0, 255);
            this.ln.endColor = new Color32(0, 255, 0, 255);

            this.ln.material.color = new Color32(0, 255, 0, 255);
        }

        if (subjectElementIsExecuted && relatedElementIsExecuted == false)
        {
            this.ln.startColor = new Color32(0, 255, 255, 255);
            this.ln.endColor = new Color32(0, 255, 255, 255);

            this.ln.material.color = new Color32(0, 255, 255, 255);
        }

        if (subjectElementIsExecuted == false && relatedElementIsExecuted == false)
        {
            this.ln.startColor = new Color32(255, 0, 0, 255);
            this.ln.endColor = new Color32(255, 0, 0, 255);

            this.ln.material.color = new Color32(255, 0, 0, 255);
        }
    }

    public void UpdateRelationCanvas()
    {
        if (this.relationCanvas != null)
            this.relationCanvas.enabled = isVisable;

        // Relocate text position (in the middle of the line)
        if (this.relationCanvas.gameObject != this.gameObject)
        {
            this.relationCanvas.transform.position = (subjectElement.FindCreateSetController().FindSetView().transform.position + relatedElement.FindCreateSetController().FindSetView().transform.position) / 2; //(GetStartingPoint() - GetEndingPoint()) / 2;
            this.relationCanvas.transform.position = (this.relationCanvas.transform.position + PerspectiveManager.instance.GetCurrentCamera().transform.position) / 2; //(GetStartingPoint() - GetEndingPoint()) / 2;
        }


        // Face controlled camera
        this.relationCanvas.transform.LookAt(2 * relationCanvas.transform.position - PerspectiveManager.instance.GetCurrentCamera().transform.position);
    }

    public void UpdateRelationText()
    {
        if (this.relationText != null)
        {
            this.relationText.enabled = isVisable;

            if (isVisable == true)
            {
                if (this.relation.GetType() == typeof(Connects))
                    relationText.text = "[" + ((Connects)relation).elementConnectionType.ToString() + "]";
                else if (this.relation.GetType() == typeof(Contributes))
                    relationText.text = "[" + ((Contributes)relation).contributionValue.ToString() + "]";
                else if (this.relation.GetType() == typeof(Dependency))
                    relationText.text = "[" + "depends" + "]";
            }
        }
    }

    public void InitialiseRelationController(IntentionalElementView ie, Relation relation)
    {
        if (ie == null || ie.FindCreateSetController().FindSetModel() == null)
        {
            Debug.Log("Destroying this " + GetType().ToString());
            GameObject.Destroy(this.gameObject);
        }

        subjectElement = (IntentionalElementModel)ie.FindCreateSetController().FindSetModel();

        if (CheckIfRelationExists(relation))
            if (relation.GetElementLink() != null)
            {
                this.relation = relation;
                relatedElement = (IntentionalElementModel)relation.GetElementLink();
                relationType = relation.GetType().ToString();
            }
    }

    protected bool CheckIfRelationExists(Relation relation)
    {
        if(relation.GetType() == typeof(Connects))
            return ((ActivityModel)subjectElement).Connects.Contains(relation);
        else if (relation.GetType() == typeof(Contributes))
            return subjectElement.Contributes.Contains(relation);
        else if (relation.GetType() == typeof(Dependency))
            return subjectElement.Depends.Contains(relation);
        else
            return false;
    }
}
