﻿using Object.ControlSetup;
using Object.Movement;
using Object.MovementInput;
using Object.MovementRestriction;
using ObjectControl.Type;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectControlModel : BaseModel, IObjectControlModel
{
    // Constructor
    public ObjectControlModel(string entityID, ComplexObjectModel complexObject) : base(entityID)
    {
        controlWielder = complexObject;
    }

    // Constructor
    public ObjectControlModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
    }

    // Returns the controlType
    public ControlTypeEnum GetControlType()
    {
        if (this.GetType() == typeof(CameraControlModel))
            return ControlTypeEnum.CAMERACONTROL;
        else
            return ControlTypeEnum.OBJECTCONTROL;

    }

    /* controls settings */

        // An Array of "ObjectMovmentInput" object, that define default inputKey for every possible cameraMovement 
        protected ObjectMovementInput[] b_defaultObjectMovementInput;
        public ObjectMovementInput[] defaultObjectMovementInput
        {
        get
        {
            if (b_defaultObjectMovementInput == null)
            {
                SetDefaultMovementInput();
            }

            return b_defaultObjectMovementInput;

        }
        set
        {
            // Only if the backing field changes
            if (b_customObjectMovementInput != value)
            {
                // Set new value
                b_customObjectMovementInput = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new CustomObjectMovementInputChangedEventArgs();
                OnCustomObjectMovementInputChanged(this, eventArgs);
            }
        }
    }

        // An Array of "ObjectMovmentInput" object, that define custom inputKey for every possible cameraMovement 
        private ObjectMovementInput[] b_customObjectMovementInput;
        public ObjectMovementInput[] customObjectMovementInput
        {
            get { return b_customObjectMovementInput; }
            set
            {
                // Only if the backing field changes
                if (b_customObjectMovementInput != value)
                {
                // Set new value
                b_customObjectMovementInput = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new CustomObjectMovementInputChangedEventArgs();
                    OnCustomObjectMovementInputChanged(this, eventArgs);
                }
            }
        }

        // An Array of "ObjectMovmentInput" object, that all allowed inputKey for every possible cameraMovement of the current camera ( = defaultObjectMovementInput/customCameraMovementInput containing only for cameraMovement)
        private ObjectMovementInput[] b_availableObjectMovementInput;
        public ObjectMovementInput[] availableObjectMovementInput
        {
            get { return b_availableObjectMovementInput; }
            set
            {
                // Only if the backing field changes
                if (b_availableObjectMovementInput != value)
                {
                    // Set new value
                    b_availableObjectMovementInput = value;
                
                    // Dispatch the deticated "changed" event
                    var eventArgs = new AvailableObjectMovementInputChangedEventArgs();
                    OnAvailableObjectMovementInputChanged(this, eventArgs);
                }
            }
        }

        // Defines the objects input controls and the movement they control
        private ControlSetupEnum b_controlSetup = ControlSetupEnum.DEFAULT;
        public ControlSetupEnum controlSetup
        {
            get { return b_controlSetup; }
            set
            {
                // Only if the backing field changes
                if (b_controlSetup != value)
                {
                // Set new value
                b_controlSetup = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new ControlSetupChangedEventArgs();
                    OnControlSetupChanged(this, eventArgs);
                }
            }
        }

        // Defines the posible movement of the object
        private ObjectMovementEnum[] b_movement;
        public ObjectMovementEnum[] movement
        {
            get { return b_movement; }
            set
            {
                // Only if the backing field changes
                if (b_movement != value)
                {
                    // Set new value
                    b_movement = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MovementChangedEventArgs();
                    OnMovementChanged(this, eventArgs);
                }
            }
        }

        // Defines the not-posible movement of the object
        private ObjectMovementRestrictionEnum[] b_movementRestrictions;
        public ObjectMovementRestrictionEnum[] movementRestrictions
        {
            get { return b_movementRestrictions; }
            set
            {
                // Only if the backing field changes
                if (b_movementRestrictions != value)
                {
                    // Set new value
                    b_movementRestrictions = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MovementRestrictionsChangedEventArgs();
                    OnMovementRestrictionsChanged(this, eventArgs);
                }
            }
        }

    /* Axial Movement */

        // Longitudinal movement speed
        private float b_longitudinalMovementSpeed = 1.0F;
        public float longitudinalMovementSpeed
        {
            get { return b_longitudinalMovementSpeed; }
            set
            {
                // Only if the backing field changes
                if (b_longitudinalMovementSpeed != value)
                {
                // Set new value
                b_longitudinalMovementSpeed = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new LongitudinalMovementSpeedChangedEventArgs();
                    OnLongitudinalMovementSpeedChanged(this, eventArgs);
                }
            }
        }

        // Starting location longitudinal distance
        private float b_startingDistanceLongitudinalMovement = 0.0F;
        public float startingDistanceLongitudinalMovement
        {
            get { return b_startingDistanceLongitudinalMovement; }
            set
            {
                // Only if the backing field changes
                if (b_startingDistanceLongitudinalMovement != value)
                {
                // Set new value
                b_startingDistanceLongitudinalMovement = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new StartingDistanceLongitudinalMovementChangedEventArgs();
                    OnStartingDistanceLongitudinalMovementChanged(this, eventArgs);
                }
            }
        }
        
        // Toggle max. lateral movement ristriction
        private bool b_maxLongitudinalMoveDistanceRestricted = false;       
        public bool maxLongitudinalMoveDistanceRestricted
                {
            get { return b_maxLongitudinalMoveDistanceRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_maxLongitudinalMoveDistanceRestricted != value)
                {
                // Set new value
                b_maxLongitudinalMoveDistanceRestricted = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxLongitudinalMoveDistanceRestrictedChangedEventArgs();
                    OnMaxLongitudinalMoveDistanceRestrictedChanged(this, eventArgs);
                }
            }
        }
    
        // Max. lateral move distance
        private float b_maxLongitudinalMoveDistance = 100.0F;
        public float maxLongitudinalMoveDistance
        {
            get { return b_maxLongitudinalMoveDistance; }
            set
            {
                // Only if the backing field changes
                if (b_maxLongitudinalMoveDistance != value)
                {
                // Set new value
                b_maxLongitudinalMoveDistance = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxLongitudinalMoveDistanceChangedEventArgs();
                    OnMaxLongitudinalMoveDistanceChanged(this, eventArgs);
                }
            }
        }

        //Toggle min. lateral movement ristriction
        private bool b_minLongitudinalMoveDistanceRestricted = false;
        public bool minLongitudinalMoveDistanceRestricted
        {
            get { return b_minLongitudinalMoveDistanceRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_minLongitudinalMoveDistanceRestricted != value)
                {
                // Set new value
                b_minLongitudinalMoveDistanceRestricted = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MinLongitudinalMoveDistanceRestrictedChangedEventArgs();
                    OnMinLongitudinalMoveDistanceRestrictedChanged(this, eventArgs);
                }
            }
        }

        // Min. lateral move distance
        private float b_minLongitudinalMoveDistance = -100.0F;
        public float minLongitudinalMoveDistance
        {
            get { return b_minLongitudinalMoveDistance; }
            set
            {
                // Only if the backing field changes
                if (b_minLongitudinalMoveDistance != value)
                {
                    // Set new value
                    b_minLongitudinalMoveDistance = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new b_minLongitudinalMoveDistanceChangedEventArgs();
                    OnMinLongitudinalMoveDistanceChanged(this, eventArgs);
                }
            }
        }


        // Vertical movement speed
        private float b_verticalMovementSpeed = 1.0F;
        public float verticalMovementSpeed
        {
            get { return b_verticalMovementSpeed; }
            set
            {
                // Only if the backing field changes
                if (b_verticalMovementSpeed != value)
                {
                    // Set new value
                    b_verticalMovementSpeed = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new VerticalMovementSpeedChangedEventArgs();
                    OnVerticalMovementSpeedChanged(this, eventArgs);
                }
            }
        }

        // Starting location vertical distance
        private float b_startingDistanceVerticalMovement = 0.0F;
        public float startingDistanceVerticalMovement
        {
            get { return b_startingDistanceVerticalMovement; }
            set
            {
                // Only if the backing field changes
                if (b_startingDistanceVerticalMovement != value)
                {
                    // Set new value
                    b_startingDistanceVerticalMovement = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new StartingDistanceVerticalMovementChangedEventArgs();
                    OnStartingDistanceVerticalMovementChanged(this, eventArgs);
                }
            }
        }

        // Toggle max. vertical movement ristriction
        private bool b_maxVerticalMoveDistanceRestricted = false;
        public bool maxVerticalMoveDistanceRestricted
        {
            get { return b_maxVerticalMoveDistanceRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_maxVerticalMoveDistanceRestricted != value)
                {
                    // Set new value
                    b_maxVerticalMoveDistanceRestricted = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxVerticalMoveDistanceRestrictedChangedEventArgs();
                    OnMaxVerticalMoveDistanceRestrictedChanged(this, eventArgs);
                }
            }
        }

        // Max. vertical move distance
        private float b_maxVerticalMoveDistance = 100.0F;
        public float maxVerticalMoveDistance
        {
            get { return b_maxVerticalMoveDistance; }
            set
            {
                // Only if the backing field changes
                if (b_maxVerticalMoveDistance != value)
                {
                    // Set new value
                    b_maxVerticalMoveDistance = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxVerticalMoveDistanceChangedEventArgs();
                    OnMaxVerticalMoveDistanceChanged(this, eventArgs);
                }
            }
        }

        //Toggle min. vertical movement ristriction
        private bool b_minVerticalMoveDistanceRestricted = false;
        public bool minVerticalMoveDistanceRestricted
        {
            get { return b_minVerticalMoveDistanceRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_minVerticalMoveDistanceRestricted != value)
                {
                // Set new value
                b_minVerticalMoveDistanceRestricted = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MinVerticalMoveDistanceRestrictedChangedEventArgs();
                    OnMinVerticalMoveDistanceRestrictedChanged(this, eventArgs);
                }
            }
        }

        // Min. vertical move distance
        private float b_minVerticalMoveDistance = -100.0F;
        public float minVerticalMoveDistance
        {
            get { return b_minVerticalMoveDistance; }
            set
            {
                // Only if the backing field changes
                if (b_minVerticalMoveDistance != value)
                {
                    // Set new value
                    b_minVerticalMoveDistance = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MinVerticalMoveDistanceChangedEventArgs();
                    OnMinVerticalMoveDistanceChanged(this, eventArgs);
                }
            }
        }


        // Lateral  movement speed
        private float b_lateralMovementSpeed = 1.0F;
        public float lateralMovementSpeed
        {
            get { return b_lateralMovementSpeed; }
            set
            {
                // Only if the backing field changes
                if (b_lateralMovementSpeed != value)
                {
                    // Set new value
                    b_lateralMovementSpeed = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new LateralMovementSpeedChangedEventArgs();
                    OnLateralMovementSpeedChanged(this, eventArgs);
                }
            }
        }

        // Starting location lateral distance
        private float b_startingDistanceLateralMovement = 0.0F;
        public float startingDistanceLateralMovement
        {
            get { return b_startingDistanceLateralMovement; }
            set
            {
                // Only if the backing field changes
                if (b_startingDistanceLateralMovement != value)
                {
                    // Set new value
                    b_startingDistanceLateralMovement = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new StartingDistanceLateralMovementChangedEventArgs();
                    OnStartingDistanceLateralMovementChanged(this, eventArgs);
                }
            }
        }

        // Toggle max. vertical movement ristriction
        private bool b_maxLateralMoveDistanceRestricted = false;
        public bool maxLateralMoveDistanceRestricted
        {
            get { return b_maxLateralMoveDistanceRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_maxLateralMoveDistanceRestricted != value)
                {
                    // Set new value
                    b_maxLateralMoveDistanceRestricted = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxLateralMoveDistanceRestrictedChangedEventArgs();
                    OnMaxLateralMoveDistanceRestrictedChanged(this, eventArgs);
            }
            }
        }

        // Max. vertical move distance
        private float b_maxLateralMoveDistance = 100.0F;
        public float maxLateralMoveDistance
        {
            get { return b_maxLateralMoveDistance; }
            set
            {
                // Only if the backing field changes
                if (b_maxLateralMoveDistance != value)
                {
                    // Set new value
                    b_maxLateralMoveDistance = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxLateralMoveDistanceChangedEventArgs();
                    OnMaxLateralMoveDistanceChanged(this, eventArgs);
                }
            }
        }

        //Toggle min. vertical movement ristriction
        private bool b_minLateralMoveDistanceRestricted = false;
        public bool minLateralMoveDistanceRestricted
        {
            get { return b_minLateralMoveDistanceRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_minLateralMoveDistanceRestricted != value)
                {
                    // Set new value
                    b_minLateralMoveDistanceRestricted = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MinLateralMoveDistanceRestrictedChangedEventArgs();
                    OnMinLateralMoveDistanceRestrictedChanged(this, eventArgs);
                }
            }
        }

        // Min. vertical move distance
        private float b_minLateralMoveDistance = -100.0F;
        public float minLateralMoveDistance
        {
            get { return b_minLateralMoveDistance; }
            set
            {
                // Only if the backing field changes
                if (b_minLateralMoveDistance != value)
                {
                    // Set new value
                    b_minLateralMoveDistance = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MinLateralMoveDistanceChangedEventArgs();
                    OnMinLateralMoveDistanceChanged(this, eventArgs);
                }
            }
        }


    /* Radial movement/Rotation on this object. Based on aircraft principal axes pitch (x-axes rotation), yaw (y-axes rotation), roll (z-axes rotation) */

        // Pitch rotation around speed 
        private float b_pitchRotationSpeed = 50.0F;
        public float pitchRotationSpeed
        {
            get { return b_pitchRotationSpeed; }
            set
            {
                // Only if the backing field changes
                if (b_pitchRotationSpeed != value)
                {
                    // Set new value
                    b_pitchRotationSpeed = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new PitchRotationSpeedChangedEventArgs();
                    OnPitchRotationSpeedChanged(this, eventArgs);
                }
            }
        }

        // Toggle max. pitch rotation angle ristriction
        private bool b_pitchRotationAngleRestricted = false;
        public bool pitchRotationAngleRestricted
        {
            get { return b_pitchRotationAngleRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_pitchRotationAngleRestricted != value)
                {
                    // Set new value
                    b_pitchRotationAngleRestricted = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new PitchRotationAngleRestrictedChangedEventArgs();
                    OnPitchRotationAngleRestrictedChanged(this, eventArgs);
                }
            }
        }

        // Starting pitch rotation angle
        private float b_startingPitchRotationAngle = 0.0F;
        public float startingPitchRotationAngle
        {
            get { return b_startingPitchRotationAngle; }
            set
            {
                // Only if the backing field changes
                if (b_startingPitchRotationAngle != value)
                {
                    // Set new value
                    b_startingPitchRotationAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new StartingPitchRotationAngleChangedEventArgs();
                    OnStartingPitchRotationAngleChanged(this, eventArgs);
                }
            }
        }

        // Max. backward pitch rotation angle
        private float b_maxPitchNegRotationAngle = 180.0F;
        public float maxPitchNegRotationAngle
        {
            get { return b_maxPitchNegRotationAngle; }
            set
            {
                // Only if the backing field changes
                if (b_maxPitchNegRotationAngle != value)
                {
                    // Set new value
                    b_maxPitchNegRotationAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxPitchNegRotationAngleChangedEventArgs();
                    OnMaxPitchNegRotationAngleChanged(this, eventArgs);
                }
            }
        }

        // Max. forward pitch rotation angle
        private float b_maxPitchPosRotationAngle = 180.0F;
        public float maxPitchPosRotationAngle
        {
            get { return b_maxPitchPosRotationAngle; }
            set
            {
                // Only if the backing field changes
                if (b_maxPitchPosRotationAngle != value)
                {
                    // Set new value
                    b_maxPitchPosRotationAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxPitchPosRotationAngleChangedEventArgs();
                    OnMaxPitchPosRotationAngleChanged(this, eventArgs);
                }
            }
        }


        // Yaw rotation speed 
        private float b_yawRotationSpeed = 50.0F;
        public float yawRotationSpeed
        {
            get { return b_yawRotationSpeed; }
            set
            {
                // Only if the backing field changes
                if (b_yawRotationSpeed != value)
                {
                    // Set new value
                    b_yawRotationSpeed = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new YawRotationSpeedChangedEventArgs();
                    OnYawRotationSpeedChanged(this, eventArgs);
                }
            }
        }

        // Toggle max. yaw angle rotation around  ristriction
        private bool b_yawRotationAngleRestricted = false;
        public bool yawRotationAngleRestricted
        {
            get { return b_yawRotationAngleRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_yawRotationAngleRestricted != value)
                {
                    // Set new value
                    b_yawRotationAngleRestricted = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new YawRotationAngleRestrictedChangedEventArgs();
                    OnYawRotationAngleRestrictedChanged(this, eventArgs);
                }
            }
        }

        // Starting yaw rotation angle
        private float b_startingYawRotationAngle = 0.0F;
        public float startingYawRotationAngle
        {
            get { return b_startingYawRotationAngle; }
            set
            {
                // Only if the backing field changes
                if (b_startingYawRotationAngle != value)
                {
                    // Set new value
                    b_startingYawRotationAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new StartingYawRotationAngleChangedEventArgs();
                    OnStartingYawRotationAngleChanged(this, eventArgs);
                }
            }
        }

        // Max. down angle rotation around 
        private float b_maxYawNegRotationAngle = 180.0F;
        public float maxYawNegRotationAngle
        {
            get { return b_maxYawNegRotationAngle; }
            set
            {
                // Only if the backing field changes
                if (b_maxYawNegRotationAngle != value)
                {
                    // Set new value
                    b_maxYawNegRotationAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxYawNegRotationAngleChangedEventArgs();
                    OnMaxYawNegRotationAngleChanged(this, eventArgs);
                }
            }
        }

        // Max. up angle rotation around 
        private float b_maxYawPosRotationAngle = 180.0F;
        public float maxYawPosRotationAngle
        {
            get { return b_maxYawPosRotationAngle; }
            set
            {
                // Only if the backing field changes
                if (b_maxYawPosRotationAngle != value)
                {
                    // Set new value
                    b_maxYawPosRotationAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxYawPosRotationAngleChangedEventArgs();
                    OnMaxYawPosRotationAngleChanged(this, eventArgs);
                }
            }
        }
    

        // Roll rotation speed 
        private float b_rollRotationSpeed = 1.0F;
        public float rollRotationSpeed
        {
            get { return b_rollRotationSpeed; }
            set
            {
                // Only if the backing field changes
                if (b_rollRotationSpeed != value)
                {
                    // Set new value
                    b_rollRotationSpeed = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new RollRotationSpeedChangedEventArgs();
                    OnRollRotationSpeedChanged(this, eventArgs);
                }
            }
        }

        // Toggle max. roll rotation angle ristriction
        private bool b_rollRotationAngleRestricted = false;
        public bool rollRotationAngleRestricted
        {
            get { return b_rollRotationAngleRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_rollRotationAngleRestricted != value)
                {
                    // Set new value
                    b_rollRotationAngleRestricted = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new RollRotationAngleRestrictedChangedEventArgs();
                    OnRollRotationAngleRestrictedChanged(this, eventArgs);
                }
            }
        }

        // Starting roll rotation angle
        private float b_startingRollRotationAngle = 0.0F;
        public float startingRollRotationAngle
        {
            get { return b_startingRollRotationAngle; }
            set
            {
                // Only if the backing field changes
                if (b_startingRollRotationAngle != value)
                {
                    // Set new value
                    b_startingRollRotationAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new StartingRollRotationAngleChangedEventArgs();
                    OnStartingRollRotationAngleChanged(this, eventArgs);
                }
            }
        }

        // Max. left roll rotation angle 
        private float b_maxRollNegRotationAngle = 180.0F;
        public float maxRollNegRotationAngle
        {
            get { return b_startingRollRotationAngle; }
            set
            {
                // Only if the backing field changes
                if (b_startingRollRotationAngle != value)
                {
                    // Set new value
                    b_startingRollRotationAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxRollNegRotationAngleChangedEventArgs();
                    OnMaxRollNegRotationAngleChanged(this, eventArgs);
                }
            }
        }

        // Max. right roll rotation angle  
        private float b_maxRollPosRotationAngle = 180.0F;
        public float maxRollPosRotationAngle
        {
            get { return b_startingRollRotationAngle; }
            set
            {
                // Only if the backing field changes
                if (b_startingRollRotationAngle != value)
                {
                    // Set new value
                    b_startingRollRotationAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxRollPosRotationAngleChangedEventArgs();
                    OnMaxRollPosRotationAngleChanged(this, eventArgs);
                }
            }
        }

    /* Radial movement around/Rotation around subject. Based on the spherical coordinate system: radius (r), polar angle θ(theta), and azimuthal angle φ(phi). */

        // Radial distance rotation change speed 
        private float b_radiusChangeSpeed = 1.0F;
        public float radiusChangeSpeed
        {
            get { return b_radiusChangeSpeed; }
            set
            {
                // Only if the backing field changes
                if (b_radiusChangeSpeed != value)
                {
                    // Set new value
                    b_radiusChangeSpeed = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new RadiusChangeSpeedChangedEventArgs();
                    OnRadiusChangeSpeedChanged(this, eventArgs);
                }
            }
        }

        // Toggle max. horizontal rotation around angle ristriction
        private bool b_radiusRestricted = false;
        public bool radiusRestricted
        {
            get { return b_radiusRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_radiusRestricted != value)
                {
                    // Set new value
                    b_radiusRestricted = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new RadiusRestrictedChangedEventArgs();
                    OnRadiusRestrictedChanged(this, eventArgs);
                }
            }
        }

        // Max. radial distance around expansion from starting coordinate
        private float b_maxRadiusExpansion = 100.0F;
        public float maxRadiusExpansion
        {
            get { return b_maxRadiusExpansion; }
            set
            {
                // Only if the backing field changes
                if (b_maxRadiusExpansion != value)
                {
                    // Set new value
                    b_maxRadiusExpansion = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxRadiusExpansionChangedEventArgs();
                    OnMaxRadiusExpansionChanged(this, eventArgs);
                }
            }
        }

        // Min. radial distance around from objects from center coordinate
        private float b_minRadiusFromCenter = 0F;
        public float minRadiusFromCenter
        {
            get { return b_minRadiusFromCenter; }
            set
            {
                // Only if the backing field changes
                if (b_minRadiusFromCenter != value)
                {
                // Set new value
                b_minRadiusFromCenter = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MinRadiusFromCenterChangedEventArgs();
                    OnMinRadiusFromCenterChanged(this, eventArgs);
                }
            }
        }
  

        // Polar angle rotation around speed (This is horizontal or as in geography "latitude")
        private float b_polarAngleCoordinatesChangeSpeed = 1.0F;
        public float polarAngleCoordinatesChangeSpeed
        {
            get { return b_polarAngleCoordinatesChangeSpeed; }
            set
            {
                // Only if the backing field changes
                if (b_polarAngleCoordinatesChangeSpeed != value)
                {
                    // Set new value
                    b_polarAngleCoordinatesChangeSpeed = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new PolarAngleCoordinatesChangeSpeedChangedEventArgs();
                    OnPolarAngleCoordinatesChangeSpeedChanged(this, eventArgs);
                }
            }
        }

        // Toggle max. polar angle rotation around ristriction
        private bool b_polarRotationAroundAngleRestricted = false;
        public bool polarRotationAroundAngleRestricted
        {
            get { return b_polarRotationAroundAngleRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_polarRotationAroundAngleRestricted != value)
                {
                    // Set new value
                    b_polarRotationAroundAngleRestricted = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new PolarRotationAroundAngleRestrictedChangedEventArgs();
                    OnPolarRotationAroundAngleRestrictedChanged(this, eventArgs);
                }
            }
        }

        // Starting angle rotation around polar angle
        private float b_startingPolarRotationAroundAngle = 0.0F;
        public float startingPolarRotationAroundAngle
        {
            get { return b_startingPolarRotationAroundAngle; }
            set
            {
                // Only if the backing field changes
                if (b_startingPolarRotationAroundAngle != value)
                {
                    // Set new value
                    b_startingPolarRotationAroundAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new StartingPolarRotationAroundAngleChangedEventArgs();
                    OnStartingPolarRotationAroundAngleChanged(this, eventArgs);
                }
            }
        }

        // Max. Down rotation around polar angle
        private float b_maxPolarNegRotationAroundAngle = 180.0F;
        public float maxPolarNegRotationAroundAngle
        {
            get { return b_maxPolarNegRotationAroundAngle; }
            set
            {
                // Only if the backing field changes
                if (b_maxPolarNegRotationAroundAngle != value)
                {
                    // Set new value
                    b_maxPolarNegRotationAroundAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxPolarNegRotationAroundAngleChangedEventArgs();
                    OnMaxPolarNegRotationAroundAngleChanged(this, eventArgs);
                }
            }
        }

        // Max. Up rotation around polar angle
        private float b_maxPolarPosRotationAroundAngle = 180.0F;
        public float maxPolarPosRotationAroundAngle
        {
            get { return b_maxPolarPosRotationAroundAngle; }
            set
            {
                // Only if the backing field changes
                if (b_maxPolarPosRotationAroundAngle != value)
                {
                    // Set new value
                    b_maxPolarPosRotationAroundAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxPolarPosRotationAroundAngleChangedEventArgs();
                    OnMaxPolarPosRotationAroundAngleChanged(this, eventArgs);
                }
            }
        }


        // Azimuthal rotation around (This is vertical or as in geography "longtitude")
        private float b_azimuthalAngleCoordinatesChangeSpeed = 1.0F;
        public float azimuthalAngleCoordinatesChangeSpeed
        {
            get { return b_azimuthalAngleCoordinatesChangeSpeed; }
            set
            {
                // Only if the backing field changes
                if (b_azimuthalAngleCoordinatesChangeSpeed != value)
                {
                    // Set new value
                    b_azimuthalAngleCoordinatesChangeSpeed = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new AzimuthalAngleCoordinatesChangeSpeedChangedEventArgs();
                    OnAzimuthalAngleCoordinatesChangeSpeedChanged(this, eventArgs);
                }
            }
        }

        // Toggle max. azimuthal rotation around angle ristriction
        private bool b_azimuthalRotationAroundAngleRestricted = false;
        public bool azimuthalRotationAroundAngleRestricted
        {
            get { return b_azimuthalRotationAroundAngleRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_azimuthalRotationAroundAngleRestricted != value)
                {
                    // Set new value
                    b_azimuthalRotationAroundAngleRestricted = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new AzimuthalRotationAroundAngleRestrictedChangedEventArgs();
                    OnAzimuthalRotationAroundAngleRestrictedChanged(this, eventArgs);
                }
            }
        }

        // Starting angle rotation around azimuthal angle
        private float b_startingAzimuthalRotationAroundAngle = 0.0F;
        public float startingAzimuthalRotationAroundAngle
        {
            get { return b_startingAzimuthalRotationAroundAngle; }
            set
            {
                // Only if the backing field changes
                if (b_startingAzimuthalRotationAroundAngle != value)
                {
                    // Set new value
                    b_startingAzimuthalRotationAroundAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new StartingAzimuthalRotationAroundAngleChangedEventArgs();
                    OnStartingAzimuthalRotationAroundAngleChanged(this, eventArgs);
                }
            }
        }

        // Max. left rotation around azimuthal angle
        private float b_maxAzimuthalNegRotationAroundAngle = 180.0F;
        public float maxAzimuthalNegRotationAroundAngle
        {
            get { return b_maxAzimuthalNegRotationAroundAngle; }
            set
            {
                // Only if the backing field changes
                if (b_maxAzimuthalNegRotationAroundAngle != value)
                {
                    // Set new value
                    b_maxAzimuthalNegRotationAroundAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxAzimuthalNegRotationAroundAngleChangedEventArgs();
                    OnMaxAzimuthalNegRotationAroundAngleChanged(this, eventArgs);
                }
            }
        }

        // Max. right rotation around azimuthal angle
        private float b_maxAzimuthalPosRotationAroundAngle = 180.0F;
        public float maxAzimuthalPosRotationAroundAngle
        {
            get { return b_maxAzimuthalPosRotationAroundAngle; }
            set
            {
                // Only if the backing field changes
                if (b_maxAzimuthalPosRotationAroundAngle != value)
                {
                    // Set new value
                    b_maxAzimuthalPosRotationAroundAngle = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new MaxAzimuthalPosRotationAroundAngleChangedEventArgs();
                    OnMaxAzimuthalPosRotationAroundAngleChanged(this, eventArgs);
                }
            }
        }


    /* Targets for interaction and control */

        // Object that holds the controlView
        private ComplexObjectModel b_controlWielder = null;
        public ComplexObjectModel controlWielder
       {
            get { return b_controlWielder; }
            set
            {
                // Only if the backing field changes
                if (b_controlWielder != value)
                {
                    // Set new value
                    b_controlWielder = value;

                    b_controlWielder.ControlSettings = this;

                        // Dispatch the deticated "changed" event
                    var eventArgs = new SubjectChangedEventArgs();
                    OnSubjectChanged(this, eventArgs);
                }
            }
        }

        // Indicates the GameObject which controls this object
        private ObjectModel b_subject = null;
        public ObjectModel subject
        {
            get { return b_subject; }
            set
            {
                // Only if the backing field changes
                if (b_subject != value)
                {
                    // Set new value
                    b_subject = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new SubjectChangedEventArgs();
                    OnSubjectChanged(this, eventArgs);
                }
            }
        }

        // Indicates the currently controlled object
        private ObjectModel b_dedicatedObject = null;
        public ObjectModel dedicatedObject
        {
            get { return b_dedicatedObject; }
            set
            {
                // Only if the backing field changes
                if (b_dedicatedObject != value)
                {
                    // Set new value
                    b_dedicatedObject = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new DedicatedObjectChangedEventArgs();
                    OnDedicatedObjectChanged(this, eventArgs);
                }
            }
        }

        // Backing field for the interactable object of intrest
        private ObjectModel b_target = null;
        public ObjectModel target
        {
            get { return b_target; }
            set
            {
                // Only if the backing field changes
                if (b_target != value)
                {
                    // Set new value
                    b_target = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new TargetChangedEventArgs();
                    OnTargetChanged(this, eventArgs);
                }
            }
        }

        // Finds for an existing controller
        public sealed override BaseController FindSetController()
        {
            if (b_controller != null)
            {
                if (this is CameraControlModel)
                {
                    // Looks up for the existence of a controller with the same objectURI
                    CameraControlController cameraControlController = ((CameraControlController)ObjectFactory.Instance.entityControllers[EntityID]);

                    if (cameraControlController != null)
                    {
                        this.Controller = cameraControlController;
                        return Controller;
                    }
                    else
                        return null;
                }

                else
                {
                    // Looks up for the existence of a controller with the same objectURI
                    BaseController controller = ObjectFactory.Instance.entityControllers[EntityID];

                    if (controller != null)
                    {
                        this.Controller = controller;
                        return controller;
                    }
                    else
                        return null;
                }
            }
            else
            {
                return b_controller;
            }
        }

        // Finds for an existing controller, else creates one
        public override BaseController FindCreateSetController()
        {
            // Looks up for the existence of a view with the same entityID
            if (FindSetController() != null)
                return FindSetController();

            else if (this is CameraControlModel)
            {
                this.Controller = ObjectFactory.Instance.controllerFactory.createForCameraControl(((CameraControlModel)this), false);
                return this.Controller;
            }

            else
            {
                this.Controller = ObjectFactory.Instance.controllerFactory.createForObjectControl(this, false);
                return this.Controller;
            }
        }

        protected virtual void SetDefaultMovementInput()
        {
        //b_defaultObjectMovementInput = new ObjectMovementInput[7];

        //int arrayIndex = 0;

        //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.MOVELATERAL;
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Horizontal_Move");

        //arrayIndex++;

        //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.MOVEVERTICAL;
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Vertical_Move");

        //arrayIndex++;

        //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEAROUNDAZIMUTHAL;
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Horizontal_RotateAround");

        //arrayIndex++;

        //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEAROUNDPOLAR;
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Vertical_RotateAround");

        //arrayIndex++;

        //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEYAW;
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Horizontal_Rotate");

        //arrayIndex++;

        //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEPITCH;
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Vertical_Rotate");

        //arrayIndex++;

        //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.MOVELONGITUDINAL;
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Depth_Move");

        //arrayIndex++;

        b_defaultObjectMovementInput = new ObjectMovementInput[4];

        int arrayIndex = 0;

        b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.MOVELATERAL;
        b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Horizontal_Move");

        arrayIndex++;

        b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.MOVEVERTICAL;
        b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Vertical_Move");

        arrayIndex++;

        //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEAROUNDAZIMUTHAL;
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Horizontal_RotateAround");

        //arrayIndex++;

        //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEAROUNDPOLAR;
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Vertical_RotateAround");

        //arrayIndex++;

        b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEYAW;
        b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Horizontal_Rotate");

        arrayIndex++;

        //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEPITCH;
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Vertical_Rotate");

        //arrayIndex++;

        b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
        b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
        b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.MOVELONGITUDINAL;
        b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
        b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Depth_Move");

        arrayIndex++;
    }

    // Dispatch EventHandler for controls settings changes
    public event EventHandler<DefaultObjectMovementInputChangedEventArgs> OnDefaultObjectMovementInputChanged = (sender, e) => { };
    public event EventHandler<CustomObjectMovementInputChangedEventArgs> OnCustomObjectMovementInputChanged = (sender, e) => { };
    public event EventHandler<AvailableObjectMovementInputChangedEventArgs> OnAvailableObjectMovementInputChanged = (sender, e) => { };

    public event EventHandler<ControlSetupChangedEventArgs> OnControlSetupChanged = (sender, e) => { };
    public event EventHandler<MovementChangedEventArgs> OnMovementChanged = (sender, e) => { };
    public event EventHandler<MovementRestrictionsChangedEventArgs> OnMovementRestrictionsChanged = (sender, e) => { };

    // Dispatch EventHandler for axial Movement changes
    public event EventHandler<LongitudinalMovementSpeedChangedEventArgs> OnLongitudinalMovementSpeedChanged = (sender, e) => { };
    public event EventHandler<StartingDistanceLongitudinalMovementChangedEventArgs> OnStartingDistanceLongitudinalMovementChanged = (sender, e) => { };
    public event EventHandler<MaxLongitudinalMoveDistanceRestrictedChangedEventArgs> OnMaxLongitudinalMoveDistanceRestrictedChanged = (sender, e) => { };
    public event EventHandler<MaxLongitudinalMoveDistanceChangedEventArgs> OnMaxLongitudinalMoveDistanceChanged = (sender, e) => { };
    public event EventHandler<MinLongitudinalMoveDistanceRestrictedChangedEventArgs> OnMinLongitudinalMoveDistanceRestrictedChanged = (sender, e) => { };
    public event EventHandler<b_minLongitudinalMoveDistanceChangedEventArgs> OnMinLongitudinalMoveDistanceChanged = (sender, e) => { };

    public event EventHandler<VerticalMovementSpeedChangedEventArgs> OnVerticalMovementSpeedChanged = (sender, e) => { };
    public event EventHandler<StartingDistanceVerticalMovementChangedEventArgs> OnStartingDistanceVerticalMovementChanged = (sender, e) => { };
    public event EventHandler<MaxVerticalMoveDistanceRestrictedChangedEventArgs> OnMaxVerticalMoveDistanceRestrictedChanged = (sender, e) => { };
    public event EventHandler<MaxVerticalMoveDistanceChangedEventArgs> OnMaxVerticalMoveDistanceChanged = (sender, e) => { };
    public event EventHandler<MinVerticalMoveDistanceRestrictedChangedEventArgs> OnMinVerticalMoveDistanceRestrictedChanged = (sender, e) => { };
    public event EventHandler<MinVerticalMoveDistanceChangedEventArgs> OnMinVerticalMoveDistanceChanged = (sender, e) => { };

    public event EventHandler<LateralMovementSpeedChangedEventArgs> OnLateralMovementSpeedChanged = (sender, e) => { };
    public event EventHandler<StartingDistanceLateralMovementChangedEventArgs> OnStartingDistanceLateralMovementChanged = (sender, e) => { };
    public event EventHandler<MaxLateralMoveDistanceRestrictedChangedEventArgs> OnMaxLateralMoveDistanceRestrictedChanged = (sender, e) => { };
    public event EventHandler<MaxLateralMoveDistanceChangedEventArgs> OnMaxLateralMoveDistanceChanged = (sender, e) => { };
    public event EventHandler<MinLateralMoveDistanceRestrictedChangedEventArgs> OnMinLateralMoveDistanceRestrictedChanged = (sender, e) => { };
    public event EventHandler<MinLateralMoveDistanceChangedEventArgs> OnMinLateralMoveDistanceChanged = (sender, e) => { };

    // Dispatch EventHandler for radial movement/Rotation changes
    public event EventHandler<PitchRotationSpeedChangedEventArgs> OnPitchRotationSpeedChanged = (sender, e) => { };
    public event EventHandler<PitchRotationAngleRestrictedChangedEventArgs> OnPitchRotationAngleRestrictedChanged = (sender, e) => { };
    public event EventHandler<StartingPitchRotationAngleChangedEventArgs> OnStartingPitchRotationAngleChanged = (sender, e) => { };
    public event EventHandler<MaxPitchNegRotationAngleChangedEventArgs> OnMaxPitchNegRotationAngleChanged = (sender, e) => { };
    public event EventHandler<MaxPitchPosRotationAngleChangedEventArgs> OnMaxPitchPosRotationAngleChanged = (sender, e) => { };

    public event EventHandler<YawRotationSpeedChangedEventArgs> OnYawRotationSpeedChanged = (sender, e) => { };
    public event EventHandler<YawRotationAngleRestrictedChangedEventArgs> OnYawRotationAngleRestrictedChanged = (sender, e) => { };
    public event EventHandler<StartingYawRotationAngleChangedEventArgs> OnStartingYawRotationAngleChanged = (sender, e) => { };
    public event EventHandler<MaxYawNegRotationAngleChangedEventArgs> OnMaxYawNegRotationAngleChanged = (sender, e) => { };
    public event EventHandler<MaxYawPosRotationAngleChangedEventArgs> OnMaxYawPosRotationAngleChanged = (sender, e) => { };

    public event EventHandler<RollRotationSpeedChangedEventArgs> OnRollRotationSpeedChanged = (sender, e) => { };
    public event EventHandler<RollRotationAngleRestrictedChangedEventArgs> OnRollRotationAngleRestrictedChanged = (sender, e) => { };
    public event EventHandler<StartingRollRotationAngleChangedEventArgs> OnStartingRollRotationAngleChanged = (sender, e) => { };
    public event EventHandler<MaxRollNegRotationAngleChangedEventArgs> OnMaxRollNegRotationAngleChanged = (sender, e) => { };
    public event EventHandler<MaxRollPosRotationAngleChangedEventArgs> OnMaxRollPosRotationAngleChanged = (sender, e) => { };

    // Dispatch EventHandler for radial movement around/Rotation around the subject changes
    public event EventHandler<RadiusChangeSpeedChangedEventArgs> OnRadiusChangeSpeedChanged = (sender, e) => { };
    public event EventHandler<RadiusRestrictedChangedEventArgs> OnRadiusRestrictedChanged = (sender, e) => { };
    public event EventHandler<MaxRadiusExpansionChangedEventArgs> OnMaxRadiusExpansionChanged = (sender, e) => { };
    public event EventHandler<MinRadiusFromCenterChangedEventArgs> OnMinRadiusFromCenterChanged = (sender, e) => { }; 
    
    // Dispatch the deticated "changed" event
    public event EventHandler<PolarAngleCoordinatesChangeSpeedChangedEventArgs> OnPolarAngleCoordinatesChangeSpeedChanged = (sender, e) => { };
    public event EventHandler<PolarRotationAroundAngleRestrictedChangedEventArgs> OnPolarRotationAroundAngleRestrictedChanged = (sender, e) => { };
    public event EventHandler<StartingPolarRotationAroundAngleChangedEventArgs> OnStartingPolarRotationAroundAngleChanged = (sender, e) => { };
    public event EventHandler<MaxPolarNegRotationAroundAngleChangedEventArgs> OnMaxPolarNegRotationAroundAngleChanged = (sender, e) => { };
    public event EventHandler<MaxPolarPosRotationAroundAngleChangedEventArgs> OnMaxPolarPosRotationAroundAngleChanged = (sender, e) => { };

    public event EventHandler<AzimuthalAngleCoordinatesChangeSpeedChangedEventArgs> OnAzimuthalAngleCoordinatesChangeSpeedChanged = (sender, e) => { };
    public event EventHandler<AzimuthalRotationAroundAngleRestrictedChangedEventArgs> OnAzimuthalRotationAroundAngleRestrictedChanged = (sender, e) => { };
    public event EventHandler<StartingAzimuthalRotationAroundAngleChangedEventArgs> OnStartingAzimuthalRotationAroundAngleChanged = (sender, e) => { };
    public event EventHandler<MaxAzimuthalNegRotationAroundAngleChangedEventArgs> OnMaxAzimuthalNegRotationAroundAngleChanged = (sender, e) => { };
    public event EventHandler<MaxAzimuthalPosRotationAroundAngleChangedEventArgs> OnMaxAzimuthalPosRotationAroundAngleChanged = (sender, e) => { };

    // Dispatch EventHandler for interaction and control targets changes 
    public event EventHandler<SubjectChangedEventArgs> OnSubjectChanged = (sender, e) => { };
    public event EventHandler<DedicatedObjectChangedEventArgs> OnDedicatedObjectChanged = (sender, e) => { };
    public event EventHandler<TargetChangedEventArgs> OnTargetChanged = (sender, e) => { };

}
