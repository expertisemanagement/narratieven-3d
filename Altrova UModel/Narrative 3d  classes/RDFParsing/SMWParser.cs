﻿using Object.Type;
using SMW.Alinea.Imagary;
using SMW.Alinea.Language;
using SMW.Relation;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;
using VDS.RDF;
using VDS.RDF.Parsing;
using VDS.RDF.Query;
using System;
using SMW.Relation.Activity;

public sealed class SMWParser
{
    // A instance of the SMWParser as part of the Singleton pattern
    private static SMWParser instance = null;

    // A Multitread singleton duplication prevention object. 
    // This object is locked the first time the Instance() is called, Instance() does nothing when the object is locked.
    private static readonly object padlock = new object();

    // a Graph Parser implementing the IRdfReader interface
    public RdfXmlParser rxpparser = new RdfXmlParser();

    // Dictionary containing main semantic ObjectTypes and there URI ending part ("category" variable from the rdf + this URI ending = the objectTypeUri)
    private Dictionary<ObjectTypeEnum, string> semanticObjectTypes = new Dictionary<ObjectTypeEnum, string>();

    // List of loaded Graphs
    private List<IGraph> loadedGraphs = new List<IGraph>();

    // All Categories
    private Dictionary<IGraph,
        Dictionary<INode, SMWCategory>> allCategories = new Dictionary<IGraph, Dictionary<INode, SMWCategory>>();

    // All base categories sorted by ObjectType per Graph
    private Dictionary<IGraph,
        Dictionary<ObjectTypeEnum,
            SMWCategory>> objectTypeBaseCategories = new Dictionary<IGraph, Dictionary<ObjectTypeEnum, SMWCategory>>();

    //------------------- Singleton  -------------------//

    // Constructor
    private SMWParser()
    {
        DefineSemanticObjectTypes();
    }

    // Returns this object's instance variable value, also creates a new ObjectFactory instance if the padlock is not locked. (Mutlitread security)
    public static SMWParser Instance
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new SMWParser();
                }
                return instance;
            }
        }
    }

    //------------------- Types definining (Sort of ORM information) -------------------//

    // Fill the semanticObjectTypes with missing possible base objectTypes 
    private void DefineSemanticObjectTypes()
    {
        // Define all relevant semantic objectTypes
        if (semanticObjectTypes.ContainsKey(ObjectTypeEnum.ACTIVITY) == false)
            semanticObjectTypes.Add(ObjectTypeEnum.ACTIVITY, "Activity");

        if (semanticObjectTypes.ContainsKey(ObjectTypeEnum.ACTOR) == false)
            semanticObjectTypes.Add(ObjectTypeEnum.ACTOR, "Actor");

        if (semanticObjectTypes.ContainsKey(ObjectTypeEnum.CONTEXT) == false)
            semanticObjectTypes.Add(ObjectTypeEnum.CONTEXT, "Context");

        if (semanticObjectTypes.ContainsKey(ObjectTypeEnum.PRACTICE) == false)
            semanticObjectTypes.Add(ObjectTypeEnum.PRACTICE, "Practice");

    }

    // Returns the ObjectModel Type for specific ObjectTypes
    public Type GetObjectModelTypePerObjectTypes(ObjectTypeEnum objectType)
    {
        switch (objectType)
        {
            case ObjectTypeEnum.OBJECT:
                return typeof(ObjectModel);
            case ObjectTypeEnum.COMPLEXOBJECT:
                return typeof(ComplexObjectModel);
            case ObjectTypeEnum.NAVOBJECT:
                return typeof(NavObjectModel);
            case ObjectTypeEnum.CAMERA:
                return typeof(CameraModel);
            case ObjectTypeEnum.SMWOBJECT:
                return typeof(SMWObjectModel);
            case ObjectTypeEnum.CONTEXT:
                return typeof(ContextModel);
            case ObjectTypeEnum.INTENTIONALELEMENT:
                return typeof(IntentionalElementModel);
            case ObjectTypeEnum.PRACTICE:
                return typeof(PracticeModel);
            case ObjectTypeEnum.ACTOR:
                return typeof(ActorModel);
            case ObjectTypeEnum.ACTIVITY:
                return typeof(ActivityModel);
            case ObjectTypeEnum.WORLDDECORATION:
                return null;
            case ObjectTypeEnum.UNASSIGNED:
                return null;
            default:
                return null;
        }
    }

    // Returns the ObjectView Type for specific ObjectTypes
    public Type GetObjectViewTypePerObjectTypes(ObjectTypeEnum objectType)
    {
        switch (objectType)
        {
            case ObjectTypeEnum.OBJECT:
                return typeof(ObjectView);
            case ObjectTypeEnum.COMPLEXOBJECT:
                return typeof(ComplexObjectView);
            case ObjectTypeEnum.NAVOBJECT:
                return typeof(NavObjectView);
            case ObjectTypeEnum.CAMERA:
                return typeof(CameraView);
            case ObjectTypeEnum.SMWOBJECT:
                return typeof(SMWObjectView);
            case ObjectTypeEnum.CONTEXT:
                return typeof(ContextView);
            case ObjectTypeEnum.INTENTIONALELEMENT:
                return typeof(IntentionalElementView);
            case ObjectTypeEnum.PRACTICE:
                return typeof(PracticeView);
            case ObjectTypeEnum.ACTOR:
                return typeof(ActorView);
            case ObjectTypeEnum.ACTIVITY:
                return typeof(ActivityView);
            case ObjectTypeEnum.WORLDDECORATION:
                return null;
            case ObjectTypeEnum.UNASSIGNED:
                return null;
            default:
                return null;
        }
    }

    // Returns the ObjectController Type for specific ObjectTypes
    private Type GetObjectControllerTypePerObjectTypes(ObjectTypeEnum objectType)
    {
        switch (objectType)
        {
            case ObjectTypeEnum.OBJECT:
                return typeof(ObjectController);
            case ObjectTypeEnum.COMPLEXOBJECT:
                return typeof(ComplexObjectController);
            case ObjectTypeEnum.NAVOBJECT:
                return typeof(NavObjectController);
            case ObjectTypeEnum.CAMERA:
                return typeof(CameraController);
            case ObjectTypeEnum.SMWOBJECT:
                return typeof(SMWObjectController);
            case ObjectTypeEnum.CONTEXT:
                return typeof(ContextController);
            case ObjectTypeEnum.INTENTIONALELEMENT:
                return typeof(IntentionalElementController);
            case ObjectTypeEnum.PRACTICE:
                return typeof(PracticeController);
            case ObjectTypeEnum.ACTOR:
                return typeof(ActorController);
            case ObjectTypeEnum.ACTIVITY:
                return typeof(ActivityController);
            case ObjectTypeEnum.WORLDDECORATION:
                return null;
            case ObjectTypeEnum.UNASSIGNED:
                return null;
            default:
                return null;
        }
    }

    //------------------- Graph loading -------------------//

    // Load the graph for the RDF file
    public IGraph LoadInExistingGraph(IGraph wikiDataGraph, string pathToRDF)
    {
        // Makes sure that the XML file is UTF8
        StreamReader sr = new StreamReader(pathToRDF, true);

        // If Graph is already loaded, than stop this methode
        if (loadedGraphs.Contains(wikiDataGraph) == false)
            loadedGraphs.Add(wikiDataGraph);

        //rxpparser.Load(wikiDataGraph, pathToRDF);
        rxpparser.Load(wikiDataGraph, sr);

        // Define possible objectTypes
        DefineSemanticObjectTypes();

        // Initialise all categories
        InitialiseAllCategories(wikiDataGraph);

        return wikiDataGraph;
    }

    // Load the graph for the RDF file
    public IGraph LoadGraphFromRDFPath(string pathToRDF)
    {
        // Create a new graph for the wiki data
        IGraph wikiDataGraph = new Graph();

        LoadInExistingGraph(wikiDataGraph, pathToRDF);

        return wikiDataGraph;
    }

    //------------------- Initializing categories -------------------//

    // Gets all category semanticnodes and there SMWCategory object and set there 
    private void InitialiseAllCategories(IGraph g)
    {
        foreach (INode semanticNode in GetAllCategoryNodes(g))
        {
            //Debug.Log(semanticNode.ToString());

            if (allCategories.ContainsKey(g) == false)
                allCategories.Add(g, new Dictionary<INode, SMWCategory>());

            if (allCategories[g].ContainsKey(semanticNode) == false)
                allCategories[g].Add(semanticNode, null);

            if (allCategories[g][semanticNode] == null)
                allCategories[g][semanticNode] = new SMWCategory(g.GetTriplesWithSubject(semanticNode).ToList());

            foreach (ObjectTypeEnum objectType in semanticObjectTypes.Keys)
            {
                // Make sure this baseCategory cache is initialised for this IGraph
                if (objectTypeBaseCategories.ContainsKey(g) == false)
                    objectTypeBaseCategories.Add(g, new Dictionary<ObjectTypeEnum, SMWCategory>());

                // Make sure the baseCategory per ObjectType is initialised in the cache (for this IGraph)
                if (objectTypeBaseCategories[g].ContainsKey(objectType) == false)
                    objectTypeBaseCategories[g].Add(objectType, null);

                if (objectTypeBaseCategories[g][objectType] == null)
                {
                    // Check if this node is a ObjectType baseCategory
                    if (semanticNode.ToString() == GetObjectTypeSemanticURI(g, objectType))
                    {
                        if (allCategories[g][semanticNode].objectType != objectType)
                            allCategories[g][semanticNode].objectType = objectType;

                        objectTypeBaseCategories[g][objectType] = allCategories[g][semanticNode];
                    }
                }
            }
        }

        // Get the baseCategory foreach ObjectType and loop through it's subcategory. Every subcategory will be checked for a corresponding ObjectType
        foreach (ObjectTypeEnum existingObjectType in objectTypeBaseCategories[g].Keys)
        {
            // If there is a category for this ObjectType
            if (objectTypeBaseCategories[g][existingObjectType] != null)
            {
                // List of categories that inherited this class (this will be returned)
                List<SMWCategory> foundSubClasses = new List<SMWCategory>();

                // Current foundSubClasses of intressed (starts with this SMWCategory)
                List<SMWCategory> unCheckedSubClasses = new List<SMWCategory>();
                unCheckedSubClasses.Add(objectTypeBaseCategories[g][existingObjectType]);

                // currentSubClass to loop through
                SMWCategory currentSubClass = unCheckedSubClasses[0];

                // While there is currentSubClass look for instances
                while (currentSubClass != null) //unCheckedSubClasses != null || unCheckedSubClasses.Count >= 1)
                {
                    // Add to return list (Means it is a subClass and it has been check for foundSubClasses)
                    if (currentSubClass != objectTypeBaseCategories[g][existingObjectType])
                        foundSubClasses.Add(currentSubClass);

                    // Check if the currentSubClass has foundSubClasses
                    List<SMWCategory> subClassFoundInCurrentSubClasses = currentSubClass.superClassOf;

                    // Remove this subClass from the subClass look up index (Because it found the foundSubClasses)
                    unCheckedSubClasses.Remove(currentSubClass);

                    // Add the foundfoundSubClasses to the look up index (these are next to check foor foundSubClasses)
                    if (subClassFoundInCurrentSubClasses != null)
                        if(subClassFoundInCurrentSubClasses.Count >= 1)
                            foreach(SMWCategory c in subClassFoundInCurrentSubClasses)
                                if(c != null)
                                    unCheckedSubClasses.Add(c);

                    // Set objectType of the currentSubClass to existingObjectType, unless it has no superclass with an other objectType
                    List<ObjectTypeEnum> excludedSemanticObjectTypes = new List<ObjectTypeEnum>();
                    excludedSemanticObjectTypes.Add(existingObjectType);

                    if (CheckIfObjectTypeIsSemanticObjectType(currentSubClass.objectType, excludedSemanticObjectTypes) == true)
                    {
                        if (currentSubClass.subClassOf.FindAll(c => CheckIfObjectTypeIsSemanticObjectType(c.objectType, excludedSemanticObjectTypes) == true).Count >= 1)
                        {
                            currentSubClass.objectType = existingObjectType;
                        }
                    }
                    else
                    {
                        currentSubClass.objectType = existingObjectType;
                    }

                    // If there are still subclasses than set the first in the list as next currentSubClass to check, else set null
                    if (unCheckedSubClasses.Count >= 1)
                        currentSubClass = unCheckedSubClasses[0];
                    else
                        currentSubClass = null;
                }
            }
        }
    }

    //------------------- Getting ObjectType Categories (a.k.a ontology CLASSES) -------------------//

    // Search for all inheritence of a basecategoryDependency
    public List<INode> GetObjectTypeCategories(IGraph g, string baseCategoryURI, int categoryInheritenceDepth)
    {
        // Use the SparqlQueryParser to give us a SparqlQuery object
        // Should get a Graph back from a CONSTRUCT query
        SparqlQueryParser sparqlparser = new SparqlQueryParser();

        // construct statement settings for the SPARQL-query
        string constructSearchSettings = "?s rdfs:subClassOf <" + baseCategoryURI + "> .";

        // WHERE statement settings for the SPARQL-query. If where is IntentionalElement, then use a none recursive where (to prevent double Activities and Actors)!
        string whereSearchSettings = "?s (rdfs:subClassOf)* <" + baseCategoryURI + "> ."; //"{ ?s (rdfs:subClassOf)* <" + baseCategoryURI + "> } UNION { ?s owl:Class <" + baseCategoryURI + "> }";
        //if (baseCategoryURI == GetObjectTypeSemanticURI(g, ObjectTypeEnum.INTENTIONALELEMENT)) constructSearchSettings = "?s (rdfs:subClassOf)+ <" + baseCategoryURI + "> .";

        // LIMIT statement settings for the SPARQL-query
        string limitSearchSettings;
        if (categoryInheritenceDepth <= 0)
            limitSearchSettings = "";
        else
            limitSearchSettings = "LIMIT " + categoryInheritenceDepth.ToString();

        // Query for each subject the is a type of a class that is a subclass of a context class
        SparqlQuery query = sparqlparser.ParseFromString(
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
            "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
            "PREFIX owl: <http://www.w3.org/2002/07/owl#> " +
            "CONSTRUCT { " + constructSearchSettings + " }" +
            "WHERE { " + whereSearchSettings + " }" +
            limitSearchSettings);

        // Get query results in a IGraph format
        IGraph results = (IGraph)g.ExecuteQuery(query);

        // Convert List of triples to list of nodes
        List<INode> listObjectTypes = new List<INode>();

        foreach (Triple triple in results.Triples.ToList())
        {
            listObjectTypes.Add(triple.Subject);
        }

        return listObjectTypes;
    }

    // Returns all category semanticnodes and there SMWCategory object
    public Dictionary<INode, SMWCategory> GetAllCategories(IGraph g)
    {
        if (allCategories.ContainsKey(g))
            return allCategories[g];
        else
            return null;
    }

    // Returns all category semanticnodes
    public List<INode> GetAllCategoryNodes(IGraph g)
    {
        if (allCategories.ContainsKey(g) == false)
        {
            allCategories.Add(g, new Dictionary<INode, SMWCategory>());

            List<Triple> foundCategoryTriples = g.GetTriplesWithPredicateObject(g.CreateUriNode(UriFactory.Create(g.NamespaceMap.GetNamespaceUri("rdf") + "type")), g.CreateUriNode(UriFactory.Create(g.NamespaceMap.GetNamespaceUri("owl") + "Class"))).ToList();

            foreach (Triple triple in foundCategoryTriples)
            {
                if (allCategories[g].ContainsKey(triple.Subject) == false)
                    allCategories[g].Add(triple.Subject, null);
            }

            return allCategories[g].Keys.ToList();
        }
        else
        {
            return GetAllCategories(g).Keys.ToList();
        }
    }

    // Returns all category SMWCategories
    public List<SMWCategory> GetAllCategorySMWCategory(IGraph g)
    {
        return GetAllCategories(g).Values.ToList();
    }

    // Returns the base class for the specific ObjectType per graph
    public SMWCategory GetObjectTypeBaseCategory(IGraph g, ObjectTypeEnum objectType)
    {
        if (objectTypeBaseCategories.ContainsKey(g))
            if (objectTypeBaseCategories[g].ContainsKey(objectType))
                return objectTypeBaseCategories[g][objectType];

        return null;
    }

    // Return Graph base seamntic ObjectTypes (categories) 
    public Dictionary<ObjectTypeEnum, SMWCategory> GetObjectTypesBaseCategory(IGraph g)
    {
        if (objectTypeBaseCategories != null && objectTypeBaseCategories.ContainsKey(g) == true)
            if (objectTypeBaseCategories[g] != null)
                return objectTypeBaseCategories[g];

        return null;
    }

    // Return Graph base seamntic ObjectTypes (categories) 
    public List<SMWCategory> GetObjectTypeAllCategories(IGraph g, ObjectTypeEnum objectType)
    {
        // Result list
        List<SMWCategory> resultAllObjectTypeCategories = new List<SMWCategory>();

        if (objectTypeBaseCategories != null && objectTypeBaseCategories.ContainsKey(g) == true)
        {
            // Add the base category to the list
            resultAllObjectTypeCategories.Add(objectTypeBaseCategories[g][objectType]);

            if (objectTypeBaseCategories[g] != null && objectTypeBaseCategories[g].ContainsKey(objectType) == true)
            {
                // Add the child categories of the base category to the list
                resultAllObjectTypeCategories.AddRange(objectTypeBaseCategories[g][objectType].ChildCategories);
            }
        }

        return resultAllObjectTypeCategories;
    }

    // Return Graph base seamntic ObjectTypes (categories) 
    public Dictionary<ObjectTypeEnum, List<SMWCategory>> GetObjectTypeAllCategories(IGraph g)
    {
        if (objectTypeBaseCategories != null && objectTypeBaseCategories.ContainsKey(g) == true)
            if (objectTypeBaseCategories[g] != null)
            {
                // Store found results
                Dictionary<ObjectTypeEnum, List<SMWCategory>> allCategoriesPerObjectType = new Dictionary<ObjectTypeEnum, List<SMWCategory>>();

                foreach (ObjectTypeEnum objectType in objectTypeBaseCategories[g].Keys)
                {
                    allCategoriesPerObjectType.Add(objectType, GetObjectTypeAllCategories(g, objectType));
                }
            }

        return null;
    }

    // Retrurns the category with the same name
    public SMWCategory GetCategoryByName(IGraph g, string name)
    {
        // Return the category with the same name as the paramanter string "name"
        return GetAllCategorySMWCategory(g).Find(c => c.name == name);
    }

    // Returns the baseParent category of a category
    public List<INode> GetBaseParentCategories(INode categoryNode)
    {
        // Use the SparqlQueryParser to give us a SparqlQuery object
        // Should get a Graph back from a CONSTRUCT query
        SparqlQueryParser sparqlparser = new SparqlQueryParser();

        Debug.Log(
            "PREFIX rdf: <" + categoryNode.Graph.NamespaceMap.GetNamespaceUri("rdf") + "> " +
            "PREFIX rdfs: <" + categoryNode.Graph.NamespaceMap.GetNamespaceUri("rdfs") + "> " +
            "PREFIX owl: <" + categoryNode.Graph.NamespaceMap.GetNamespaceUri("owl") + "> " +
            "CONSTRUCT { ?s (rdfs:subClassOf)* " + categoryNode.ToString() + " }" +
            "WHERE { ?s (rdfs:subClassOf)* " + categoryNode.ToString() + ". }");

        // Query for each subject the is a type of a class that is a subclass of a context class
        SparqlQuery query = sparqlparser.ParseFromString(
            "PREFIX rdf: <" + categoryNode.Graph.NamespaceMap.GetNamespaceUri("rdf") + "> " +
            "PREFIX rdfs: <" + categoryNode.Graph.NamespaceMap.GetNamespaceUri("rdfs") + "> " +
            "PREFIX owl: <" + categoryNode.Graph.NamespaceMap.GetNamespaceUri("owl") + "> " +
            "CONSTRUCT { ?s (rdfs:subClassOf)* " + categoryNode.ToString() + " }" +
            "WHERE { ?s (rdfs:subClassOf)* " + categoryNode.ToString() + ". }");

        // Get query results in a IGraph format
        IGraph results = (IGraph)categoryNode.Graph.ExecuteQuery(query);

        // Convert List of triples to list of nodes
        List<INode> listObjectTypes = new List<INode>();

        foreach (Triple triple in results.Triples.ToList())
        {
            Debug.Log(triple);
            listObjectTypes.Add(triple.Subject);
        }

        return listObjectTypes;
    }

    // Returns the baseParent category of a category
    public List<SMWCategory> GetBaseParentCategories(SMWCategory category)
    {
        // Result list
        List<SMWCategory> baseParentCategory = new List<SMWCategory>();

        foreach (INode node in GetBaseParentCategories(category.semanticNode))
        {
            baseParentCategory.Add(GetCategoryBySemanticNode(node));
        }

        return baseParentCategory;
    }

    // Returns a category by it's semanticNode
    public SMWCategory GetCategoryBySemanticNode(INode semanticCategoryNode)
    {
        if (GetAllCategories(semanticCategoryNode.Graph).ContainsKey(semanticCategoryNode) == true)
            return GetAllCategories(semanticCategoryNode.Graph)[semanticCategoryNode];
        else
            return null;
    }

    // Get Category by it's semanticNode
    public SMWCategory GetCategoryBySemanticNode(INode semanticCategoryNode, ObjectTypeEnum limitSearchForOnlyObjectType)
    {
        return GetObjectTypeAllCategories(semanticCategoryNode.Graph, limitSearchForOnlyObjectType).Find(c => c.semanticNode == semanticCategoryNode);
    }

    // Get Category by it's semanticNode
    public SMWCategory GetCategoryBySemanticNode(INode semanticCategoryNode, List<ObjectTypeEnum> limitSearchForOnlyObjectTypes)
    {
        foreach (ObjectTypeEnum objectType in limitSearchForOnlyObjectTypes)
        {
            SMWCategory foundCategory = GetCategoryBySemanticNode(semanticCategoryNode, objectType);
            if (foundCategory != null)
                return foundCategory;
        }

        return null;

    }
    
    // Returns a category holding semanticNode the parameter semanticNode
    public SMWCategory GetCategoryOfAInstanceByInstanceNode(INode semanticInstanceNode)
    {
        List<SMWCategory> allRelevantCategories = GetAllCategories(semanticInstanceNode.Graph).Values.ToList();

        if (allRelevantCategories != null)
            foreach (SMWCategory c in allRelevantCategories)
            {
                INode foundNode = c.GetSpecificInstanceNode(semanticInstanceNode.ToString());
                if (foundNode == semanticInstanceNode)
                    return c;
            }

        return null;
    }

    // Get Category holding semanticNode
    public SMWCategory GetCategoryOfAInstanceByInstanceNode(INode semanticInstanceNode, ObjectTypeEnum limitSearchForOnlyObjectType)
    {
        List<SMWCategory> allRelevantCategories = GetObjectTypeAllCategories(semanticInstanceNode.Graph, limitSearchForOnlyObjectType);
        if (allRelevantCategories != null)
            foreach (SMWCategory c in allRelevantCategories)
            {
                INode foundNode = c.GetSpecificInstanceNode(semanticInstanceNode.ToString());
                if (foundNode == semanticInstanceNode)
                    return c;
            }
        return null;
    }

    // Get Category holding semanticNode
    public SMWCategory GetCategoryOfAInstanceByInstanceNode(INode semanticInstanceNode, List<ObjectTypeEnum> limitSearchForOnlyObjectTypes)
    {
        foreach (ObjectTypeEnum objectType in limitSearchForOnlyObjectTypes)
        {
            SMWCategory foundCategory = GetCategoryOfAInstanceByInstanceNode(semanticInstanceNode, objectType);
            if (foundCategory != null)
                return foundCategory;
        }

        return null;

    }

    //------------------- Getting ObjectType Instances by Categories (a.k.a ontology OBJECTS) -------------------//

    // [Alternative vesion of GetAllInstancesByObjectType()] Returns the lowest level isntances (no superclasses) of a category
    public List<SMWObjectModel> GetBaseInstancesOfObjectTypes(IGraph g, ObjectTypeEnum objectType)
    {
        // List of found instances
        List<SMWObjectModel> returnList = new List<SMWObjectModel>();

        if (objectTypeBaseCategories.ContainsKey(g) == true)
            if (objectTypeBaseCategories[g].ContainsKey(objectType) == true)
                foreach (SMWCategory category in objectTypeBaseCategories[g][objectType].smwBaseClasses)
                {
                    foreach (SMWCategory c in objectTypeBaseCategories[g][objectType].smwBaseClasses)
                        Debug.Log(c.name);

                    // Found instances
                    List<SMWObjectModel> foundInstances = category.GetAllnstanceModelsFromThisCategory().Values.ToList();

                    // Add to return list
                    if (foundInstances != null)
                        returnList.AddRange(foundInstances);
                }

        return returnList;
    }

    // Returns all instances of the category with the categoryName (+ all instances from every subCategory) 
    public List<SMWObjectModel> GetAllInstancesBySpecificCategoryName(IGraph g, string categoryName, bool includeInstancesFromAllSubCategories)
    {
        // Get the category by the categoryName
        SMWCategory smwCategory = GetCategoryByName(g, categoryName);

        // If no smwCategory found, stop this methode
        if (smwCategory == null)
            return new List<SMWObjectModel>();

        // List of all instances from the smwCategory and it's subcategories
        List<SMWObjectModel> smwInstances = new List<SMWObjectModel>();

        if (includeInstancesFromAllSubCategories == true)
            foreach (Dictionary<INode, SMWObjectModel> dic in smwCategory.GetAllnstanceModelsFromThisCategoryAndSubCategories().Values.ToList())
                smwInstances.AddRange(dic.Values);
        else
            smwInstances.AddRange(smwCategory.GetAllnstanceModelsFromThisCategory().Values.ToList());

        // Return result
        return smwInstances;
    }

    // Returns all instances of the category with the categoryName of a ObjectType (+ all instances from every subCategory) 
    public List<SMWObjectModel> GetAllInstancesByObjectType(IGraph g, ObjectTypeEnum objectType, bool includeInstancesFromAllSubCategories)
    {
        // If there are no categories for this objectType than stop the methode and return
        if (this.semanticObjectTypes.ContainsKey(objectType) == false)
                return null;

        return GetAllInstancesBySpecificCategoryName(g, this.semanticObjectTypes[objectType], includeInstancesFromAllSubCategories);

    }


    //------------------- Getting SMWObject Graph relations (parents, childs and other refferences to SMWObjects) -------------------//

    // Returns the ContextModel representing the super context of of the the given Context (ContextModel) 
    public ContextModel GetSuperContext(ContextModel context)
    {
        // Return ContextModel
        ContextModel superContext = (ContextModel) ObjectFactory.Instance.GetSemanticObjectModelByNode(context.SuperContextNode);

        // If there is a superContextNode but no model, then create a model 
        if (context.SuperContextNode != null && superContext == null)
            ObjectFactory.Instance.modelFactory.createForContext(context.SuperContextNode, GetCategoryOfAInstanceByInstanceNode(context.SuperContextNode, ObjectTypeEnum.CONTEXT), null);

        // Return the found/Created Context
        return superContext;
    }

    // Returns all ContextModels representing the sub Contexts of the given Context (ContextModel)
    public ContextModel[] GetSubContexts(ContextModel context)
    {
        // Return ContextModel
        List<ContextModel> subContexts = new List<ContextModel>();

        // Relevant Graph 
        IGraph graph = context.SemanticNode.Graph;

        // Look for subContexts existing ContextModel inside the objectFactory, else create one
        INode[] subContextNodes = context.SubContextNodes;

        // Get all "Context" category instances
        List<ContextModel> allContextCategories = GetObjectTypeBaseCategory(graph, ObjectTypeEnum.CONTEXT).GetAllnstanceModelsFromThisCategoryAndSubCategoriesUnOrdered().Cast<ContextModel>().ToList();

        foreach (INode subContextNode in subContextNodes)
        {
            // Return ContextModel, found by querying all the Contexts
            ContextModel subContext = allContextCategories.Find(c => c.SemanticNode == subContextNode);

            // If there is a subContextNode but no model, then create a model 
            if (subContextNode != null && subContext != null)
                ObjectFactory.Instance.modelFactory.createForContext(subContextNode, GetCategoryOfAInstanceByInstanceNode(subContextNode, ObjectTypeEnum.CONTEXT), null);

            subContexts.Add(subContext);
        }

        // Return found Contexts
        return subContexts.ToArray();
    }
    
    // Return all Intetional Elements that are part of the given intentional Elements (IntentionalElementModel) 
    public IntentionalElementModel[] GetIntentionalElementPartOf(IntentionalElementModel intentionalElement)
    {
        // Relevant Graph 
        IGraph graph = intentionalElement.SemanticNode.Graph;

        // Look for Intentional ELements that are part of this existing Intentional Element inside the objectFactory, else create one
        INode[] relevantNodes = intentionalElement.PartOfNodes;

        // Return the models corresponding to the relevantNodes, searched within the Intentional Element baseCategory
        return GetObjectTypeBaseCategory(graph, ObjectTypeEnum.INTENTIONALELEMENT).GetSpecificInstancesModels(relevantNodes, true).Values.Cast<IntentionalElementModel>().ToArray();
    }

    // Return all Intetional Elements which the given intentional Element (IntentionalElementModel) is partOf 
    public IntentionalElementModel[] GetIntentionalElementPartOfThis(IntentionalElementModel intentionalElement)
    {
        // Relevant Graph 
        IGraph graph = intentionalElement.SemanticNode.Graph;

        // Look for Intentional ELements that are part of this existing Intentional Element inside the objectFactory, else create one
        INode[] relevantNodes = intentionalElement.PartOfThisNodes;

        // Return the models corresponding to the relevantNodes, searched within the Intentional Element baseCategory
        return GetObjectTypeBaseCategory(graph, ObjectTypeEnum.INTENTIONALELEMENT).GetSpecificInstancesModels(relevantNodes, true).Values.Cast<IntentionalElementModel>().ToArray();
    }


    // Return all Intetional Elements that are part of the given intentional Elements (IntentionalElementModel) 
    public IntentionalElementModel[] GetIntentionalElementInstanceOf(IntentionalElementModel intentionalElement)
    {
        // Relevant Graph 
        IGraph graph = intentionalElement.SemanticNode.Graph;

        // Look for Intentional ELements that are part of this existing Intentional Element inside the objectFactory, else create one
        INode[] relevantNodes = intentionalElement.InstanceOfNodes;

        // Return the models corresponding to the relevantNodes, searched within the Intentional Element baseCategory
        return GetObjectTypeBaseCategory(graph, ObjectTypeEnum.INTENTIONALELEMENT).GetSpecificInstancesModels(relevantNodes, true).Values.Cast<IntentionalElementModel>().ToArray();
    }

    // Return all Intetional Elements which the given intentional Element (IntentionalElementModel) is partOf 
    public IntentionalElementModel[] GetIntentionalElementInstanceOfThis(IntentionalElementModel intentionalElement)
    {
        // Relevant Graph 
        IGraph graph = intentionalElement.SemanticNode.Graph;

        // Look for Intentional ELements that are part of this existing Intentional Element inside the objectFactory, else create one
        INode[] relevantNodes = intentionalElement.InstanceOfThisNodes;

        // Return the models corresponding to the relevantNodes, searched within the Intentional Element baseCategory
        return GetObjectTypeBaseCategory(graph, ObjectTypeEnum.INTENTIONALELEMENT).GetSpecificInstancesModels(relevantNodes, true).Values.Cast<IntentionalElementModel>().ToArray();
    }


    // Return all Intetional Elements that are part of the given intentional Elements (IntentionalElementModel) 
    public IntentionalElementModel[] GetIntentionalElementConcerns(IntentionalElementModel intentionalElement)
    {
        // Relevant Graph 
        IGraph graph = intentionalElement.SemanticNode.Graph;

        // Look for Intentional ELements that are part of this existing Intentional Element inside the objectFactory, else create one
        INode[] relevantNodes = intentionalElement.ConcernsNodes;

        // Return the models corresponding to the relevantNodes, searched within the Intentional Element baseCategory
        return GetObjectTypeBaseCategory(graph, ObjectTypeEnum.INTENTIONALELEMENT).GetSpecificInstancesModels(relevantNodes, true).Values.Cast<IntentionalElementModel>().ToArray();
    }

    // Return all Intetional Elements which the given intentional Element (IntentionalElementModel)  is partOf
    public IntentionalElementModel[] GetIntentionalElementConcernsThis(IntentionalElementModel intentionalElement)
    {
        // Relevant Graph 
        IGraph graph = intentionalElement.SemanticNode.Graph;

        // Look for Intentional ELements that are part of this existing Intentional Element inside the objectFactory, else create one
        INode[] relevantNodes = intentionalElement.ConcernsThisNodes;

        // Return the models corresponding to the relevantNodes, searched within the Intentional Element baseCategory
        return GetObjectTypeBaseCategory(graph, ObjectTypeEnum.INTENTIONALELEMENT).GetSpecificInstancesModels(relevantNodes, true).Values.Cast<IntentionalElementModel>().ToArray();
    }


    // Return all Intetional Elements that are consume the given activity (ActivityModel) 
    public IntentionalElementModel[] GetActivityConsumes(ActivityModel activity)
    {
        // Relevant Graph 
        IGraph graph = activity.SemanticNode.Graph;

        // Look for Intentional ELements that are part of this existing Intentional Element inside the objectFactory, else create one
        INode[] relevantNodes = activity.ConcernsNodes;

        // Return the models corresponding to the relevantNodes, searched within the Intentional Element baseCategory
        return GetObjectTypeBaseCategory(graph, ObjectTypeEnum.INTENTIONALELEMENT).GetSpecificInstancesModels(relevantNodes, true).Values.Cast<IntentionalElementModel>().ToArray();
    }

    // Return all Intetional Elements which the given activity (ActivityModel) is consumed by 
    public IntentionalElementModel[] GetActivityConsumesThis(ActivityModel activity)
    {
        // Relevant Graph 
        IGraph graph = activity.SemanticNode.Graph;

        // Look for Intentional ELements that are part of this existing Intentional Element inside the objectFactory, else create one
        INode[] relevantNodes = activity.ConcernsThisNodes;

        // Return the models corresponding to the relevantNodes, searched within the Intentional Element baseCategory
        return GetObjectTypeBaseCategory(graph, ObjectTypeEnum.INTENTIONALELEMENT).GetSpecificInstancesModels(relevantNodes, true).Values.Cast<IntentionalElementModel>().ToArray();
    }


    // Return all Intetional Elements that are consume the given activity (ActivityModel) 
    public IntentionalElementModel[] GetActivityProduces(ActivityModel activity)
    {
        // Relevant Graph 
        IGraph graph = activity.SemanticNode.Graph;

        // Look for Intentional ELements that are part of this existing Intentional Element inside the objectFactory, else create one
        INode[] relevantNodes = activity.ProducesNodes;

        // Return the models corresponding to the relevantNodes, searched within the Intentional Element baseCategory
        return GetObjectTypeBaseCategory(graph, ObjectTypeEnum.INTENTIONALELEMENT).GetSpecificInstancesModels(relevantNodes, true).Values.Cast<IntentionalElementModel>().ToArray();
    }

    // Return all Intetional Elements which the given activity (ActivityModel) is consumed by 
    public IntentionalElementModel[] GetActivityProducesThis(ActivityModel activity)
    {
        // Relevant Graph 
        IGraph graph = activity.SemanticNode.Graph;

        // Look for Intentional ELements that are part of this existing Intentional Element inside the objectFactory, else create one
        INode[] relevantNodes = activity.ProducesThisNodes;

        // Return the models corresponding to the relevantNodes, searched within the Intentional Element baseCategory
        return GetObjectTypeBaseCategory(graph, ObjectTypeEnum.INTENTIONALELEMENT).GetSpecificInstancesModels(relevantNodes, true).Values.Cast<IntentionalElementModel>().ToArray();
    }


    //------------------- Other -------------------//

    // Returns the loaded Graphs
    public List<IGraph> GetLoadedGraphs()
    {
        return loadedGraphs;
    }

    public List<ObjectTypeEnum> GetAllExistingObjectTypeFromGraph(IGraph g)
    {
        if (objectTypeBaseCategories.ContainsKey(g) == true)
            if (objectTypeBaseCategories[g] != null)
                return objectTypeBaseCategories[g].Keys.ToList();

        return null;
    }

    // Returns a merged version of the Graph categoryURi and the semanticObjectType, this equals the objectTypeURI for the Graph
    private string GetObjectTypeSemanticURI(IGraph g, ObjectTypeEnum objectType)
    {
        return GetGraphCategoryURI(g) + semanticObjectTypes[objectType];
    }

    // returns the value of the "wiki" variable 
    public string GetGraphWikiURI(IGraph g)
    {
        return g.NamespaceMap.GetNamespaceUri("wikiurl").ToString();
    }
    
    // returns the value of the "wiki" variable 
    public string GetGraphCategoryURI(IGraph g)
    {
        return g.NamespaceMap.GetNamespaceUri("category").ToString();
    }

    // returns the value of the "wiki" variable 
    public string GetGraphPropertyURI(IGraph g) 
    {
        return g.NamespaceMap.GetNamespaceUri("property").ToString();
    }

    // Returns the Graphs an objectURI belongs to
    public INode GetNodeByObjectURI(string nodeURI)
    {
        // All availableGraphs
        List<IGraph> graphs = GetLoadedGraphs();

        foreach (IGraph g in graphs)
        {
            if (g.GetTriplesWithSubject(UriFactory.Create(nodeURI)).Count() >= 1)
            {
                return g.GetTriplesWithSubject(UriFactory.Create(nodeURI)).ToArray()[0].Subject;
            }
        }

        return null;
    }

    // Returns true if the node has triples difine a "Role" as objectNode value and "Context_Type" predicateNode value
    public bool CheckIfContextNodeIsRole(INode contextNode)
    {
        if(contextNode.Graph.GetTriplesWithSubjectPredicate(contextNode, contextNode.Graph.CreateUriNode(UriFactory.Create(GetGraphPropertyURI(contextNode.Graph) + "Context_type")))
            .ToList().FindAll(t => RemoveUriPrefixFromNodeValueEnding(t.Object).ToUpper() == "ROLE").Count >= 1)
            return true;

        return false;
    }

    // Checks if an objectType is a know semanticObjectType
    private bool CheckIfObjectTypeIsSemanticObjectType(ObjectTypeEnum objectTypeToCheck, List<ObjectTypeEnum> objectTypesToExclude)
    {
        // Check if current has a knownObjectType else set it
        foreach (ObjectTypeEnum semanticObjectType in semanticObjectTypes.Keys)
            if (objectTypesToExclude.FindIndex(o => o == semanticObjectType) == -1)
                    if (semanticObjectType == objectTypeToCheck)
                        return true;

        return false;
    }

    // Returns a string without the uriprefix
    public string RemoveUriPrefixFromNodeValue(IGraph g, string NodeValue)
    {
        string newNodeValue = new String(NodeValue.ToCharArray()).ToString();

        foreach (string prefix in g.NamespaceMap.Prefixes)
        {
            if (newNodeValue.StartsWith(g.NamespaceMap.GetNamespaceUri(prefix).AbsoluteUri) == true)
            {
                String str = new String(newNodeValue.Replace(g.NamespaceMap.GetNamespaceUri(prefix).AbsoluteUri, "").ToCharArray());
                return str;
            }
        }

        return newNodeValue;
    }

    // Returns the node value without the prefix ending
    public string RemoveUriPrefixFromNodeValueEnding(INode semanticNode)
    {
        switch (semanticNode.NodeType)
        {
            case NodeType.Literal:
                return ((ILiteralNode)semanticNode).Value;
            case NodeType.Uri:
                return SMWParser.Instance.RemoveUriPrefixFromNodeValue(semanticNode.Graph, semanticNode.ToString());
            default:
                return semanticNode.ToString();
        }
    }

    // searchCache for non-ObjectType triples for a specific (ObjectType) SubjectNodes. 
    private Dictionary<INode, List<Triple>> nonObjectTypeSubObjectTriples = new Dictionary<INode, List<Triple>>();


    // Dictionary holding a (Aliena) nonObjectTypeNode as a key and it's triples as value
    private Dictionary<INode, List<Triple>> cacheNonObjectTypeAlineaTriples = new Dictionary<INode, List<Triple>>();
    // Dictionary holding a (Aliena) nonObjectTypeNode as a key and it's parsed Aliena as value
    private Dictionary<INode, Struct_SMW_Alinea> cacheNonObjectTypeAlinea = new Dictionary<INode, Struct_SMW_Alinea>();


    // Dictionary holding a (Imagary) nonObjectTypeNode as a key and it's triples as value
    private Dictionary<INode, List<Triple>> cacheNonObjectTypeImageryTriples = new Dictionary<INode, List<Triple>>();
    // Dictionary holding a (Imagary) nonObjectTypeNode as a key and it's parsed Imagary as value
    private Dictionary<INode, Struct_SMW_Imagary> cacheNonObjectTypeImagery = new Dictionary<INode, Struct_SMW_Imagary>();


    // Dictionary holding a (IntentionalElementContributes) nonObjectTypeNode as a key and it's triples as value
    private Dictionary<INode, List<Triple>> cacheNonObjectTypeIntentionalElementContributesTriples = new Dictionary<INode, List<Triple>>();
    // Dictionary holding a (IntentionalElementContributes) nonObjectTypeNode as a key and it's parsed IntentionalElementContributes as value
    public Dictionary<INode, Contributes> cacheNonObjectTypeIntentionalElementContributes = new Dictionary<INode, Contributes>();


    // Dictionary holding a (IntentionalElementDependency) nonObjectTypeNode as a key and it's triples as value
    private Dictionary<INode, List<Triple>> cacheNonObjectTypeIntentionalElementDependencyTriples = new Dictionary<INode, List<Triple>>();
    // Dictionary holding a (IntentionalElementDependency) nonObjectTypeNode as a key and it's parsed IntentionalElementDependency as value
    public Dictionary<INode, Dependency> cacheNonObjectTypeIntentionalElementDependency = new Dictionary<INode, Dependency>();


    // Dictionary holding a (IntentionalElementConnect) nonObjectTypeNode as a key and it's triples as value
    private Dictionary<INode, List<Triple>> cacheNonObjectTypeIntentionalElementConnectsTriples = new Dictionary<INode, List<Triple>>();
    // Dictionary holding a (IntentionalElementConnects) nonObjectTypeNode as a key and it's parsed IntentionalElementDependency as value
    public Dictionary<INode, Connects> cacheNonObjectTypeIntentionalElementConnects = new Dictionary<INode, Connects>();


    // Dictionary holding a (Practice) nonObjectTypeNode as a key and it's triples as value
    private Dictionary<INode, List<Triple>> cacheNonObjectTypePracticeSelectsTriples = new Dictionary<INode, List<Triple>>();
    // Dictionary holding a (Practice) nonObjectTypeNode as a key and it's parsed IntentionalElementDependency as value
    public Dictionary<INode, Selects> cacheNonObjectTypePracticeSelect = new Dictionary<INode, Selects>();

    // cache all nonObjectType triples for specific set of SubjectNodes
    public void CacheNonObjectTypeNodeTriples(List<INode> objectTypeInstances)
    {
        // Gather triples and then add the Node and triples to the cache 
        foreach (INode node in objectTypeInstances)
        {
            if (nonObjectTypeSubObjectTriples.ContainsKey(node) == false)
            {
                // Gather triples for specifc nonObjectType
                List<Triple> tempListOfTriples = new List<Triple>(
                    node.Graph.GetTriplesWithSubjectPredicate(
                        node.Graph.CreateUriNode(UriFactory.Create(node.ToString())),
                        node.Graph.CreateUriNode(UriFactory.Create( SMWParser.Instance.GetGraphPropertyURI(node.Graph) + "Has_subobject"))
                ));

                // Add to cache
                nonObjectTypeSubObjectTriples.Add(node, tempListOfTriples);
            }
        }
    }

    // Retrieve a set of alinea that belong to the given objectType(SubjectNode)
    public List<Struct_SMW_Alinea> GetAlineaForSMWObjectNode(INode SubjectNodeSMWObject)
    {
        // List of tips corresponding to the subjectNodeForOperationalPerspective
        List<Struct_SMW_Alinea> correspondingAlineas = new List<Struct_SMW_Alinea>();

        // Relevant Graph
        IGraph g = SubjectNodeSMWObject.Graph;

        // Check if triples are cached
        if (nonObjectTypeSubObjectTriples.ContainsKey(SubjectNodeSMWObject) == false)
        {
            // Cache all nonObjectTypeTriples for specific subjectNodeForOperationalPerspective
            List<INode> listOfSubjectNodeSMWObject = new List<INode>();
            listOfSubjectNodeSMWObject.Add(SubjectNodeSMWObject);
            CacheNonObjectTypeNodeTriples(listOfSubjectNodeSMWObject);
        }

        // Retrieve Triples operationalPerspective relevante nodes
        foreach (Triple triple in nonObjectTypeSubObjectTriples[SubjectNodeSMWObject])
        {
            // Chech ik cached
            if (cacheNonObjectTypeAlineaTriples.ContainsKey(triple.Object) == false)
            {
                // Gather triples for specifc nonObjectTypeNode
                List<Triple> tempListOfTriples = new List<Triple>(g.GetTriplesWithSubject(triple.Object));

                // Check if triples are from a tip, if so then add these to the cache
                if (tempListOfTriples.Find(x => x.Predicate.ToString() == GetGraphPropertyURI(g) + "Paragraph") != null)
                    cacheNonObjectTypeAlineaTriples.Add(triple.Object, tempListOfTriples);
            }
        }

        //Check there is there already exisits a parsed tip, else parsed the tip and cache it  
        foreach (INode node in cacheNonObjectTypeAlineaTriples.Keys)
        {
            if (cacheNonObjectTypeAlinea.ContainsKey(node) == false)
            {
                // Create new Alinea
                Struct_SMW_Alinea newAlinea = new Struct_SMW_Alinea();

                //foreach (Triple triple in cacheNonObjectTypeAlineaTriples[node])
                //{
                //    Debug.Log(triple.Object.ToString());
                //}
                
                // Set Alinea number
                Triple alineaNummer = cacheNonObjectTypeAlineaTriples[node].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Paragraph_number");
                //if (alineaNummer != null) 
                    //newAlinea.alineaNummer = Int32.Parse(alineaNummer.Object.ToString());

                // Set Alinea Language
                Triple alineaLang = cacheNonObjectTypeAlineaTriples[node].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Paragraph_language");
                
                AlineaLanguageSupportEnum convertedAlineaLang = AlineaLanguageSupportEnum.UNKOWN;

                if (alineaLang != null)
                {
                    switch (alineaLang.Object.ToString().ToUpper())
                    {
                        case "DUTCH":
                            convertedAlineaLang = AlineaLanguageSupportEnum.DUTCH;
                            break;
                        case "DUTCH-ENGLISH":
                            convertedAlineaLang = AlineaLanguageSupportEnum.DUTCH_ENGLISH;
                            break;
                        case "ENGLISH":
                            convertedAlineaLang = AlineaLanguageSupportEnum.ENGLISH;
                            break;
                        default:
                            convertedAlineaLang = AlineaLanguageSupportEnum.UNKOWN;
                            break;
                    }
                }

                newAlinea.alineaLang = convertedAlineaLang;

                // Set Alineahead
                Triple alineaHead = cacheNonObjectTypeAlineaTriples[node].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Paragraph_subheading");
                if(alineaHead != null)
                    newAlinea.alineaHead = alineaHead.Object.ToString();

                // Set AlineaCollapsible
                Triple alineaCollapsible = cacheNonObjectTypeAlineaTriples[node].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Paragraph_collapsible");

                bool convertedAlineaCollapsible = false;

                if (alineaCollapsible != null)
                {
                    switch (alineaCollapsible.Object.ToString().ToUpper())
                    {
                        case "false":
                            convertedAlineaCollapsible = false;
                            break;
                        case "0":
                            convertedAlineaCollapsible = false;
                            break;
                        case "TRUE":
                            convertedAlineaCollapsible = true;
                            break;
                        case "1":
                            convertedAlineaCollapsible = true;
                            break;
                        default:
                            convertedAlineaCollapsible = false;
                            break;
                    }
                }

                newAlinea.alineaCollapsible = convertedAlineaCollapsible;

                // Set AlineaContent
                Triple alineaContent = cacheNonObjectTypeAlineaTriples[node].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Paragraph");
                if (alineaContent != null)
                    newAlinea.alineaContent = alineaContent.Object.ToString();

                // Cache the aliena
                cacheNonObjectTypeAlinea.Add(node, newAlinea);
            }

            // Add tip to the return list
            correspondingAlineas.Add(cacheNonObjectTypeAlinea[node]);

        }
        // return list of related triples
        return correspondingAlineas;
    }

    // Retrieve a set of imagary that belong to the given objectType(SubjectNode)
    public List<Struct_SMW_Imagary> GetImagaryForSMWObjectNode(INode SubjectNodeSMWObject)
    {
        // List of tips corresponding to the subjectNodeForOperationalPerspective
        List<Struct_SMW_Imagary> correspondingImagary = new List<Struct_SMW_Imagary>();

        // Relevant Graph
        IGraph g = SubjectNodeSMWObject.Graph;

        // Check if triples are cached
        if (nonObjectTypeSubObjectTriples.ContainsKey(SubjectNodeSMWObject) == false)
        {
            // Cache all nonObjectTypeTriples for specific subjectNodeForOperationalPerspective
            List<INode> listOfsubjectNodeForSMWObject = new List<INode>();
            listOfsubjectNodeForSMWObject.Add(SubjectNodeSMWObject);
            CacheNonObjectTypeNodeTriples(listOfsubjectNodeForSMWObject);
        }

        // Retrieve Triples operationalPerspective relevante nodes
        foreach (Triple triple in nonObjectTypeSubObjectTriples[SubjectNodeSMWObject])
        {
            // Chech ik cached
            if (cacheNonObjectTypeImageryTriples.ContainsKey(triple.Object) != true)
            {
                // Gather triples for specifc nonObjectTypeNode
                List<Triple> tempListOfTriples = new List<Triple>(g.GetTriplesWithSubject(triple.Object));

                // Check if triples are from a tip, if so then add these to the cache
                if (tempListOfTriples.Find(x => x.Predicate.ToString() == GetGraphPropertyURI(g) + "Beeldmateriaal") != null)
                {
                    cacheNonObjectTypeImageryTriples.Add(triple.Object, tempListOfTriples);
                }
            }
        }

        //Check there is there already exisits a parsed tip, else parsed the tip and cache it  
        foreach (INode node in cacheNonObjectTypeImageryTriples.Keys)
        {
            if (cacheNonObjectTypeImagery.ContainsKey(node) == false)
            {
                Struct_SMW_Imagary newImagary = new Struct_SMW_Imagary();
                Triple imagary = cacheNonObjectTypeImageryTriples[node].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Beeldmateriaal");
                newImagary.imagary = "";
                newImagary.imagary = imagary.Object.ToString();

                Triple imagaryType = cacheNonObjectTypeImageryTriples[node].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Beeldmateriaal_type");

                newImagary.imagaryType = AlineaImagaryTypeEnum.UNASSIGNED;

                if (imagary != null)
                {
                    switch (imagaryType.Object.ToString().ToUpper())
                    {
                        case "IMAGE":
                            newImagary.imagaryType = AlineaImagaryTypeEnum.IMAGE;
                            break;
                        case "MAP":
                            newImagary.imagaryType = AlineaImagaryTypeEnum.MAP;
                            break;
                        case "PHOTO":
                            newImagary.imagaryType = AlineaImagaryTypeEnum.PHOTO;
                            break;
                        case "TBD":
                            newImagary.imagaryType = AlineaImagaryTypeEnum.TBD;
                            break;
                        default:
                            newImagary.imagaryType = AlineaImagaryTypeEnum.UNASSIGNED;
                            break;
                    }
                }

                cacheNonObjectTypeImagery.Add(node, newImagary);
            }

            // Add tip to the return list
            correspondingImagary.Add(cacheNonObjectTypeImagery[node]);

        }
        // return list of related triples
        return correspondingImagary;
    }

    // Retrieve a set of contributes that belong to the given objectType(SubjectNode)
    public List<Contributes> GetContributesForIntentionalElementNode(INode SubjectNodeSMWObject)
    {
        // List of tips corresponding to the subjectNodeForOperationalPerspective
        List<Contributes> correspondingContributes = new List<Contributes>();

        // Relevant Graph
        IGraph g = SubjectNodeSMWObject.Graph;

        // Check if triples are cached
        if (nonObjectTypeSubObjectTriples.ContainsKey(SubjectNodeSMWObject) == false)
        {
            // Cache all nonObjectTypeTriples for specific subjectNodeForOperationalPerspective
            List<INode> listOfsubjectNodeForSMWObject = new List<INode>();
            listOfsubjectNodeForSMWObject.Add(SubjectNodeSMWObject);
            CacheNonObjectTypeNodeTriples(listOfsubjectNodeForSMWObject);
        }

        // Retrieve Triples operationalPerspective relevante nodes
        foreach (Triple triple in nonObjectTypeSubObjectTriples[SubjectNodeSMWObject])
        {
            // node of the nonobject type connects
            INode nonObjectTypeNode = triple.Object;

            // Chech ik cached
            if (cacheNonObjectTypeIntentionalElementContributesTriples.ContainsKey(nonObjectTypeNode) != true)
            {
                // Gather triples for specifc nonObjectTypeNode
                List<Triple> tempListOfTriples = new List<Triple>(g.GetTriplesWithSubject(nonObjectTypeNode));

                // Check if triples are from a tip, if so then add these to the cache
                // [old] if (tempListOfTriples.Find(x => x.Predicate.ToString() == GetGraphPropertyURI(g) + "Element_contribution_value") != null)
                if (RemoveUriPrefixFromNodeValueEnding(tempListOfTriples.Find(x => x.Predicate.ToString() == GetGraphPropertyURI(g) + "Element_link_type").Object) == "Contributes")
                    cacheNonObjectTypeIntentionalElementContributesTriples.Add(nonObjectTypeNode, tempListOfTriples);
            }

            if (cacheNonObjectTypeIntentionalElementContributes.ContainsKey(nonObjectTypeNode) == false && cacheNonObjectTypeIntentionalElementContributesTriples.ContainsKey(nonObjectTypeNode) == true)
            {
                Contributes newContribute = new Contributes();

                // Get elementType
                Triple elementType = cacheNonObjectTypeIntentionalElementContributesTriples[nonObjectTypeNode].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Element_link");
                newContribute.elementLink = elementType.Object.ToString();

                // Get selectionContribution
                Triple selectionContribution = cacheNonObjectTypeIntentionalElementContributesTriples[nonObjectTypeNode].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Selection_contribution_value");
                newContribute.contributionValue = ContributionValueEnum.UNASSIGNED;
                if (selectionContribution != null)
                {
                    switch (SMWParser.instance.RemoveUriPrefixFromNodeValueEnding(selectionContribution.Object).ToUpper())
                    {
                        case "MIN":
                            newContribute.contributionValue = ContributionValueEnum.MIN;
                            break;
                        case "-":
                            newContribute.contributionValue = ContributionValueEnum.MIN;
                            break;
                        case "MIN_MIN":
                            newContribute.contributionValue = ContributionValueEnum.MIN_MIN;
                            break;
                        case "--":
                            newContribute.contributionValue = ContributionValueEnum.MIN_MIN;
                            break;
                        case "MIN_MIN_MIN":
                            newContribute.contributionValue = ContributionValueEnum.MIN_MIN_MIN;
                            break;
                        case "---":
                            newContribute.contributionValue = ContributionValueEnum.MIN_MIN_MIN;
                            break;
                        case "PLUS":
                            newContribute.contributionValue = ContributionValueEnum.PLUS;
                            break;
                        case "+":
                            newContribute.contributionValue = ContributionValueEnum.PLUS;
                            break;
                        case "PLUS_PLUS":
                            newContribute.contributionValue = ContributionValueEnum.PLUS_PLUS;
                            break;
                        case "++":
                            newContribute.contributionValue = ContributionValueEnum.PLUS_PLUS;
                            break;
                        case "PLUS_PLUS_PLUS":
                            newContribute.contributionValue = ContributionValueEnum.PLUS_PLUS_PLUS;
                            break;
                        case "+++":
                            newContribute.contributionValue = ContributionValueEnum.PLUS_PLUS_PLUS;
                            break;
                        case "PLUS_MIN":
                            newContribute.contributionValue = ContributionValueEnum.PLUS_MINUS;
                            break;
                        case "+/-":
                            newContribute.contributionValue = ContributionValueEnum.PLUS_MINUS;
                            break;
                        case "ZERO":
                            newContribute.contributionValue = ContributionValueEnum.ZERO;
                            break;
                        case "0":
                            newContribute.contributionValue = ContributionValueEnum.ZERO;
                            break;
                        default:
                            newContribute.contributionValue = ContributionValueEnum.UNASSIGNED;
                            break;
                    }
                }

                // Get Selection link note
                Triple linkNote = cacheNonObjectTypeIntentionalElementContributesTriples[nonObjectTypeNode].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Selection_link_note");
                if (linkNote != null)
                    newContribute.note = linkNote.Object.ToString();
                else
                    newContribute.note = "";

                cacheNonObjectTypeIntentionalElementContributes.Add(nonObjectTypeNode, newContribute);
            }

            // Add tip to the return list
            if(cacheNonObjectTypeIntentionalElementContributes.ContainsKey(nonObjectTypeNode))
                correspondingContributes.Add(cacheNonObjectTypeIntentionalElementContributes[nonObjectTypeNode]);

        }
            // return list of related triples
            return correspondingContributes;
    }

    // Retrieve a set of contributes that belong to the given objectType(SubjectNode)
    public List<Dependency> GetDependencyForIntentionalElementNode(INode SubjectNodeSMWObject)
    {
        // List of tips corresponding to the subjectNodeForOperationalPerspective
        List<Dependency> correspondingDependency = new List<Dependency>();

        // Relevant Graph
        IGraph g = SubjectNodeSMWObject.Graph;

        // Check if triples are cached
        if (nonObjectTypeSubObjectTriples.ContainsKey(SubjectNodeSMWObject) == false)
        {
            // Cache all nonObjectTypeTriples for specific subjectNodeForOperationalPerspective
            List<INode> listOfsubjectNodeForSMWObject = new List<INode>();
            listOfsubjectNodeForSMWObject.Add(SubjectNodeSMWObject);
            CacheNonObjectTypeNodeTriples(listOfsubjectNodeForSMWObject);
        }

        // Retrieve Triples operationalPerspective relevante nodes
        foreach (Triple triple in nonObjectTypeSubObjectTriples[SubjectNodeSMWObject])
        {
            // node of the nonobject type connects
            INode nonObjectTypeNode = triple.Object;

            // Chech ik cached
            if (cacheNonObjectTypeIntentionalElementDependencyTriples.ContainsKey(nonObjectTypeNode) != true)
            {
                // Gather triples for specifc nonObjectTypeNode
                List<Triple> tempListOfTriples = new List<Triple>(g.GetTriplesWithSubject(nonObjectTypeNode));

                // Check if triples are from a tip, if so then add these to the cache
                // [old] if (tempListOfTriples.Find(x => x.Predicate.ToString() == GetGraphPropertyURI(g) + "") != null)
                if (RemoveUriPrefixFromNodeValueEnding(tempListOfTriples.Find(x => x.Predicate.ToString() == GetGraphPropertyURI(g) + "Element_link_type").Object) == "Depends")
                    cacheNonObjectTypeIntentionalElementDependencyTriples.Add(nonObjectTypeNode, tempListOfTriples);
            }

        //Check there is there already exisits a parsed tip, else parsed the tip and cache it  


            if (cacheNonObjectTypeIntentionalElementDependency.ContainsKey(nonObjectTypeNode) == false && cacheNonObjectTypeIntentionalElementDependencyTriples.ContainsKey(nonObjectTypeNode) == true)
            {
                Dependency newDependens = new Dependency();

                // Get elementType
                Triple elementlink = cacheNonObjectTypeIntentionalElementDependencyTriples[nonObjectTypeNode].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Element_link");
                newDependens.elementLink = elementlink.Object.ToString();

                // Get Selection link note
                Triple linkNote = cacheNonObjectTypeIntentionalElementDependencyTriples[nonObjectTypeNode].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Selection_link_note");
                if (linkNote != null)
                    newDependens.note = linkNote.Object.ToString();
                else
                    newDependens.note = "";

                cacheNonObjectTypeIntentionalElementDependency.Add(nonObjectTypeNode, newDependens);
            }

            // Add tip to the return list
            if (cacheNonObjectTypeIntentionalElementDependency.ContainsKey(nonObjectTypeNode))
                correspondingDependency.Add(cacheNonObjectTypeIntentionalElementDependency[nonObjectTypeNode]);

        }

        // return list of related triples
        return correspondingDependency;
    }

    // Retrieve a set of contributes that belong to the given objectType(SubjectNode)
    public List<Connects> GetConnectsForActivityNode(INode SubjectNodeSMWObject)
    {
        // List of tips corresponding to the subjectNodeForOperationalPerspective
        List<Connects> correspondingConnects = new List<Connects>();

        // Relevant Graph
        IGraph g = SubjectNodeSMWObject.Graph;

        // Check if triples are cached
        if (nonObjectTypeSubObjectTriples.ContainsKey(SubjectNodeSMWObject) == false)
        {
            // Cache all nonObjectTypeTriples for specific subjectNodeForOperationalPerspective
            List<INode> listOfsubjectNodeForSMWObject = new List<INode>();
            listOfsubjectNodeForSMWObject.Add(SubjectNodeSMWObject);
            CacheNonObjectTypeNodeTriples(listOfsubjectNodeForSMWObject);
        }

        // Retrieve Triples operationalPerspective relevante nodes
        foreach (Triple triple in nonObjectTypeSubObjectTriples[SubjectNodeSMWObject])
        {

            // node of the nonobject type connects
            INode nonObjectTypeNode = triple.Object;

            // Chech ik cached
            if (cacheNonObjectTypeIntentionalElementConnectsTriples.ContainsKey(nonObjectTypeNode) != true)
            {
                // Gather triples for specifc nonObjectTypeNode
                List<Triple> tempListOfTriples = new List<Triple>(g.GetTriplesWithSubject(nonObjectTypeNode));
                
                if (RemoveUriPrefixFromNodeValueEnding(tempListOfTriples.Find(x => x.Predicate.ToString() == GetGraphPropertyURI(g) + "Element_link_type").Object) == "Connects")
                    cacheNonObjectTypeIntentionalElementConnectsTriples.Add(nonObjectTypeNode, tempListOfTriples);
            }

            //Check there is there already exisits a parsed tip, else parsed the tip and cache it 


                if (cacheNonObjectTypeIntentionalElementConnects.ContainsKey(nonObjectTypeNode) == false && cacheNonObjectTypeIntentionalElementConnectsTriples.ContainsKey(nonObjectTypeNode) == true)
                {
                    Connects newConnects = new Connects();

                    // Get elementType
                    Triple elementType = cacheNonObjectTypeIntentionalElementConnectsTriples[nonObjectTypeNode].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Element_link");
                    newConnects.elementLink = elementType.Object.ToString();

                    // Get selectionConnects
                    Triple connectionType = cacheNonObjectTypeIntentionalElementConnectsTriples[nonObjectTypeNode].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Element_connection_type");

                    newConnects.elementConnectionType = ConnectionTypeEnum.UNASSIGNED;
                    if (connectionType != null)
                    {
                        switch (SMWParser.instance.RemoveUriPrefixFromNodeValueEnding(connectionType.Object).ToUpper())
                        {
                            case "SEQ":
                                newConnects.elementConnectionType = ConnectionTypeEnum.SEQ;
                                break;
                            case "PAR":
                                newConnects.elementConnectionType = ConnectionTypeEnum.PAR;
                                break;
                            case "JOIN":
                                newConnects.elementConnectionType = ConnectionTypeEnum.JOIN;
                                break;
                            case "SYNC":
                                newConnects.elementConnectionType = ConnectionTypeEnum.SYNC;
                                break;
                            case "PRODUCES":
                                newConnects.elementConnectionType = ConnectionTypeEnum.PRODUCES;
                                break;
                            case "CONSUMES":
                                newConnects.elementConnectionType = ConnectionTypeEnum.CONSUMES;
                                break;
                            default:
                                newConnects.elementConnectionType = ConnectionTypeEnum.UNASSIGNED;
                                break;
                        }
                    }

                    // Get Selection link note
                    Triple linkNote = cacheNonObjectTypeIntentionalElementConnectsTriples[nonObjectTypeNode].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Selection_link_note");
                    if (linkNote != null)
                        newConnects.note = linkNote.Object.ToString();
                    else
                        newConnects.note = "";

                    cacheNonObjectTypeIntentionalElementConnects.Add(nonObjectTypeNode, newConnects);
                }

            // Add tip to the return list
            if (cacheNonObjectTypeIntentionalElementConnects.ContainsKey(nonObjectTypeNode))
                correspondingConnects.Add(cacheNonObjectTypeIntentionalElementConnects[nonObjectTypeNode]);
        }

        // return list of related triples
        return correspondingConnects;
    }

    // Retrieve a set of selection that belong to the given objectType(SubjectNode)
    public List<Selects> GetSelectsForPracticeNode(INode SubjectNodeSMWObject)
    {
        // List of tips corresponding to the subjectNodeForOperationalPerspective
        List<Selects> correspondingSelects = new List<Selects>();

        // Relevant Graph
        IGraph g = SubjectNodeSMWObject.Graph;

        // Check if triples are cached
        if (nonObjectTypeSubObjectTriples.ContainsKey(SubjectNodeSMWObject) == false)
        {
            // Cache all nonObjectTypeTriples for specific subjectNodeForOperationalPerspective
            List<INode> listOfsubjectNodeForSMWObject = new List<INode>();
            listOfsubjectNodeForSMWObject.Add(SubjectNodeSMWObject);
            CacheNonObjectTypeNodeTriples(listOfsubjectNodeForSMWObject);
        }

        // Retrieve Triples operationalPerspective relevante nodes
        foreach (Triple triple in nonObjectTypeSubObjectTriples[SubjectNodeSMWObject])
        {
            // Chech ik cached
            if (cacheNonObjectTypePracticeSelectsTriples.ContainsKey(triple.Object) != true)
            {
                // Gather triples for specifc nonObjectTypeNode
                List<Triple> tempListOfTriples = new List<Triple>(g.GetTriplesWithSubject(triple.Object));

                // Check if triples are from a tip, if so then add these to the cache
                if (tempListOfTriples.Find(x => x.Predicate.ToString() == GetGraphPropertyURI(g) + "") != null)
                {
                    cacheNonObjectTypePracticeSelectsTriples.Add(triple.Object, tempListOfTriples);
                }
            }
        }

        //Check there is there already exisits a parsed tip, else parsed the tip and cache it  
        foreach (INode node in cacheNonObjectTypePracticeSelectsTriples.Keys)
        {
            if (cacheNonObjectTypePracticeSelect.ContainsKey(node) == false)
            {
                Selects newSelection = new Selects();

                // Get selectionType
                Triple selectType = cacheNonObjectTypePracticeSelectsTriples[node].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Selection_type");
                newSelection.selectionType = SelectionTypeEnum.UNASSIGNED;
                if (selectType != null)
                {
                    switch (selectType.Object.ToString().ToUpper())
                    {
                        case "SELECTS":
                            newSelection.selectionType = SelectionTypeEnum.SELECTS;
                            break;
                        case "REJECTS":
                            newSelection.selectionType = SelectionTypeEnum.REJECTS;
                            break;
                        default:
                            newSelection.selectionType = SelectionTypeEnum.UNASSIGNED;
                            break;
                    }
                }

                // Get selectionlink
                Triple selectionLink = cacheNonObjectTypePracticeSelectsTriples[node].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Selection_link");
                newSelection.selectionLink = selectionLink.Object.ToString();

                // Get selectionContribution
                Triple selectionContribution = cacheNonObjectTypePracticeSelectsTriples[node].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Selection_contribution_value");
                newSelection.selectionContributionValue = ContributionValueEnum.UNASSIGNED;
                if (selectionContribution != null)
                {
                    switch (selectionContribution.Object.ToString().ToUpper())
                    {
                        case "MIN":
                            newSelection.selectionContributionValue = ContributionValueEnum.MIN;
                            break;
                        case "-":
                            newSelection.selectionContributionValue = ContributionValueEnum.MIN;
                            break;
                        case "MIN_MIN":
                            newSelection.selectionContributionValue = ContributionValueEnum.MIN_MIN;
                            break;
                        case "--":
                            newSelection.selectionContributionValue = ContributionValueEnum.MIN_MIN;
                            break;
                        case "MIN_MIN_MIN":
                            newSelection.selectionContributionValue = ContributionValueEnum.MIN_MIN_MIN;
                            break;
                        case "---":
                            newSelection.selectionContributionValue = ContributionValueEnum.MIN_MIN_MIN;
                            break;
                        case "PLUS":
                            newSelection.selectionContributionValue = ContributionValueEnum.PLUS;
                            break;
                        case "+":
                            newSelection.selectionContributionValue = ContributionValueEnum.PLUS;
                            break;
                        case "PLUS_PLUS":
                            newSelection.selectionContributionValue = ContributionValueEnum.PLUS_PLUS;
                            break;
                        case "++":
                            newSelection.selectionContributionValue = ContributionValueEnum.PLUS_PLUS;
                            break;
                        case "PLUS_PLUS_PLUS":
                            newSelection.selectionContributionValue = ContributionValueEnum.PLUS_PLUS_PLUS;
                            break;
                        case "+++":
                            newSelection.selectionContributionValue = ContributionValueEnum.PLUS_PLUS_PLUS;
                            break;
                        case "PLUS_MIN":
                            newSelection.selectionContributionValue = ContributionValueEnum.PLUS_MINUS;
                            break;
                        case "+/-":
                            newSelection.selectionContributionValue = ContributionValueEnum.PLUS_MINUS;
                            break;
                        case "ZERO":
                            newSelection.selectionContributionValue = ContributionValueEnum.ZERO;
                            break;
                        case "0":
                            newSelection.selectionContributionValue = ContributionValueEnum.ZERO;
                            break;
                        default:
                            newSelection.selectionContributionValue = ContributionValueEnum.UNASSIGNED;
                            break;
                    }
                }

                // Get Selection link note
                Triple selectionLinkNote = cacheNonObjectTypePracticeSelectsTriples[node].Find(t => t.Predicate.ToString() == GetGraphPropertyURI(g) + "Selection_link_note");
                newSelection.note = selectionLink.Object.ToString();

                cacheNonObjectTypePracticeSelect.Add(node, newSelection);
            }

            // Add tip to the return list
            correspondingSelects.Add(cacheNonObjectTypePracticeSelect[node]);

        }
        // return list of related triples
        return correspondingSelects;
    }

    // Retrieve a set of (Predicate)Triple that belong to the given objectType(SubjectNode)
    //public List<Struct_SMW_Tip> GetTipsForOperationalPerspectiveNode(INode subjectNodeForOperationalPerspective)
    //{
    //    // List of tips corresponding to the subjectNodeForOperationalPerspective
    //    List<Struct_SMW_Tip> correspondingTips = new List<Struct_SMW_Tip>();

    //    // Relevant Graph
    //    IGraph g = subjectNodeForOperationalPerspective.Graph;

    //    // Check if triples are cached
    //    if (nonObjectTypeSubObjectTriples.ContainsKey(subjectNodeForOperationalPerspective) == false)
    //    {
    //        // Cache all nonObjectTypeTriples for specific subjectNodeForOperationalPerspective
    //        List<INode> listOfsubjectNodeForOperationalPerspective = new List<INode>();
    //        listOfsubjectNodeForOperationalPerspective.Add(subjectNodeForOperationalPerspective);
    //        CacheNonObjectTypeNodeTriples(listOfsubjectNodeForOperationalPerspective);
    //    }

    //    // Retrieve Triples operationalPerspective relevante nodes
    //    foreach (Triple triple in nonObjectTypeSubObjectTriples[subjectNodeForOperationalPerspective])
    //    {
    //        // Chech ik cached
    //        if (cacheNonObjectTypeTipsTriples.ContainsKey(triple.Object) != true)
    //        {
    //            // Gather triples for specifc nonObjectTypeNode
    //            List<Triple> tempListOfTriples = new List<Triple>(g.GetTriplesWithSubject(triple.Object));

    //            // Check if triples are from a tip, if so then add these to the cache
    //            if (tempListOfTriples.Find(x => x.Predicate.ToString() == "http://www.projectenportfolio.nl/wiki/index.php/Speciaal:URIResolver/Property-3ATip_back_link") != null)
    //            {
    //                cacheNonObjectTypeTipsTriples.Add(triple.Object, tempListOfTriples);
    //            }
    //        }
    //    }

    //    //Check there is there already exisits a parsed tip, else parsed the tip and cache it  
    //    foreach (INode node in CacheNonObjectTypeTipsTriples.Keys)
    //    {
    //        if (cacheNonObjectTypeTips.ContainsKey(node) == false)
    //        {
    //            Struct_SMW_Tip newTip = new Struct_SMW_Tip();
    //            Triple description = cacheNonObjectTypeTipsTriples[node].Find(t => t.Predicate.ToString() == "http://www.projectenportfolio.nl/wiki/index.php/Speciaal:URIResolver/Property-3ATip_description");

    //            // ToString version
    //            newTip.description = description.Object.ToString();

    //            // XmlWriter version
    //            //description.Object.WriteXml(writer);
    //            //newTip.description = sb.ToString();

    //            Triple targetAudience = cacheNonObjectTypeTipsTriples[node].Find(t => t.Predicate.ToString() == "http://www.projectenportfolio.nl/wiki/index.php/Speciaal:URIResolver/Property-3ATip_target_audience");

    //            // ToString version
    //            newTip.targetAudience = targetAudience.Object.ToString();

    //            // XmlWriter version
    //            //targetAudience.Object.WriteXml(writer);
    //            //newTip.targetAudience = sb.ToString();

    //            cacheNonObjectTypeTips.Add(node, newTip);
    //        }

    //        // Add tip to the return list
    //        correspondingTips.Add(cacheNonObjectTypeTips[node]);

    //    }
    //    // return list of related triples
    //    return correspondingTips;
    //}

    // Retrieve a set of 
    public List<UriNode> InstantiateObjectsForTriples(IGraph semanticGraph)
    {
        List<UriNode> specificObjectURINodes = new List<UriNode>();

        return specificObjectURINodes;
    }
}

