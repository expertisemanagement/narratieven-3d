﻿using Object.Type;
using SMW.EMM.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VDS.RDF;

public class ContextModel : SMWObjectModel, IContextModel {

    // Constructor
    public ContextModel(string entityID, INode semanticNode) : base(entityID, semanticNode)
    {
    }

    // Constructor
    public ContextModel(string entityID, INode semanticNode, SMWCategory smwCategory, NavObjectModel navObject) : base(entityID, semanticNode, smwCategory, navObject)
    {
    }

    // Constructor
    public ContextModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
    }

    // Constructor
    public ContextModel(BaseView view, bool syncViewToModel, NavObjectModel navObject) : base(view, syncViewToModel, navObject)
    {
    }

    // Returns a Context which this Context is a part of (Context of within this Context exists)
    private INode b_superContextNode;
    public INode SuperContextNode
    {
        get
        {
            if (b_superContextNode == null)
            {
                Triple[] relevantTriples = TripleData.FindAll(t => t.Predicate.ToString() == SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Supercontext").ToArray();

                if (relevantTriples == null)
                    return null;

                if (relevantTriples.Count() <= 0)
                    return null;

                b_superContextNode = relevantTriples[0].Object;
            }

            return b_superContextNode;
        }
        set
        {
            // Only if the backing field changes
            if (b_superContextNode != value)
            {
                // Set new value
                b_superContextNode = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new SuperContextChangedEventArgs();
                OnSuperContextChanged(this, eventArgs);
            }
        }
    }
    public ContextModel SuperContext
    {
        get
        {
            return SMWParser.Instance.GetSuperContext(this);
        }
        private set { }
    }

    // Return Contexts that are part of this Context (exists within/ depend on this one)
    private INode[] b_subContextNodes;
    public INode[] SubContextNodes
    {
        get
        {
            if (b_subContextNodes == null)
            {
                Triple[] relevantTriples = SemanticNode.Graph.GetTriplesWithPredicateObject(SemanticNode.Graph.CreateUriNode(UriFactory.Create(SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Supercontext")), this.SemanticNode).ToArray();

                if (relevantTriples == null)
                    return new INode[0];

                if (relevantTriples.Count() <= 0)
                        return new INode[0];

                List<INode> relevantNodesFromTheTriples = new List<INode>();

                foreach (Triple triple in relevantTriples)
                    relevantNodesFromTheTriples.Add(triple.Subject);

                b_subContextNodes = relevantNodesFromTheTriples.ToArray();
            }

            return b_subContextNodes;
        }
        set
        {
            // Only if the backing field changes
            if (b_subContextNodes != value)
            {
                // Set new value
                b_subContextNodes = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new SubContextsChangedEventArgs();
                OnSubContextsChanged(this, eventArgs);
            }
        }
    }
    public ContextModel[] SubContexts
    {
        get
        {
            return SMWParser.Instance.GetSubContexts(this);
        }
        set { }
    }

    private ContextTypeEnum b_contextType = ContextTypeEnum.UNASSIGNED;
    public ContextTypeEnum ContextType
    {
        get
        {
            if (b_contextType == ContextTypeEnum.UNASSIGNED)
            {
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate.ToString() == SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) +  "Context_type").ToArray();

                if (relevantTriples != null)
                    if(relevantTriples.Count() >= 1)
                    {
                        
                        switch (SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object).ToUpper())
                        {
                            case "ROLE":
                                b_contextType = ContextTypeEnum.ROLE;
                                break;
                            case "SITUATION":
                                b_contextType = ContextTypeEnum.SITUATION;
                                break;
                            default:
                                b_contextType = ContextTypeEnum.UNASSIGNED;
                                break;
                        }
                    }
            }

            return b_contextType;
        }
        set
        {
            // Only if the backing field changes
            if (b_contextType != value)
            {
                // Set new value
                b_contextType = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ContextTypeChangedEventArgs();
                OnContextTypeChanged(this, eventArgs);
            }
        }
    }

    private string b_description;
    public string Description
    {
        get
        {
            if (b_description == null)
            {
                Triple[] relevantTriples = TripleData.FindAll(t => t.Predicate.ToString() == SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Description").ToArray();

                if (relevantTriples == null)
                    return null;

                if (relevantTriples.Count() <= 0)
                    return null;

                b_description = SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object);
            }

            return b_description;
        }
        set
        {
            // Only if the backing field changes
            if (b_description != value)
            {
                // Set new value
                b_description = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new DescriptionChangedEventArgs();
                OnDescriptionChanged(this, eventArgs);
            }
        }
    }

    // Finds all realted Practices
    public List<PracticeModel> b_relatedPractices;
    public List<PracticeModel> RelatedPractices
    {
        get
        {
            // If there is a cache, then use it
            if (b_relatedPractices != null)
                return b_relatedPractices;

            // Get all practices that belong to this context
            b_relatedPractices = SMWParser.Instance.GetAllInstancesByObjectType(SemanticNode.Graph, ObjectTypeEnum.PRACTICE, true).Cast<PracticeModel>().ToList().FindAll(p => p.Context == this).ToList();

            return b_relatedPractices;
        }
    }

    // Finds all related IntentionalElements
    private List<ActivityModel> b_relatedIntentionalElements;
    public List<ActivityModel> RelatedIntentionalElements
    {
        get
        {
            // If there is a cache, then use it
            if (b_relatedIntentionalElements != null)
                return b_relatedIntentionalElements;

            // Get all practices that belong to this context
            b_relatedIntentionalElements = SMWParser.Instance.GetAllInstancesByObjectType(SemanticNode.Graph, ObjectTypeEnum.INTENTIONALELEMENT, true).Cast<ActivityModel>().ToList().FindAll(a => a.Context == this).ToList();

            return b_relatedActivities;
        }
    }

    // Finds all related IntentionalElements
    private List<ActivityModel> b_relatedActivities;
    public List<ActivityModel> RelatedActivities
    {
        get
        {
            // If there is a cache, then use it
            if (b_relatedActivities != null)
                return b_relatedActivities;

            // Get all practices that belong to this context
            b_relatedActivities = SMWParser.Instance.GetAllInstancesByObjectType(SemanticNode.Graph, ObjectTypeEnum.ACTIVITY, true).Cast<ActivityModel>().ToList().FindAll(a => a.Context == this).ToList();

            return b_relatedActivities;
        }
    }

    // Finds all related Actors
    private List<ActorModel> b_relatedActors;
    public List<ActorModel> RelatedActors
    {
        get
        { 
            // If there is a cache, then use it
            if (b_relatedActors != null)
                return b_relatedActors;

            // Get all actors that belong to this context
            b_relatedActors = SMWParser.Instance.GetAllInstancesByObjectType(SemanticNode.Graph, ObjectTypeEnum.ACTOR, true).Cast<ActorModel>().ToList().FindAll(a => a.Context == this).ToList();

            return b_relatedActors;

        }
    }

    // Finds all related IntentionalElements
    private List<ContextModel> b_relatedRoles;
    public List<ContextModel> RelatedRoles
    {
        get
        {
            // If there is a cache, then use it
            if (b_relatedRoles != null)
                return b_relatedRoles;

            // Get all rolls from subcontexts of this context
            b_relatedRoles = SMWParser.Instance.GetAllInstancesByObjectType(SemanticNode.Graph, ObjectTypeEnum.CONTEXT, true).Cast<ContextModel>().ToList().FindAll(c => c.SuperContext == this).ToList();

            // cache and return the results
            return b_relatedRoles;
        }
    }

    // Dispatch EventHandlers for SWM pageform info changes
    public event EventHandler<SuperContextChangedEventArgs> OnSuperContextChanged = (sender, e) => { };
    public event EventHandler<SubContextsChangedEventArgs> OnSubContextsChanged = (sender, e) => { };
    public event EventHandler<ContextTypeChangedEventArgs> OnContextTypeChanged = (sender, e) => { };
    public event EventHandler<DescriptionChangedEventArgs> OnDescriptionChanged = (sender, e) => { };

}
