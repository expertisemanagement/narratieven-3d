﻿using Boo.Lang;
using Object.Type;
using SMW.Relation.IntetionalELement;
using System;
using System.Linq;
using VDS.RDF;

public class ActivityModel : IntentionalElementModel, IActivityModel
{
    // Constructor
    public ActivityModel(string entityID, INode semanticNode) : base(entityID, semanticNode)
    {
    }

    // Constructor
    public ActivityModel(string entityID, INode semanticNode, SMWCategory smwCategory, NavObjectModel navObject) : base(entityID, semanticNode, smwCategory, navObject)
    {
    }

    // Constructor
    public ActivityModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
    }

    // Constructor
    public ActivityModel(BaseView view, bool syncViewToModel, NavObjectModel navObject) : base(view, syncViewToModel, navObject)
    {
    }

    // Returns all Activity (nodes or ActivityModel) that are consumed by this Activity
    private INode[] b_consumesNodes;
    public INode[] ConsumesNodes
    {
        get
        {
            if (b_consumesNodes == null)
            {
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate == SemanticNode.Graph.CreateUriNode(UriFactory.Create(SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Consumes"))).ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_consumesNodes = null;
                else
                {
                    List<INode> relevantNodesFromTheTriples = new List<INode>();

                    foreach (Triple triple in relevantTriples)
                        relevantNodesFromTheTriples.Add(triple.Object);

                    b_consumesNodes = relevantNodesFromTheTriples.ToArray();
                }

            }

            return b_consumesNodes;
        }
        set
        {
            // Only if the backing field changes
            if (b_consumesNodes != value)
            {
                // Set new value
                b_consumesNodes = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ConsumesChangedEventArgs();
                OnConsumesChanged(this, eventArgs);
            }
        }
    }
    public IntentionalElementModel[] Consumes
    {
        get
        {
            return SMWParser.Instance.GetActivityConsumes(this);
        }
        private set { }
    }

    // Returns all Activity (nodes or ActivityModels) by which this ActivityModel is consumed
    private INode[] b_consumesThisNodes;
    public INode[] ConsumesThisNodes
    {
        get
        {
            if (b_consumesThisNodes == null)
            {
                Triple[] relevantTriples = SemanticNode.Graph.GetTriplesWithPredicateObject(SemanticNode.Graph.CreateUriNode(UriFactory.Create(SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Consumes")), this.SemanticNode).ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_consumesThisNodes = null;
                else
                {
                    List<INode> relevantNodesFromTheTriples = new List<INode>();

                    foreach (Triple triple in relevantTriples)
                        relevantNodesFromTheTriples.Add(triple.Subject);

                    b_consumesThisNodes = relevantNodesFromTheTriples.ToArray();
                }
            }

            return b_consumesThisNodes;
        }
        set
        {
            // Only if the backing field changes
            if (b_consumesThisNodes != value)
            {
                // Set new value
                b_consumesThisNodes = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ConsumesThisChangedEventArgs();
                OnConsumesThisChanged(this, eventArgs);
            }
        }
    }
    public IntentionalElementModel[] ConsumesThis
    {
        get
        {
            return SMWParser.Instance.GetActivityConsumesThis(this);
        }
        private set { }
    }

    // Returns all Activity (nodes or ActivityModel) that are consumed by this Activity
    private INode[] b_producesNodes;
    public INode[] ProducesNodes
    {
        get
        {
            if (b_producesNodes == null)
            {
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate == SemanticNode.Graph.CreateUriNode(UriFactory.Create(SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Produces"))).ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_producesNodes = null;
                else
                {
                    List<INode> relevantNodesFromTheTriples = new List<INode>();

                    foreach (Triple triple in relevantTriples)
                        relevantNodesFromTheTriples.Add(triple.Object);

                    b_producesNodes = relevantNodesFromTheTriples.ToArray();
                }

            }

            return b_producesNodes;
        }
        set
        {
            // Only if the backing field changes
            if (b_producesNodes != value)
            {
                // Set new value
                b_producesNodes = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ProducesChangedEventArgs();
                OnProducesChanged(this, eventArgs);
            }
        }
    }
    public IntentionalElementModel[] Produces
    {
        get
        {
            return SMWParser.Instance.GetActivityProduces(this);
        }
        private set { }
    }

    // Returns all Activity (nodes or ActivityModels) by which this ActivityModel is consumed
    private INode[] b_producesThisNodes;
    public INode[] ProducesThisNodes
    {
        get
        {
            if (b_producesThisNodes == null)
            {
                Triple[] relevantTriples = SemanticNode.Graph.GetTriplesWithPredicateObject(SemanticNode.Graph.CreateUriNode(UriFactory.Create(SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Produces")), this.SemanticNode).ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_producesThisNodes = null;
                else
                {
                    List<INode> relevantNodesFromTheTriples = new List<INode>();

                    foreach (Triple triple in relevantTriples)
                        relevantNodesFromTheTriples.Add(triple.Subject);

                    b_producesThisNodes = relevantNodesFromTheTriples.ToArray();
                }
            }

            return b_producesThisNodes;
        }
        set
        {
            // Only if the backing field changes
            if (b_producesThisNodes != value)
            {
                // Set new value
                b_producesThisNodes = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ProducesThisChangedEventArgs();
                OnProducesThisChanged(this, eventArgs);
            }
        }
    }
    public IntentionalElementModel[] ProducesThis
    {
        get
        {
            return SMWParser.Instance.GetActivityProducesThis(this);
        }
        private set { }
    }

    private Connects[] b_connects;
    public Connects[] Connects
    {
        get
        {
            if (b_connects == null)
                return SMWParser.Instance.GetConnectsForActivityNode(SemanticNode).ToArray();
            else
                return b_connects;
        }
        set
        {
            // Only if the backing field changes
            if (b_connects != value)
            {
                // Set new value
                b_connects = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ConnectsChangedEventArgs();
                OnConnectsChanged(this, eventArgs);
            }
        }
    }
    
    public event EventHandler<ConsumesChangedEventArgs> OnConsumesChanged = (sender, e) => { };
    public event EventHandler<ConsumesThisChangedEventArgs> OnConsumesThisChanged = (sender, e) => { };
    public event EventHandler<ProducesChangedEventArgs> OnProducesChanged = (sender, e) => { };
    public event EventHandler<ProducesThisChangedEventArgs> OnProducesThisChanged = (sender, e) => { };
    public event EventHandler<ConnectsChangedEventArgs> OnConnectsChanged = (sender, e) => { };
}