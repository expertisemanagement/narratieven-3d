﻿using Object.Type;
using SMW.Relation.IntetionalELement;
using System;
using System.Collections.Generic;
using VDS.RDF;

public class ActorModel : IntentionalElementModel, IActorModel
{
    // Constructor
    public ActorModel(string entityID, INode semanticNode) : base(entityID, semanticNode)
    {
    }

    // Constructor
    public ActorModel(string entityID, INode semanticNode, SMWCategory smwCategory, NavObjectModel navObject) : base(entityID, semanticNode, smwCategory, navObject)
    {
    }

    // Constructor
    public ActorModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
    }

    // Constructor
    public ActorModel(BaseView view, NavObjectModel navObject, bool syncViewToModel) : base(view, syncViewToModel, navObject)
    {
    }

    private string b_name;
    public string name
    {
        get { return b_name; }
        set
        {
            // Only if the backing field changes
            if (b_name != value)
            {
                // Set new value
                b_name = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new NameChangedEventArgs();
                OnNameChanged(this, eventArgs);
            }
        }
    }

    private string b_address;
    public string Address
    {
        get { return b_address; }
        set
        {
            // Only if the backing field changes
            if (b_address != value)
            {
                // Set new value
                b_address = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new AddressChangedEventArgs();
                OnAddressChanged(this, eventArgs);
            }
        }
    }

    private string b_houseNumber;
    public string HouseNumber
    {
        get { return b_houseNumber; }
        set
        {
            // Only if the backing field changes
            if (b_houseNumber != value)
            {
                // Set new value
                b_houseNumber = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new HouseNumberChangedEventArgs();
                OnHouseNumberChanged(this, eventArgs);
            }
        }
    }

    private string b_zipcode;
    public string Zipcode
    {
        get { return b_zipcode; }
        set
        {
            // Only if the backing field changes
            if (b_zipcode != value)
            {
                // Set new value
                b_zipcode = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ZipcodeChangedEventArgs();
                OnZipcodeChanged(this, eventArgs);
            }
        }
    }

    private string b_location;
    public string Location
    {
        get { return b_location; }
        set
        {
            // Only if the backing field changes
            if (b_location != value)
            {
                // Set new value
                b_location = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new LocationChangedEventArgs();
                OnLocationChanged(this, eventArgs);
            }
        }
    }

    private string b_phone;
    public string Phone
    {
        get { return b_phone; }
        set
        {
            // Only if the backing field changes
            if (b_phone != value)
            {
                // Set new value
                b_phone = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new PhoneChangedEventArgs();
                OnPhoneChanged(this, eventArgs);
            }
        }
    }

    private string b_email;
    public string Email
    {
        get { return b_email; }
        set
        {
            // Only if the backing field changes
            if (b_email != value)
            {
                // Set new value
                b_email = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new EmailChangedEventArgs();
                OnEmailChanged(this, eventArgs);
            }
        }
    }

    private string b_website;
    public string Website
    {
        get { return b_website; }
        set
        {
            // Only if the backing field changes
            if (b_website != value)
            {
                // Set new value
                b_website = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new WebsiteChangedEventArgs();
                OnWebsiteChanged(this, eventArgs);
            }
        }
    }

    private string b_info;
    public string Info
    {
        get { return b_info; }
        set
        {
            // Only if the backing field changes
            if (b_info != value)
            {
                // Set new value
                b_info = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new InfoChangedEventArgs();
                OnInfoChanged(this, eventArgs);
            }
        }
    }

    // Currently null
    public List<PracticeModel> b_relatedPractices;
    public List<PracticeModel> RelatedPractices
    {
        get
        {
            return null;
        }
    }

    // Dispatch eventhandler when the ...

    public event EventHandler<NameChangedEventArgs> OnNameChanged = (sender, e) => { };
    public event EventHandler<AddressChangedEventArgs> OnAddressChanged = (sender, e) => { };
    public event EventHandler<HouseNumberChangedEventArgs> OnHouseNumberChanged = (sender, e) => { };
    public event EventHandler<ZipcodeChangedEventArgs> OnZipcodeChanged = (sender, e) => { };
    public event EventHandler<LocationChangedEventArgs> OnLocationChanged = (sender, e) => { };
    public event EventHandler<PhoneChangedEventArgs> OnPhoneChanged = (sender, e) => { };
    public event EventHandler<EmailChangedEventArgs> OnEmailChanged = (sender, e) => { };
    public event EventHandler<WebsiteChangedEventArgs> OnWebsiteChanged = (sender, e) => { };
    public event EventHandler<InfoChangedEventArgs> OnInfoChanged = (sender, e) => { };
}
