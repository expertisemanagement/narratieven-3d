﻿using System;
using VDS.RDF;
using System.Linq;
using System.Collections.Generic;
using Object.Type;
using SMW.Relation.IntetionalELement;
using UnityEngine;

public class IntentionalElementModel : SMWObjectModel, IIntentionalElementModel
{
    // Constructor
    public IntentionalElementModel(string entityID, INode semanticNode) : base(entityID, semanticNode)
    {
    }

    // Constructor
    public IntentionalElementModel(string entityID, INode semanticNode, SMWCategory smwCategory, NavObjectModel navObject) : base(entityID, semanticNode, smwCategory, navObject)
    {
    }

    // Constructor
    public IntentionalElementModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
    }

    // Constructor
    public IntentionalElementModel(BaseView view, bool syncViewToModel, NavObjectModel navObject) : base(view, syncViewToModel, navObject)
    {
    }

    private ContextModel b_context;
    public ContextModel Context
    {
        get
        {
            if (b_context == null)
            {
                //Triple[] relevantTriples = tripleData.Where(t => t.Predicate == SMWParser.Instance.GetGraphPropertyURI(semanticNode.Graph) + "Context").ToArray();
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate.ToString() == SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Context").ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_context = null;
                else
                    b_context = ((ContextModel)SMWParser.Instance.GetCategoryByName(SemanticNode.Graph, "Context").GetSpecificInstanceModel(relevantTriples[0].Object, true)); //OLD -> GameManager.instance.modelsBySementicURI[objectType][relevantTriples[0].Subject.ToString()]);



            }

            return b_context;
        }
        set
        {
            // Only if the backing field changes
            if (b_context != value)
            {
                // Set new value
                b_context = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ContextChangedEventArgs();
                OnContextChanged(this, eventArgs);
            }
        }
    }

    protected IntentionalElementTypeEnum b_intentionalElementType = IntentionalElementTypeEnum.UNASSIGNED;
    public IntentionalElementTypeEnum IntentionalElementType
    {
        get
        {
            if (b_intentionalElementType == IntentionalElementTypeEnum.UNASSIGNED)
            {
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate.ToString() == SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Intentional_Element_type").ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_intentionalElementType = IntentionalElementTypeEnum.UNASSIGNED;
                else if (SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object).ToUpper().Contains("ACTIVITY"))
                    b_intentionalElementType = IntentionalElementTypeEnum.ACTIVITY;
                else if (SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object).ToUpper().Contains("ACTOR"))
                    b_intentionalElementType = IntentionalElementTypeEnum.ACTOR;
                else if (SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object).ToUpper().Contains("BELIEF"))
                    b_intentionalElementType = IntentionalElementTypeEnum.BELIEF;
                else if (SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object).ToUpper().Contains("GOAL"))
                    b_intentionalElementType = IntentionalElementTypeEnum.GOAL;
                else if (SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object).ToUpper().Contains("OUTCOME"))
                    b_intentionalElementType = IntentionalElementTypeEnum.OUTCOME;
            }

            return b_intentionalElementType;
        }
        set
        {
            // Only if the backing field changes
            if (b_intentionalElementType != value)
            {
                // Set new value
                b_intentionalElementType = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new IntentionalElementTypeChangedEventArgs();
                OnIntentionalElementTypeChanged(this, eventArgs);
            }
        }
    }

    private ElementCompositionTypeEnum b_decompositionType = ElementCompositionTypeEnum.UNASSIGNED;
    public ElementCompositionTypeEnum DecompositionType
    {
        get
        {
            if (b_decompositionType == ElementCompositionTypeEnum.UNASSIGNED)
            {
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate.ToString() == SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Intentional_Element_decomposition_type").ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_decompositionType = ElementCompositionTypeEnum.UNASSIGNED;
                else if (SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object).ToUpper().Contains("IOR"))
                    b_decompositionType = ElementCompositionTypeEnum.IOR;
                else if (SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object).ToUpper().Contains("AND"))
                    b_decompositionType = ElementCompositionTypeEnum.AND;
                else if (SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object).ToUpper().Contains("XOR"))
                    b_decompositionType = ElementCompositionTypeEnum.XOR;

            }

            return b_decompositionType;
        }
        set
        {
            // Only if the backing field changes
            if (b_decompositionType != value)
            {
                // Set new value
                b_decompositionType = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ElementCompositionTypeChangedEventArgs();
                OnElementCompositionTypeChanged(this, eventArgs);
            }
        }
    }

    // Returns all IntentionalELements (nodes or IntentionalElementModels) of which this IntentionalElement is part of
    private INode[] b_partOfNodes;
    public INode[] PartOfNodes
    {
        get
        {
            if (b_partOfNodes == null)
            {
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate == SemanticNode.Graph.CreateUriNode(UriFactory.Create(SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Part_Of"))).ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_partOfNodes = null;
                else
                {
                    List<INode> relevantNodesFromTheTriples = new List<INode>();

                    foreach (Triple triple in relevantTriples)
                        relevantNodesFromTheTriples.Add(triple.Object);

                    b_partOfNodes = relevantNodesFromTheTriples.ToArray();
                }

            }

            return b_partOfNodes;
        }
        set
        {
            // Only if the backing field changes
            if (b_partOfNodes != value)
            {
                // Set new value
                b_partOfNodes = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new PartOfChangedEventArgs();
                OnPartOfChanged(this, eventArgs);
            }
        }
    }
    public IntentionalElementModel[] PartOf
    {
        get
        {
            return SMWParser.Instance.GetIntentionalElementPartOf(this);
        }
        private set { }
    }

    // Returns all IntentionalELements (nodes or IntentionalElementModels) that are part of this IntentionalElement
    private INode[] b_partOfThisNodes;
    public INode[] PartOfThisNodes
    {
        get
        {
            if (b_partOfThisNodes == null)
            {
                Triple[] relevantTriples = SemanticNode.Graph.GetTriplesWithPredicateObject(SemanticNode.Graph.CreateUriNode(UriFactory.Create(SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Supercontext")), this.SemanticNode).ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_partOfThisNodes = null;
                else
                {
                    List<INode> relevantNodesFromTheTriples = new List<INode>();

                    foreach (Triple triple in relevantTriples)
                        relevantNodesFromTheTriples.Add(triple.Subject);

                    b_partOfThisNodes = relevantNodesFromTheTriples.ToArray();
                }
            }

            return b_partOfThisNodes;
        }
        set
        {
            // Only if the backing field changes
            if (b_partOfThisNodes != value)
            {
                // Set new value
                b_partOfThisNodes = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new PartOfThisChangedEventArgs();
                OnPartOfThisChanged(this, eventArgs);
            }
        }
    }
    public IntentionalElementModel[] PartOfThis
    {
        get
        {
            return SMWParser.Instance.GetIntentionalElementPartOfThis(this);
        }
        private set { }
    }

    
    // Returns all IntentionalELements (nodes or IntentionalElementModels) of which this IntentionalElement is a instance of
    private INode[] b_instanceOfNodes;
    public INode[] InstanceOfNodes
    {
        get
        {
            if (b_instanceOfNodes == null)
            {
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate == SemanticNode.Graph.CreateUriNode(UriFactory.Create(SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Instance_Of"))).ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_instanceOfNodes = null;
                else
                {
                    List<INode> relevantNodesFromTheTriples = new List<INode>();

                    foreach (Triple triple in relevantTriples)
                        relevantNodesFromTheTriples.Add(triple.Object);

                    b_instanceOfNodes = relevantNodesFromTheTriples.ToArray();
                }

            }

            return b_instanceOfNodes;
        }
        set
        {
            // Only if the backing field changes
            if (b_instanceOfNodes != value)
            {
                // Set new value
                b_instanceOfNodes = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new InstanceOfChangedEventArgs();
                OnInstanceOfChanged(this, eventArgs);
            }
        }
    }
    public IntentionalElementModel[] InstanceOf
    {
        get
        {
            return SMWParser.Instance.GetIntentionalElementInstanceOf(this);
        }
        private set { }
    }

    // Returns all IntentionalELements (nodes or IntentionalElementModels) that are instances of this IntentionalElement
    private INode[] b_instanceOfThisNodes;
    public INode[] InstanceOfThisNodes
    {
        get
        {
            if (b_instanceOfThisNodes == null)
            {
                Triple[] relevantTriples = SemanticNode.Graph.GetTriplesWithPredicateObject(SemanticNode.Graph.CreateUriNode(UriFactory.Create(SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Instance_Of")), this.SemanticNode).ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_instanceOfThisNodes = null;
                else
                {
                    List<INode> relevantNodesFromTheTriples = new List<INode>();

                    foreach (Triple triple in relevantTriples)
                        relevantNodesFromTheTriples.Add(triple.Subject);

                    b_instanceOfThisNodes = relevantNodesFromTheTriples.ToArray();
                }
            }

            return b_instanceOfThisNodes;
        }
        set
        {
            // Only if the backing field changes
            if (b_instanceOfNodes != value)
            {
                // Set new value
                b_instanceOfNodes = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new InstanceOfThisChangedEventArgs();
                OnInstanceOfThisChanged(this, eventArgs);
            }
        }
    }
    public IntentionalElementModel[] InstanceOfThis
    {
        get
        {
            return SMWParser.Instance.GetIntentionalElementInstanceOfThis(this);
        }
        private set { }
    }


    // Returns all IntentionalELements (nodes or IntentionalElementModels) of which this IntentionalElement is a concerned
    private INode[] b_concernsNodes;
    public INode[] ConcernsNodes
    {
        get
        {
            if (b_concernsNodes == null)
            {
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate == SemanticNode.Graph.CreateUriNode(UriFactory.Create(SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Concerns"))).ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_concernsNodes = null;
                else
                {
                    List<INode> relevantNodesFromTheTriples = new List<INode>();

                    foreach (Triple triple in relevantTriples)
                        relevantNodesFromTheTriples.Add(triple.Object);

                    b_concernsNodes = relevantNodesFromTheTriples.ToArray();
                }

            }

            return b_concernsNodes;
        }
        set
        {
            // Only if the backing field changes
            if (b_concernsNodes != value)
            {
                // Set new value
                b_concernsNodes = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ConcernsChangedEventArgs();
                OnConcernsChanged(this, eventArgs);
            }
        }
    }
    public IntentionalElementModel[] Concerns
    {
        get
        {
            return SMWParser.Instance.GetIntentionalElementConcerns(this);
        }
        private set { }
    }

    // Returns all IntentionalELements (nodes or IntentionalElementModels) that are concerned of this IntentionalElement
    private INode[] b_concernsThisNodes;
    public INode[] ConcernsThisNodes
    {
        get
        {
            if (b_concernsThisNodes == null)
            {
                Triple[] relevantTriples = SemanticNode.Graph.GetTriplesWithPredicateObject(SemanticNode.Graph.CreateUriNode(UriFactory.Create(SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Concerns")), this.SemanticNode).ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_concernsThisNodes = null;
                else
                {
                    List<INode> relevantNodesFromTheTriples = new List<INode>();

                    foreach (Triple triple in relevantTriples)
                        relevantNodesFromTheTriples.Add(triple.Subject);

                    b_concernsThisNodes = relevantNodesFromTheTriples.ToArray();
                }
            }

            return b_concernsThisNodes;
        }
        set
        {
            // Only if the backing field changes
            if (b_concernsNodes != value)
            {
                // Set new value
                b_concernsNodes = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ConcernsThisChangedEventArgs();
                OnConcernsThisChanged(this, eventArgs);
            }
        }
    }
    public IntentionalElementModel[] ConcernsThis
    {
        get
        {
            return SMWParser.Instance.GetIntentionalElementConcernsThis(this);
        }
        private set { }
    }


    private Contributes[] b_contributes;
    public Contributes[] Contributes
    {
        get
        {
            if (b_contributes == null)
                return SMWParser.Instance.GetContributesForIntentionalElementNode(SemanticNode).ToArray();
            else
                return b_contributes;
        }
        set
        {
            // Only if the backing field changes
            if (b_contributes != value)
            {
                // Set new value
                b_contributes = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ContributesChangedEventArgs();
                OnContributesChanged(this, eventArgs);
            }
        }
    }
   
    private Dependency[] b_depends;
    public Dependency[] Depends
    {
        get {
            if (b_depends == null)
                return SMWParser.Instance.GetDependencyForIntentionalElementNode(SemanticNode).ToArray();
            else
                return b_depends;
        }
        set
        {
            // Only if the backing field changes
            if (b_depends != value)
            {
                // Set new value
                b_depends = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new DependsChangedEventArgs();
                OnDependsChanged(this, eventArgs);
            }
        }
    }

    // Reset tripleDataCaches, by emptying the caches for each tripledata related variable for this object
    protected override void ResetTripleData()
    {
        // Wipe all know cach data and request data (this triggers new caching)

        base.ResetTripleData();

        this.b_context = null;
        this.b_context = this.Context;

        this.b_decompositionType = ElementCompositionTypeEnum.UNASSIGNED;
        this.b_decompositionType = this.DecompositionType;

        this.b_intentionalElementType = IntentionalElementTypeEnum.UNASSIGNED;
        this.b_intentionalElementType = this.IntentionalElementType;

        this.b_partOfNodes = null;
        this.b_partOfNodes = this.PartOfNodes;

        this.b_instanceOfNodes = null;
        this.b_instanceOfNodes = this.InstanceOfNodes;

        this.b_concernsNodes = null;
        this.b_concernsNodes = this.ConcernsNodes;

        this.b_contributes = null;
        this.b_contributes = this.Contributes;

        this.b_depends = null;
        this.b_depends = this.Depends;
    }

    public event EventHandler<ContextChangedEventArgs> OnContextChanged = (sender, e) => { };
    public event EventHandler<IntentionalElementTypeChangedEventArgs> OnIntentionalElementTypeChanged = (sender, e) => { };
    public event EventHandler<ElementCompositionTypeChangedEventArgs> OnElementCompositionTypeChanged = (sender, e) => { };
    public event EventHandler<DecompositionTypeChangedEventArgs> OnDecompositionTypeChanged = (sender, e) => { };
    public event EventHandler<PartOfChangedEventArgs> OnPartOfChanged = (sender, e) => { };
    public event EventHandler<PartOfThisChangedEventArgs> OnPartOfThisChanged = (sender, e) => { };
    public event EventHandler<InstanceOfChangedEventArgs> OnInstanceOfChanged = (sender, e) => { };
    public event EventHandler<InstanceOfThisChangedEventArgs> OnInstanceOfThisChanged = (sender, e) => { };
    public event EventHandler<ConcernsChangedEventArgs> OnConcernsChanged = (sender, e) => { };
    public event EventHandler<ConcernsThisChangedEventArgs> OnConcernsThisChanged = (sender, e) => { };
    public event EventHandler<ContributesChangedEventArgs> OnContributesChanged = (sender, e) => { };
    public event EventHandler<DependsChangedEventArgs> OnDependsChanged = (sender, e) => { };
}
