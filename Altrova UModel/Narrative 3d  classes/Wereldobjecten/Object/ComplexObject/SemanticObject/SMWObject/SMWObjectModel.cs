﻿using Object.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using VDS.RDF;

public class SMWObjectModel : SemanticObjectModel, ISMWObjectModel
{
    // Constructor
    public SMWObjectModel(string entityID, INode semanticNode) : base(entityID, semanticNode)
    {
        InitialiseCategoryData(FindSetCategory(), true);
    }

    // Constructor
    public SMWObjectModel(string entityID, INode semanticNode, SMWCategory smwCategory, NavObjectModel navObject) : base(entityID, semanticNode, navObject)
    {
        InitialiseCategoryData(smwCategory, true);
        this.b_navObject = navObject;
    }

    // Constructor
    public SMWObjectModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
    }

    // Constructor
    public SMWObjectModel(BaseView view, bool syncViewToModel, NavObjectModel navObject) : base(view, syncViewToModel, navObject)
    {
        this.b_navObject = navObject;
    }

    // Category this SMWObject instantiates
    private SMWCategory b_category;
    public SMWCategory Category
    {
        get
        {
            if (b_category == null)
                b_category = FindSetCategory();
            
            return b_category;
        }
        set
        {
            // Only if the backing field changes
            if (b_category != value)
            {
                // Set new value
                b_category = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new CategoryChangedEventArgs();
                OnCategoryChanged(this, eventArgs);
            }
        }
    }

    // List of triple holding the data
    public sealed override List<Triple> TripleData
    {
        get
        {
            if (b_tripleData == null || b_tripleData.Count <= 0)
            {
                // OLD -> b_tripleData = SMWParser.Instance.GetCategoryInstancesTripleData(category, 0)[semanticNode];
                b_tripleData = Category.GetSpecificInstanceTriples(this.SemanticNode);

                // Set all cached tripledata to null
                ResetTripleData();
            }

            return b_tripleData;
        }
        set
        {
            b_tripleData = value;

            // Set all cached tripledata to null
            ResetTripleData();
        }
    }

    // Structured text consist out of existing paragraphheadings en alineas:
    private string b_paragraphHeadNL;
    public string ParagraphHeadNL
    {
        get
        {
            if (b_paragraphHeadNL == null)
            {
                
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate.ToString() == SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Heading_nl").ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_paragraphHeadNL = null;
                else
                    b_paragraphHeadNL = SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object);
            }

            return b_paragraphHeadNL;
        }
        set
        {
            // Only if the backing field changes
            if (b_paragraphHeadNL != value)
            {
                // Set new value
                b_paragraphHeadNL = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ParagraphHeadNLChangedEventArgs();
                OnParagraphHeadNLChanged(this, eventArgs);
            }
        }
    }

    private string b_paragraphHeadEN;
    public string ParagraphHeadEN
    {
        get
        {
            if (b_paragraphHeadNL == null)
            {
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate.ToString() == SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Heading_en").ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_paragraphHeadEN = null;
                else
                    b_paragraphHeadEN = SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object);
            }

            return b_paragraphHeadEN;
        }
        set
        {
            // Only if the backing field changes
            if (b_paragraphHeadEN != value)
            {
                // Set new value
                b_paragraphHeadEN = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ParagraphHeadENChangedEventArgs();
                OnParagraphHeadENChanged(this, eventArgs);
            }
        }
    }

    private Struct_SMW_Alinea[] b_alineas;
    public Struct_SMW_Alinea[] Alineas
    {
        get
        {
            if (b_alineas == null)
                return SMWParser.Instance.GetAlineaForSMWObjectNode(SemanticNode).ToArray();
            else
                return b_alineas;
        }
        set
        {
            // Only if the backing field changes
            if (b_alineas != value)
            {
                // Set new value
                b_alineas = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new AlineasChangedEventArgs();
                OnAlineasChanged(this, eventArgs);
            }
        }
    }

    private Struct_SMW_Imagary[] b_imagary;
    public Struct_SMW_Imagary[] Imagary
    {
        get
        {
            if (b_imagary == null)
                return SMWParser.Instance.GetImagaryForSMWObjectNode(SemanticNode).ToArray();
            else
                return b_imagary;
        }
        set
        {
            // Only if the backing field changes
            if (b_imagary != value)
            {
                // Set new value
                b_imagary = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ImagaryChangedEventArgs();
                OnImagaryChanged(this, eventArgs);
            }
        }
    }

    // NON-SEMANTIC (Can't retrieve from RDF)
    private string b_freeText;
    public string FreeText
    {
        get
        {
            if (b_paragraphHeadNL == null)
            {
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate.ToString() == SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "free_text").ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_freeText = null;
                else
                    b_freeText = SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object);
            }

            return b_freeText;
        }
        set
        {
            // Only if the backing field changes
            if (b_freeText != value)
            {
                // Set new value
                b_freeText = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new FreeTextChangedEventArgs();
                OnFreeTextChanged(this, eventArgs);
            }
        }
    }

    // NON-SEMANTIC (Can't retrieve from RDF)
    private string b_summery;
    public string Summery
    {
        get
        {
            if (b_paragraphHeadNL == null)
            {
                Triple[] relevantTriples = TripleData.Where(t => t.Predicate.ToString() == SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Summary").ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_summery = null;
                else
                    b_summery = SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object);
            }

            return b_summery;
        }
        set
        {
            // Only if the backing field changes
            if (b_summery != value)
            {
                // Set new value
                b_summery = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new SummeryChangedEventArgs();
                OnSummeryChanged(this, eventArgs);
            }
        }
    }

    // Initialises the model, by connecting applying the tripledata and defining it's category
    private void InitialiseCategoryData(SMWCategory category, bool resetTripleDataCaches)
    {
        // Refer to it's subject INode and SMWCategory
        this.Category = category;

        // Set tripledata,  based on the category and INode
        //if (category.GetAllInstanceTriples().Keys.Contains(this.semanticNode))
        //    this.tripleData = category.GetAllInstanceTriples()[this.semanticNode];
        b_tripleData = category.GetSpecificInstanceTriples(this.SemanticNode);

        if (resetTripleDataCaches)
            ResetTripleData();
    }

    // Reset tripleDataCaches, by emptying the caches for each tripledata related variable for this object
    protected override void ResetTripleData()
    {
        // Wipe all know cach data and request data (this triggers new caching)
        base.ResetTripleData();

        this.b_paragraphHeadNL = null;
        this.b_paragraphHeadNL = this.ParagraphHeadNL;

        this.b_paragraphHeadEN = null;
        this.b_paragraphHeadEN = this.ParagraphHeadEN;

        this.b_alineas = null;
        this.b_alineas = this.Alineas;

        this.b_imagary = null;
        this.b_imagary = this.Imagary;
    }

    // Returns all smeantic models that have a relation with this model
    public override SemanticObjectModel GetRelatedSemanticModels()
    {
        return null; // no current relations can be defined
    }

    // Find, cache and return the category which this is an instance of
    private SMWCategory FindSetCategory()
    {
        // if there is a know category return that one and stop this methode
        if (b_category != null)
            return b_category;

        // If no category is know to this node, than search, cache and return this category
        this.Category = SMWParser.Instance.GetCategoryOfAInstanceByInstanceNode(SemanticNode, ObjectType);

        // Return cached category (this is the b_category instead of category, to prevent infinite loops)
        return b_category;
    }

    // Dispatched EventHandlers for category
    public event EventHandler<CategoryChangedEventArgs> OnCategoryChanged = (sender, e) => { };

    // Dispatch EventHandlers for SWM pageform info changes
    public event EventHandler<ParagraphHeadNLChangedEventArgs> OnParagraphHeadNLChanged = (sender, e) => { };
    public event EventHandler<ParagraphHeadENChangedEventArgs> OnParagraphHeadENChanged = (sender, e) => { };
    public event EventHandler<AlineasChangedEventArgs> OnAlineasChanged = (sender, e) => { };
    public event EventHandler<ImagaryChangedEventArgs> OnImagaryChanged = (sender, e) => { };
    public event EventHandler<FreeTextChangedEventArgs> OnFreeTextChanged = (sender, e) => { };
    public event EventHandler<SummeryChangedEventArgs> OnSummeryChanged = (sender, e) => { };

}
