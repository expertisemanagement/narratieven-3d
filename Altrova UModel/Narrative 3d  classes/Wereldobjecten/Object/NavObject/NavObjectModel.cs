﻿using Object.Type;
using System;

public class NavObjectModel : ObjectModel, IObjectModel
{
    // Constructor
    public NavObjectModel(string entityID, SemanticObjectModel semanticObjectModel) : base(entityID)
    {
        this.SemanticObject = semanticObjectModel;
    }

    // Constructor
    public NavObjectModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
    }

    // Semantic model this navigation object refers to
    protected SemanticObjectModel b_semanticObject;
    public SemanticObjectModel SemanticObject
    {
        get
        {
           return b_semanticObject;

        }
        set
        {
            // Only if the backing field changes
            if (b_semanticObject != value)
            {
                // Set new value
                b_semanticObject = value;

                // Set the current navObject of this semanticObject to this navobject
                SemanticObject.NavObject = this;

                // Dispatch the deticated "changed" event
                var eventArgs = new SemanticObjectChangedEventArgs();
                OnSemanticObjectChanged(this, eventArgs);
            }
        }
    }

    // Dispatch EventHandlers for objectControlSettings
    public event EventHandler<SemanticObjectChangedEventArgs> OnSemanticObjectChanged = (sender, e) => { };
}
