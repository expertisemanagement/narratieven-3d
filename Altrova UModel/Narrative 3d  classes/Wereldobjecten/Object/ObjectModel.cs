﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object.Type;
using Object.GUIstate;

public class ObjectModel : BaseModel, IObjectModel
{
    // Constructor
    public ObjectModel(string entityID) : base(entityID)
    {
        SetDefaultObjectType();
    }

    // Constructor
    public ObjectModel(string entityID, ObjectTypeEnum objectType) : base(entityID)
    {
        SetDefaultObjectType();
        if(objectType != ObjectTypeEnum.UNASSIGNED)
            this.b_objectType = objectType;
    }

    // Constructor
    public ObjectModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
        SetDefaultObjectType();
        if (syncViewToModel == false)
            this.b_objectType = ((ObjectView)Controller.View).ObjectType;
    }

    [NonSerialized]
    // Parent ObjectModels
    protected List<ObjectModel> b_parents = new List<ObjectModel>();
    public List<ObjectModel> Parents
    {
        get { return b_parents; }
        set
        {
            // Only if the backing field changes
            if (b_parents != value)
            {
                // Set new value
                b_parents = value;

                 // Dispatch the deticated "changed" event
                var eventArgs = new ParentsChangedEventArgs();
                OnParentsChanged(this, eventArgs);
            }
        }
    }

    // List of Children ObjectModels
    protected List<ObjectModel> b_childs = new List<ObjectModel>();
    public List<ObjectModel> Childs
    {
        get { return b_childs; }
        set
        {
            // Only if the backing field changes
            if (b_childs != value)
            {
                // Set new value
                b_childs = value;

                 // Dispatch the deticated "changed" event
                var eventArgs = new ChildsChangedEventArgs();
                OnChildsChanged(this, eventArgs);
            }
        }
    }

    // Object's position in the scene
    private Vector3 b_Position = new Vector3(0, 0, 0);
    public Vector3 Position
    {
        get { return b_Position; }
        set
        {
            // Only if the backing field changes
            if (b_Position != value)
            {
                // Set new value
                b_Position = value;

                 // Dispatch the deticated "changed" event
                var eventArgs = new PositionChangedEventArgs();
                OnPositionChanged(this, eventArgs);
            }
        }
    }

    // Object's rotation in the scene
    private Quaternion b_Rotation = new Quaternion(0, 0, 0, 0);
    public Quaternion Rotation
    {
        get { return b_Rotation; }
        set
        {
            // Only if the rotation changes
            if (b_Rotation != value)
            {
                // Set new rotation
                b_Rotation = value;

                // Dispatch the 'rotation changed' event
                var eventArgs = new RotationChangedEventArgs();
                OnRotationChanged(this, eventArgs);
            }
        }
    }

    // Object's scale in the scene
    private Vector3 b_Scale = new Vector3(0, 0, 0);
    public Vector3 Scale
    {
        get { return b_Scale; }
        set
        {
            // Only if the rotation changes
            if (b_Scale != value)
            {
                // Set new rotation
                b_Scale = value;

                // Dispatch the 'rotation changed' event
                var eventArgs = new ScaleChangedEventArgs();
                OnScaleChanged(this, eventArgs);
            }
        }
    }

    // field that defines the objects Type
    protected ObjectTypeEnum b_objectType = ObjectTypeEnum.UNASSIGNED;
    public ObjectTypeEnum ObjectType
    {
        get { return b_objectType; }
        set
        {
            // Only if the backing field changes
            if (b_objectType != value)
            {
                // Set new value
                b_objectType = value;

                 // Dispatch the deticated "changed" event
                var eventArgs = new TypeChangedEventArgs();
                OnTypeChanged(this, eventArgs);
            }
        }
    }

    // object guiState (for visual feedback on selcing in the navigation or showing it is being focessed) 
    protected ObjectGUIStateEnum b_GUIState = ObjectGUIStateEnum.INACTIVE;
    public ObjectGUIStateEnum GUIState
    {

        get { return b_GUIState; }
        set
        {
            // Only if the backing field changes
            if (b_GUIState != value)
            {
                // Set new value
                b_GUIState = value;

                 // Dispatch the deticated "changed" event
                var eventArgs = new GUIStateChangedEventArgs();
                OnGUIStateChanged(this, eventArgs);
            }
        }
    }

    // Sets the default ObjectType for this object
    protected void SetDefaultObjectType()
    {
        if (this.GetType() == typeof(PracticeModel))
            this.b_objectType = ObjectTypeEnum.PRACTICE;

        else if (this.GetType() == typeof(ContextModel))
            this.b_objectType = ObjectTypeEnum.CONTEXT;

        else if (this.GetType() == typeof(IntentionalElementModel))
            this.b_objectType = ObjectTypeEnum.INTENTIONALELEMENT;

        else if (this.GetType() == typeof(ActorModel))
            this.b_objectType = ObjectTypeEnum.ACTOR;

        else if (this.GetType() == typeof(ActivityModel))
            this.b_objectType = ObjectTypeEnum.ACTIVITY;

        else if (this.GetType() == typeof(SMWObjectModel))
            this.b_objectType = ObjectTypeEnum.SMWOBJECT;

        else if (this.GetType() == typeof(SemanticObjectModel))
            this.b_objectType = ObjectTypeEnum.SEMANTICOBJECT;

        else if (this.GetType() == typeof(ComplexObjectModel))
            this.b_objectType = ObjectTypeEnum.COMPLEXOBJECT;

        else if (this.GetType() == typeof(NavObjectModel))
            this.b_objectType = ObjectTypeEnum.NAVOBJECT;

        else if (this.GetType() == typeof(CameraModel))
            this.b_objectType = ObjectTypeEnum.CAMERA;
        else
            this.b_objectType = ObjectTypeEnum.UNASSIGNED;
    }

    // Finds for an existing controller
    public sealed override BaseController FindSetController()
    {
        if (b_controller == null)
        {
            if (this is SemanticObjectModel)
            {
                // Looks up for the existence of a controller with the same objectURI
                SemanticObjectController semanticController = ObjectFactory.Instance.GetSemanticObjectControllerByURI(((SemanticObjectModel)this).ObjectURI);

                if (semanticController != null)
                {
                    this.Controller = semanticController;
                    return Controller;
                }
                else
                    return null;
            }

            else
            {
                // Looks up for the existence of a controller with the same objectURI
                BaseController controller = ObjectFactory.Instance.GetObjectControllerByEntityID(EntityID);

                if (controller != null)
                {
                    this.Controller = controller;
                    return controller;
                }
                else
                    return null;
            }
        }
        else
        {
            return b_controller;
        }
    }

    // Finds for an existing controller, else creates one
    public override BaseController FindCreateSetController()
    {
        // Looks up for the existence of a view with the same entityID
        if (FindSetController() != null)
            return FindSetController();

        else if (this is SemanticObjectModel)
        {
            this.Controller = ObjectFactory.Instance.controllerFactory.createForSemanticObject(((SemanticObjectModel)this), false);
            return this.Controller;
        }

        else
        {
            this.Controller = ObjectFactory.Instance.controllerFactory.createForObject(this, false);
            return this.Controller;
        }
    }

    // Dispatch EventHandler for Parent & Child node(s) refferences changes
    public event EventHandler<ParentsChangedEventArgs> OnParentsChanged = (sender, e) => { };
    public event EventHandler<ChildsChangedEventArgs> OnChildsChanged = (sender, e) => { };

    // Dispatch EventHandlers for Object Scene placement changes
    public event EventHandler<PositionChangedEventArgs> OnPositionChanged = (sender, e) => { };
    public event EventHandler<RotationChangedEventArgs> OnRotationChanged = (sender, e) => { };
    public event EventHandler<ScaleChangedEventArgs> OnScaleChanged = (sender, e) => { };

    // Dispatch EventHandler for object's machine and GUI state changes
    public event EventHandler<TypeChangedEventArgs> OnTypeChanged = (sender, e) => { };
    public event EventHandler<GUIStateChangedEventArgs> OnGUIStateChanged = (sender, e) => { };
}
