﻿using System.Collections.Generic;
using Object.CameraObject.ActionInput;

namespace Object.CameraZoomInput
{
    [System.Serializable]
    public struct CameraActionInput
    {
        // level of smoothing (0 == no adding of fleuncy, 0 > Adding fleuncy)
        public int amountOfSmooothing;

        // Describes the objectMovement of concern
        public CameraActionInputEnum actionInput;

        // Describes the inputKey variable from the Unity inputmanager, that controls the objectMovement
        public List<string> inputKeyAxes;
    }
}

