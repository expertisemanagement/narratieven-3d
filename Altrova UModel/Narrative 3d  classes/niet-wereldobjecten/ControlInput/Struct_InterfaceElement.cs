﻿using Interface.ActivationControl;
using UnityEngine;
using System.Collections.Generic;

namespace Interface.Element
{
    [System.Serializable]
    public struct InterfaceElement
    {
        // Describes the type of interfaceElement
        public InterfaceElementEnum interfaceElement;

        // Holds the interfaceElement gameObject
        public GameObject interfaceElementGameObject;

        // Holds the interfaceElement gameObject
        public ActivationControl[] activationControls;

        // List of interfacesElements to disable on activation of this 
        public List<InterfaceElementEnum> interfacesElementsRequiredForThisActivation;

        // List of interfacesElements to disable on activation of this 
        public List<InterfaceElementEnum> interfacesElementsToDisableInputOnThisActive;

    }

    public struct ActivationControl
    {
        // Describes the type of interfaceElement
        public InterfaceActivationControlEnum interfaceElement;

        // Input axes defined by the inputManager. For example, "Horizontal_Move" or "Jump". 
        public string inputManagerAxes;
    }
}
