﻿namespace Object
{
    namespace Type
    {
        // Enumeration for each object type. These are EMont elements, worlddecoration and wikidocumentation (non-EMont representing wikipage, images, text or video/audio)
        public enum ObjectTypeEnum {

            // Foundation objecttype for all objects
            OBJECT,

                // A complex basic for an controllable object
                COMPLEXOBJECT,

                    // Foundation objecttype hierarchy for defining the 
                    SEMANTICOBJECT, 
                        SMWOBJECT,
                            CONTEXT,
                                //CONTEXTCASE,
                                //RELATABLESITUATION,
                                //SSMSITUATION,
                            INTENTIONALELEMENT,
                                ACTIVITY,
                                    //OPERATIONALPERSPECTIVE,
                                ACTOR,
                                    //ORGANISATION,
                                    //PERSON,
                                //BELIEF,
                                //CONDITION,
                                //GOAL,
                                //OUTCOME,
                            PRACTICE,

                    // Camera objecttype used for defining view perspectives
                    CAMERA,

            // A object (3D node) within the navigation
            NAVOBJECT,

            // Worlddecoration objecttype marks a object as being just decoration
            WORLDDECORATION,

            // Placeholder objectType for when there isn't yet assigned a real type
            UNASSIGNED
        };

            // OLD EMONTACTOR, EMONTACTIVITY, PRACTICE, EMONTGOAL, EMONTWORLDVIEW, EMONTCONTEXT, EMONTCONTEXTROLL, EMONTPRODUCT, WORLDDECORATION, WIKIDOCUMENTATION, CAMERA };
    }

    namespace FSM
    {
        // Enumeration for each finite-state machine (FSM) of the object in the world (a.k.a machine state)
        public enum ObjectFSMEnum { AIINTERACTIVE, AINONINTERACTIVE, STATICINTERACTIVE, STATICNONINTERACTIVE, PLAYERCONTROLLED, HIDDEN };
    }

    namespace Movement
    {
        // Enumeration  for each possible object control setup
        public enum ObjectMovementEnum { MOVELATERAL, MOVEVERTICAL, MOVELONGITUDINAL, ROTATEYAW, ROTATEPITCH, ROTATEROLL, ROTATEAROUNDPOLAR, ROTATEAROUNDAZIMUTHAL, JUMP, RUN, CROUCH, LAYDOWN };
    }

    namespace MovementSpace
    {
        // Enumeration for the possible space for the relative movement
        public enum ObjectMovementSpaceEnum { WORLD, LOCAL};
    }

    namespace MovementRestriction
    {
        public enum ObjectMovementRestrictionEnum { LOCKEDPOSITION, LOCKEDROTATION, LOCKEDROTATIONAROUND, LOCKEDZOOM }
    }

    namespace CameraObject
    {
        namespace ActionInput
        {
            // Enumeration for each possible camera action input
            public enum CameraActionInputEnum { ZOOMIN, ZOOMOUT, ZOOMING }
        }
    }

    namespace ControlSetup
    {
        // Enumeration for each possible object control setup
        public enum ControlSetupEnum { DEFAULT, CUSTOM }
    }

    namespace GUIstate
    {
        // Enumeration for each special state of the object in the navigation (GUI state)
        public enum ObjectGUIStateEnum { ACTIVE, INACTIVE, HIGHLIGHTED, HIDDEN };
    }

    namespace Transport
    {
        namespace Type
        {
            // Enumeration for each possible type of transport (this is used for AI movement)
            public enum ObjectTransportTypeEnum { ONFOOT, WHEELS, PLANE, BOAT, SUB };
        }

        namespace TravelSpace
        {
            namespace SpaceType
            {
                // Enumeration for controlling the transport (RESTSPACE = chair/bench/bed/pod a.k.a non-movement zone, MOVESPACE = non-RESTSPACE)
                public enum ObjectTravelSpaceTypeEnum { RESTSPACE, MOVESPACE};
            }

            namespace ControlType
            {
                // Enumeration for controlling the transport
                public enum ObjectTransportControlTypeEnum { COMMAND, PASSENGER, CARGO };
            }
        }
    }
}

namespace ObjectControl
{
    namespace Type
    {
        // Enumeration for each Camera in the scene
        public enum ControlTypeEnum { OBJECTCONTROL, CAMERACONTROL};
    }
}

namespace SCamera
{
    namespace Type
    {
        // Enumeration for each Camera in the scene
        public enum CameraTypeEnum { FIRSTPERSON, THIRTPERSON, OMNISCENT, MINIMAP, BROWSERNAVIGATION };
    }

    namespace State
    {
        // Enumeration for each Camera in the scene
        public enum CameraStateEnum { MANUAL, AUTO};
    }
}

namespace Interface
{
    namespace Element
    {
        // Enumeration for each interfaceElement in the scene
        public enum InterfaceElementEnum { MINIMAP, BIGMAP, BROWSERNAVIGATION, PROJECTBROWSER, RECRODINGHUD, SIMPLEUISELECTOR, PROTAGONISTBROWSER };
    }

    namespace ActivationControl
    {
        // Enumeration for each interfaceElement in the scene
        public enum InterfaceActivationControlEnum { TOGGLE, HOLD, QUIT };
    }
}

namespace Dimensions
{
    // Enummeration of indicating a specific dimension 
    public enum DimensionEnum { WORLD, NAV, UNASSIGNED };
}


namespace SMW
{
    namespace Alinea
    {
        namespace Language
        {
            public enum AlineaLanguageSupportEnum { UNKOWN, DUTCH, ENGLISH, DUTCH_ENGLISH };
        }

        namespace Imagary
        {
            public enum AlineaImagaryTypeEnum { IMAGE, PHOTO, MAP, TBD, UNASSIGNED };
        }
    }

    namespace Relation
    {

        namespace Practice
        {
            public enum PracticeTypeEnum { PRACTICE, EXPERIENCE, UNASSIGNED }
            
        }

        namespace IntetionalELement
        {
            public enum IntentionalElementTypeEnum { GOAL, OUTCOME, BELIEF, CONDITION, ACTIVITY, ACTOR, UNASSIGNED }

            public enum ElementCompositionTypeEnum { AND, XOR, IOR, UNASSIGNED };

        }

        namespace Activity
        {
            public enum ConnectionTypeEnum { SEQ, PAR, JOIN, SYNC, PRODUCES, CONSUMES, UNASSIGNED };
        }

        public enum SelectionTypeEnum { SELECTS, REJECTS, UNASSIGNED };
        public enum ContributionValueEnum { PLUS_MINUS, MIN_MIN_MIN, MIN_MIN, MIN, ZERO, PLUS, PLUS_PLUS, PLUS_PLUS_PLUS, UNASSIGNED };
    }

    namespace EMM
    {
        namespace Context
        {
            public enum ContextTypeEnum { SITUATION, ROLE, UNASSIGNED };
        }
    }
}