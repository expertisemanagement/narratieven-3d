﻿using SMW.Relation;
using System.Linq;
using System;

[System.Serializable]
public class Contributes : Relation
{
    public ContributionValueEnum contributionValue;
}
