﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VDS.RDF;

public class Relation {

    // this is the semantic uri
    public string elementLink;

    public string note;

    // Returns the IntentionalElementModel that has 
    public IntentionalElementModel GetElementLink()
    {
        SemanticObjectModel foundRelationObjectModel = ObjectFactory.Instance.GetSemanticObjectModelByURI(elementLink);

        if (foundRelationObjectModel == null)
        {
            INode foundRelationObjectNode = SMWParser.Instance.GetNodeByObjectURI(elementLink);

            if (foundRelationObjectNode != null)
                return (IntentionalElementModel)ObjectFactory.Instance.modelFactory.createForIntentionalElement(foundRelationObjectNode, null, null);
            else
                return null;
        }

        return (IntentionalElementModel) foundRelationObjectModel;
    }

}
