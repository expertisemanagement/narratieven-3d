﻿using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EditorProjectManager
{
    public class EditorProjectManager
    {
        [MenuItem("ProjectManager/Create new project")]
        static void OpenNewProjectCreationWindow()
        {
            ProjectManagerCreateProjectWindow window = ScriptableObject.CreateInstance <ProjectManagerCreateProjectWindow>();
            window.ShowUtility();
        }

        [MenuItem("ProjectManager/Manage ProjectAssetBundle")]
        static void OpenNewProjectAssetBundleGenerationWindow()
        {
            ProjectManagerGenerateImportableAssetBundlesWindow window = ScriptableObject.CreateInstance < ProjectManagerGenerateImportableAssetBundlesWindow>();
            window.ShowUtility();
        }
    }

    public class ProjectManagerGenerateImportableAssetBundlesWindow : EditorWindow
    {
        public string widowTitle = "Generate importable AssetsBundles";
        public string editorWindowText = "Generate/Update AssetBundle for the following existing projects: ";

        public ProjectManagerGenerateImportableAssetBundlesWindow()
        {
            this.titleContent = new GUIContent(widowTitle);
        }

        void OnGUI()
        {
            if (Directory.Exists(ProjectManagerGlobals.defaultPathProjectsResourcesDirectory))
            {
                List<string> allChildFolders = new List<string>();

                foreach (string folder in Directory.GetDirectories(ProjectManagerGlobals.defaultPathProjectsResourcesDirectory))
                {
                    if (Path.GetFileName(folder) != ProjectManagerGlobals.defaultAssetBundlesDirectoryName)
                    {
                        Debug.Log(Path.GetFileName(folder));
                        allChildFolders.Add(Path.GetFileName(folder));
                    }
                }

                if (allChildFolders.Count > 0)
                {
                    EditorGUILayout.LabelField(editorWindowText);
                    
                    foreach (string folderName in allChildFolders)
                    {
                        if (GUILayout.Button("Create/Update AssetBundle for " + folderName))
                            GenerateNewAssetBundles(folderName);
                    }
                }
                else {
                    EditorGUILayout.LabelField("No existing projects");
                }

            } else {
                EditorUtility.DisplayDialog("ERROR: Could't convert Projects into importable AssetBundles", "No projectfodlers found in" + "'" + ProjectManagerGlobals.defaultPathProjectsResourcesDirectory + "'" + ".", "ok");
            }

            if (GUILayout.Button("Close"))
                Close();
        }

        static void GenerateNewAssetBundles(string projectFolderName)
        {
            string inputProjectFiles = ProjectManagerGlobals.defaultPathProjectsResourcesDirectory + "/" + projectFolderName;

            string assetBundleOutputPathWindows = ProjectManagerGlobals.defaultAssetBundlesDirectory + "/" + projectFolderName + "/Windows";
            string assetBundleOutputPathWebGL = ProjectManagerGlobals.defaultAssetBundlesDirectory + "/" + projectFolderName + "/WebGL";
            string assetBundleOutputPathIOS = ProjectManagerGlobals.defaultAssetBundlesDirectory + "/" + projectFolderName + "/IOS";
            string assetBundleOutputPathLinux = ProjectManagerGlobals.defaultAssetBundlesDirectory + "/" + projectFolderName + "/Linux";
            string assetBundleOutputPathAndriod = ProjectManagerGlobals.defaultAssetBundlesDirectory + "/" + projectFolderName + "/Andriod";

            // Build Windows assetBundle
            AssetBundleBuild[] widowsBuildMap = BuildAssetBundle(inputProjectFiles, projectFolderName );
            BuildAssetBundlesToFolder(assetBundleOutputPathWindows, widowsBuildMap, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);

            // Build WebGL assetBundle
            AssetBundleBuild[] webGLBuildMap = BuildAssetBundle(inputProjectFiles, "Project_" + projectFolderName + "_WebGL_AssetBundle");
            BuildAssetBundlesToFolder(assetBundleOutputPathWebGL, widowsBuildMap, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);

            // Build iOS assetBundle
            AssetBundleBuild[] iOSBuildMap = BuildAssetBundle(inputProjectFiles, "Project_" + projectFolderName + "_IOS_AssetBundle");
            BuildAssetBundlesToFolder(assetBundleOutputPathIOS , widowsBuildMap, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);

            // Build Linux assetBundle
            AssetBundleBuild[] linuxGLBuildMap = BuildAssetBundle(inputProjectFiles, "Project_" + projectFolderName + "_Linux_AssetBundle");
            BuildAssetBundlesToFolder(assetBundleOutputPathLinux, widowsBuildMap, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);

            // Build Andriod assetBundle
            AssetBundleBuild[] andriodBuildMap = BuildAssetBundle(inputProjectFiles, "Project_" + projectFolderName + "_Andriod_AssetBundle");
            BuildAssetBundlesToFolder(assetBundleOutputPathAndriod, widowsBuildMap, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);

            AssetDatabase.Refresh();

            EditorUtility.DisplayDialog("SUCCES: Conversion projects into importable AssetBundles", "All projects are converted into importable AssetsBundles, see: " + "'" + ProjectManagerGlobals.defaultPathProjectsResourcesDirectory + "'" + ".", "ok");
        }

        void OnInspectorUpdate()
        {
            Repaint();
        }

        // A methode that builds all given AssetBundleBuilds to the given directory and creates it when not found.
        private static void BuildAssetBundlesToFolder(string outPutFolder, AssetBundleBuild[] buildMap, BuildAssetBundleOptions buildOptions, BuildTarget targetOS)
        {
            if (!Directory.Exists(outPutFolder))
                Directory.CreateDirectory(outPutFolder);

            BuildPipeline.BuildAssetBundles(outPutFolder, buildMap, buildOptions, targetOS);
        }

        // Puts all files in from designated filelocation in a AssetBundleBuild
        private static AssetBundleBuild[] BuildAssetBundle(string pathInputAssetsFolder, string assetBundleName) {

            AssetBundleBuild[] buildMap = new AssetBundleBuild[2];

            // Get all files in every directory within the specified folder
            List<string> allfiles = new List<string>(Directory.GetFiles(pathInputAssetsFolder, "*", System.IO.SearchOption.AllDirectories));
            
            // Add directories to the file list
            foreach (var file in allfiles)
            {
                FileInfo info = new FileInfo(file);
                System.IO.Path.GetDirectoryName(info.FullName);
            }

            // Sort the files and directory path in the allFie
            allfiles.Sort();

            // Create the array of bundle build details for only Unityscenes.
            buildMap[0].assetBundleName = "Project_" + assetBundleName + "_Windows_SceneAssetBundle";
            buildMap[0].assetNames = allfiles.ToList().FindAll(str => str.EndsWith(".unity")).ToArray();

            // Create the array of bundle build details for all other Assets (".unity" a.k.a scenes removed).
            buildMap[1].assetBundleName = "Project_" + assetBundleName + "_Windows_AssetBundle";

            foreach (string fileName in allfiles.ToList().Where(str => str.EndsWith(".unity")))
                allfiles.Remove(fileName);

            buildMap[1].assetNames = allfiles.ToArray();

            return buildMap;
        }
    }

    // Adds the a function to the Asset menu (left top corner of the Unity engine) that allows a simpel export for all AssetBundles of a project
    public class ProjectManagerCreateProjectWindow : EditorWindow
    {
        public string widowTitle = "Create new Project";
        public string editorWindowText = "Choose a project name: ";
        string newProjectName = ProjectManagerGlobals.defaultProjectName;

        public ProjectManagerCreateProjectWindow() {
            this.titleContent = new GUIContent(widowTitle);
        }

        void OnGUI()
        {
            newProjectName = EditorGUILayout.TextField(editorWindowText, newProjectName);

            if (GUILayout.Button("Create new project"))
                CreateNewProject(newProjectName);

            if (GUILayout.Button("Close"))
                Close();
        }

        static void CreateNewProject(string projectName)
        {
            string newProjectName = projectName;
            int projectNumber = 0;

            if (Directory.Exists(ProjectManagerGlobals.defaultPathProjectsResourcesDirectory + "/" + newProjectName) == true){
                projectNumber++;
                while (Directory.Exists(ProjectManagerGlobals.defaultPathProjectsResourcesDirectory + "/" + newProjectName + projectNumber) == true)
                    projectNumber++;
            }

            if (projectNumber == 0)
            {
                Directory.CreateDirectory(ProjectManagerGlobals.defaultPathProjectsResourcesDirectory + "/" + newProjectName);
                CopyFilesAndDirsInsideFolder(ProjectManagerGlobals.defaultPathTemplateProjectDirectory, ProjectManagerGlobals.defaultPathProjectsResourcesDirectory + "/" + newProjectName);
                EditorUtility.DisplayDialog("SUCCES: New project " + newProjectName + " created!", "New project folder " + "'" + newProjectName + "'" + " created in " + "'" + ProjectManagerGlobals.defaultPathProjectsResourcesDirectory + "'.", "ok");

            }
            else
            {
                Directory.CreateDirectory(ProjectManagerGlobals.defaultPathProjectsResourcesDirectory + "/" + newProjectName + projectNumber);
                CopyFilesAndDirsInsideFolder(ProjectManagerGlobals.defaultPathTemplateProjectDirectory, ProjectManagerGlobals.defaultPathProjectsResourcesDirectory + "/" + newProjectName + projectNumber);
                EditorUtility.DisplayDialog("SUCCES: New project " + newProjectName + projectNumber + " created!", "New project folder " + "'" + newProjectName + projectNumber + "'" + " created in " + "'" + ProjectManagerGlobals.defaultPathProjectsResourcesDirectory + "'.", "ok");
            }

            AssetDatabase.Refresh();
        }

        void OnInspectorUpdate()
        {
            Repaint();
        }

        // Copies all files from the one dir into another
        static void CopyFilesAndDirsInsideFolder(string SourcePath, string DestinationPath)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*",
                SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath), true);
        }
    }

}