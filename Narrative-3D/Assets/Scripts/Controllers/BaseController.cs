﻿using System;
using System.Linq;
using UnityEngine;

public class BaseController : IBaseController
{
    // Keep references to the model and view
    protected BaseModel b_model;
    public virtual BaseModel Model
    {
        get
        {
            return b_model;
        }
        set
        {
            if (b_model != value)
            {
                if (value != null)
                {
                    // Set new model
                    b_model = value;

                    // Set indentification variables for View Model
                    SetIdentifiableModelVariables();

                    // Set non-indentification variables for Model
                    SetNonIdentifiableModelVariables();

                    // Set the EventHandlers for the Model
                    SetModelHandlers();
                }
            }
            else
            {
                if (b_model != null)
                {
                    //// Remove indentification variables for model
                    //RemoveIdentifiableModelVariables();

                    //// Remove non-indentification variables for model
                    //RemoveNonIdentifiableModelVariables();

                    // Remove the EventHandlers for the View
                    RemoveModelHandlers();
                }

                // Set new model
                b_model = value;
            }
        }
    }

    protected BaseView b_view;
    public virtual BaseView View
    {
        get
        {
            return b_view;
        }
        set
        {
            if (b_view != value)
            {
                if (value != null)
                {
                    // Set new View
                    b_view = value;

                    // Set indentification variables for View
                    SetIdentifiableViewVariables();

                    // Set non-indentification variables for View
                    SetNonIdentifiableViewVariables();

                    // Set the EventHandlers for the View
                    SetViewHandlers();
                }
                else
                {
                    if (b_view != null)
                    {
                        //// Remove indentification variables for View
                        //RemoveIdentifiableViewVariables();

                        //// Remove non-indentification variables for View
                        //RemoveNonIdentifiableViewVariables();

                        // Remove the EventHandlers for the View
                        RemoveViewHandlers();
                    }

                    // Set new View
                    b_view = value;
                }
            }
            else
            {
                b_view = value;
            }

        }
    }

    // cache for looking up corresponding entityID
    public string b_entityID;
    public virtual string EntityID
    {
        get
        {
            if (b_entityID != null || b_entityID == "")
                return b_entityID;
            else if (View != null)
                return View.EntityID;
            else if (Model != null)
                return Model.EntityID;
            else
                return b_entityID;
        }
        set
        {
            b_entityID = value;
        }
    }

    public BaseController(BaseModel model, BaseView view)
    {
        // Set the given View and Model
        this.Model = model;
        this.View = view;            

        // Log a false constructor
        if (this.View == null && this.Model == null)
            Debug.Log("!!!=== No Model nor View ===!!!");
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected virtual void SetIdentifiableModelVariables()
    {
        b_entityID = Model.EntityID;
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected virtual void RemoveIdentifiableModelVariables()
    {
        b_entityID = null;
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected virtual void SetIdentifiableViewVariables()
    {
        b_entityID = View.EntityID;
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected virtual void RemoveIdentifiableViewVariables()
    {
        b_entityID = null;
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected virtual void SetNonIdentifiableModelVariables()
    {
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected virtual void RemoveNonIdentifiableModelVariables()
    {
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected virtual void SetNonIdentifiableViewVariables()
    {
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected virtual void RemoveNonIdentifiableViewVariables()
    {
    }

    // This methode sets the handlers for the View-events
    protected virtual void SetViewHandlers()
    {
    }

    // This methode removes the handlers for the View-events
    protected virtual void RemoveViewHandlers()
    {
    }

    // This methode sets the handlers for the Model-events
    protected virtual void SetModelHandlers()
    {
        ((BaseModel)this.Model).OnEntityIDChanged += HandleEntityIDChanged;
        //((BaseModel)this.model).OnControllerChanged += HandleControllerChanged;
    }

    // This methode removes the handlers for the Model-events
    protected virtual void RemoveModelHandlers()
    {
        ((BaseModel)this.Model).OnEntityIDChanged -= HandleEntityIDChanged;
        //((BaseModel)this.model).OnControllerChanged -= HandleControllerChanged;
    }


    // Called when the model's objectType changed
    protected virtual void HandleEntityIDChanged(object sender, EntityIDChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        SyncEntityIDModelToView();
    }

    // Sync the model's entityID with the view's entityID
    public virtual void SyncEntityIDViewToModel(string entityID)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((BaseModel)this.Model).EntityID = entityID;
    }

    // Sync the model's entityID with the views's entityID
    public virtual void SyncEntityIDModelToView()
    {
        ((BaseView)this.View).EntityID = this.Model.EntityID;
    }

    // Sync the view with the model (all variables)
    public virtual void SyncModelToView()
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        SyncEntityIDModelToView();
    }

    // Find the entityID for which this controller was intended
    public string FindEntityID()
    {
        if (b_entityID == null || b_entityID == "")
        {

            if (this.Model != null)
                return Model.EntityID;

            else if (this.View != null)
                return View.EntityID;

            else
                return ObjectFactory.Instance.entityControllers.FirstOrDefault(x => x.Value == this).Key;
        }
        else
        {
            return b_entityID;
        }
    }

     // Looks for an existing View, else creates one
    public virtual BaseView FindSetView()
    {
        // Not possible for this object type
        return View;
    }
    
     // Looks for an existing View, else creates one
    public virtual BaseView FindCreateSetView(Transform parentTransform)
    {
        // Can't be done with the typeOf;
        return View;
    }

    //  Looks for an existing Model, else creates one
    public virtual BaseModel FindSetModel()
    {
        // Not possible for this object type
        return Model;
    }

    //  Looks for an existing Model, else creates one
    public virtual BaseModel FindCreateSetModel()
    {
        // Can't be done with the typeOf;
        return Model;
    }
}