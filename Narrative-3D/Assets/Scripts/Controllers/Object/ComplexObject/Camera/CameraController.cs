﻿using Object.Type;
using SCamera.Type;
using UnityEngine;

public class CameraController : ComplexObjectController, ICameraController
{

    public CameraController(CameraModel model, CameraView view) : base(model, view)
    {
    }

    // Called when Unity's Update() function is called
    protected override void HandleUpdateEvent()
    {
        // If the camera is a FPScamera then enable it
        if (FindSetModel() != null && Model is CameraModel)
        {
            if (((CameraModel)Model).CameraControlSettings != null)
            {
                if (((CameraModel)Model).CameraControlSettings.cameraType == CameraTypeEnum.FIRSTPERSON)
                {
                    if (this.FindSetView() != null)
                    {
                        FpsHudController fpsHudController = View.GetComponent<FpsHudController>();

                        if (fpsHudController != null)
                            fpsHudController.enabled = true;
                    }
                }
            }
        }
    }

}
