﻿using Object.FSM;
using Object.Type;
using UnityEngine;

public class ComplexObjectController : ObjectController, IComplexObjectController
{

    public ComplexObjectController(ComplexObjectModel model, ComplexObjectView view) : base(model, view)
    {
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void SetIdentifiableModelVariables()
    {
        // Inherit logic
        base.SetIdentifiableModelVariables();
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void RemoveIdentifiableModelVariables()
    {
        // Inherit logic
        base.RemoveIdentifiableModelVariables();
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void SetIdentifiableViewVariables()
    {
        // Inherit logic
        base.SetIdentifiableViewVariables();
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void RemoveIdentifiableViewVariables()
    {
        // Inherit logic
        base.RemoveIdentifiableViewVariables();
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void SetNonIdentifiableModelVariables()
    {
        // Inherit logic
        base.SetNonIdentifiableModelVariables();
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void RemoveNonIdentifiableModelVariables()
    {
        // Inherit logic
        base.RemoveNonIdentifiableModelVariables();
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void SetNonIdentifiableViewVariables()
    {
        // Inherit logic
        base.SetNonIdentifiableViewVariables();
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void RemoveNonIdentifiableViewVariables()
    {
        // Inherit logic
        base.RemoveNonIdentifiableViewVariables();
    }

    // This methode sets the handlers for the View-events
    protected override void SetViewHandlers()
    {
        // Inherit logic
        base.SetViewHandlers();
    }

    // This methode removes the handlers for the View-events
    protected override void RemoveViewHandlers()
    {
        // Inherit logic
        base.RemoveViewHandlers();
    }

    // This methode sets the handlers for the Model-events
    protected override void SetModelHandlers()
    {
        // Inherit logic
        base.SetModelHandlers();

        ((ComplexObjectModel)this.Model).OnControllabilityChanged += HandleControllabilityChanged;
        ((ComplexObjectModel)this.Model).OnControlSettingsChanged += HandleControlSettingsChanged;
        ((ComplexObjectModel)this.Model).OnMachineStateChanged += HandleMachineStateChanged;
    }

    // This methode removes the handlers for the Model-events
    protected override void RemoveModelHandlers()
    {
        // Inherit logic
        base.RemoveModelHandlers();

        ((ComplexObjectModel)this.Model).OnControllabilityChanged -= HandleControllabilityChanged;
        ((ComplexObjectModel)this.Model).OnControlSettingsChanged -= HandleControlSettingsChanged;
        ((ComplexObjectModel)this.Model).OnMachineStateChanged -= HandleMachineStateChanged;
    }


    // Called when the model's controllablility changed
    protected void HandleControllabilityChanged(object sender, ControllabilityChangedEventArgs e)
    {
        SyncControllabilityModelToView();
    }

    // Sync the model's controllalilty with the view's controllalilty
    public void SyncControllabilityViewToModel(bool controllablilty)
    {
        ((ComplexObjectModel)this.Model).Controllable = controllablilty;
    }

    // Sync the view's controllalilty with the model's controllalilty
    protected void SyncControllabilityModelToView()
    {
        ((ComplexObjectView)this.View).Controllable = ((ComplexObjectModel)this.Model).Controllable;
    }


    // Called when the model's controllablility changed
    protected void HandleControlSettingsChanged(object sender, ControlSettingsChangedEventArgs e)
    {
        SyncControlSettingsModelToView();
    }

    // Sync the model's controllalilty with the view's controllalilty
    public void SyncControlSettingsViewToModel(ObjectControlModel controlSettings)
    {
        ((ComplexObjectModel)this.Model).ControlSettings = controlSettings;
    }

    // Sync the view's controllalilty with the model's controllalilty
    protected void SyncControlSettingsModelToView()
    {
        ((ComplexObjectView)this.View).ControlSettings = ((ComplexObjectModel)this.Model).ControlSettings;
    }


    // Called when the model's machine state changed
    protected virtual void HandleMachineStateChanged(object sender, MachineStateChangedEventArgs e)
    {
        SyncMachineStateModelToView();
    }

    // Sync the model's machine state with the view's machine state
    public virtual void SyncMachineStateViewToModel(ObjectFSMEnum backedMachineState)
    {
        ((ComplexObjectModel)this.Model).MachineState = backedMachineState;
    }

    // Sync the view's machine state with the model's machine state
    protected virtual void SyncMachineStateModelToView()
    {
        ((ComplexObjectView)this.View).MachineState = ((ComplexObjectModel)this.Model).MachineState;
    }

    // Sync the view with the model (all variables)
    public override void SyncModelToView()
    {
        base.SyncModelToView();

        SyncControllabilityModelToView();
        SyncControlSettingsModelToView();
        SyncMachineStateModelToView();

    }

}
