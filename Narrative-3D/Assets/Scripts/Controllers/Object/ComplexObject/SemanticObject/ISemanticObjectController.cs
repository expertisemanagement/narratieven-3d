﻿using VDS.RDF;

public interface ISemanticObjectController : IObjectController
{
    // Sync the mmodel's sementic Node with the view's sementic Node
    void SyncSemanticNodeViewToModel(INode backedSemanticNode);

    // Sync the mmodel's sementic Node with the view's sementic Node
    void SyncObjectURIViewToModel(string backedObjectURI);

    // Sync the mmodel's sementic Node with the view's sementic Node
    void SyncNavObjectViewToModel(NavObjectModel backedNavObject);

}