﻿using Object.Type;
using UnityEngine;

public class ActivityController : IntentionalElementController, IActivityController
{
    public ActivityController(ActivityModel model, ActivityView view) : base(model, view)
    {
    }
}
