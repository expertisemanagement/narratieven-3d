﻿using Object.Type;

public class IntentionalElementController : SMWObjectController, IIntentionalElementController
{
    public IntentionalElementController(IntentionalElementModel model, IntentionalElementView view) : base(model, view)
    {
    }
}
