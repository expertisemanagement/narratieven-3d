﻿using Object.Type;
using System.Linq;
using UnityEngine;
using VDS.RDF;

public class SemanticObjectController : ComplexObjectController, ISemanticObjectController {

    // cache for looking up corresponding objectURI
    protected string b_objectURI = "";

    public SemanticObjectController(SemanticObjectModel model, SemanticObjectView view) : base(model, view)
    {
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void SetIdentifiableModelVariables()
    {
        // Inherit logic
        base.SetIdentifiableModelVariables();

        b_objectURI = ((SemanticObjectModel)this.Model).ObjectURI;
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void RemoveIdentifiableModelVariables()
    {
        // Inherit logic
        base.RemoveIdentifiableModelVariables();

        b_objectURI = null;
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void SetIdentifiableViewVariables()
    {
        // Inherit logic
        base.SetIdentifiableViewVariables();

        b_objectURI = ((SemanticObjectView)this.View).ObjectURI;
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void RemoveIdentifiableViewVariables()
    {
        // Inherit logic
        base.RemoveIdentifiableViewVariables();

        b_objectURI = null;
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void SetNonIdentifiableModelVariables()
    {
        // Inherit logic
        base.SetNonIdentifiableModelVariables();
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void RemoveNonIdentifiableModelVariables()
    {
        // Inherit logic
        base.RemoveNonIdentifiableModelVariables();
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void SetNonIdentifiableViewVariables()
    {
        // Inherit logic
        base.SetNonIdentifiableViewVariables();
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void RemoveNonIdentifiableViewVariables()
    {
        // Inherit logic
        base.RemoveNonIdentifiableViewVariables();
    }

    // This methode sets the handlers for the View-events
    protected override void SetViewHandlers()
    {
        // Inherit logic
        base.SetViewHandlers();
    }

    // This methode removes the handlers for the View-events
    protected override void RemoveViewHandlers()
    {
        // Inherit logic
        base.RemoveViewHandlers();
    }

    // This methode sets the handlers for the Model-events
    protected override void SetModelHandlers()
    {
        // Inherit logic
        base.SetModelHandlers();

        ((SemanticObjectModel)this.Model).OnSemanticNodeChanged += HandleSemanticNodeChanged;
        ((SemanticObjectModel)this.Model).OnObjectURIChanged += HandleObjectURIChanged;
        ((SemanticObjectModel)this.Model).OnNavObjectChanged += HandleNavObjectChanged;
    }

    // This methode removes the handlers for the Model-events
    protected override void RemoveModelHandlers()
    {
        // Inherit logic
        base.RemoveModelHandlers();

        ((SemanticObjectModel)this.Model).OnSemanticNodeChanged -= HandleSemanticNodeChanged;
        ((SemanticObjectModel)this.Model).OnObjectURIChanged -= HandleObjectURIChanged;
        ((SemanticObjectModel)this.Model).OnNavObjectChanged -= HandleNavObjectChanged;
    }

    // Called when the model's semantic node changed
    protected virtual void HandleSemanticNodeChanged(object sender, SemanticNodeChangedEventArgs e)
    {
        SyncSemanticNodeModelToView();
    }

    // Sync the mmodel's sementic Node with the view's sementic Node
    public virtual void SyncSemanticNodeViewToModel(INode backedSemanticNode)
    {
        ((SemanticObjectModel)this.Model).SemanticNode = backedSemanticNode;
    }

    // Sync the view's sementic Node with the model's sementic Node
    protected virtual void SyncSemanticNodeModelToView()
    {
        ((SemanticObjectView)this.View).SemanticNode = ((SemanticObjectModel)this.Model).SemanticNode;
    }


    // Called when the model's semantic node changed
    protected virtual void HandleObjectURIChanged(object sender, ObjectURIChangedEventArgs e)
    {
        SyncObjectURIModelToView();
    }

    // Sync the mmodel's sementic Node with the view's sementic Node
    public virtual void SyncObjectURIViewToModel(string backedObjectURI)
    {
        ((SemanticObjectModel)this.Model).ObjectURI = backedObjectURI;
    }

    // Sync the view's sementic Node with the model's sementic Node
    protected virtual void SyncObjectURIModelToView()
    {
        ((SemanticObjectView)this.View).ObjectURI = ((SemanticObjectModel)this.Model).ObjectURI;
    }


    // Called when the model's semantic node schanged
    protected virtual void HandleNavObjectChanged(object sender, NavObjectChangedEventArgs e)
    {
        SyncNavObjectModelToView();
    }

    // Sync the mmodel's sementic Node with the view's sementic Node
    public virtual void SyncNavObjectViewToModel(NavObjectModel backedNavObject)
    {
        ((SemanticObjectModel)this.Model).NavObject = backedNavObject;
    }

    // Sync the view's sementic Node with the model's sementic Node
    protected virtual void SyncNavObjectModelToView()
    {
        ((SemanticObjectView)this.View).NavObject = ((SemanticObjectModel)this.Model).NavObject;
    }

    // Find the objectURI for which this controller was intended
    public string FindObjectURI()
    {
        if (b_objectURI == null || b_objectURI == "")
        {
            if (Model != null)
                return ((SemanticObjectModel)Model).ObjectURI;

            else if (View != null)
                return ((SemanticObjectView)View).ObjectURI;

            else
            {
                return ObjectFactory.Instance.semanticControllers.FirstOrDefault(x => x.Value == this).Key;
            }
        }
        else
        {
            return b_objectURI;
        }
    }

    // Find the semantic node with the objectURI for which this controller was intended
    public INode FindSemanticNode()
    {
        return SMWParser.Instance.GetNodeByObjectURI(FindObjectURI());
    }

}
