﻿using System;
using Object.Type;

public class NavObjectController : ObjectController, INavObjectController
{
    public NavObjectController(ObjectModel model, ObjectView view) : base(model,view)
    {
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void SetIdentifiableModelVariables()
    {
        // Inherit logic
        base.SetIdentifiableModelVariables();
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void RemoveIdentifiableModelVariables()
    {
        // Inherit logic
        base.RemoveIdentifiableModelVariables();
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void SetIdentifiableViewVariables()
    {
        // Inherit logic
        base.SetIdentifiableViewVariables();
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void RemoveIdentifiableViewVariables()
    {
        // Inherit logic
        base.RemoveIdentifiableViewVariables();
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void SetNonIdentifiableModelVariables()
    {
        // Inherit logic
        base.SetNonIdentifiableModelVariables();
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void RemoveNonIdentifiableModelVariables()
    {
        // Inherit logic
        base.RemoveNonIdentifiableModelVariables();
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void SetNonIdentifiableViewVariables()
    {
        // Inherit logic
        base.SetNonIdentifiableViewVariables();
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void RemoveNonIdentifiableViewVariables()
    {
        // Inherit logic
        base.RemoveNonIdentifiableViewVariables();
    }

    // This methode sets the handlers for the View-events
    protected override void SetViewHandlers()
    {
        // Inherit logic
        base.SetViewHandlers();
    }

    // This methode removes the handlers for the View-events
    protected override void RemoveViewHandlers()
    {
        // Inherit logic
        base.RemoveViewHandlers();
    }


    // This methode sets the handlers for the Model-events
    protected override void SetModelHandlers()
    {
        // Inherit logic
        base.SetModelHandlers();

        // Listen to changes in the model
        ((NavObjectModel)this.Model).OnSemanticObjectChanged += HandleSemanticObjectChanged;
    }

    // This methode removes the handlers for the Model-events
    protected override void RemoveModelHandlers()
    {
        // Inherit logic
        base.RemoveModelHandlers();

        // Listen to changes in the model
        ((NavObjectModel)this.Model).OnSemanticObjectChanged += HandleSemanticObjectChanged;
    }

    private void HandleSemanticObjectChanged(object sender, SemanticObjectChangedEventArgs e)
    {
        SyncSemanticObjectModelToView();
    }

    // Sync the model's controllalilty with the view's controllalilty
    public void SyncSemanticObjectViewToModel(SemanticObjectModel semanticObject)
    {
        ((NavObjectModel)this.Model).SemanticObject = semanticObject;
    }

    // Sync the view's controllalilty with the model's controllalilty
    protected void SyncSemanticObjectModelToView()
    {
        ((NavObjectView)this.View).semanticObject = ((NavObjectModel)this.Model).SemanticObject;
    }


    // Sync the view with the model (all variables)
    public override void SyncModelToView()
    {
        base.SyncModelToView();

        SyncSemanticObjectModelToView();

    }

}
