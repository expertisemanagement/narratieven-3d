﻿using UnityEngine;
using Object.Type;
using Object.GUIstate;
using VDS.RDF;

/// <summary>
/// This is the base Controller class for an Object.
/// </summary>
public class ObjectController : BaseController, IObjectController
{    
    // cache for looking up the corresponding ObjectType
    protected ObjectTypeEnum b_objectType = ObjectTypeEnum.UNASSIGNED;


    public ObjectController(ObjectModel model, ObjectView view) : base(model, view)
    {
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void SetIdentifiableModelVariables()
    {
        // Inherit logic
        base.SetIdentifiableModelVariables();
        
        b_objectType = ((ObjectModel)Model).ObjectType;
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void RemoveIdentifiableModelVariables()
    {
        // Inherit logic
        base.RemoveIdentifiableModelVariables();

        b_objectType = ObjectTypeEnum.UNASSIGNED;
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void SetIdentifiableViewVariables()
    {
        // Inherit logic
        base.SetIdentifiableViewVariables();
        
        b_objectType = ((ObjectView)View).ObjectType;
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void RemoveIdentifiableViewVariables()
    {
        // Inherit logic
        base.RemoveIdentifiableViewVariables();

        b_objectType = ObjectTypeEnum.UNASSIGNED;
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void SetNonIdentifiableModelVariables()
    {
        // Inherit logic
        base.SetNonIdentifiableModelVariables();
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void RemoveNonIdentifiableModelVariables()
    {
        // Inherit logic
        base.RemoveNonIdentifiableModelVariables();
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void SetNonIdentifiableViewVariables()
    {
        // Inherit logic
        base.SetNonIdentifiableViewVariables();
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void RemoveNonIdentifiableViewVariables()
    {
        // Inherit logic
        base.RemoveNonIdentifiableViewVariables();
    }

    // This methode sets the handlers for the View-events
    protected override void SetViewHandlers()
    {
        // Inherit logic
        base.SetViewHandlers();

        // Listen to input from the view
        ((ObjectView)this.View).OnClicked += HandleViewClicked;
        ((ObjectView)this.View).OnMoved += HandleViewMoved;
        ((ObjectView)this.View).OnLookedAt += HandleViewLookedAt;
        ((ObjectView)this.View).OnInteracted += HandleViewInteracted;

        // Listen to MonoDevelop events (more view events)
        ((ObjectView)this.View).AwakeEvent += HandleAwakeEvent;
        ((ObjectView)this.View).StartEvent += HandleStartEvent;
        ((ObjectView)this.View).UpdateEvent += HandleUpdateEvent;
        ((ObjectView)this.View).OnCollisionEnterEvent += HandleCollisionEnterEvent;
        ((ObjectView)this.View).OnCollisionExitEvent += HandleCollisionExitEvent;
        ((ObjectView)this.View).OnValidateEvent += HandleValidate; // This methodes checks the inspector input a.k.a Designer input
    }

    // This methode removes the handlers for the View-events
    protected override void RemoveViewHandlers()
    {
        // Inherit logic
        base.RemoveViewHandlers();

        // Listen to input from the view
        ((ObjectView)this.View).OnClicked -= HandleViewClicked;
        ((ObjectView)this.View).OnMoved -= HandleViewMoved;
        ((ObjectView)this.View).OnLookedAt -= HandleViewLookedAt;
        ((ObjectView)this.View).OnInteracted -= HandleViewInteracted;

        // Listen to MonoDevelop events (more view events)
        ((ObjectView)this.View).AwakeEvent -= HandleAwakeEvent;
        ((ObjectView)this.View).StartEvent -= HandleStartEvent;
        ((ObjectView)this.View).UpdateEvent -= HandleUpdateEvent;
        ((ObjectView)this.View).OnCollisionEnterEvent -= HandleCollisionEnterEvent;
        ((ObjectView)this.View).OnCollisionExitEvent -= HandleCollisionExitEvent;
        ((ObjectView)this.View).OnValidateEvent -= HandleValidate; // This methodes checks the inspector input a.k.a Designer input
    }

    // This methode sets the handlers for the Model-events
    protected override void SetModelHandlers()
    {
        // Inherit logic
        base.SetModelHandlers();

        //((ObjectModel)this.model).OnParentsChanged += HandleParentsChanged;
        //((ObjectModel)this.model).OnChildsChanged += HandleChildsChanged;
        ((ObjectModel)this.Model).OnPositionChanged += HandlePositionChanged;
        ((ObjectModel)this.Model).OnRotationChanged += HandleRotationChanged;
        ((ObjectModel)this.Model).OnScaleChanged += HandleScaleChanged;
        ((ObjectModel)this.Model).OnTypeChanged += HandleObjectTypeChanged;
        ((ObjectModel)this.Model).OnGUIStateChanged += HandleGUIStateChanged;
    }

    // This methode removes the handlers for the Model-events
    protected override void RemoveModelHandlers()
    {
        // Inherit logic
        base.RemoveModelHandlers();

        //((ObjectModel)this.model).OnParentsChanged -= HandleParentsChanged;
        //((ObjectModel)this.model).OnChildsChanged -= HandleChildsChanged;
        ((ObjectModel)this.Model).OnPositionChanged -= HandlePositionChanged;
        ((ObjectModel)this.Model).OnRotationChanged -= HandleRotationChanged;
        ((ObjectModel)this.Model).OnScaleChanged -= HandleScaleChanged;
        ((ObjectModel)this.Model).OnTypeChanged -= HandleObjectTypeChanged;
        ((ObjectModel)this.Model).OnGUIStateChanged -= HandleGUIStateChanged;
    }


    // Called when the model's parents changed
    //protected virtual void HandleParentsChanged(object sender, ParentsChangedEventArgs e)
    //{
    //    syncParentsModelToView();
    //}

    // Sync the model's parents with the parent's position
    //public virtual void SyncParentsViewToModel(List<ObjectModel> childs)
    //{
    //    ((ObjectModel)this.model).parents = parents;
    //}

    // Sync the view's parents with the model's parents
    //protected virtual void syncParentsModelToView()
    //{
    //    ((ObjectView)this.view).parents = ((ObjectModel)this.model).parents;
    //}


    // Called when the model's Childs changed
    //protected virtual void HandleChildsChanged(object sender, ChildsChangedEventArgs e)
    //{
    //    syncChildsModelToView();
    //}

    // Sync the model's childs with the views's position
    //public virtual void SyncChildsViewToModel(List<ObjectModel> childs)
    //{
    //    ((ObjectModel)this.model).childs = childs;
    //}

    // Sync the view's childs with the model's childs
    //protected virtual void syncChildsModelToView()
    //{
    //    ((ObjectView)this.view).childs = ((ObjectModel)this.model).childs;
    //}


    // Called when the model's position changed
    protected virtual void HandlePositionChanged(object sender, PositionChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncPositionModelToView();
    }

    // Sync the model's position with the views's position
    public virtual void SyncPositionViewToModel(Vector3 backedPosition)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((ObjectModel)this.Model).Position = backedPosition;
    }

    // Sync the view's position with the model's position
    protected virtual void SyncPositionModelToView()
    {
        ((ObjectView)this.View).Position = ((ObjectModel)this.Model).Position;
    }


    // Called when the model's rotation changed
    protected virtual void HandleRotationChanged(object sender, RotationChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncRotationModelToView();
    }

    // Sync the model's rotation with the view's rotation
    public virtual void SyncRotationViewToModel(Quaternion backedRotation)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((ObjectModel)this.Model).Rotation = backedRotation;
    }

    // Sync the view's rotation with the model's rotation
    protected virtual void SyncRotationModelToView()
    {
        ((ObjectView)this.View).Rotation = ((ObjectModel)this.Model).Rotation;
    }


    // Called when the model's objectType changed
    protected virtual void HandleScaleChanged(object sender, ScaleChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncScaleModelToView();
    }

    // Sync the model's scale with the view's scale
    public virtual void SyncScaleViewToModel(Vector3 backedScale)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((ObjectModel)this.Model).Scale = backedScale;
    }

    // Sync the view's scale with the model's scale
    protected virtual void SyncScaleModelToView()
    {
        ((ObjectView)this.View).Scale = ((ObjectModel)this.Model).Scale;
    }


    // Called when the model's objectType changed
    protected virtual void HandleObjectTypeChanged(object sender, TypeChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncObjectTypeModelToView();
    }

    // Sync the model's ObjectType with the view's ObjectType
    public virtual void SyncObjectTypeViewToModel(ObjectTypeEnum objectType)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((ObjectModel)this.Model).ObjectType = objectType;
    }

    // Sync the view's ObjectType with the model's ObjectType
    protected virtual void SyncObjectTypeModelToView()
    {
        ((ObjectView)this.View).ObjectType = ((ObjectModel)this.Model).ObjectType;
    }


    // Called when the model's GUI state changed
    protected virtual void HandleGUIStateChanged(object sender, GUIStateChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncGUIStateModelToView();
    }

    // Sync the model's GUI state with the view's GUI state
    public virtual void SyncGUIStateViewToModel(ObjectGUIStateEnum backedGUIState)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((ObjectModel)this.Model).GUIState = backedGUIState;
    }

    // Sync the view's GUI state with the model's GUI state
    protected virtual void SyncGUIStateModelToView()
    {
        ((ObjectView)this.View).GuiState = ((ObjectModel)this.Model).GUIState;
    }


    // Called when the view is clicked
    protected virtual void HandleViewClicked(object sender, ObjectClickedEventArgs e)
    {
        //TODO
    }

    // Called when the view has moved
    protected virtual void HandleViewMoved(object sender, ObjectMovedEventArgs e)
    {
        //TODO
    }

    // Called when the view is looked at
    protected virtual void HandleViewLookedAt(object sender, ObjectLookedAtEventArgs e)
    {
        //TODO
    }

    // Called when the view is interacted with
    protected virtual void HandleViewInteracted(object sender, ObjectInteractedEventArgs e)
    {
        if (FindSetView() != null && e != null && e.subject != null)
        {
            ObjectInteractionMenu interactionMenu = View.gameObject.GetComponentInChildren<ObjectInteractionMenu>(true);

            if (interactionMenu != null)
            {

                if(e.subject.GetComponent<CameraView>())
                    interactionMenu.Initialise(e.subject.GetComponent<CameraView>().GetComponent<CameraControlView>().b_subject, this.FindSetView().gameObject, e.indirectObject);
                else
                    interactionMenu.Initialise(e.subject, this.FindSetView().gameObject, e.indirectObject);

                // Enable interactionmenu
                interactionMenu.gameObject.SetActive(true);
                interactionMenu.enabled = true;
            }
        }
    }

    // Called when Unity's Awake() function is called
    protected virtual void HandleAwakeEvent()
    {
        //TODO
    }

    // Called when Unity's Start() function is called
    protected virtual void HandleStartEvent()
    {
        //TODO
    }

    // Called when Unity's Update() function is called
    protected virtual void HandleUpdateEvent()
    {
        //TODO
    }

    // Called when Unity's OnCollisionEnter() function is called
    protected virtual void HandleCollisionEnterEvent(Collision param)
    {
        //TODO
    }

    // Called when Unity's OnCollisionExit() function is called
    protected virtual void HandleCollisionExitEvent(Collision param)
    {
        //TODO
    }

    // Called when values in the inspector are changed
    protected virtual void HandleValidate()
    {
        //TODO
    }


    // Sync the view with the model (all variables)
    public override void SyncModelToView()
    {
        base.SyncModelToView();

        SyncPositionModelToView();
        SyncRotationModelToView();
        SyncScaleModelToView();
        SyncGUIStateModelToView();

    }


    // Sets an ObjectModel of a ObjectController as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void AddParent(ObjectController controller)
    {
        AddParent(((ObjectModel)controller.Model));
    }

    // Sets an other ObjectModel as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void AddParent(ObjectModel model)
    {
        // If model is already in parentsrecord, stop this methode (do nothing)
        if (((ObjectModel)this.Model).Parents.IndexOf(model) < 0)
            return;

        // If model is already in childsrecord of the controller model, stop this methode (do nothing)
        if (model.Childs.IndexOf(model) < 0)
            return;

        // Add the given model to the parentsrecord of this controller's model
        ((ObjectModel)this.Model).Parents.Add(model);

        // Add the given model to the childs of the controller
        ((ObjectController)model.Controller).AddChild(((ObjectModel)this.Model));
    }

    // Adds a set of objectModels as parent. This models define this object's as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void AddParents(ObjectController[] controllers)
    {
        // Input parameter incorrect
        if (controllers == null || controllers.Length < 1)
            return;

        // For each controller try adding it's model as a parent
        foreach (ObjectController controller in controllers)
            AddParent(((ObjectModel)controller.Model));
    }

    // Adds a set of objectModels as parent. This models define this object's as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void AddParents(ObjectModel[] models)
    {
        // Input parameter incorrect
        if (models == null || models.Length < 1)
            return;

        // For each controller try adding it's model as a parent
        foreach (ObjectModel model in models)
            AddParent(model);
    }

    // Removes a objectModel of a ObjectController as a parent. This model define this object's as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void RemoveParent(ObjectController controller)
    {
        RemoveParent(((ObjectModel)controller.Model));
    }

    // Removes a objectModel as a parent. This model define this object's as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void RemoveParent(ObjectModel model)
    {
        // If parent is not in parentsrecord, stop this methode (do nothing)
        if (((ObjectModel)this.Model).Parents.IndexOf(model) > -1)
            return;

        // First, remove this controller's model from the parent model childrecord
        if (model.Childs.IndexOf(((ObjectModel)this.Model)) > -1)
            model.Childs.Remove(((ObjectModel)this.Model));

        // Secondly, dechild this model from the parent.
        ((ObjectModel)this.Model).Parents.Remove(model);
    }

    // Adds a set of objectModels as parents. This models define this object's as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void RemoveParents(ObjectController[] controllers)
    {
        // Input parameter incorrect
        if (controllers == null || controllers.Length < 1)
            return;

        // For each controller try removing
        foreach (ObjectController controller in controllers)
            RemoveParent(((ObjectModel)controller.Model));
    }

    // Removes a set of objectModels as parents. This models define this object's as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void RemoveParents(ObjectModel[] models)
    {
        // Input parameter incorrect
        if (models == null || models.Length < 1)
            return;

        // For each controller try removing
        foreach (ObjectModel model in models)
            RemoveParent(model);
    }



    // Sets an ObjectModel of a ObjectController as a child (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void AddChild(ObjectController controller)
    {
        AddChild(((ObjectModel)controller.Model));
    }

    // Sets an other ObjectModel as a child (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void AddChild(ObjectModel model)
    {
        // If model is already in childsrecord, stop this methode (do nothing)
        if (((ObjectModel)this.Model).Childs.IndexOf(model) < 0)
            return;

        // If model is already in parentsrecord of the controller model, stop this methode (do nothing)
        if (model.Parents.IndexOf(model) < 0)
            return;

        // Add the given model to the parentsrecord of this controller's model
        ((ObjectModel)this.Model).Childs.Add(model);

        // Add the given model to the parents of the controller
        ((ObjectController)model.Controller).AddParent(((ObjectModel)this.Model));
    }

    // Adds a set of objectModels as parent. This models define this object's as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void AddChilds(ObjectController[] controllers)
    {
        // Input parameter incorrect
        if (controllers == null || controllers.Length < 1)
            return;

        // For each controller try adding it's model as a parent
        foreach (ObjectController controller in controllers)
            AddChild(((ObjectModel)controller.Model));
    }

    // Adds a set of objectModels as parent. This models define this object's as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void AddChild(ObjectModel[] models)
    {
        // Input parameter incorrect
        if (models == null || models.Length < 1)
            return;

        // For each controller try adding it's model as a parent
        foreach (ObjectModel model in models)
            AddChild(model);
    }

    // Removes a objectModel a specific controller as a parent. This model define this object's as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void RemoveChild(ObjectController controller)
    {
        RemoveChild(((ObjectModel)controller.Model));
    }

    // Removes a objectModel as a parent. This model define this object's as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void RemoveChild(ObjectModel model)
    {
        // If child is not in childsrecord, stop this methode (do nothing)
        if (((ObjectModel)this.Model).Childs.IndexOf(model) > -1)
            return;

        // First, remove this controller's model from the parent model childrecord
        if (model.Parents.IndexOf(((ObjectModel)this.Model)) > -1)
            model.Parents.Remove(((ObjectModel)this.Model));

        // Secondly, dechild this model from the parent.
        ((ObjectModel)this.Model).Childs.Remove(model);
    }

    // Adds a set of objectModels as parents. This models define this object's as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void RemoveChilds(ObjectController[] controllers)
    {
        // Input parameter incorrect
        if (controllers == null || controllers.Length < 1)
            return;

        // For each controller try removing
        foreach (ObjectController controller in controllers)
            RemoveChild(((ObjectModel)controller.Model));
    }

    // Removes a set of objectModels as parents. This models define this object's as a parent (this defines a hierarchy/composition of object relations; composite design pattern)
    public virtual void RemoveChilds(ObjectModel[] models)
    {
        // Input parameter incorrect
        if (models == null || models.Length < 1)
            return;

        // For each controller try removing
        foreach (ObjectModel model in models)
            RemoveChild(model);
    }


    // Finds the objectType this controller is supposed to
    public ObjectTypeEnum FindObjectType()
    {
        if (b_objectType != ObjectTypeEnum.UNASSIGNED)
            return b_objectType;

        if (this.GetType() == typeof(PracticeController))
            this.b_objectType = ObjectTypeEnum.PRACTICE;

        else if (this.GetType() == typeof(ContextController))
            this.b_objectType = ObjectTypeEnum.CONTEXT;

        else if (this.GetType() == typeof(IntentionalElementController))
            this.b_objectType = ObjectTypeEnum.INTENTIONALELEMENT;

        else if (this.GetType() == typeof(ActorController))
            this.b_objectType = ObjectTypeEnum.ACTOR;

        else if (this.GetType() == typeof(ActivityController))
            this.b_objectType = ObjectTypeEnum.ACTIVITY;

        else if (this.GetType() == typeof(SMWObjectController))
            this.b_objectType = ObjectTypeEnum.SMWOBJECT;

        else if (this.GetType() == typeof(SemanticObjectController))
            this.b_objectType = ObjectTypeEnum.SEMANTICOBJECT;

        else if (this.GetType() == typeof(ComplexObjectController))
            this.b_objectType = ObjectTypeEnum.COMPLEXOBJECT;

        else if (this.GetType() == typeof(NavObjectController))
            this.b_objectType = ObjectTypeEnum.NAVOBJECT;

        else if (this.GetType() == typeof(CameraController))
            this.b_objectType = ObjectTypeEnum.CAMERA;
        else
            this.b_objectType = ObjectTypeEnum.UNASSIGNED;

        return b_objectType;
    }


     // Looks for an existing View, else creates one
    public override BaseView FindSetView()
    {
        if (this.View == null)
        {
            if (this is SemanticObjectController)
            {
                // Looks up for the existence of a view with the same objectURI
                SemanticObjectView semanticView = ObjectFactory.Instance.GetSemanticObjectViewByURI(((SemanticObjectController)this).FindObjectURI());

                if (semanticView != null)
                {
                    this.View = semanticView;
                    return this.View;
                }
                else
                    return null;
            }

            else
            {
                // Looks up for the existence of a view with the same entityID
                BaseView view = ObjectFactory.Instance.GetObjectViewByEnityID(FindEntityID());

                if (view != null)
                {
                    this.View = view;
                    return this.View;
                }
                else
                    return null;
            }
        }
        else
        {
            return this.View;
        }
    }

     // Looks for an existing View, else creates one
    public override BaseView FindCreateSetView(Transform parentTransform)
    {
        // Looks up for the existence of a view with the same entityID/objectURI
        if (FindSetView() != null)
            return FindSetView();
        
        if (this is SemanticObjectController)
        {
            // Get this instance node, if not found stop this methode
            INode semanticNode = ((SemanticObjectController)this).FindSemanticNode();
            if (semanticNode == null) return null;

            // Get ObjectType
            ObjectTypeEnum objectType = FindObjectType();

            // Get Category
            SMWCategory category = SMWParser.Instance.GetCategoryOfAInstanceByInstanceNode(semanticNode, objectType);
            if (category == null) return null;

            // Looks up for the existence of a view with the same objectURI
            this.View = ObjectFactory.Instance.viewFactory.createForSemanticObject(semanticNode, category, null, parentTransform);
            return this.View;
        }
        else
        {
            // Get entityID, stop if none is found
            string entityID = FindEntityID();
            if (entityID == null || entityID == "") return null;

            this.View = ObjectFactory.Instance.viewFactory.createForObject(entityID, parentTransform);
            return this.View = View;
        }
        
    }

    // Finds for an existing model, else creates one
    public override BaseModel FindSetModel()
    {
        if (this.Model == null)
        {
            if (this is SemanticObjectController)
            {
                // Looks up for the existence of a model with the same objectURI
                SemanticObjectModel semanticModel = ObjectFactory.Instance.GetSemanticObjectModelByURI(((SemanticObjectController)this).FindObjectURI());

                if (semanticModel != null)
                {
                    this.Model = semanticModel;
                    return this.Model;
                }
                else
                    return null;
            }

            else
            {
                if (EntityID == null || EntityID == "")
                    return null;

                // Looks up for the existence of a view with the same entityID
                BaseModel model = ObjectFactory.Instance.GetObjectModelByEntityID(EntityID);

                if (model != null)
                {
                    this.Model = model;
                    return this.Model;
                }
                else
                    return null;
            }
        }
        else
        {
            return this.Model;
        }
    }

     // Looks for an existing View, else creates one
    public override BaseModel FindCreateSetModel()
    {
        // Looks up for the existence of a view with the same entityID
        if (FindSetModel() != null)
            return FindSetModel();

        else if (this is SemanticObjectController)
        {
            // Get this instance node, if not found stop this methode
            INode semanticNode = ((SemanticObjectController)this).FindSemanticNode();
            if (semanticNode == null) return null;

            Debug.Log(((SemanticObjectController)this).FindSemanticNode());

            // Get ObjectType
            ObjectTypeEnum objectType = FindObjectType();

            // Get Category
            SMWCategory category = SMWParser.Instance.GetCategoryOfAInstanceByInstanceNode(semanticNode, objectType);
            if (category == null) return null;

            // Looks up for the existence of a model with the same objectURI
            this.Model = ObjectFactory.Instance.modelFactory.createForSemanticObject(semanticNode, category, null);

            return this.Model;
        }

        else
        {
            // Get entityID, stop if none is found
            string entityID = FindEntityID();
            if (entityID == null || entityID == "") return null;

            switch (this.GetType().ToString())
            {
                case "ObjectController":
                    this.Model = ObjectFactory.Instance.modelFactory.createForObject(entityID);
                    break;
                case "ComplexObjectController":
                    this.Model = ObjectFactory.Instance.modelFactory.createForComplexObject(entityID);
                    break;
                case "CameraController":
                    this.Model = ObjectFactory.Instance.modelFactory.createForCamera(entityID);
                    break;
                default:
                    this.Model = ObjectFactory.Instance.modelFactory.createForObject(entityID);
                    break;
            }

            return this.Model = Model;
        }
    }
}
