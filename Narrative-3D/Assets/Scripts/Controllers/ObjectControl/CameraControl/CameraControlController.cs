﻿using System;
using Object.CameraZoomInput;
using SCamera.State;
using SCamera.Type;
using Object.CameraObject.ActionInput;
using ObjectControl.Type;

public class CameraControlController : ObjectControlController, ICameraControlController{

    public CameraControlController(BaseModel model, BaseView view) : base(model, view)
    {
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void SetIdentifiableModelVariables()
    {
        // Inherit logic
        base.SetIdentifiableModelVariables();
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void RemoveIdentifiableModelVariables()
    {
        // Inherit logic
        base.RemoveIdentifiableModelVariables();
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void SetIdentifiableViewVariables()
    {
        // Inherit logic
        base.SetIdentifiableViewVariables();
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void RemoveIdentifiableViewVariables()
    {
        // Inherit logic
        base.RemoveIdentifiableViewVariables();
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void SetNonIdentifiableModelVariables()
    {
        // Inherit logic
        base.SetNonIdentifiableModelVariables();
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void RemoveNonIdentifiableModelVariables()
    {
        // Inherit logic
        base.RemoveNonIdentifiableModelVariables();
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void SetNonIdentifiableViewVariables()
    {
        // Inherit logic
        base.SetNonIdentifiableViewVariables();
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void RemoveNonIdentifiableViewVariables()
    {
        // Inherit logic
        base.RemoveNonIdentifiableViewVariables();
    }

    // This methode sets the handlers for the View-events
    protected override void SetViewHandlers()
    {
        // Inherit logic
        base.SetViewHandlers();
    }

    // This methode removes the handlers for the View-events
    protected override void RemoveViewHandlers()
    {
        // Inherit logic
        base.RemoveViewHandlers();
    }

    // This methode sets the handlers for the Model-events
    protected override void SetModelHandlers()
    {
        // Inherit logic
        base.SetModelHandlers();

        ((CameraControlModel)this.Model).OnCameraTypeChanged += HandleCameraTypeChanged;
        ((CameraControlModel)this.Model).OnCameraStateChanged += HandleCameraStateChanged;

        ((CameraControlModel)this.Model).OnDefaultCameraActionInputChanged += HandleDefaultCameraActionInputChanged;
        ((CameraControlModel)this.Model).OnCustomCameraActionInputChanged += HandleCustomCameraActionInputChanged;
        ((CameraControlModel)this.Model).OnAvailableCameraActionInputChanged += HandleAvailableCameraActionInputChanged;
        ((CameraControlModel)this.Model).OnCameraActionRestrictionsChanged += HandleCameraActionRestrictionsChanged;

        ((CameraControlModel)this.Model).OnZoomSpeedChanged += HandleZoomSpeedChanged;
        ((CameraControlModel)this.Model).OnStartingZoomFactorChanged += HandleStartingZoomFactorChanged;
        ((CameraControlModel)this.Model).OnMaxZoomRestrictedChanged += HandleMaxZoomRestrictedChanged;
        ((CameraControlModel)this.Model).OnMaxZoomFactorChanged += HandleMaxZoomFactorChanged;
        ((CameraControlModel)this.Model).OnMinZoomRestrictedChanged += HandleMinZoomRestrictedChanged;
        ((CameraControlModel)this.Model).OnMinZoomFactorChanged += HandleMinZoomFactorChanged;
    }

    // This methode removes the handlers for the Model-events
    protected override void RemoveModelHandlers()
    {
        // Inherit logic
        base.RemoveModelHandlers();

        ((CameraControlModel)this.Model).OnCameraTypeChanged -= HandleCameraTypeChanged;
        ((CameraControlModel)this.Model).OnCameraStateChanged -= HandleCameraStateChanged;

        ((CameraControlModel)this.Model).OnDefaultCameraActionInputChanged -= HandleDefaultCameraActionInputChanged;
        ((CameraControlModel)this.Model).OnCustomCameraActionInputChanged -= HandleCustomCameraActionInputChanged;
        ((CameraControlModel)this.Model).OnAvailableCameraActionInputChanged -= HandleAvailableCameraActionInputChanged;
        ((CameraControlModel)this.Model).OnCameraActionRestrictionsChanged -= HandleCameraActionRestrictionsChanged;

        ((CameraControlModel)this.Model).OnZoomSpeedChanged -= HandleZoomSpeedChanged;
        ((CameraControlModel)this.Model).OnStartingZoomFactorChanged -= HandleStartingZoomFactorChanged;
        ((CameraControlModel)this.Model).OnMaxZoomRestrictedChanged -= HandleMaxZoomRestrictedChanged;
        ((CameraControlModel)this.Model).OnMaxZoomFactorChanged -= HandleMaxZoomFactorChanged;
        ((CameraControlModel)this.Model).OnMinZoomRestrictedChanged -= HandleMinZoomRestrictedChanged;
        ((CameraControlModel)this.Model).OnMinZoomFactorChanged -= HandleMinZoomFactorChanged;
    }

    // Called when the model's cameraType changed
    private void HandleCameraTypeChanged(object sender, CameraTypeChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncCameraTypeModelToView();
    }

    // Sync the model's cameraState with the views's cameraState
    public virtual void SyncCameraStateViewToModel(CameraTypeEnum backedCameraType)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((CameraControlModel)this.Model).cameraType = backedCameraType;
    }

    // Sync the view's cameraState with the model's cameraState
    protected virtual void SyncCameraTypeModelToView()
    {
        ((CameraControlView)this.View).cameraType = ((CameraControlModel)this.Model).cameraType;
    }


    // Called when the model's cameraState changed
    private void HandleCameraStateChanged(object sender, CameraStateChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncCameraStateModelToView();
    }

    // Sync the model's cameraState with the views's cameraState
    public virtual void SyncCameraStateViewToModel(CameraStateEnum backedCameraState)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((CameraControlModel)this.Model).cameraState = backedCameraState;
    }

    // Sync the view's cameraState with the model's cameraState
    protected virtual void SyncCameraStateModelToView()
    {
        ((CameraControlView)this.View).cameraState = ((CameraControlModel)this.Model).cameraState;
    }


    // Called when the model's defaultActionInput changed
    private void HandleDefaultCameraActionInputChanged(object sender, DefaultCameraActionInputChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncCustomCameraActionModelToView();
    }

    // Sync the model's zoomSpeed with the views's zoomSpeed
    public virtual void SyncDefaultCameraInputViewToModel(CameraActionInput[] backedDefaultCameraActionInput)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((CameraControlModel)this.Model).defaultCameraActionInput = backedDefaultCameraActionInput;
    }

    // Sync the view's zoomSpeed with the model's zoomSpeed
    protected virtual void SyncDefaultCameraActionModelToView()
    {
        ((CameraControlView)this.View).defaultCameraActionInput = ((CameraControlModel)this.Model).defaultCameraActionInput;
    }


    // Called when the model's customActionInput changed
    private void HandleCustomCameraActionInputChanged(object sender, CustomCameraActionInputChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncCustomCameraActionModelToView();
    }

    // Sync the model's zoomSpeed with the views's zoomSpeed
    public virtual void SyncCustomCameraInputViewToModel(CameraActionInput[] backedCustomCameraActionInput)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((CameraControlModel)this.Model).customCameraActionInput = backedCustomCameraActionInput;
    }

    // Sync the view's zoomSpeed with the model's zoomSpeed
    protected virtual void SyncCustomCameraActionModelToView()
    {
        ((CameraControlView)this.View).customCameraActionInput = ((CameraControlModel)this.Model).customCameraActionInput;
    }


    // Called when the model's availableActionInput changed
    private void HandleAvailableCameraActionInputChanged(object sender, AvailableCameraActionInputChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncAvailableCameraActionModelToView();
    }

    // Sync the model's zoomSpeed with the views's zoomSpeed
    public virtual void SyncAvailableCameraInputViewToModel(CameraActionInput[] backedAvailableCameraActionInput)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((CameraControlModel)this.Model).availableCameraActionInput = backedAvailableCameraActionInput;
    }

    // Sync the view's zoomSpeed with the model's zoomSpeed
    protected virtual void SyncAvailableCameraActionModelToView()
    {
        ((CameraControlView)this.View).availableCameraActionInput = ((CameraControlModel)this.Model).availableCameraActionInput;
    }

    // 
    private void HandleCameraActionRestrictionsChanged(object sender, CameraActionRestrictionsChangedEventArgs e)
    {
        SyncCameraActionRestrictionModelToView();
    }

    // Sync the model's zoomSpeed with the views's zoomSpeed
    public virtual void SyncCameraActionRestrictionViewToModel(CameraActionInputEnum[] backedCameraActionRestrictions)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((CameraControlModel)this.Model).actionRestrictions = backedCameraActionRestrictions;
    }

    // Sync the view's zoomSpeed with the model's zoomSpeed
    protected virtual void SyncCameraActionRestrictionModelToView()
    {
        ((CameraControlView)this.View).actionRestrictions = ((CameraControlModel)this.Model).actionRestrictions;
    }



    // Called when the model's zoomSpeed changed
    private void HandleZoomSpeedChanged(object sender, ZoomSpeedChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncZoomSpeedModelToView();
    }

    // Sync the model's zoomSpeed with the views's zoomSpeed
    public virtual void SyncZoomSpeedViewToModel(float backedZoomSpeed)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((CameraControlModel)this.Model).zoomSpeed = backedZoomSpeed;
    }

    // Sync the view's zoomSpeed with the model's zoomSpeed
    protected virtual void SyncZoomSpeedModelToView()
    {
        ((CameraControlView)this.View).zoomSpeed = ((CameraControlModel)this.Model).zoomSpeed;
    }


    // Called when the model's startingZoomSpeed changed
    private void HandleStartingZoomFactorChanged(object sender, StartingZoomFactorChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncStartingZoomFactorModelToView();
    }

    // Sync the model's startingZoomFactor with the views's startingZoomFactor
    public virtual void SyncStartingZoomFactorViewToModel(float backedStartingZoomFactor)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((CameraControlModel)this.Model).startingZoomFactor = backedStartingZoomFactor;
    }

    // Sync the view's startingZoomFactor with the model's startingZoomFactor
    protected virtual void SyncStartingZoomFactorModelToView()
    {
        ((CameraControlView)this.View).startingZoomFactor = ((CameraControlModel)this.Model).startingZoomFactor;
    }


    // Called when the model's maxZoomRestricted changed
    private void HandleMaxZoomRestrictedChanged(object sender, MaxZoomRestrictedChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncMaxZoomRestrictedModelToView();
    }

    // Sync the model's maxZoomRestricted with the views's maxZoomRestricted
    public virtual void SyncMaxZoomRestrictedViewToModel(bool backedMaxZoomRestricted)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((CameraControlModel)this.Model).minZoomRestricted = backedMaxZoomRestricted;
    }

    // Sync the view's maxZoomRestricted with the model's maxZoomRestricted
    protected virtual void SyncMaxZoomRestrictedModelToView()
    {
        ((CameraControlView)this.View).minZoomRestricted = ((CameraControlModel)this.Model).minZoomRestricted;
    }


    // Called when the model's maxZoomFactor changed
    private void HandleMaxZoomFactorChanged(object sender, MaxZoomFactorChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncMaxZoomFactorModelToView();
    }

    // Sync the model's maxZoomFactor with the views's maxZoomFactor
    public virtual void SyncMaxZoomFactorViewToModel(float backedMaxZoomFactor)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((CameraControlModel)this.Model).minZoomFactor = backedMaxZoomFactor;
    }

    // Sync the view's maxZoomFactor with the model's maxZoomFactor
    protected virtual void SyncMaxZoomFactorModelToView()
    {
        ((CameraControlView)this.View).minZoomFactor = ((CameraControlModel)this.Model).minZoomFactor;
    }


    // Called when the model's minZoomRestricted changed
    private void HandleMinZoomRestrictedChanged(object sender, MinZoomRestrictedChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncMinZoomRestrictedModelToView();
    }

    // Sync the model's minZoomRestricted with the views's minZoomRestricted
    public virtual void SyncMinZoomRestrictedViewToModel(bool backedMinZoomRestricted)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((CameraControlModel)this.Model).minZoomRestricted = backedMinZoomRestricted;
    }

    // Sync the view's minZoomRestricted with the model's minZoomRestricted
    protected virtual void SyncMinZoomRestrictedModelToView()
    {
        ((CameraControlView)this.View).minZoomRestricted = ((CameraControlModel)this.Model).minZoomRestricted;
    }


    // Called when the model's minZoomFactor changed
    private void HandleMinZoomFactorChanged(object sender, MinZoomFactorChangedEventArgs e)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.View == null) return;

        //Sync Model To View
        SyncMinZoomFactorModelToView();
    }

    // Sync the model's minZoomFactor with the views's minZoomFactor
    public virtual void SyncMinZoomFactorViewToModel(float backedMinZoomFactor)
    {
        //Check if view exists, if not exists this function will be stopt
        if (this.Model == null) return;

        ((CameraControlModel)this.Model).minZoomFactor = backedMinZoomFactor;
    }

    // Sync the view's minZoomFactor with the model's minZoomFactor
    protected virtual void SyncMinZoomFactorModelToView()
    {
        ((CameraControlView)this.View).minZoomFactor = ((CameraControlModel)this.Model).minZoomFactor;
    }

}
