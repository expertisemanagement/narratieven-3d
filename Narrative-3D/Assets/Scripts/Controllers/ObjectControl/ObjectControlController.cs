﻿using Object.ControlSetup;
using Object.Movement;
using Object.MovementInput;
using Object.MovementRestriction;
using Object.Type;
using ObjectControl.Type;
using UnityEngine;

public class ObjectControlController : BaseController, IObjectControlController{

    public ObjectControlController(BaseModel model, BaseView view) : base(model, view)
    {
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void SetIdentifiableModelVariables()
    {
        // Inherit logic
        base.SetIdentifiableModelVariables();
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void RemoveIdentifiableModelVariables()
    {
        // Inherit logic
        base.RemoveIdentifiableModelVariables();
    }

    // This methode sets the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void SetIdentifiableViewVariables()
    {
        // Inherit logic
        base.SetIdentifiableViewVariables();
    }

    // This methode removes the variables for that are required for indectification (the "Find"-methodes use these)
    protected override void RemoveIdentifiableViewVariables()
    {
        // Inherit logic
        base.RemoveIdentifiableViewVariables();
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void SetNonIdentifiableModelVariables()
    {
        // Inherit logic
        base.SetNonIdentifiableModelVariables();
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void RemoveNonIdentifiableModelVariables()
    {
        // Inherit logic
        base.RemoveNonIdentifiableModelVariables();
    }

    // This methode sets the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void SetNonIdentifiableViewVariables()
    {
        // Inherit logic
        base.SetNonIdentifiableViewVariables();
    }

    // This methode removes the variables for that are NOT required for indectification (the "Find"-methodes use these)
    protected override void RemoveNonIdentifiableViewVariables()
    {
        // Inherit logic
        base.RemoveNonIdentifiableViewVariables();
    }

    // This methode sets the handlers for the View-events
    protected override void SetViewHandlers()
    {
        // Inherit logic
        base.SetViewHandlers();
    }

    // This methode removes the handlers for the View-events
    protected override void RemoveViewHandlers()
    {
        // Inherit logic
        base.RemoveViewHandlers();
    }

    // This methode sets the handlers for the Model-events
    protected override void SetModelHandlers()
    {
        // Inherit logic
        base.SetModelHandlers();

        ((ObjectControlModel)this.Model).OnDefaultObjectMovementInputChanged += HandleDefaultObjectMovementInputChanged;
        ((ObjectControlModel)this.Model).OnCustomObjectMovementInputChanged += HandleCustomObjectMovementInputChanged;
        ((ObjectControlModel)this.Model).OnAvailableObjectMovementInputChanged += HandleAvailableObjectMovementInputChanged;

        ((ObjectControlModel)this.Model).OnControlSetupChanged += HandleControlSetupChanged;
        ((ObjectControlModel)this.Model).OnMovementChanged += HandleMovementChanged;
        ((ObjectControlModel)this.Model).OnMovementRestrictionsChanged += HandleMovementRestrictionsChanged;

        ((ObjectControlModel)this.Model).OnLongitudinalMovementSpeedChanged += HandleLongitudinalMovementSpeedChanged;
        ((ObjectControlModel)this.Model).OnStartingDistanceLongitudinalMovementChanged += HandleStartingDistanceLongitudinalMovementChanged;
        ((ObjectControlModel)this.Model).OnMaxLongitudinalMoveDistanceRestrictedChanged += HandleMaxLongitudinalMoveDistanceRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMaxLongitudinalMoveDistanceChanged += HandleMaxLongitudinalMoveDistanceChanged;
        ((ObjectControlModel)this.Model).OnMinLongitudinalMoveDistanceRestrictedChanged += HandleMinLongitudinalMoveDistanceRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMinLongitudinalMoveDistanceChanged += HandleMinLongitudinalMoveDistanceChanged;

        ((ObjectControlModel)this.Model).OnVerticalMovementSpeedChanged += HandleVerticalMovementSpeedChanged;
        ((ObjectControlModel)this.Model).OnStartingDistanceVerticalMovementChanged += HandleStartingDistanceVerticalMovementChanged;
        ((ObjectControlModel)this.Model).OnMaxVerticalMoveDistanceRestrictedChanged += HandleMaxVerticalMoveDistanceRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMaxVerticalMoveDistanceChanged += HandleMaxVerticalMoveDistanceChanged;
        ((ObjectControlModel)this.Model).OnMinVerticalMoveDistanceRestrictedChanged += HandleMinVerticalMoveDistanceRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMinVerticalMoveDistanceChanged += HandleMinVerticalMoveDistanceChanged;

        ((ObjectControlModel)this.Model).OnLateralMovementSpeedChanged += HandleLateralMovementSpeedChanged;
        ((ObjectControlModel)this.Model).OnStartingDistanceLateralMovementChanged += HandleStartingDistanceLateralMovementChanged;
        ((ObjectControlModel)this.Model).OnMaxLateralMoveDistanceRestrictedChanged += HandleMaxLateralMoveDistanceRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMaxLateralMoveDistanceChanged += HandleMaxLateralMoveDistanceChanged;
        ((ObjectControlModel)this.Model).OnMinLateralMoveDistanceRestrictedChanged += HandleMinLateralMoveDistanceRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMinLateralMoveDistanceChanged += HandleMinLateralMoveDistanceChanged;

        ((ObjectControlModel)this.Model).OnPitchRotationSpeedChanged += HandlePitchRotationSpeedChanged;
        ((ObjectControlModel)this.Model).OnPitchRotationAngleRestrictedChanged += HandlePitchRotationAngleRestrictedChanged;
        ((ObjectControlModel)this.Model).OnStartingPitchRotationAngleChanged += HandleStartingPitchRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxPitchNegRotationAngleChanged += HandleMaxPitchNegRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxPitchPosRotationAngleChanged += HandleMaxPitchPosRotationAngleChanged;

        ((ObjectControlModel)this.Model).OnYawRotationSpeedChanged += HandleYawRotationSpeedChanged;
        ((ObjectControlModel)this.Model).OnYawRotationAngleRestrictedChanged += HandleYawRotationAngleRestrictedChanged;
        ((ObjectControlModel)this.Model).OnStartingYawRotationAngleChanged += HandleStartingYawRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxYawNegRotationAngleChanged += HandleMaxYawNegRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxYawPosRotationAngleChanged += HandleMaxYawPosRotationAngleChanged;

        ((ObjectControlModel)this.Model).OnRollRotationSpeedChanged += HandleRollRotationSpeedChanged;
        ((ObjectControlModel)this.Model).OnRollRotationAngleRestrictedChanged += HandleRollRotationAngleRestrictedChanged;
        ((ObjectControlModel)this.Model).OnStartingRollRotationAngleChanged += HandleStartingRollRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxRollNegRotationAngleChanged += HandleMaxRollNegRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxRollPosRotationAngleChanged += HandleMaxRollPosRotationAngleChanged;

        ((ObjectControlModel)this.Model).OnRadiusChangeSpeedChanged += HandleRadiusChangeSpeedChanged;
        ((ObjectControlModel)this.Model).OnRadiusRestrictedChanged += HandleRadiusRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMaxRadiusExpansionChanged += HandleMaxRadiusExpansionChanged;
        ((ObjectControlModel)this.Model).OnMinRadiusFromCenterChanged += HandleMinRadiusFromCenterChanged;

        ((ObjectControlModel)this.Model).OnPolarAngleCoordinatesChangeSpeedChanged += HandlePolarAngleCoordinatesChangeSpeedChanged;
        ((ObjectControlModel)this.Model).OnPolarRotationAroundAngleRestrictedChanged += HandlePolarRotationAroundAngleRestrictedChanged;
        ((ObjectControlModel)this.Model).OnStartingPolarRotationAroundAngleChanged += HandleStartingPolarRotationAroundAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxPolarNegRotationAroundAngleChanged += HandleMaxPolarNegRotationAroundAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxPolarPosRotationAroundAngleChanged += HandleMaxPolarPosRotationAroundAngleChanged;

        ((ObjectControlModel)this.Model).OnAzimuthalAngleCoordinatesChangeSpeedChanged += HandleAzimuthalAngleCoordinatesChangeSpeedChanged;
        ((ObjectControlModel)this.Model).OnAzimuthalRotationAroundAngleRestrictedChanged += HandleAzimuthalRotationAroundAngleRestrictedChanged;
        ((ObjectControlModel)this.Model).OnStartingAzimuthalRotationAroundAngleChanged += HandleStartingAzimuthalRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxAzimuthalNegRotationAroundAngleChanged += HandleMaxAzimuthalNegRotationAroundAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxAzimuthalPosRotationAroundAngleChanged += HandleMaxAzimuthalPosRotationAroundAngleChanged;

        ((ObjectControlModel)this.Model).OnSubjectChanged += HandleSubjectChanged;
        ((ObjectControlModel)this.Model).OnDedicatedObjectChanged += HandleDedicatedObjectChanged;
        ((ObjectControlModel)this.Model).OnTargetChanged += HandleTargetChanged;
    }

    // This methode removes the handlers for the Model-events
    protected override void RemoveModelHandlers()
    {
        // Inherit logic
        base.RemoveModelHandlers();


        ((ObjectControlModel)this.Model).OnDefaultObjectMovementInputChanged -= HandleDefaultObjectMovementInputChanged;
        ((ObjectControlModel)this.Model).OnCustomObjectMovementInputChanged -= HandleCustomObjectMovementInputChanged;
        ((ObjectControlModel)this.Model).OnAvailableObjectMovementInputChanged -= HandleAvailableObjectMovementInputChanged;

        ((ObjectControlModel)this.Model).OnControlSetupChanged -= HandleControlSetupChanged;
        ((ObjectControlModel)this.Model).OnMovementChanged -= HandleMovementChanged;
        ((ObjectControlModel)this.Model).OnMovementRestrictionsChanged -= HandleMovementRestrictionsChanged;

        ((ObjectControlModel)this.Model).OnLongitudinalMovementSpeedChanged -= HandleLongitudinalMovementSpeedChanged;
        ((ObjectControlModel)this.Model).OnStartingDistanceLongitudinalMovementChanged -= HandleStartingDistanceLongitudinalMovementChanged;
        ((ObjectControlModel)this.Model).OnMaxLongitudinalMoveDistanceRestrictedChanged -= HandleMaxLongitudinalMoveDistanceRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMaxLongitudinalMoveDistanceChanged -= HandleMaxLongitudinalMoveDistanceChanged;
        ((ObjectControlModel)this.Model).OnMinLongitudinalMoveDistanceRestrictedChanged -= HandleMinLongitudinalMoveDistanceRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMinLongitudinalMoveDistanceChanged -= HandleMinLongitudinalMoveDistanceChanged;

        ((ObjectControlModel)this.Model).OnVerticalMovementSpeedChanged -= HandleVerticalMovementSpeedChanged;
        ((ObjectControlModel)this.Model).OnStartingDistanceVerticalMovementChanged -= HandleStartingDistanceVerticalMovementChanged;
        ((ObjectControlModel)this.Model).OnMaxVerticalMoveDistanceRestrictedChanged -= HandleMaxVerticalMoveDistanceRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMaxVerticalMoveDistanceChanged -= HandleMaxVerticalMoveDistanceChanged;
        ((ObjectControlModel)this.Model).OnMinVerticalMoveDistanceRestrictedChanged -= HandleMinVerticalMoveDistanceRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMinVerticalMoveDistanceChanged -= HandleMinVerticalMoveDistanceChanged;

        ((ObjectControlModel)this.Model).OnLateralMovementSpeedChanged -= HandleLateralMovementSpeedChanged;
        ((ObjectControlModel)this.Model).OnStartingDistanceLateralMovementChanged -= HandleStartingDistanceLateralMovementChanged;
        ((ObjectControlModel)this.Model).OnMaxLateralMoveDistanceRestrictedChanged -= HandleMaxLateralMoveDistanceRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMaxLateralMoveDistanceChanged -= HandleMaxLateralMoveDistanceChanged;
        ((ObjectControlModel)this.Model).OnMinLateralMoveDistanceRestrictedChanged -= HandleMinLateralMoveDistanceRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMinLateralMoveDistanceChanged -= HandleMinLateralMoveDistanceChanged;

        ((ObjectControlModel)this.Model).OnPitchRotationSpeedChanged -= HandlePitchRotationSpeedChanged;
        ((ObjectControlModel)this.Model).OnPitchRotationAngleRestrictedChanged -= HandlePitchRotationAngleRestrictedChanged;
        ((ObjectControlModel)this.Model).OnStartingPitchRotationAngleChanged -= HandleStartingPitchRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxPitchNegRotationAngleChanged -= HandleMaxPitchNegRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxPitchPosRotationAngleChanged -= HandleMaxPitchPosRotationAngleChanged;

        ((ObjectControlModel)this.Model).OnYawRotationSpeedChanged -= HandleYawRotationSpeedChanged;
        ((ObjectControlModel)this.Model).OnYawRotationAngleRestrictedChanged -= HandleYawRotationAngleRestrictedChanged;
        ((ObjectControlModel)this.Model).OnStartingYawRotationAngleChanged -= HandleStartingYawRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxYawNegRotationAngleChanged -= HandleMaxYawNegRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxYawPosRotationAngleChanged -= HandleMaxYawPosRotationAngleChanged;

        ((ObjectControlModel)this.Model).OnRollRotationSpeedChanged -= HandleRollRotationSpeedChanged;
        ((ObjectControlModel)this.Model).OnRollRotationAngleRestrictedChanged -= HandleRollRotationAngleRestrictedChanged;
        ((ObjectControlModel)this.Model).OnStartingRollRotationAngleChanged -= HandleStartingRollRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxRollNegRotationAngleChanged -= HandleMaxRollNegRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxRollPosRotationAngleChanged -= HandleMaxRollPosRotationAngleChanged;

        ((ObjectControlModel)this.Model).OnRadiusChangeSpeedChanged -= HandleRadiusChangeSpeedChanged;
        ((ObjectControlModel)this.Model).OnRadiusRestrictedChanged -= HandleRadiusRestrictedChanged;
        ((ObjectControlModel)this.Model).OnMaxRadiusExpansionChanged -= HandleMaxRadiusExpansionChanged;
        ((ObjectControlModel)this.Model).OnMinRadiusFromCenterChanged -= HandleMinRadiusFromCenterChanged;

        ((ObjectControlModel)this.Model).OnPolarAngleCoordinatesChangeSpeedChanged -= HandlePolarAngleCoordinatesChangeSpeedChanged;
        ((ObjectControlModel)this.Model).OnPolarRotationAroundAngleRestrictedChanged -= HandlePolarRotationAroundAngleRestrictedChanged;
        ((ObjectControlModel)this.Model).OnStartingPolarRotationAroundAngleChanged -= HandleStartingPolarRotationAroundAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxPolarNegRotationAroundAngleChanged -= HandleMaxPolarNegRotationAroundAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxPolarPosRotationAroundAngleChanged -= HandleMaxPolarPosRotationAroundAngleChanged;

        ((ObjectControlModel)this.Model).OnAzimuthalAngleCoordinatesChangeSpeedChanged -= HandleAzimuthalAngleCoordinatesChangeSpeedChanged;
        ((ObjectControlModel)this.Model).OnAzimuthalRotationAroundAngleRestrictedChanged -= HandleAzimuthalRotationAroundAngleRestrictedChanged;
        ((ObjectControlModel)this.Model).OnStartingAzimuthalRotationAroundAngleChanged -= HandleStartingAzimuthalRotationAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxAzimuthalNegRotationAroundAngleChanged -= HandleMaxAzimuthalNegRotationAroundAngleChanged;
        ((ObjectControlModel)this.Model).OnMaxAzimuthalPosRotationAroundAngleChanged -= HandleMaxAzimuthalPosRotationAroundAngleChanged;

        ((ObjectControlModel)this.Model).OnSubjectChanged -= HandleSubjectChanged;
        ((ObjectControlModel)this.Model).OnDedicatedObjectChanged -= HandleDedicatedObjectChanged;
        ((ObjectControlModel)this.Model).OnTargetChanged -= HandleTargetChanged;
    }


    private void HandleDefaultObjectMovementInputChanged(object sender, DefaultObjectMovementInputChangedEventArgs e)
    {
        SyncDefaultObjectMovementInputModelToView();
    }

    public void SyncDefaultObjectMovementInputViewToModel(ObjectMovementInput[] objectMovementInput)
    {
        ((ObjectControlModel)this.Model).defaultObjectMovementInput = objectMovementInput;
    }

    private void SyncDefaultObjectMovementInputModelToView()
    {
        ((ObjectControlView)this.View).defaultObjectMovementInput = ((ObjectControlModel)this.Model).defaultObjectMovementInput;
    }

    private void HandleCustomObjectMovementInputChanged(object sender, CustomObjectMovementInputChangedEventArgs e)
    {
        SyncCustomObjectMovementInputModelToView();
    }

    public void SyncCustomObjectMovementInputViewToModel(ObjectMovementInput[] customObjectMovementInput)
    {
        ((ObjectControlModel)this.Model).customObjectMovementInput = customObjectMovementInput;
    }

    private void SyncCustomObjectMovementInputModelToView()
    {
        ((ObjectControlView)this.View).customObjectMovementInput = ((ObjectControlModel)this.Model).customObjectMovementInput;
    }

    private void HandleAvailableObjectMovementInputChanged(object sender, AvailableObjectMovementInputChangedEventArgs e)
    {
        SyncAvailableObjectMovementInputModelToView();
    }

    public void SyncAvailableObjectMovementInputViewToModel(ObjectMovementInput[] availableObjectMovementInput)
    {
        ((ObjectControlModel)this.Model).availableObjectMovementInput = availableObjectMovementInput;
    }

    private void SyncAvailableObjectMovementInputModelToView()
    {
        ((ObjectControlView)this.View).availableObjectMovementInput = ((ObjectControlModel)this.Model).availableObjectMovementInput;
    }


    private void HandleControlSetupChanged(object sender, ControlSetupChangedEventArgs e)
    {
        SyncControlSetupModelToView();
    }

    public void SyncControlSetupViewToModel(ControlSetupEnum controlSetup)
    {
        ((ObjectControlModel)this.Model).controlSetup = controlSetup;
    }

    private void SyncControlSetupModelToView()
    {
        ((ObjectControlView)this.View).controlSetup = ((ObjectControlModel)this.Model).controlSetup;
    }

    private void HandleMovementChanged(object sender, MovementChangedEventArgs e)
    {
        SyncMovementModelToView();
    }

    public void SyncMovementViewToModel(ObjectMovementEnum[] movement)
    {
        ((ObjectControlModel)this.Model).movement = movement;
    }

    private void SyncMovementModelToView()
    {
        ((ObjectControlView)this.View).movement = ((ObjectControlModel)this.Model).movement;
    }

    private void HandleMovementRestrictionsChanged(object sender, MovementRestrictionsChangedEventArgs e)
    {
        SyncMovementRestrictionsModelToView();
    }

    public void SyncMovementRestrictionsViewToModel(ObjectMovementRestrictionEnum[] movementRestriction)
    {
        ((ObjectControlModel)this.Model).movementRestrictions = movementRestriction;
    }

    private void SyncMovementRestrictionsModelToView()
    {
        ((ObjectControlView)this.View).movementRestrictions = ((ObjectControlModel)this.Model).movementRestrictions;
    }


    private void HandleLongitudinalMovementSpeedChanged(object sender, LongitudinalMovementSpeedChangedEventArgs e)
    {
        SyncLongitudinalMovementSpeedModelToView();
    }

    public void SyncLongitudinalMovementSpeedViewToModel(float backedLongitudinalMovementSpeed)
    {
        ((ObjectControlModel)this.Model).longitudinalMovementSpeed = backedLongitudinalMovementSpeed;
    }

    private void SyncLongitudinalMovementSpeedModelToView()
    {
        ((ObjectControlView)this.View).longitudinalMovementSpeed = ((ObjectControlModel)this.Model).longitudinalMovementSpeed;
    }

    private void HandleStartingDistanceLongitudinalMovementChanged(object sender, StartingDistanceLongitudinalMovementChangedEventArgs e)
    {
        SyncStartingDistanceLongitudinalMovementModelToView();
    }

    public void SyncStartingDistanceLongitudinalMovementViewToModel(float backedStartingDistanceLongitudinalMovementChanged)
    {
        ((ObjectControlModel)this.Model).startingDistanceLongitudinalMovement = backedStartingDistanceLongitudinalMovementChanged;
    }

    private void SyncStartingDistanceLongitudinalMovementModelToView()
    {
        ((ObjectControlView)this.View).startingDistanceLongitudinalMovement = ((ObjectControlModel)this.Model).startingDistanceLongitudinalMovement;
    }

    private void HandleMaxLongitudinalMoveDistanceRestrictedChanged(object sender, MaxLongitudinalMoveDistanceRestrictedChangedEventArgs e)
    {
        SyncMaxLongitudinalMoveDistanceRestrictedModelToView();
    }

    public void SyncMaxLongitudinalMoveDistanceRestrictedViewToModel(bool backedMaxLongitudinalMoveDistanceRestricted)
    {
        ((ObjectControlModel)this.Model).maxLongitudinalMoveDistanceRestricted = backedMaxLongitudinalMoveDistanceRestricted;
    }

    private void SyncMaxLongitudinalMoveDistanceRestrictedModelToView()
    {
        ((ObjectControlView)this.View).maxLongitudinalMoveDistanceRestricted = ((ObjectControlModel)this.Model).maxLongitudinalMoveDistanceRestricted;
    }

    private void HandleMaxLongitudinalMoveDistanceChanged(object sender, MaxLongitudinalMoveDistanceChangedEventArgs e)
    {
        SyncMaxLongitudinalMoveDistanceModelToView();
    }

    public void SyncMaxLongitudinalMoveDistanceViewToModel(float backedMaxLongitudinalMoveDistance)
    {
        ((ObjectControlModel)this.Model).maxLongitudinalMoveDistance = backedMaxLongitudinalMoveDistance;
    }

    private void SyncMaxLongitudinalMoveDistanceModelToView()
    {
        ((ObjectControlView)this.View).maxLongitudinalMoveDistance = ((ObjectControlModel)this.Model).maxLongitudinalMoveDistance;
    }

    private void HandleMinLongitudinalMoveDistanceRestrictedChanged(object sender, MinLongitudinalMoveDistanceRestrictedChangedEventArgs e)
    {
        SyncMinLongitudinalMoveDistanceRestrictedModelToView();
    }

    public void SyncMinLongitudinalMoveDistanceRestrictedViewToModel(bool backedMinLongitudinalMoveDistanceRestricted)
    {
        ((ObjectControlModel)this.Model).minLongitudinalMoveDistanceRestricted = backedMinLongitudinalMoveDistanceRestricted;
    }

    private void SyncMinLongitudinalMoveDistanceRestrictedModelToView()
    {
        ((ObjectControlView)this.View).minLongitudinalMoveDistanceRestricted = ((ObjectControlModel)this.Model).minLongitudinalMoveDistanceRestricted;
    }

    private void HandleMinLongitudinalMoveDistanceChanged(object sender, b_minLongitudinalMoveDistanceChangedEventArgs e)
    {
        SyncMinLongitudinalMoveDistanceModelToView();
    }

    public void SyncMinLongitudinalMoveDistanceViewToModel(float backedMinLongitudinalMoveDistance)
    {
        ((ObjectControlModel)this.Model).minLongitudinalMoveDistance = backedMinLongitudinalMoveDistance;
    }

    private void SyncMinLongitudinalMoveDistanceModelToView()
    {
        ((ObjectControlView)this.View).minLongitudinalMoveDistance = ((ObjectControlModel)this.Model).minLongitudinalMoveDistance;
    }


    private void HandleVerticalMovementSpeedChanged(object sender, VerticalMovementSpeedChangedEventArgs e)
    {
        SyncVerticalMovementSpeedModelToView();
    }

    public void SyncVerticalMovementSpeedViewToModel(float backedVerticalMovementSpeed)
    {
        ((ObjectControlModel)this.Model).verticalMovementSpeed = backedVerticalMovementSpeed;
    }

    private void SyncVerticalMovementSpeedModelToView()
    {
        ((ObjectControlView)this.View).verticalMovementSpeed = ((ObjectControlModel)this.Model).verticalMovementSpeed;
    }

    private void HandleStartingDistanceVerticalMovementChanged(object sender, StartingDistanceVerticalMovementChangedEventArgs e)
    {
        SyncStartingDistanceVerticalMovementChangedModelToView();
    }

    public void SyncStartingDistanceVerticalMovementViewToModel(float backedStartingDistanceVerticalMovement)
    {
        ((ObjectControlModel)this.Model).startingDistanceVerticalMovement = backedStartingDistanceVerticalMovement;
    }

    private void SyncStartingDistanceVerticalMovementChangedModelToView()
    {
        ((ObjectControlView)this.View).startingDistanceVerticalMovement = ((ObjectControlModel)this.Model).startingDistanceVerticalMovement;
    }

    private void HandleMaxVerticalMoveDistanceRestrictedChanged(object sender, MaxVerticalMoveDistanceRestrictedChangedEventArgs e)
    {
        SyncMaxVerticalMoveDistanceRestrictedModelToView();
    }

    public void SyncMaxVerticalMoveDistanceRestrictedViewToModel(bool backedMaxVerticalMoveDistanceRestricted)
    {
        ((ObjectControlModel)this.Model).maxVerticalMoveDistanceRestricted = backedMaxVerticalMoveDistanceRestricted;
    }

    private void SyncMaxVerticalMoveDistanceRestrictedModelToView()
    {
        ((ObjectControlView)this.View).maxVerticalMoveDistanceRestricted = ((ObjectControlModel)this.Model).maxVerticalMoveDistanceRestricted;
    }

    private void HandleMaxVerticalMoveDistanceChanged(object sender, MaxVerticalMoveDistanceChangedEventArgs e)
    {
        SyncMaxVerticalMoveDistanceModelToView();
    }

    public void SyncMaxVerticalMoveDistanceViewToModel(float backedMaxVerticalMoveDistance)
    {
        ((ObjectControlModel)this.Model).maxVerticalMoveDistance = backedMaxVerticalMoveDistance;
    }

    private void SyncMaxVerticalMoveDistanceModelToView()
    {
        ((ObjectControlView)this.View).maxVerticalMoveDistance = ((ObjectControlModel)this.Model).maxVerticalMoveDistance;
    }

    private void HandleMinVerticalMoveDistanceRestrictedChanged(object sender, MinVerticalMoveDistanceRestrictedChangedEventArgs e)
    {
        SyncMinVerticalMoveDistanceRestrictedModelToView();
    }

    public void SyncMinVerticalMoveDistanceRestrictedViewToModel(bool backedMinVerticalMoveDistanceRestricted)
    {
        ((ObjectControlModel)this.Model).minVerticalMoveDistanceRestricted = backedMinVerticalMoveDistanceRestricted;
    }

    private void SyncMinVerticalMoveDistanceRestrictedModelToView()
    {
        ((ObjectControlView)this.View).minVerticalMoveDistanceRestricted = ((ObjectControlModel)this.Model).minVerticalMoveDistanceRestricted;
    }

    private void HandleMinVerticalMoveDistanceChanged(object sender, MinVerticalMoveDistanceChangedEventArgs e)
    {
        SyncMinVerticalMoveDistanceModelToView();
    }

    public void SyncMinVerticalMoveDistanceViewToModel(float backedMinVerticalMoveDistance)
    {
        ((ObjectControlModel)this.Model).minVerticalMoveDistance = backedMinVerticalMoveDistance;
    }

    private void SyncMinVerticalMoveDistanceModelToView()
    {
        ((ObjectControlView)this.View).minVerticalMoveDistance = ((ObjectControlModel)this.Model).minVerticalMoveDistance;
    }


    private void HandleLateralMovementSpeedChanged(object sender, LateralMovementSpeedChangedEventArgs e)
    {
        SyncLateralMovementSpeedModelToView();
    }

    public void SyncLateralMovementSpeedViewToModel(float backedLateralMovementSpeed)
    {
        ((ObjectControlModel)this.Model).lateralMovementSpeed = backedLateralMovementSpeed;
    }

    private void SyncLateralMovementSpeedModelToView()
    {
        ((ObjectControlView)this.View).lateralMovementSpeed = ((ObjectControlModel)this.Model).lateralMovementSpeed;
    }

    private void HandleStartingDistanceLateralMovementChanged(object sender, StartingDistanceLateralMovementChangedEventArgs e)
    {
        SyncStartingDistanceLateralMovementModelToView();
    }

    public void SyncStartingDistanceLateralMovementViewToModel(float backedStartingDistanceLateralMovement)
    {
        ((ObjectControlModel)this.Model).startingDistanceLateralMovement = backedStartingDistanceLateralMovement;
    }

    private void SyncStartingDistanceLateralMovementModelToView()
    {
        ((ObjectControlView)this.View).startingDistanceLateralMovement = ((ObjectControlModel)this.Model).startingDistanceLateralMovement;
    }

    private void HandleMaxLateralMoveDistanceRestrictedChanged(object sender, MaxLateralMoveDistanceRestrictedChangedEventArgs e)
    {
        SyncMaxLateralMoveDistanceRestrictedModelToView();
    }

    public void SyncMaxLateralMoveDistanceRestrictedViewToModel(bool backedMaxLateralMoveDistanceRestricted)
    {
        ((ObjectControlModel)this.Model).maxLateralMoveDistanceRestricted = backedMaxLateralMoveDistanceRestricted;
    }

    private void SyncMaxLateralMoveDistanceRestrictedModelToView()
    {
        ((ObjectControlView)this.View).maxLateralMoveDistance = ((ObjectControlModel)this.Model).maxLateralMoveDistance;
    }

    private void HandleMaxLateralMoveDistanceChanged(object sender, MaxLateralMoveDistanceChangedEventArgs e)
    {
        SyncMaxLateralMoveDistanceModelToView();
    }

    public void SyncMaxLateralMoveDistanceViewToModel(float backedMaxLateralMoveDistance)
    {
        ((ObjectControlModel)this.Model).maxLateralMoveDistance = backedMaxLateralMoveDistance;
    }

    private void SyncMaxLateralMoveDistanceModelToView()
    {
        ((ObjectControlView)this.View).maxLateralMoveDistance = ((ObjectControlModel)this.Model).maxLateralMoveDistance;
    }

    private void HandleMinLateralMoveDistanceRestrictedChanged(object sender, MinLateralMoveDistanceRestrictedChangedEventArgs e)
    {
        SyncMinLateralMoveDistanceRestrictedModelToView();
    }

    public void SyncMinLateralMoveDistanceRestrictedViewToModel(bool backedMinLateralMoveDistanceRestricted)
    {
        ((ObjectControlModel)this.Model).minLateralMoveDistanceRestricted = backedMinLateralMoveDistanceRestricted;
    }

    private void SyncMinLateralMoveDistanceRestrictedModelToView()
    {
        ((ObjectControlView)this.View).minLateralMoveDistanceRestricted = ((ObjectControlModel)this.Model).minLateralMoveDistanceRestricted;
    }

    private void HandleMinLateralMoveDistanceChanged(object sender, MinLateralMoveDistanceChangedEventArgs e)
    {
        SyncMinLateralMoveDistanceModelToView();
    }

    public void SyncMinLateralMoveDistanceViewToModel(float backedMinLateralMoveDistance)
    {
        ((ObjectControlModel)this.Model).minLateralMoveDistance = backedMinLateralMoveDistance;
    }

    private void SyncMinLateralMoveDistanceModelToView()
    {
        ((ObjectControlView)this.View).minLateralMoveDistance = ((ObjectControlModel)this.Model).minLateralMoveDistance;
    }


    private void HandlePitchRotationSpeedChanged(object sender, PitchRotationSpeedChangedEventArgs e)
    {
        SyncPitchRotationSpeedModelToView();
    }

    public void SyncPitchRotationSpeedViewToModel(float backedPitchRotationSpeed)
    {
        ((ObjectControlModel)this.Model).pitchRotationSpeed = backedPitchRotationSpeed;
    }

    private void SyncPitchRotationSpeedModelToView()
    {
        ((ObjectControlView)this.View).pitchRotationSpeed = ((ObjectControlModel)this.Model).pitchRotationSpeed;
    }

    private void HandlePitchRotationAngleRestrictedChanged(object sender, PitchRotationAngleRestrictedChangedEventArgs e)
    {
        SyncPitchRotationAngleRestrictedModelToView();
    }

    public void SyncPitchRotationAngleRestrictedViewToModel(bool backedPitchRotationAngleRestricted)
    {
        ((ObjectControlModel)this.Model).pitchRotationAngleRestricted = backedPitchRotationAngleRestricted;
    }

    private void SyncPitchRotationAngleRestrictedModelToView()
    {
        ((ObjectControlView)this.View).pitchRotationAngleRestricted = ((ObjectControlModel)this.Model).pitchRotationAngleRestricted;
    }

    private void HandleStartingPitchRotationAngleChanged(object sender, StartingPitchRotationAngleChangedEventArgs e)
    {
        SyncStartingPitchRotationAngleModelToView();
    }

    public void SyncStartingPitchRotationAngleViewToModel(float backedStartingPitchRotationAngle)
    {
        ((ObjectControlModel)this.Model).startingPitchRotationAngle = backedStartingPitchRotationAngle;
    }

    private void SyncStartingPitchRotationAngleModelToView()
    {
        ((ObjectControlView)this.View).startingPitchRotationAngle = ((ObjectControlModel)this.Model).startingPitchRotationAngle;
    }

    private void HandleMaxPitchNegRotationAngleChanged(object sender, MaxPitchNegRotationAngleChangedEventArgs e)
    {
        SyncMaxPitchNegRotationAngleModelToView();
    }

    public void SyncMaxPitchNegRotationAngleViewToModel(float backedMaxPitchNegRotationAngle)
    {
        ((ObjectControlModel)this.Model).maxPitchNegRotationAngle = backedMaxPitchNegRotationAngle;
    }

    private void SyncMaxPitchNegRotationAngleModelToView()
    {
        ((ObjectControlView)this.View).maxPitchNegRotationAngle = ((ObjectControlModel)this.Model).maxPitchNegRotationAngle;
    }

    private void HandleMaxPitchPosRotationAngleChanged(object sender, MaxPitchPosRotationAngleChangedEventArgs e)
    {
        SyncMaxPitchPosRotationAngleModelToView();
    }

    public void SyncMaxPitchPosRotationAngleViewToModel(float backedMaxPitchPosRotationAngle)
    {
        ((ObjectControlModel)this.Model).maxPitchPosRotationAngle = backedMaxPitchPosRotationAngle;
    }

    private void SyncMaxPitchPosRotationAngleModelToView()
    {
        ((ObjectControlView)this.View).maxPitchPosRotationAngle = ((ObjectControlModel)this.Model).maxPitchPosRotationAngle;
    }


    private void HandleYawRotationSpeedChanged(object sender, YawRotationSpeedChangedEventArgs e)
    {
        SyncYawRotationSpeedModelToView();
    }

    public void SyncYawRotationSpeedViewToModel(float backedYawRotationSpeed)
    {
        ((ObjectControlModel)this.Model).yawRotationSpeed = backedYawRotationSpeed;
    }

    private void SyncYawRotationSpeedModelToView()
    {
        ((ObjectControlView)this.View).yawRotationSpeed = ((ObjectControlModel)this.Model).yawRotationSpeed;
    }

    private void HandleYawRotationAngleRestrictedChanged(object sender, YawRotationAngleRestrictedChangedEventArgs e)
    {
        SyncYawRotationAngleRestrictedModelToView();
    }

    public void SyncYawRotationAngleRestrictedViewToModel(bool backedYawRotationAngleRestricted)
    {
        ((ObjectControlModel)this.Model).yawRotationAngleRestricted = backedYawRotationAngleRestricted;
    }

    private void SyncYawRotationAngleRestrictedModelToView()
    {
        ((ObjectControlView)this.View).yawRotationAngleRestricted = ((ObjectControlModel)this.Model).yawRotationAngleRestricted;
    }

    private void HandleStartingYawRotationAngleChanged(object sender, StartingYawRotationAngleChangedEventArgs e)
    {
        SyncStartingYawRotationAngleModelToView();
    }

    public void SyncStartingYawRotationAngleViewToModel(float backedStartingYawRotationAngle)
    {
        ((ObjectControlModel)this.Model).startingYawRotationAngle = backedStartingYawRotationAngle;
    }

    private void SyncStartingYawRotationAngleModelToView()
    {
        ((ObjectControlView)this.View).startingYawRotationAngle = ((ObjectControlModel)this.Model).startingYawRotationAngle;
    }

    private void HandleMaxYawNegRotationAngleChanged(object sender, MaxYawNegRotationAngleChangedEventArgs e)
    {
        SyncMaxYawNegRotationAngleModelToView();
    }

    public void SyncMaxYawNegRotationAngleViewToModel(float backedMaxYawNegRotationAngle)
    {
        ((ObjectControlModel)this.Model).maxYawNegRotationAngle = backedMaxYawNegRotationAngle;
    }

    private void SyncMaxYawNegRotationAngleModelToView()
    {
        ((ObjectControlView)this.View).maxYawNegRotationAngle = ((ObjectControlModel)this.Model).maxYawNegRotationAngle;
    }

    private void HandleMaxYawPosRotationAngleChanged(object sender, MaxYawPosRotationAngleChangedEventArgs e)
    {
        SyncMaxYawPosRotationAngleModelToView();
    }
    
    public void SyncMaxYawPosRotationAngleViewToModel(float backedMaxYawPosRotationAngle)
    {
        ((ObjectControlModel)this.Model).maxYawPosRotationAngle = backedMaxYawPosRotationAngle;
    }

    private void SyncMaxYawPosRotationAngleModelToView()
    {
        ((ObjectControlView)this.View).maxYawPosRotationAngle = ((ObjectControlModel)this.Model).maxYawPosRotationAngle;
    }


    private void HandleRollRotationSpeedChanged(object sender, RollRotationSpeedChangedEventArgs e)
    {
        SyncRollRotationSpeedModelToView();
    }

    public void SyncRollRotationSpeedViewToModel(float backedRollRotationpeed)
    {
        ((ObjectControlModel)this.Model).rollRotationSpeed = backedRollRotationpeed;
    }

    private void SyncRollRotationSpeedModelToView()
    {
        ((ObjectControlView)this.View).rollRotationSpeed = ((ObjectControlModel)this.Model).rollRotationSpeed;
    }

    private void HandleRollRotationAngleRestrictedChanged(object sender, RollRotationAngleRestrictedChangedEventArgs e)
    {
        SyncRollRotationAngleRestrictedModelToView();
    }

    public void SyncRollRotationAngleRestrictedViewToModel(bool backedRollRotationAngleRestricted)
    {
        ((ObjectControlModel)this.Model).rollRotationAngleRestricted = backedRollRotationAngleRestricted;
    }

    private void SyncRollRotationAngleRestrictedModelToView()
    {
        ((ObjectControlView)this.View).rollRotationAngleRestricted = ((ObjectControlModel)this.Model).rollRotationAngleRestricted;
    }

    private void HandleStartingRollRotationAngleChanged(object sender, StartingRollRotationAngleChangedEventArgs e)
    {
        SyncStartingRollRotationAngleModelToView();
    }

    public void SyncStartingRollRotationAngleViewToModel(float backedStartingRollRotationAngle)
    {
        ((ObjectControlModel)this.Model).startingRollRotationAngle = backedStartingRollRotationAngle;
    }

    private void SyncStartingRollRotationAngleModelToView()
    {
        ((ObjectControlView)this.View).startingRollRotationAngle = ((ObjectControlModel)this.Model).startingRollRotationAngle;
    }

    private void HandleMaxRollNegRotationAngleChanged(object sender, MaxRollNegRotationAngleChangedEventArgs e)
    {
        SyncMaxRollNegRotationAngleModelToView();
    }

    public void SyncMaxRollNegRotationAngleViewToModel(float backedMaxRollNegRotationAngle)
    {
        ((ObjectControlModel)this.Model).maxRollNegRotationAngle = backedMaxRollNegRotationAngle;
    }

    private void SyncMaxRollNegRotationAngleModelToView()
    {
        ((ObjectControlView)this.View).maxRollNegRotationAngle = ((ObjectControlModel)this.Model).maxRollNegRotationAngle;
    }

    private void HandleMaxRollPosRotationAngleChanged(object sender, MaxRollPosRotationAngleChangedEventArgs e)
    {
        SyncMaxRollPosRotationAngleModelToView();
    }

    public void SyncMaxRollPosRotationAngleViewToModel(float backedMaxRollPosRotationAngle)
    {
        ((ObjectControlModel)this.Model).maxRollPosRotationAngle = backedMaxRollPosRotationAngle;
    }

    private void SyncMaxRollPosRotationAngleModelToView()
    {
        ((ObjectControlView)this.View).maxRollPosRotationAngle = ((ObjectControlModel)this.Model).maxRollPosRotationAngle;
    }


    private void HandleRadiusChangeSpeedChanged(object sender, RadiusChangeSpeedChangedEventArgs e)
    {
        SyncRadiusChangeSpeedModelToView();
    }

    public void SyncRadiusChangeSpeedViewToModel(float backedRadiusChangeSpeed)
    {
        ((ObjectControlModel)this.Model).radiusChangeSpeed = backedRadiusChangeSpeed;
    }

    private void SyncRadiusChangeSpeedModelToView()
    {
        ((ObjectControlView)this.View).radiusChangeSpeed = ((ObjectControlModel)this.Model).radiusChangeSpeed;
    }

    private void HandleRadiusRestrictedChanged(object sender, RadiusRestrictedChangedEventArgs e)
    {
        SyncRadiusRestrictedModelToView();
    }

    public void SyncRadiusRestrictedViewToModel(bool backedRadiusRestricted)
    {
        ((ObjectControlModel)this.Model).radiusRestricted = backedRadiusRestricted;
    }

    private void SyncRadiusRestrictedModelToView()
    {
        ((ObjectControlView)this.View).radiusRestricted = ((ObjectControlModel)this.Model).radiusRestricted;
    }

    private void HandleMaxRadiusExpansionChanged(object sender, MaxRadiusExpansionChangedEventArgs e)
    {
        SyncMaxRadiusExpansionModelToView();
    }

    public void SyncMaxRadiusExpansionViewToModel(float backedMaxRadiusExpansion)
    {
        ((ObjectControlModel)this.Model).maxRadiusExpansion = backedMaxRadiusExpansion;
    }

    private void SyncMaxRadiusExpansionModelToView()
    {
        ((ObjectControlView)this.View).maxRadiusExpansion = ((ObjectControlModel)this.Model).maxRadiusExpansion;
    }

    private void HandleMinRadiusFromCenterChanged(object sender, MinRadiusFromCenterChangedEventArgs e)
    {
        SyncMinRadiusExpansionModelToView();
    }

    public void SyncMinRadiusFromCenterViewToModel(float backedMinRadiusFromCenter)
    {
        ((ObjectControlModel)this.Model).minRadiusFromCenter = backedMinRadiusFromCenter;
    }

    private void SyncMinRadiusExpansionModelToView()
    {
        ((ObjectControlView)this.View).minRadiusFromCenter = ((ObjectControlModel)this.Model).minRadiusFromCenter;
    }


    private void HandlePolarAngleCoordinatesChangeSpeedChanged(object sender, PolarAngleCoordinatesChangeSpeedChangedEventArgs e)
    {
        SyncPolarAngleCoordinatesChangeSpeedModelToView();
    }

    public void SyncPolarAngleCoordinatesChangeSpeedViewToModel(float backedPolarAngleCoordinatesChangeSpeed)
    {
        ((ObjectControlModel)this.Model).polarAngleCoordinatesChangeSpeed = backedPolarAngleCoordinatesChangeSpeed;
    }

    private void SyncPolarAngleCoordinatesChangeSpeedModelToView()
    {
        ((ObjectControlView)this.View).polarAngleCoordinatesChangeSpeed = ((ObjectControlModel)this.Model).polarAngleCoordinatesChangeSpeed;
    }

    private void HandlePolarRotationAroundAngleRestrictedChanged(object sender, PolarRotationAroundAngleRestrictedChangedEventArgs e)
    {
        SyncPolarRotationAroundAngleRestrictedModelToView();
    }

    public void SyncPolarRotationAroundAngleRestrictedViewToModel(bool backedPolarRotationAroundAngleRestricted)
    {
        ((ObjectControlModel)this.Model).polarRotationAroundAngleRestricted = backedPolarRotationAroundAngleRestricted;
    }

    private void SyncPolarRotationAroundAngleRestrictedModelToView()
    {
        ((ObjectControlView)this.View).polarRotationAroundAngleRestricted = ((ObjectControlModel)this.Model).polarRotationAroundAngleRestricted;
    }

    private void HandleStartingPolarRotationAroundAngleChanged(object sender, StartingPolarRotationAroundAngleChangedEventArgs e)
    {
        SyncStartingPolarRotationAroundAngleModelToView();
    }

    public void SyncStartingPolarRotationAroundAngleViewToModel(float backedStartingPolarRotationAroundAngle)
    {
        ((ObjectControlModel)this.Model).startingPolarRotationAroundAngle = backedStartingPolarRotationAroundAngle;
    }

    private void SyncStartingPolarRotationAroundAngleModelToView()
    {
        ((ObjectControlView)this.View).startingPolarRotationAroundAngle = ((ObjectControlModel)this.Model).startingPolarRotationAroundAngle;
    }

    private void HandleMaxPolarNegRotationAroundAngleChanged(object sender, MaxPolarNegRotationAroundAngleChangedEventArgs e)
    {
        SyncMaxPolarNegRotationAroundAngleModelToView();
    }

    public void SyncMaxPolarNegRotationAroundAngleViewToModel(float backedMaxPolarNegRotationAroundAngle)
    {
        ((ObjectControlModel)this.Model).maxPolarNegRotationAroundAngle = backedMaxPolarNegRotationAroundAngle;
    }

    private void SyncMaxPolarNegRotationAroundAngleModelToView()
    {
        ((ObjectControlView)this.View).maxPolarNegRotationAroundAngle = ((ObjectControlModel)this.Model).maxPolarNegRotationAroundAngle;
    }

    private void HandleMaxPolarPosRotationAroundAngleChanged(object sender, MaxPolarPosRotationAroundAngleChangedEventArgs e)
    {
        SyncMaxPolarPosRotationAroundAngleModelToView();
    }

    public void SyncMaxPolarPosRotationAroundAngleViewToModel(float backedMaxPolarPosRotationAroundAngle)
    {
        ((ObjectControlModel)this.Model).maxPolarPosRotationAroundAngle = backedMaxPolarPosRotationAroundAngle;
    }

    private void SyncMaxPolarPosRotationAroundAngleModelToView()
    {
        ((ObjectControlView)this.View).maxPolarPosRotationAroundAngle = ((ObjectControlModel)this.Model).maxPolarPosRotationAroundAngle;
    }


    private void HandleAzimuthalAngleCoordinatesChangeSpeedChanged(object sender, AzimuthalAngleCoordinatesChangeSpeedChangedEventArgs e)
    {
        SyncAzimuthalAngleCoordinatesChangeSpeedModelToView();
    }

    public void SyncAzimuthalAngleCoordinatesChangeSpeedViewToModel(float backedAzimuthalAngleCoordinatesChangeSpeed)
    {
        ((ObjectControlModel)this.Model).azimuthalAngleCoordinatesChangeSpeed = backedAzimuthalAngleCoordinatesChangeSpeed;
    }

    private void SyncAzimuthalAngleCoordinatesChangeSpeedModelToView()
    {
        ((ObjectControlView)this.View).azimuthalAngleCoordinatesChangeSpeed = ((ObjectControlModel)this.Model).azimuthalAngleCoordinatesChangeSpeed;
    }

    private void HandleAzimuthalRotationAroundAngleRestrictedChanged(object sender, AzimuthalRotationAroundAngleRestrictedChangedEventArgs e)
    {
        SyncAzimuthalRotationAroundAngleRestrictedModelToView();
    }

    public void SyncAzimuthalRotationAroundAngleRestrictedViewToModel(bool backedAzimuthalRotationAroundAngleRestricted)
    {
        ((ObjectControlModel)this.Model).azimuthalRotationAroundAngleRestricted = backedAzimuthalRotationAroundAngleRestricted;
    }

    private void SyncAzimuthalRotationAroundAngleRestrictedModelToView()
    {
        ((ObjectControlView)this.View).azimuthalRotationAroundAngleRestricted = ((ObjectControlModel)this.Model).azimuthalRotationAroundAngleRestricted;
    }

    private void HandleStartingAzimuthalRotationAngleChanged(object sender, StartingAzimuthalRotationAroundAngleChangedEventArgs e)
    {
        SyncStartingAzimuthalRotationAngleModelToView();
    }

    public void SyncStartingAzimuthalRotationAroundAngleViewToModel(float backedStartingAzimuthalRotationAroundAngle)
    {
        ((ObjectControlModel)this.Model).startingAzimuthalRotationAroundAngle = backedStartingAzimuthalRotationAroundAngle;
    }

    private void SyncStartingAzimuthalRotationAngleModelToView()
    {
        ((ObjectControlView)this.View).startingAzimuthalRotationAroundAngle = ((ObjectControlModel)this.Model).startingAzimuthalRotationAroundAngle;
    }

    private void HandleMaxAzimuthalNegRotationAroundAngleChanged(object sender, MaxAzimuthalNegRotationAroundAngleChangedEventArgs e)
    {
        SyncMaxAzimuthalNegRotationAroundAngleModelToView();
    }

    public void SyncMaxAzimuthalNegRotationAroundAngleViewToModel(float backedMaxAzimuthalNegRotationAnglee)
    {
        ((ObjectControlModel)this.Model).maxAzimuthalNegRotationAroundAngle = backedMaxAzimuthalNegRotationAnglee;
    }

    private void SyncMaxAzimuthalNegRotationAroundAngleModelToView()
    {
        ((ObjectControlView)this.View).maxAzimuthalNegRotationAroundAngle = ((ObjectControlModel)this.Model).maxAzimuthalNegRotationAroundAngle;
    }

    private void HandleMaxAzimuthalPosRotationAroundAngleChanged(object sender, MaxAzimuthalPosRotationAroundAngleChangedEventArgs e)
    {
        SyncMaxAzimuthalPosRotationAroundAngleModelToView();
    }

    public void SyncMaxAzimuthalPosRotationAroundAngleViewToModel(float backedMaxAzimuthalPosRotationAngle)
    {
        ((ObjectControlModel)this.Model).maxAzimuthalPosRotationAroundAngle = backedMaxAzimuthalPosRotationAngle;
    }

    private void SyncMaxAzimuthalPosRotationAroundAngleModelToView()
    {
        ((ObjectControlView)this.View).maxAzimuthalPosRotationAroundAngle = ((ObjectControlModel)this.Model).maxAzimuthalPosRotationAroundAngle;
    }


    private void HandleSubjectChanged(object sender, SubjectChangedEventArgs e)
    {
        SyncSubjectModelToView();
    }

    public void SyncSubjectViewToModel(ComplexObjectView backedSubject)
    {
        if(backedSubject.FindSetController().FindSetModel() != null)
            ((ObjectControlModel)this.Model).subject = (ComplexObjectModel) backedSubject.FindSetController().FindSetModel();
    }

    private void SyncSubjectModelToView()
    {
        ((ObjectControlView)this.View).subject = (ComplexObjectView)((ObjectControlModel)this.Model).subject.FindSetController().FindSetView();
    }

    private void HandleDedicatedObjectChanged(object sender, DedicatedObjectChangedEventArgs e)
    {
        SyncDedicatedObjectModelToView();
    }

    public void SyncDedicatedObjectViewToModel(ComplexObjectView backedDedicatedObject)
    {
        ((ObjectControlModel)this.Model).dedicatedObject = (ComplexObjectModel)backedDedicatedObject.FindSetController().FindSetModel();
    }

    private void SyncDedicatedObjectModelToView()
    {
        ((ObjectControlView)this.View).dedicatedObject = (ComplexObjectView) ((ObjectControlModel)this.Model).dedicatedObject.FindSetController().FindSetView();
    }

    private void HandleTargetChanged(object sender, TargetChangedEventArgs e)
    {
        SyncTargetModelToView();
    }

    public void SyncTargetViewToModel(ComplexObjectView backedTarget)
    {
        ((ObjectControlModel)this.Model).target = (ComplexObjectModel)backedTarget.FindSetController().FindSetModel();
    }

    private void SyncTargetModelToView()
    {
        SyncDefaultObjectMovementInputModelToView();
        SyncCustomObjectMovementInputModelToView();
        SyncAvailableObjectMovementInputModelToView();


        SyncControlSetupModelToView();
        SyncMovementModelToView();
        SyncMovementRestrictionsModelToView();

        SyncLongitudinalMovementSpeedModelToView();
        SyncStartingDistanceLongitudinalMovementModelToView();
        SyncMaxLongitudinalMoveDistanceRestrictedModelToView();
        SyncMaxLongitudinalMoveDistanceModelToView();
        SyncMinLongitudinalMoveDistanceRestrictedModelToView();
        SyncMinLongitudinalMoveDistanceModelToView();

        SyncVerticalMovementSpeedModelToView();
        SyncStartingDistanceVerticalMovementChangedModelToView();
        SyncMaxVerticalMoveDistanceRestrictedModelToView();
        SyncMaxVerticalMoveDistanceModelToView();
        SyncMinVerticalMoveDistanceRestrictedModelToView();
        SyncMinVerticalMoveDistanceModelToView();

        SyncLateralMovementSpeedModelToView();
        SyncStartingDistanceLateralMovementModelToView();
        SyncMaxLateralMoveDistanceRestrictedModelToView();
        SyncMaxLateralMoveDistanceModelToView();
        SyncMinLateralMoveDistanceRestrictedModelToView();
        SyncMinLateralMoveDistanceModelToView();

        SyncPitchRotationSpeedModelToView();
        SyncPitchRotationAngleRestrictedModelToView();
        SyncStartingPitchRotationAngleModelToView();
        SyncMaxPitchNegRotationAngleModelToView();
        SyncMaxPitchPosRotationAngleModelToView();

        SyncYawRotationSpeedModelToView();
        SyncYawRotationAngleRestrictedModelToView();
        SyncStartingYawRotationAngleModelToView();
        SyncMaxYawNegRotationAngleModelToView();
        SyncMaxYawPosRotationAngleModelToView();

        SyncRollRotationSpeedModelToView();
        SyncRollRotationAngleRestrictedModelToView();
        SyncStartingRollRotationAngleModelToView();
        SyncMaxRollNegRotationAngleModelToView();
        SyncMaxRollPosRotationAngleModelToView();

        SyncRadiusChangeSpeedModelToView();
        SyncRadiusRestrictedModelToView();
        SyncMaxRadiusExpansionModelToView();
        SyncMinRadiusExpansionModelToView();

        SyncPolarAngleCoordinatesChangeSpeedModelToView();
        SyncPolarRotationAroundAngleRestrictedModelToView();
        SyncStartingPolarRotationAroundAngleModelToView();
        SyncMaxPolarNegRotationAroundAngleModelToView();
        SyncMaxPolarPosRotationAroundAngleModelToView();

        SyncAzimuthalAngleCoordinatesChangeSpeedModelToView();
        SyncAzimuthalRotationAroundAngleRestrictedModelToView();
        SyncStartingAzimuthalRotationAngleModelToView();
        SyncMaxAzimuthalNegRotationAroundAngleModelToView();
        SyncMaxAzimuthalPosRotationAroundAngleModelToView();

        SyncSubjectModelToView();
        SyncDedicatedObjectModelToView();
        SyncTargetModelToView();
    }


     // Looks for an existing View, else creates one
    public override BaseView FindSetView()
    {
        if (this.View == null)
        {
            // Looks up for the existence of a view with the same entityID
            BaseView view = ObjectFactory.Instance.GetObjectViewByEnityID(FindEntityID());

            if (view != null)
            {
                this.View = view;
                return this.View;
            }
            else
                return null;
        }
        else
        {
            return this.View;
        }
    }

     // Looks for an existing View, else creates one
    public override BaseView FindCreateSetView(Transform parentTransform)
    {
        // No legit SMW MVC component, so return null!
        if (parentTransform.GetComponent<ComplexObjectView>() == null)
            return null;

        // Looks up for the existence of a view with the same entityID/objectURI
        if (FindSetView() != null)
            return FindSetView();

        else
        {
            // Get entityID, stop if none is found
            string entityID = FindEntityID();
            if (entityID == null || entityID == "") return null;

            if (this.GetType() == typeof(CameraControlController))
                this.View = ObjectFactory.Instance.viewFactory.createForCameraControl(entityID, parentTransform.GetComponent<ComplexObjectView>());
            else
                this.View = ObjectFactory.Instance.viewFactory.createForObjectControl(entityID, parentTransform.GetComponent<ComplexObjectView>());

            return this.View = View;
        }

    }

    // Finds for an existing model, else creates one
    public override BaseModel FindSetModel()
    {
        if (this.Model == null)
        {
            if (EntityID == null || EntityID == "")
                return null;

            // Looks up for the existence of a view with the same entityID
            BaseModel model = ObjectFactory.Instance.GetObjectModelByEntityID(EntityID);

            if (model != null)
            {
                this.Model = model;
                return this.Model;
            }
            else
                return null;
        }
        else
        {
            return this.Model;
        }
    }

     // Looks for an existing View, else creates one
    public override BaseModel FindCreateSetModel()
    {
        // Looks up for the existence of a view with the same entityID
        if (FindSetModel() != null)
            return FindSetModel();

        // Get entityID, stop if none is found
        string entityID = this.EntityID;
        if (entityID == null || entityID == "") return null;

        // Find a view
        if (FindSetView() == null)
            return null;

        if (this.GetType() == typeof(CameraControlController))
            this.Model = ObjectFactory.Instance.modelFactory.createForCameraControl((CameraControlView) FindSetView(), false);
        else
            this.Model = ObjectFactory.Instance.modelFactory.createForObjectControl((ObjectControlView)FindSetView(), false);

        return this.Model = Model;
    }

}
