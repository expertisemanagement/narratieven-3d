﻿using System.Collections.Generic;
using Object.Movement;

namespace Object.MovementInput
{
    [System.Serializable]
    public struct ObjectMovementInput
    {
        // Toggle movement using forces (=true) or translations (=false)
        public bool moveByForce;

        // Describes the objectMovement of concern
        public ObjectMovementEnum movement;

        // Describes the inputKey variable from the Unity inputmanager, that controls the objectMovement
        public List<string> inputKeyAxes;
    }
}

