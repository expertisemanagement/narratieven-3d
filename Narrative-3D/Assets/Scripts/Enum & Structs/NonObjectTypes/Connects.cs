﻿using SMW.Relation.Activity;
using System.Linq;

[System.Serializable]
public class Connects : Relation
{
    public ConnectionTypeEnum elementConnectionType;
    public string linkCondition;
}
