﻿using SMW.Relation;
using System.Linq;

[System.Serializable]
public class Selects
{
    public SelectionTypeEnum selectionType;
    public string selectionLink;
    public ContributionValueEnum selectionContributionValue;
    public string note;

    // Returns the IntentionalElementModel that has 
    public PracticeModel GetSelectionLink()
    {
        if (ObjectFactory.Instance.semanticModels.ContainsKey(selectionLink) && ObjectFactory.Instance.semanticModels[selectionLink] is PracticeModel)
            return ((PracticeModel)ObjectFactory.Instance.semanticModels[selectionLink]);
        else if (ObjectFactory.Instance.semanticModels[selectionLink] is PracticeModel && SMWParser.Instance.cacheNonObjectTypePracticeSelect.Keys.First(n => n.ToString() == selectionLink) != null)
            return (PracticeModel)ObjectFactory.Instance.modelFactory.createForActivity(SMWParser.Instance.cacheNonObjectTypePracticeSelect.Keys.First(n => n.ToString() == selectionLink), null, null);
        else
            return null;
    }
}
