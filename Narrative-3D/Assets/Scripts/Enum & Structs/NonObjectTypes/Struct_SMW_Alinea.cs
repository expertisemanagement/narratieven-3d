﻿using SMW.Alinea.Language;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Struct_SMW_Alinea {

    public int alineaNummer;
    public AlineaLanguageSupportEnum alineaLang;
    public string alineaHead;
    public bool alineaCollapsible;
    public string alineaContent;
}
