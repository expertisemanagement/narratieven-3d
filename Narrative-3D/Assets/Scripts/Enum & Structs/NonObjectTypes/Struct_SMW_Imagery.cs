﻿using SMW.Alinea.Imagary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Struct_SMW_Imagary {

    public string imagary;
    public AlineaImagaryTypeEnum imagaryType;

}
