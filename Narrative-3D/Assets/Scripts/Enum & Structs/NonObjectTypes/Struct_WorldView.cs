﻿namespace SMW
{
    namespace SSM
    {
        namespace WorldView
        {
            [System.Serializable]
            public struct Struct_WorldView
            {
                public string ssmRole;
                public string stakeholder;
                public string what;
                public string how;
                public string why;
                public string condition;
                public string belief;
                public string success;
            }
        }
    }
}
