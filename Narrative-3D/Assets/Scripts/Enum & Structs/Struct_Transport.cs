﻿using Object.Transport.TravelSpace.ControlType;
using Object.Transport.TravelSpace.SpaceType;

namespace Object.Transport
{
    public struct TravelSpaceStruct
    {
        // An travel space object like a chair, bench, pod, bed or even a freely moving zone
        ObjectTravelSpaceTypeEnum travelSpace;

        // Type of control over transport
        ObjectTransportControlTypeEnum transportControlType;
    }
}
