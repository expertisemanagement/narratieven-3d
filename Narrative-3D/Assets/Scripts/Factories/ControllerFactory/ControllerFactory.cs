﻿using Object.FSM;
using Object.Type;
using ObjectControl.Type;
using System.Linq;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ControllerFactory {

    // A instance of the ObjectFactory as part of the Singleton pattern
    private static ControllerFactory instance = null;

    // A Multitread singleton duplication prevention object. 
    // This object is locked the first time the Instance() is called, Instance() does nothing when the object is locked.
    private static readonly object padlock = new object();

    // Constructor
    private ControllerFactory()
    {
    }

    // Returns this object's instance variable value, also creates a new ObjectFactory instance if the padlock is not locked. (Mutlitread security)
    public static ControllerFactory Instance
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new ControllerFactory();
                }
                return instance;
            }
        }
    }

    //--------------------------------// Creation methodes for objects MVC components //--------------------------------//

    // creates a Controller for a object, with a using data from a given model
    public ObjectController createForObject(ObjectModel existingModel, bool syncModelToView)
    {
        // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
        if (existingModel == null) return null;

        // Check if this is the right create methode, else execute the right one
        if (existingModel.GetType() != typeof(ObjectModel))
        {
            ObjectController controllerCreatedFromExistingController = ExecuteCorrespondingObjectCreationMethode(existingModel, syncModelToView);
                return controllerCreatedFromExistingController;
        }

        // Result variable
        ObjectController controller;

        // Generate entityID for the object
        string newEntityID = existingModel.EntityID;

        // Check if already exists
        if (existingModel.FindSetController() != null) return ((ObjectController)existingModel.FindSetController());

        else
        {
            // Create new ObjectController
            controller = new ObjectController(existingModel, null);

            // Add to index(es)
            AddObjectToIndex(((ObjectModel)controller.Model).ObjectType, controller);
        }

        // If syncModelToView call this methode
        if (syncModelToView == true)
            controller.SyncModelToView();

        // Return the return variable
        return controller;
    }

    // creates a Controller for a object, with a using data from a given view
    public ObjectController createForObject(ObjectView existingView, bool syncModelToView)
    {
        // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
        if (existingView == null) return null;

        // Check if this is the right create methode, else execute the right one
        if (existingView.GetType() != typeof(ObjectView))
        {
            ObjectController controllerCreatedFromExistingController = ExecuteCorrespondingObjectCreationMethode(existingView, syncModelToView);
            return controllerCreatedFromExistingController;
        }

        // Result variable
        ObjectController controller;

        // Generate entityID for the object
        string newEntityID = existingView.EntityID;

        // Check if already exists
        if (existingView.FindSetController() != null) return ((ObjectController)existingView.FindSetController());

        else
        {
            // Create new ObjectController
            controller = new ObjectController(null, existingView);

            // Add to index(es)
            AddObjectToIndex(((ObjectView)controller.View).ObjectType, controller);
        }

        // If syncModelToView call this methode
        if (syncModelToView == true)
            controller.SyncModelToView();

        // Return the return variable
        return controller;
    }


        // creates a Controller for a complexobject, with a using data from a given model
        public ObjectController createForComplexObject(ComplexObjectModel existingModel, bool syncModelToView)
        {
            // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
            if (existingModel == null) return null;

            // Check if this is the right create methode, else execute the right one
            if (existingModel.GetType() != typeof(ObjectModel))
            {
                ObjectController controllerCreatedFromExistingController = ExecuteCorrespondingObjectCreationMethode(existingModel, syncModelToView);
                return controllerCreatedFromExistingController;
            }

            // Result variable
            ComplexObjectController controller;
        
            // Generate entityID for the object
            string newEntityID = existingModel.EntityID;

            // Check if already exists
            if (existingModel.FindSetController() != null) return ((ComplexObjectController)existingModel.FindSetController());

            else
            {
                // Create new ObjectController
                controller = new ComplexObjectController(existingModel, null);

                // Add to index(es)
                AddObjectToIndex(((ObjectModel)controller.Model).ObjectType, controller);
            }

            // If syncModelToView call this methode
            if (syncModelToView == true)
                controller.SyncModelToView();

            // Return the return variable
            return controller;
            }

        // creates a Controller for a complexobject, with a using data from a given view
        public ObjectController createForComplexObject(ComplexObjectView existingView, bool syncModelToView)
        {
            // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
            if (existingView == null) return null;

            // Check if this is the right create methode, else execute the right one
            if (existingView.GetType() != typeof(ComplexObjectView))
            {
                ObjectController controllerCreatedFromExistingController = ExecuteCorrespondingObjectCreationMethode(existingView, syncModelToView);
                return controllerCreatedFromExistingController;
            }

            // Result variable
            ComplexObjectController controller;

            // Generate entityID for the object
            string newEntityID = existingView.EntityID;

            // Check if already exists
            if (existingView.FindSetController() != null) return ((ComplexObjectController)existingView.FindSetController());

            else
            {
                // Create new ObjectController
                controller = new ComplexObjectController(null, existingView);

                // Add to index(es)
                AddObjectToIndex(((ObjectView)controller.View).ObjectType, controller);
            }

            // If syncModelToView call this methode
            if (syncModelToView == true)
                controller.SyncModelToView();

            // Return the return variable
            return controller;
        }


            // creates a Controller for a complexobject, with a using data from a given model
            public ObjectController createForCamera(CameraModel existingModel, bool syncModelToView)
            {
                // if no existing Model is given stop this creation methode!
                if (existingModel == null) return null;

                // Check if this is the right create methode, else execute the right one
                if (existingModel.GetType() != typeof(CameraModel))
                {
                    ObjectController controllerCreatedFromExistingController = ExecuteCorrespondingObjectCreationMethode(existingModel, syncModelToView);
                    return controllerCreatedFromExistingController;
                }

                // Result variable
                CameraController controller;

                // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                if (existingModel == null) return null;

                // Generate entityID for the object
                string newEntityID = existingModel.EntityID;

                // Check if already exists
                if (existingModel.FindSetController() != null) return ((CameraController)existingModel.FindSetController());

                else
                {
                    // Create new ObjectController
                    controller = new CameraController(existingModel, null);

                    // Add to index(es)
                    AddObjectToIndex(((ObjectModel)controller.Model).ObjectType, controller);
                }

                // Return the return variable
                return controller;
            }

            // creates a Controller for a complexobject, with a using data from a given view
            public ObjectController createForCamera(CameraView existingView, bool syncModelToView)
            {
                // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                if (existingView == null) return null;

                // Check if this is the right create methode, else execute the right one
                if (existingView.GetType() != typeof(CameraView))
                {
                    ObjectController controllerCreatedFromExistingController = ExecuteCorrespondingObjectCreationMethode(existingView, syncModelToView);
                    return controllerCreatedFromExistingController;
                }

                // Result variable
                CameraController controller;

                // Generate entityID for the object
                string newEntityID = existingView.EntityID;

                // Check if already exists
                if (existingView.FindSetController() != null) return ((CameraController)existingView.FindSetController());
                
                else
                {
                    // Create new ObjectController
                    controller = new CameraController(null, existingView);

                    // Add to index(es)
                    AddObjectToIndex(((ObjectView)controller.View).ObjectType, controller);
                }

                // If syncModelToView call this methode
                if (syncModelToView == true)
                    controller.SyncModelToView();

                // Return the return variable
                return controller;
            }


        // creates a Controller for a NavObject, with a using data from a given model (, and create a view class, if createView is true)
        public ObjectController createForNavObject(NavObjectModel existingModel, bool syncModelToView)
        {
            // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
            if (existingModel == null) return null;

            // Check if this is the right create methode, else execute the right one
            if (existingModel.GetType() != typeof(NavObjectModel))
            {
                ObjectController controllerCreatedFromExistingController = ExecuteCorrespondingObjectCreationMethode(existingModel, syncModelToView);
                return controllerCreatedFromExistingController;
            }

            // Result variable
            NavObjectController controller;

            // Generate entityID for the object
            string newEntityID = existingModel.EntityID;

            // Check if already exists
            if (existingModel.FindSetController() != null) return ((NavObjectController)existingModel.FindSetController());

            else
            {
                // Create new ObjectController
                controller = new NavObjectController(existingModel, null);

                // Add to index(es)
                AddObjectToIndex(((ObjectModel)controller.Model).ObjectType, controller);
            }

            // If syncModelToView call this methode
            if (syncModelToView == true)
                controller.SyncModelToView();

            // Return the return variable
            return controller;
        }

        // creates a Controller for a NavObject, with a using data from a given view (, and create a model class, if createModel is true)
        public ObjectController createForNavObject(NavObjectView existingView, bool syncModelToView)
        {
            // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
            if (existingView == null) return null;

            // Check if this is the right create methode, else execute the right one
            if (existingView.GetType() != typeof(NavObjectView))
            {
                ObjectController controllerCreatedFromExistingController = ExecuteCorrespondingObjectCreationMethode(existingView, syncModelToView);
                return controllerCreatedFromExistingController;
            }

            // Result variable
            NavObjectController controller;

            // Generate entityID for the object
            string newEntityID = existingView.EntityID;

            // Check if already exists
            if (existingView.FindSetController() != null) return ((NavObjectController)existingView.FindSetController());

            else
            {
                // Create new ObjectController
                controller = new NavObjectController(null, existingView);

                // Add to index(es)
                AddObjectToIndex(((ObjectView)controller.View).ObjectType, controller);
            }

            // If syncModelToView call this methode
            if (syncModelToView == true)
                controller.SyncModelToView();

            // Return the return variable
            return controller;
        }


    //--------------------------------// Creation methodes for Semantic objects MVC components //--------------------------------//

    // creates a Controller for a SemanticObject, with a using data from a given model (, and create a view class, if createView is true)
    public SemanticObjectController createForSemanticObject(SemanticObjectModel existingModel, bool syncModelToView)
    {
        // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
        if (existingModel == null) return null;

        // Check if this is the right create methode, else execute the right one
        if (existingModel.GetType() != typeof(SemanticObjectModel))
        {
            SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingModel, syncModelToView);
            return controllerCreatedFromExistingController;
        }

        // Result variable
        SemanticObjectController controller;

        // Generate entityID for the object
        string newEntityID = existingModel.EntityID;

        // Check if already exists
        if (existingModel.FindSetController() != null) return ((SemanticObjectController)existingModel.FindSetController());

        // Create new SemanticObjectController
        else
        {
            controller = new SemanticObjectController(existingModel, null);

            AddSemanticObjectToIndex(((ObjectModel)controller.Model).ObjectType, controller);
        }

        // If syncModelToView call this methode
        if (syncModelToView == true)
            controller.SyncModelToView();

        // Return the return variable
        return controller;
    }

    // creates a Controller for a SemanticObject, with a using data from a given view (, and create a model class, if createModel is true)
    public SemanticObjectController createForSemanticObject(SemanticObjectView existingView, bool syncModelToView)
    {
        // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
        if (existingView == null) return null;

        // Check if this is the right create methode, else execute the right one
        if (existingView.GetType() != typeof(SemanticObjectView))
        {
            SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingView, syncModelToView);
            return controllerCreatedFromExistingController;
        }

        // Result variable
        SemanticObjectController controller;

        // Generate entityID for the object
        string newEntityID = existingView.EntityID;

        // Check if already exists
        if (existingView.FindSetController() != null) return ((SemanticObjectController)existingView.FindSetController());

        // Create new SemanticObjectController
        else
        {
            controller = new SemanticObjectController(null, existingView);

            AddSemanticObjectToIndex(((ObjectView)controller.View).ObjectType, controller);
        }

        // If syncModelToView call this methode
        if (syncModelToView == true)
            controller.SyncModelToView();

        // Return the return variable
        return controller;
    }


        // creates a Controller for a SMWObject, with a using data from a given model (, and create a view class, if createView is true)
        public SemanticObjectController createForSMWObject(SMWObjectModel existingModel, bool syncModelToView)
        {
            // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
            if (existingModel == null) return null;

            // Check if this is the right create methode, else execute the right one
            if (existingModel.GetType() != typeof(SMWObjectModel))
            {
                SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingModel, syncModelToView);
                return controllerCreatedFromExistingController;
            }

            // Result variable
            SMWObjectController controller;

            // Generate entityID for the object
            string newEntityID = existingModel.EntityID;

            // Check if already exists
            if (existingModel.FindSetController() != null) return ((SMWObjectController)existingModel.FindSetController());

            // Create new SemanticObjectController
            else
            {
                controller = new SMWObjectController(existingModel, null);

                AddSemanticObjectToIndex(((ObjectModel)controller.Model).ObjectType, controller);
            }

            // If syncModelToView call this methode
            if (syncModelToView == true)
                controller.SyncModelToView();

            // Return the return variable
            return controller;
        }

        // creates a Controller for a SMWObject, with a using data from a given view (, and create a model class, if createModel is true)
        public SemanticObjectController createForSMWObject(SMWObjectView existingView, bool syncModelToView)
        {
            // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
            if (existingView == null) return null;

            // Check if this is the right create methode, else execute the right one
            if (existingView.GetType() != typeof(SMWObjectView))
            {
                SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingView, syncModelToView);
                return controllerCreatedFromExistingController;
            }

            // Result variable
            SMWObjectController controller;

            // Generate entityID for the object
            string newEntityID = existingView.EntityID;

            // Check if already exists
            if (existingView.FindSetController() != null) return ((SMWObjectController)existingView.FindSetController());

            // Create new SemanticObjectController
            else
            {
                controller = new SMWObjectController(null, existingView);

                AddSemanticObjectToIndex(((ObjectView)controller.View).ObjectType, controller);
            }

            // If syncModelToView call this methode
            if (syncModelToView == true)
                controller.SyncModelToView();

            // Return the return variable
            return controller;
        }


            // creates a Controller for a Context, with a using data from a given model (, and create a view class, if createView is true)
            public SemanticObjectController createForContext(ContextModel existingModel, bool syncModelToView)
            {
                // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                if (existingModel == null) return null;

                // Check if this is the right create methode, else execute the right one
                if (existingModel.GetType() != typeof(ContextModel))
                {
                    SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingModel, syncModelToView);
                    return controllerCreatedFromExistingController;
                }

                // Result variable
                ContextController controller;

                // Generate entityID for the object
                string newEntityID = existingModel.EntityID;

                // Check if already exists
                if (existingModel.FindSetController() != null) return ((ContextController)existingModel.FindSetController());

                // Create new SemanticObjectController
                else
                {
                    controller = new ContextController(existingModel, null);

                    AddSemanticObjectToIndex(((ObjectModel)controller.Model).ObjectType, controller);
                }

                // If syncModelToView call this methode
                if (syncModelToView == true)
                    controller.SyncModelToView();

                // Return the return variable
                return controller;
            }

            // creates a Controller for a Context, with a using data from a given view (, and create a model class, if createModel is true)
            public SemanticObjectController createForContext(ContextView existingView, bool syncModelToView)
            {
                // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                if (existingView == null) return null;

                // Check if this is the right create methode, else execute the right one
                if (existingView.GetType() != typeof(ContextView))
                {
                    SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingView, syncModelToView);
                    return controllerCreatedFromExistingController;
                }

                // Result variable
                ContextController controller;

                // Generate entityID for the object
                string newEntityID = existingView.EntityID;

                Debug.Log("Creating findingContext constructor");

                // Check if already exists
                if (existingView.FindSetController() != null) return ((ContextController)existingView.FindSetController());

                // Create new SemanticObjectController
                else
                {
                    Debug.Log("Calling the contructor..");

                    controller = new ContextController(null, existingView);

                    AddSemanticObjectToIndex(((ObjectView)controller.View).ObjectType, controller);
                }

                // If syncModelToView call this methode
                if (syncModelToView == true)
                    controller.SyncModelToView();

                // Return the return variable
                return controller;
            }


            //// creates a Controller for a ContextCase, with a using data from a given model (, and create a view class, if createView is true)
                //public ObjectController createForContextCase(ContextCaseModel existingModel, bool createView, bool syncModelToView)
                //{

                //    // returns no controller, because the lack of an existing model denied the creation of a objectcontroller
                //    if (existingModel == null) return null;

                //    // Generate entityID for the object
                //    string newEntityID = existingModel.entityID;

                //    // Set new ObjectType
                //    ObjectTypeEnum newObjectType = existingModel.objectType;

                //    // Create Controller, using the specific methode
                //    return createForObject(newEntityID, newObjectType, existingModel, false, null, createView, syncModelToView);

                //}

                //// creates a Controller for a ContextCase, with a using data from a given view (, and create a model class, if createModel is true)
                //public ObjectController createForContextCase(ContextCaseView existingView, bool createModel, bool syncModelToView)
                //{
                //    // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                //    if (existingView == null) return null;

                //    // Generate entityID for the object
                //    string newEntityID = existingView.entityID;

                //    // Set new ObjectType
                //    ObjectTypeEnum newObjectType = existingView.objectType;

                //    // Create Controller, using the specific methode
                //    return createForObject(newEntityID, newObjectType, null, createModel, existingView, false, syncModelToView);
                //}


                //// creates a Controller for a Context, with a using data from a given model (, and create a view class, if createView is true)
                //public ObjectController createForRelatableSituation(ContextRelatableSituationModel existingModel, bool createView, bool syncModelToView)
                //{

                //    // returns no controller, because the lack of an existing model denied the creation of a objectcontroller
                //    if (existingModel == null) return null;

                //    // Generate entityID for the object
                //    string newEntityID = existingModel.entityID;

                //    // Set new ObjectType
                //    ObjectTypeEnum newObjectType = existingModel.objectType;

                //    // Create Controller, using the specific methode
                //    return createForObject(newEntityID, newObjectType, existingModel, false, null, createView, syncModelToView);

                //}

                //// creates a Controller for a Context, with a using data from a given view (, and create a model class, if createModel is true)
                //public ObjectController createForRelatableSituation(ContextRelatableSituationView existingView, bool createModel, bool syncModelToView)
                //{
                //    // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                //    if (existingView == null) return null;

                //    // Generate entityID for the object
                //    string newEntityID = existingView.entityID;

                //    // Set new ObjectType
                //    ObjectTypeEnum newObjectType = existingView.objectType;

                //    // Create Controller, using the specific methode
                //    return createForObject(newEntityID, newObjectType, null, createModel, existingView, false, syncModelToView);
                //}


                //// creates a Controller for a ContexSSMSituation, with a using data from a given model (, and create a view class, if createView is true)
                //public ObjectController createForContextSSMSituation(ContextSSMSituationModel existingModel, bool createView, bool syncModelToView)
                //{

                //    // returns no controller, because the lack of an existing model denied the creation of a objectcontroller
                //    if (existingModel == null) return null;

                //    // Generate entityID for the object
                //    string newEntityID = existingModel.entityID;

                //    // Set new ObjectType
                //    ObjectTypeEnum newObjectType = existingModel.objectType;

                //    // Create Controller, using the specific methode
                //    return createForObject(newEntityID, newObjectType, existingModel, false, null, createView, syncModelToView);

                //}

                //// creates a Controller for a ContexSSMSituation, with a using data from a given view (, and create a model class, if createModel is true)
                //public ObjectController createForContextSSMSituation(ContextSSMSituationView existingView, bool createModel, bool syncModelToView)
                //{
                //    // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                //    if (existingView == null) return null;

                //    // Generate entityID for the object
                //    string newEntityID = existingView.entityID;

                //    // Set new ObjectType
                //    ObjectTypeEnum newObjectType = existingView.objectType;

                //    // Create Controller, using the specific methode
                //    return createForObject(newEntityID, newObjectType, null, createModel, existingView, false, syncModelToView);
                //}


            // creates a Controller for a IntentionalElement, with a using data from a given model (, and create a view class, if createView is true)
            public SemanticObjectController createForIntentionalElement(IntentionalElementModel existingModel, bool syncModelToView)
            {
                // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                if (existingModel == null) return null;

                // Check if this is the right create methode, else execute the right one
                if (existingModel.GetType() != typeof(IntentionalElementModel))
                {
                    SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingModel, syncModelToView);
                    return controllerCreatedFromExistingController;
                }

                // Result variable
                IntentionalElementController controller;

                // Generate entityID for the object
                string newEntityID = existingModel.EntityID;

                // Check if already exists
                if (existingModel.FindSetController() != null) return ((IntentionalElementController)existingModel.FindSetController());

                // Create new SemanticObjectController
                else
                {
                    controller = new IntentionalElementController(existingModel, null);

                    AddSemanticObjectToIndex(((ObjectModel)controller.Model).ObjectType, controller);
                }

                // If syncModelToView call this methode
                if (syncModelToView == true)
                    controller.SyncModelToView();

                // Return the return variable
                return controller;

            }

            // creates a Controller for a IntentionalElement, with a using data from a given view (, and create a model class, if createModel is true)
            public SemanticObjectController createForIntentionalElement(IntentionalElementView existingView, bool syncModelToView)
            {
                // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                if (existingView == null) return null;

                // Check if this is the right create methode, else execute the right one
                if (existingView.GetType() != typeof(IntentionalElementView))
                {
                    SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingView, syncModelToView);
                    return controllerCreatedFromExistingController;
                }

                // Result variable
                IntentionalElementController controller;

                // Generate entityID for the object
                string newEntityID = existingView.EntityID;

                // Check if already exists
                if (existingView.FindSetController() != null) return ((IntentionalElementController)existingView.FindSetController());

                // Create new SemanticObjectController
                else
                {
                    controller = new IntentionalElementController(null, existingView);

                    AddSemanticObjectToIndex(((ObjectView)controller.View).ObjectType, controller);
                }

                // If syncModelToView call this methode
                if (syncModelToView == true)
                    controller.SyncModelToView();

                // Return the return variable
                return controller;
            }
    

                // creates a Controller for a Activity, with a using data from a given model (, and create a view class, if createView is true)
                public SemanticObjectController createForActivity(ActivityModel existingModel, bool syncModelToView)
                {
                    // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                    if (existingModel == null) return null;

                    // Check if this is the right create methode, else execute the right one
                    if (existingModel.GetType() != typeof(ActivityModel))
                    {
                        SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingModel, syncModelToView);
                        return controllerCreatedFromExistingController;
                    }

                    // Result variable
                    ActivityController controller;

                    // Generate entityID for the object
                    string newEntityID = existingModel.EntityID;

                    // Check if already exists
                    if (existingModel.FindSetController() != null) return ((ActivityController)existingModel.FindSetController());

                    // Create new SemanticObjectController
                    else
                    {
                        controller = new ActivityController(existingModel, null);

                        AddSemanticObjectToIndex(((ObjectModel)controller.Model).ObjectType, controller);
                    }

                    // If syncModelToView call this methode
                    if (syncModelToView == true)
                        controller.SyncModelToView();

                    // Return the return variable
                    return controller;
                }

                // creates a Controller for a Activity, with a using data from a given view (, and create a model class, if createModel is true)
                public SemanticObjectController createForActivity(ActivityView existingView, bool syncModelToView)
                {
                    // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                    if (existingView == null) return null;

                    // Check if this is the right create methode, else execute the right one
                    if (existingView.GetType() != typeof(ActivityView))
                    {
                        SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingView, syncModelToView);
                        return controllerCreatedFromExistingController;
                    }

                    // Result variable
                    ActivityController controller;

                    // Generate entityID for the object
                    string newEntityID = existingView.EntityID;

                    // Check if already exists
                    if (existingView.FindSetController() != null) return ((ActivityController)existingView.FindSetController());

                    // Create new SemanticObjectController
                    else
                    {
                        controller = new ActivityController(null, existingView);

                        AddSemanticObjectToIndex(((ObjectView)controller.View).ObjectType, controller);
                    }

                    // If syncModelToView call this methode
                    if (syncModelToView == true)
                        controller.SyncModelToView();

                    // Return the return variable
                    return controller;
                }


                // creates a Controller for a IntentionalElement, with a using data from a given model (, and create a view class, if createView is true)
                public SemanticObjectController createForActor(ActorModel existingModel, bool syncModelToView)
                {
                    // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                    if (existingModel == null) return null;

                    // Check if this is the right create methode, else execute the right one
                    if (existingModel.GetType() != typeof(ActorModel))
                    {
                        SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingModel, syncModelToView);
                        return controllerCreatedFromExistingController;
                    }

                    // Result variable
                    ActorController controller;

                    // Generate entityID for the object
                    string newEntityID = existingModel.EntityID;

                    // Check if already exists
                    if (existingModel.FindSetController() != null) return ((ActorController)existingModel.FindSetController());

                    // Create new SemanticObjectController
                    else
                    {
                        controller = new ActorController(existingModel, null);

                        AddSemanticObjectToIndex(((ObjectModel)controller.Model).ObjectType, controller);
                    }

                    // If syncModelToView call this methode
                    if (syncModelToView == true)
                        controller.SyncModelToView();

                    // Return the return variable
                    return controller;

                }

                // creates a Controller for a IntentionalElement, with a using data from a given view (, and create a model class, if createModel is true)
                public SemanticObjectController createForActor(ActorView existingView, bool syncModelToView)
                {
                    // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                    if (existingView == null) return null;

                    // Check if this is the right create methode, else execute the right one
                    if (existingView.GetType() != typeof(ActorView))
                    {
                        SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingView, syncModelToView);
                        return controllerCreatedFromExistingController;
                    }

                    // Result variable
                    ActorController controller;

                    // Generate entityID for the object
                    string newEntityID = existingView.EntityID;

                    // Check if already exists
                    if (existingView.FindSetController() != null) return ((ActorController)existingView.FindSetController());

                    // Create new SemanticObjectController
                    else
                    {
                        controller = new ActorController(null, existingView);

                        AddSemanticObjectToIndex(((ObjectView)controller.View).ObjectType, controller);
                    }

                    // If syncModelToView call this methode
                    if (syncModelToView == true)
                        controller.SyncModelToView();

                    // Return the return variable
                    return controller;
                }


            //// creates a Controller for a IntentionalElement, with a using data from a given model (, and create a view class, if createView is true)
                    //public ObjectController createForPerson(PersonModel existingModel, bool createView, bool syncModelToView)
                    //{

                    //    // returns no controller, because the lack of an existing model denied the creation of a objectcontroller
                    //    if (existingModel == null) return null;

                    //    // Generate entityID for the object
                    //    string newEntityID = existingModel.entityID;

                    //    // Set new ObjectType
                    //    ObjectTypeEnum newObjectType = existingModel.objectType;

                    //    // Create Controller, using the specific methode
                    //    return createForObject(newEntityID, newObjectType, existingModel, false, null, createView, syncModelToView);

                    //}

                    //// creates a Controller for a IntentionalElement, with a using data from a given view (, and create a model class, if createModel is true)
                    //public ObjectController createForPerson(PersonView existingView, bool createModel, bool syncModelToView)
                    //{
                    //    // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                    //    if (existingView == null) return null;

                    //    // Generate entityID for the object
                    //    string newEntityID = existingView.entityID;

                    //    // Set new ObjectType
                    //    ObjectTypeEnum newObjectType = existingView.objectType;

                    //    // Create Controller, using the specific methode
                    //    return createForObject(newEntityID, newObjectType, null, createModel, existingView, false, syncModelToView);
                    //}


                    //// creates a Controller for a IntentionalElement, with a using data from a given model (, and create a view class, if createView is true)
                    //public ObjectController createForOrganisation(OrganisationModel existingModel, bool createView, bool syncModelToView)
                    //{

                    //    // returns no controller, because the lack of an existing model denied the creation of a objectcontroller
                    //    if (existingModel == null) return null;

                    //    // Generate entityID for the object
                    //    string newEntityID = existingModel.entityID;

                    //    // Set new ObjectType
                    //    ObjectTypeEnum newObjectType = existingModel.objectType;

                    //    // Create Controller, using the specific methode
                    //    return createForObject(newEntityID, newObjectType, existingModel, false, null, createView, syncModelToView);

                    //}

                    //// creates a Controller for a IntentionalElement, with a using data from a given view (, and create a model class, if createModel is true)
                    //public ObjectController createForOrganisation(OrganisationView existingView, bool createModel, bool syncModelToView)
                    //{
                    //    // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                    //    if (existingView == null) return null;

                    //    // Generate entityID for the object
                    //    string newEntityID = existingView.entityID;

                    //    // Set new ObjectType
                    //    ObjectTypeEnum newObjectType = existingView.objectType;

                    //    // Create Controller, using the specific methode
                    //    return createForObject(newEntityID, newObjectType, null, createModel, existingView, false, syncModelToView);
                    //}


             // creates a Controller for a Practice, with a using data from a given model (, and create a view class, if createView is true)
            public SemanticObjectController createForPractice(PracticeModel existingModel, bool syncModelToView)
            {

                // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                if (existingModel == null) return null;

                // Check if this is the right create methode, else execute the right one
                if (existingModel.GetType() != typeof(PracticeModel))
                {
                    SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingModel, syncModelToView);
                    return controllerCreatedFromExistingController;
                }

                // Result variable
                PracticeController controller;

                // Generate entityID for the object
                string newEntityID = existingModel.EntityID;

                // Check if already exists
                if (existingModel.FindSetController() != null) return ((PracticeController)existingModel.FindSetController());

                // Create new SemanticObjectController
                else
                {
                    controller = new PracticeController(existingModel, null);

                    AddSemanticObjectToIndex(((ObjectModel)controller.Model).ObjectType, controller);
                }

                // If syncModelToView call this methode
                if (syncModelToView == true)
                    controller.SyncModelToView();

                // Return the return variable
                return controller;

            }

            // creates a Controller for a Practice, with a using data from a given view (, and create a model class, if createModel is true)
            public SemanticObjectController createForPractice(PracticeView existingView, bool syncModelToView)
            {
                // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
                if (existingView == null) return null;

                // Check if this is the right create methode, else execute the right one
                if (existingView.GetType() != typeof(PracticeView))
                {
                    SemanticObjectController controllerCreatedFromExistingController = ExecuteCorrespondingSemanticObjectCreationMethode(existingView, syncModelToView);
                    return controllerCreatedFromExistingController;
                }

                // Result variable
                PracticeController controller;

                // Generate entityID for the object
                string newEntityID = existingView.EntityID;

                // Check if already exists
                if (existingView.FindSetController() != null) return ((PracticeController)existingView.FindSetController());

                // Create new SemanticObjectController
                else
                {
                    controller = new PracticeController(null, existingView);

                    AddSemanticObjectToIndex(((ObjectView)controller.View).ObjectType, controller);
                }

                // If syncModelToView call this methode
                if (syncModelToView == true)
                    controller.SyncModelToView();

                // Return the return variable
                return controller;
            }


    //--------------------------------// Creation methodes for ObjectControl MVC components //--------------------------------//

    // creates a Controller for a Practice, with a using data from a given model (, and create a view class, if createView is true)
    public ObjectControlController createForObjectControl(ObjectControlModel existingModel, bool syncModelToView)
    {
        // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
        if (existingModel == null) return null;

        // Check if this is the right create methode, else execute the right one
        if (existingModel.GetType() != typeof(ObjectControlModel))
        {
            ObjectControlController controllerCreatedFromExistingController = ExecuteCorrespondingObjectControlCreationMethode(existingModel, syncModelToView);
            return controllerCreatedFromExistingController;
        }

        // Result variable
        ObjectControlController controller;

        // Generate entityID for the object
        string newEntityID = existingModel.EntityID;

        // Check if already exists
        if (existingModel.FindSetController() != null) return ((ObjectControlController)existingModel.FindSetController());

        // Create new ObjectController
        else controller = new ObjectControlController(existingModel, null);

        // Add to index(es)
        AddObjectControlToIndex(((CameraControlModel)controller.Model).GetControlType(), controller);

        // If syncModelToView call this methode
        if (syncModelToView == true)
            controller.SyncModelToView();

        // Return the return variable
        return controller;
    }

    // creates a Controller for a Practice, with a using data from a given view (, and create a model class, if createModel is true)
    public ObjectControlController createForObjectControl(ObjectControlView existingView, bool syncModelToView)
    {
        // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
        if (existingView == null) return null;

        // Check if this is the right create methode, else execute the right one
        if (existingView.GetType() != typeof(ObjectControlView))
        {
            ObjectControlController controllerCreatedFromExistingController = ExecuteCorrespondingObjectControlCreationMethode(existingView, syncModelToView);
            return controllerCreatedFromExistingController;
        }

        // Result variable
        ObjectControlController controller;

        // Generate entityID for the object
        string newEntityID = existingView.EntityID;

        // Check if already exists
        if (existingView.FindSetController() != null) return ((ObjectControlController)existingView.FindSetController());

        // Create new ObjectController
        else controller = new ObjectControlController(null, existingView);

        // Add to index(es)
        AddObjectControlToIndex(((ObjectControlView)controller.View).GetControlType(), controller);

        // If syncModelToView call this methode
        if (syncModelToView == true)
            controller.SyncModelToView();

        // Return the return variable
        return controller;
    }


    // creates a Controller for a Practice, with a using data from a given model (, and create a view class, if createView is true)
    public ObjectControlController createForCameraControl(CameraControlModel existingModel, bool syncModelToView)
    {
        // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
        if (existingModel == null) return null;

        // Check if this is the right create methode, else execute the right one
        if (existingModel.GetType() != typeof(CameraControlModel))
        {
            ObjectControlController controllerCreatedFromExistingController = ExecuteCorrespondingObjectControlCreationMethode(existingModel, syncModelToView);
            return controllerCreatedFromExistingController;
        }

        // Result variable
        CameraControlController controller;

        // Generate entityID for the object
        string newEntityID = existingModel.EntityID;

        // Check if already exists
        if (existingModel.FindSetController() != null) return ((CameraControlController)existingModel.FindSetController());

        // Create new ObjectController
        else controller = new CameraControlController(existingModel, null);

        // Add to index(es)
        AddObjectControlToIndex(((CameraControlModel)controller.Model).GetControlType(), controller);

        // If syncModelToView call this methode
        if (syncModelToView == true)
            controller.SyncModelToView();

        // Return the return variable
        return controller;
    }

    // creates a Controller for a Practice, with a using data from a given view (, and create a model class, if createModel is true)
    public ObjectControlController createForCameraControl(CameraControlView existingView, bool syncModelToView)
    {
        // Result variable
        CameraControlController controller;

        // Check if this is the right create methode, else execute the right one
        if (existingView.GetType() != typeof(CameraControlView))
        {
            ObjectControlController controllerCreatedFromExistingController = ExecuteCorrespondingObjectControlCreationMethode(existingView, syncModelToView);
            return controllerCreatedFromExistingController;
        }

        // returns no controller, because the lack of an existing view denied the creation of a objectcontroller
        if (existingView == null) return null;

        // Generate entityID for the object
        string newEntityID = existingView.EntityID;

        // Check if already exists
        if (existingView.FindSetController() != null) return ((CameraControlController)existingView.FindSetController());

        // Create new ObjectController
        else controller = new CameraControlController(null, existingView);

        // Add to index(es)
        AddObjectControlToIndex(((CameraControlView)controller.View).GetControlType(), controller);

        // If syncModelToView call this methode
        if (syncModelToView == true)
            controller.SyncModelToView();

        // Return the return variable
        return controller;
    }



    // Executes the right create methode for corresponding to the parameter "model" Type
    private ObjectController ExecuteCorrespondingObjectCreationMethode(ObjectModel existingModel, bool syncModelToView)
    {
        // For Non-semantic objects
        if (existingModel.GetType() == typeof(CameraModel))
            return ObjectFactory.Instance.controllerFactory.createForCamera((CameraModel)existingModel, syncModelToView);
        else if (existingModel.GetType() == typeof(ComplexObjectModel))
            return ObjectFactory.Instance.controllerFactory.createForComplexObject((ComplexObjectModel)existingModel, syncModelToView);
        else if (existingModel.GetType() == typeof(NavObjectModel))
            return ObjectFactory.Instance.controllerFactory.createForNavObject((NavObjectModel)existingModel, syncModelToView);
        else if (existingModel.GetType() == typeof(ObjectModel))
            return ObjectFactory.Instance.controllerFactory.createForObject((ObjectModel)existingModel, syncModelToView);

        // For semantic objects
        if (existingModel.GetType() == typeof(SemanticObjectModel))
            return ExecuteCorrespondingSemanticObjectCreationMethode((SemanticObjectModel)existingModel, syncModelToView);

        else
            return null;
    }

    // Executes the right create methode for corresponding to the parameter "view" Type
    private ObjectController ExecuteCorrespondingObjectCreationMethode(ObjectView existingView, bool syncModelToView)
    {
        // For Non-semantic objects
        if (existingView.GetType() == typeof(CameraView))
            return ObjectFactory.Instance.controllerFactory.createForCamera((CameraView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(ComplexObjectView))
            return ObjectFactory.Instance.controllerFactory.createForComplexObject((ComplexObjectView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(NavObjectView))
            return ObjectFactory.Instance.controllerFactory.createForNavObject((NavObjectView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(ObjectView))
            return ObjectFactory.Instance.controllerFactory.createForObject((ObjectView)existingView, syncModelToView);


        // For semantic objects
        else if (existingView.GetType() == typeof(SemanticObjectView))
            return (ObjectController) ExecuteCorrespondingSemanticObjectCreationMethode(existingView, syncModelToView);

        else
            return null;
    }


    // Executes the right create methode for corresponding to the parameter "model" Type
    private SemanticObjectController ExecuteCorrespondingSemanticObjectCreationMethode(ObjectModel existingModel, bool syncModelToView)
    {

        // For semantic objects
        if (existingModel.GetType() == typeof(PracticeModel))
            return ObjectFactory.Instance.controllerFactory.createForPractice((PracticeModel)existingModel, syncModelToView);
        else if (existingModel.GetType() == typeof(ContextModel))
            return ObjectFactory.Instance.controllerFactory.createForContext((ContextModel)existingModel, syncModelToView);
        else if (existingModel.GetType() == typeof(IntentionalElementModel))
            return ObjectFactory.Instance.controllerFactory.createForIntentionalElement((IntentionalElementModel)existingModel, syncModelToView);
        else if (existingModel.GetType() == typeof(ActorModel))
            return ObjectFactory.Instance.controllerFactory.createForActor((ActorModel)existingModel, syncModelToView);
        else if (existingModel.GetType() == typeof(ActivityModel))
            return ObjectFactory.Instance.controllerFactory.createForActivity((ActivityModel)existingModel, syncModelToView);
        else if (existingModel.GetType() == typeof(SMWObjectModel))
            return ObjectFactory.Instance.controllerFactory.createForSMWObject((SMWObjectModel)existingModel, syncModelToView);
        else if (existingModel.GetType() == typeof(SemanticObjectModel))
            return ObjectFactory.Instance.controllerFactory.createForSemanticObject((SemanticObjectModel)existingModel, syncModelToView);

        else
            return null;
    }

    // Executes the right create methode for corresponding to the parameter "view" Type
    private SemanticObjectController ExecuteCorrespondingSemanticObjectCreationMethode(ObjectView existingView, bool syncModelToView)
    {
        // For semantic objects
        if (existingView.GetType() == typeof(PracticeView))
            return ObjectFactory.Instance.controllerFactory.createForPractice((PracticeView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(ContextView))
            return ObjectFactory.Instance.controllerFactory.createForContext((ContextView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(IntentionalElementView))
            return ObjectFactory.Instance.controllerFactory.createForIntentionalElement((IntentionalElementView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(ActorView))
            return ObjectFactory.Instance.controllerFactory.createForActor((ActorView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(ActivityView))
            return ObjectFactory.Instance.controllerFactory.createForActivity((ActivityView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(SMWObjectView))
            return ObjectFactory.Instance.controllerFactory.createForSMWObject((SMWObjectView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(SemanticObjectView))
            return ObjectFactory.Instance.controllerFactory.createForSemanticObject((SemanticObjectView)existingView, syncModelToView);

        else
            return null;
    }


    // Executes the right create methode for corresponding to the parameter "model" Type
    private ObjectControlController ExecuteCorrespondingObjectControlCreationMethode(ObjectControlModel existingModel, bool syncModelToView)
    {
        // For ObjectControllers
        if (existingModel.GetType() == typeof(ObjectControlModel))
            return ObjectFactory.Instance.controllerFactory.createForObjectControl((ObjectControlModel)existingModel, syncModelToView);
        else if (existingModel.GetType() == typeof(CameraControlModel))
            return ObjectFactory.Instance.controllerFactory.createForObjectControl((CameraControlModel)existingModel, syncModelToView);

        else
            return null;
    }

    // Executes the right create methode for corresponding to the parameter "view" Type
    private ObjectControlController ExecuteCorrespondingObjectControlCreationMethode(BaseView existingView, bool syncModelToView)
    {
        // For ObjectControllers
        if (existingView.GetType() == typeof(ObjectControlView))
            return ObjectFactory.Instance.controllerFactory.createForObjectControl((ObjectControlView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(CameraControlView))
            return ObjectFactory.Instance.controllerFactory.createForObjectControl((CameraControlView)existingView, syncModelToView);

        else
            return null;
    }



    private void AddObjectToIndex(ObjectTypeEnum objectType, ObjectController controller)
    {
        // Add controller to objectfactory index
        ObjectFactory.Instance.AddToIndex(controller);

        // Updates the GameManger index
        //if (GameManager.instance.controllersByEntityID.ContainsKey(objectType) != true)
        //    GameManager.instance.controllersByEntityID.Add(objectType, new Dictionary<string, ObjectController>());

        //if (GameManager.instance.controllersByEntityID[objectType].ContainsKey(entityID) == true)
        //    GameManager.instance.controllersByEntityID[objectType][entityID] = controller;
        //else GameManager.instance.controllersByEntityID[objectType].Add(entityID, controller);

    }

    private void AddObjectControlToIndex(ControlTypeEnum controlType, ObjectControlController controller)
    {
        // Add controller to objectfactory index
        ObjectFactory.Instance.AddToIndex(controller);

        // Updates the GameManger index
        //if (GameManager.instance.controlControllersByEntityID.ContainsKey(controlType) != true)
        //    GameManager.instance.controlControllersByEntityID.Add(controlType, new Dictionary<string, ObjectControlController>());

        //if (GameManager.instance.controlControllersByEntityID[controlType].ContainsKey(entityID) == true)
        //    GameManager.instance.controlControllersByEntityID[controlType][entityID] = controller;
        //else GameManager.instance.controlControllersByEntityID[controlType].Add(entityID, controller);
    }

    private void AddSemanticObjectToIndex(ObjectTypeEnum objectType, SemanticObjectController controller)
    {
        // Add controller to objectfactory index
        ObjectFactory.Instance.AddToIndex(controller);

        // Updates the GameManger index
        //if (GameManager.instance.controllersBySementicURI.ContainsKey(objectType) != true)
        //    GameManager.instance.controllersBySementicURI.Add(objectType, new Dictionary<string, SemanticObjectController>());

        //if (GameManager.instance.controllersBySementicURI[objectType].ContainsKey(objectURI) == true)
        //    GameManager.instance.controllersBySementicURI[objectType][objectURI] = controller;
        //else GameManager.instance.controllersBySementicURI[objectType].Add(objectURI, controller);
    }

}
