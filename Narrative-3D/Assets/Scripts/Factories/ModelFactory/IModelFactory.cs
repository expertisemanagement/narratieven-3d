﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObjectModelFactory {

    // get the created objectModel
    ObjectModel objectModel { get; }

}
