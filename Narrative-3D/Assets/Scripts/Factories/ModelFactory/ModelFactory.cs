﻿using Object.Type;
using ObjectControl.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VDS.RDF;

public class ModelFactory {

    // A instance of the ObjectFactory as part of the Singleton pattern
    private static ModelFactory instance = null;

    // A Multitread singleton duplication prevention object. 
    // This object is locked the first time the Instance() is called, Instance() does nothing when the object is locked.
    private static readonly object padlock = new object();

    // Constructor
    private ModelFactory()
    {
    }

    // Returns this object's instance variable value, also creates a new ObjectFactory instance if the padlock is not locked. (Mutlitread security)
    public static ModelFactory Instance
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new ModelFactory();
                }
                return instance;
            }
        }
    }

    // Create Object model

    public ObjectModel createForObject(ObjectView view, bool syncModelToView)
    {
        // controller or view are null or if the controller already has a model, then stop this methode
        if (view == null)
            return null;

        // Check if this is the right create methode, else execute the right one
        if (view.GetType() != typeof(ObjectView))
        {
            ObjectModel modelCreatedFromExistingView = ExecuteCorrespondingCreateMethode(view, syncModelToView);
            return modelCreatedFromExistingView;
        }

        // If no controller exists, then make one
        if (view.FindCreateSetController() == null)
            return null;

        // If there is already a model return that one
        if (view.Controller.FindSetModel() != null)
            return (ObjectModel)view.Controller.Model;
        else
        {
            ObjectModel newModel = new ObjectModel(view, syncModelToView);
            AddObjectToIndex(newModel.ObjectType, newModel);
            return newModel;
        }
    }

    public ObjectModel createForObject(string entityID)
    {
        // Check if object exists
        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
            entityID = ObjectFactory.Instance.GenerateEntityID();

        ObjectModel newModel = new ObjectModel(entityID);

        AddObjectToIndex(newModel.ObjectType, newModel);

        return newModel;
    }

    public ObjectModel createForObject()
    {
        return createForObject(ObjectFactory.Instance.GenerateEntityID());
    }


        // Create ComplexObject model

        public ObjectModel createForComplexObject(ComplexObjectView view, bool syncModelToView)
            {
                // controller or view are null or if the controller already has a model, then stop this methode
                if (view == null)
                    return null;

                // Check if this is the right create methode, else execute the right one
                if (view.GetType() != typeof(ComplexObjectView))
                {
                    ObjectModel modelCreatedFromExistingView = ExecuteCorrespondingCreateMethode(view, syncModelToView);
                    return modelCreatedFromExistingView;
                }

                // If no controller exists, then make one
                if (view.FindCreateSetController() == null)
                    return null;

                // If there is already a model return that one
                if (view.Controller.FindSetModel() != null)
                    return (ObjectModel)view.Controller.Model;
                else
                {
                    ComplexObjectModel newModel = new ComplexObjectModel(view, syncModelToView);
                    AddObjectToIndex(newModel.ObjectType, newModel);
                    return newModel;
                }
            }

        public ObjectModel createForComplexObject(string entityID)
        {
        // Check if object exists
        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
            entityID = ObjectFactory.Instance.GenerateEntityID();

        ComplexObjectModel newModel = new ComplexObjectModel(entityID);

        AddObjectToIndex(newModel.ObjectType, newModel);

        return newModel;
        }

        public ObjectModel createForComplexObject()
        {
            return createForComplexObject(ObjectFactory.Instance.GenerateEntityID());
        }
    
        public ObjectModel createForComplexObject(string entityID, string customName)
        {
            // Check if object exists
            if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                entityID = ObjectFactory.Instance.GenerateEntityID();

            ComplexObjectModel objectModel = (ComplexObjectModel) createForComplexObject(entityID);

            objectModel.CustomName = customName + " " + objectModel.EntityID;

            return objectModel;
        }


        // Create Camera model

        public ObjectModel createForCamera(CameraView view, bool syncModelToView)
            {
                // controller or view are null or if the controller already has a model, then stop this methode
                if (view == null)
                    return null;

                // Check if this is the right create methode, else execute the right one
                if (view.GetType() != typeof(CameraView))
                {
                    ObjectModel modelCreatedFromExistingView = ExecuteCorrespondingCreateMethode(view, syncModelToView);
                    return modelCreatedFromExistingView;
                }

                // If no controller exists, then make one
                if (view.FindCreateSetController() == null)
                    return null;

                // If there is already a model return that one
                if (view.Controller.FindSetModel() != null)
                    return (ObjectModel)view.Controller.Model;
                else
                {
                    CameraModel newModel = new CameraModel(view, syncModelToView);
                    AddObjectToIndex(newModel.ObjectType, newModel);
                    return newModel;
                }
            }

        public ObjectModel createForCamera(string entityID)
        {
            // Check if object exists
            if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                entityID = ObjectFactory.Instance.GenerateEntityID();

            CameraModel newModel = new CameraModel(entityID);

            AddObjectToIndex(newModel.ObjectType, newModel);

            return newModel;
        }

        public ObjectModel createForCamera()
        {
            return createForCamera(ObjectFactory.Instance.GenerateEntityID());
        }


    // Create NavObject

    public ObjectModel createForNavObject(NavObjectView view, bool syncModelToView)
    {
        // controller or view are null or if the controller already has a model, then stop this methode
        if (view == null)
            return null;

        // Check if this is the right create methode, else execute the right one
        if (view.GetType() != typeof(NavObjectView))
        {
            ObjectModel modelCreatedFromExistingView = ExecuteCorrespondingCreateMethode(view, syncModelToView);
            return modelCreatedFromExistingView;
        }

        // If no controller exists, then make one
        if (view.FindCreateSetController() == null)
            return null;

        // If there is already a model return that one
        if (view.Controller.FindSetModel() != null)
            return (ObjectModel)view.Controller.Model;
        else
        {
            NavObjectModel newModel = new NavObjectModel(view, syncModelToView);
            AddObjectToIndex(newModel.ObjectType, newModel);
            return newModel;
        }
    }

    public ObjectModel createForNavObject(string entityID, SemanticObjectModel semanticObjectModel)
    {
        // Check for an existing NavObjectModel holding the same semanticModel, return that one if found
        NavObjectModel navObjectModel = ObjectFactory.Instance.entityModels.Values.ToList().Where(m => m is NavObjectModel).Cast<NavObjectModel>().First(m => m.SemanticObject == semanticObjectModel);
        if (navObjectModel != null)
            return navObjectModel;

        // Check if object exists
        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
            entityID = ObjectFactory.Instance.GenerateEntityID();

        return new NavObjectModel(entityID, semanticObjectModel);
    }

    public ObjectModel createForNavObject(SemanticObjectModel semanticObjectModel)
    {
        return createForNavObject(ObjectFactory.Instance.GenerateEntityID(), semanticObjectModel);
    }


    //--------------------------------// Creation methodes for Semantic objects MVC components //--------------------------------//

            // Create SemanticObject

            public SemanticObjectModel createForSemanticObject(SemanticObjectView view, bool syncModelToView)
            {
                // controller or view are null or if the controller already has a model, then stop this methode
                if (view == null)
                    return null;

                // Check if this is the right create methode, else execute the right one
                if (view.GetType() != typeof(SemanticObjectView))
                {
                    SemanticObjectModel modelCreatedFromExistingView = ExecuteCorrespondingSemanticObjectCreationMethode(view, syncModelToView);
                    return modelCreatedFromExistingView;
                }

                // If no controller exists, then make one
                if (view.FindCreateSetController() == null)
                    return null;

                // If there is already a model return that one
                if (view.Controller.FindSetModel() != null)
                    return (SemanticObjectModel) view.Controller.Model;
                else
                {
                    SemanticObjectModel newModel = new SemanticObjectModel(view.EntityID, view.SemanticNode, view.NavObject);
                    AddSemanticObjectToIndex(newModel.ObjectType, newModel);
                    return newModel;
                }
            }

            public SemanticObjectModel createForSemanticObject(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject)
            {
                // Find existing MVC-components and try to build  a model using those components, if successed return that result
                ObjectModel madeModelUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                if (madeModelUsingExistingComponents != null)
                    if(madeModelUsingExistingComponents is SemanticObjectModel)
                    {
                        if (navObject != null)
                            ((SemanticObjectModel)madeModelUsingExistingComponents).NavObject = navObject;
                            return (SemanticObjectModel) madeModelUsingExistingComponents;
                    }

                // If there is a category, call the other methode
                if (CheckForRightCategory(semanticNode, category) != null)
                    return createForSMWObject(entityID, semanticNode, category, navObject);

                // Check if entityID is new, else generate new ID
                //string id = entityID;
                if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                    entityID = ObjectFactory.Instance.GenerateEntityID();
        
                // Set ObjectType
                ObjectTypeEnum type = ObjectTypeEnum.SEMANTICOBJECT;

                // semanticObjectModel to return
                SemanticObjectModel newSemanticObjectModel = new SemanticObjectModel(entityID, semanticNode, navObject);
                
                // Add to index
                AddSemanticObjectToIndex(type, newSemanticObjectModel);

                return newSemanticObjectModel;
            }

            public SemanticObjectModel createForSemanticObject(INode semanticNode, SMWCategory category, NavObjectModel navObject)
            {
                return createForSemanticObject(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject);
            }


                // creates a SMWObject

                public SemanticObjectModel createForSMWObject(SMWObjectView view, bool syncModelToView)
                {
                    // controller or view are null or if the controller already has a model, then stop this methode
                    if (view == null)
                        return null;

                    // Check if this is the right create methode, else execute the right one
                    if (view.GetType() != typeof(SMWObjectView))
                    {
                        SemanticObjectModel modelCreatedFromExistingView = ExecuteCorrespondingSemanticObjectCreationMethode(view, syncModelToView);
                        return modelCreatedFromExistingView;
                    }

                    // If no controller exists, then make one
                    if (view.FindCreateSetController() == null)
                        return null;

                    // If there is already a model return that one
                    if (view.Controller.FindSetModel() != null)
                        return (SemanticObjectModel)view.Controller.Model;
                    else
                    {
                        SemanticObjectModel newModel = new SMWObjectModel(view.EntityID, view.SemanticNode, view.Category, view.NavObject);
                        AddSemanticObjectToIndex(newModel.ObjectType, newModel);
                        return newModel;
                    }
                }

                public SemanticObjectModel createForSMWObject(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject)
                {
                    // Find existing MVC-components and try to build  a model using those components, if successed return that result
                    ObjectModel madeModelUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                    if (madeModelUsingExistingComponents != null)
                        if (madeModelUsingExistingComponents is SemanticObjectModel)
                        {
                            if (navObject != null)
                                ((SemanticObjectModel)madeModelUsingExistingComponents).NavObject = navObject;
                            return (SemanticObjectModel)madeModelUsingExistingComponents;
                        }

                    // Check if current Category is correct, else search for the right one. If there still is no right category, then stop this methode
                    SMWCategory correspondingCategory = CheckForRightCategory(semanticNode, category);
                    if (correspondingCategory == null)
                        return null;

                    // Check if this is the right create methode, else execute the right one
                    if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) != typeof(SMWObjectModel))
                    {
                        SemanticObjectModel model = ExecuteCorrespondingSemanticObjectCreationMethode(entityID, semanticNode, category, navObject);
                        return model;
                    }

                    // Check if entityID is new, else generate new ID
                    //string id = entityID;
                    if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                     entityID = ObjectFactory.Instance.GenerateEntityID();

                    // Set ObjectType
                    ObjectTypeEnum type = ObjectTypeEnum.SMWOBJECT;

                    // semanticObjectModel to return
                    SemanticObjectModel newSemanticObjectModel = new SMWObjectModel(entityID, semanticNode, category, navObject);

                    // Add to index
                    AddSemanticObjectToIndex(type, newSemanticObjectModel);

                    return newSemanticObjectModel;
                }
                
                public SemanticObjectModel createForSMWObject(INode semanticNode, SMWCategory category, NavObjectModel navObject)
                {
                    return createForSMWObject(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject);
                }

                    // creates a Context

                    public SemanticObjectModel createForContext(ContextView view, bool syncModelToView)
                    {
                        // controller or view are null or if the controller already has a model, then stop this methode
                        if (view == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (view.GetType() != typeof(ContextView))
                        {
                            SemanticObjectModel modelCreatedFromExistingView = ExecuteCorrespondingSemanticObjectCreationMethode(view, syncModelToView);
                            return modelCreatedFromExistingView;
                        }

                        // If no controller exists, then make one
                        if (view.FindCreateSetController() == null)
                            return null;

                        // If there is already a model return that one
                        if (view.Controller.FindSetModel() != null)
                            return (SemanticObjectModel)view.Controller.Model;
                        else
                        {
                            SemanticObjectModel newModel = new ContextModel(view.EntityID, view.SemanticNode, view.Category, view.NavObject);
                            AddSemanticObjectToIndex(newModel.ObjectType, newModel);
                            return newModel;
                        }
                    }

                    public SemanticObjectModel createForContext(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject)
                    {
                        // Find existing MVC-components and try to build  a model using those components, if successed return that result
                        ObjectModel madeModelUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                        if (madeModelUsingExistingComponents != null)
                            if (madeModelUsingExistingComponents is SemanticObjectModel)
                            {
                                if (navObject != null)
                                    ((SemanticObjectModel)madeModelUsingExistingComponents).NavObject = navObject;
                                return (SemanticObjectModel)madeModelUsingExistingComponents;
                            }

                        // Check if current Category is correct, else search for the right one. If there still is no right category, then stop this methode
                        SMWCategory correspondingCategory = CheckForRightCategory(semanticNode, category);
                        if (correspondingCategory == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) != typeof(ContextModel))
                        {
                            SemanticObjectModel model = ExecuteCorrespondingSemanticObjectCreationMethode(entityID, semanticNode, category, navObject);
                            return model;
                        }

                        // Check if entityID is new, else generate new ID
                        //string id = entityID;
                        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                            entityID = ObjectFactory.Instance.GenerateEntityID();

                        // Set ObjectType
                        ObjectTypeEnum type = ObjectTypeEnum.CONTEXT;

                        // ContextModel to return
                        ContextModel newSemanticObjectModel = new ContextModel(entityID, semanticNode, category, navObject);

                        // Add to index
                        AddSemanticObjectToIndex(type, newSemanticObjectModel);

                        return newSemanticObjectModel;
                    }

                    public SemanticObjectModel createForContext(INode semanticNode, SMWCategory category, NavObjectModel navObject)
                    {
                        return createForContext(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject);
                    }

                    // creates a IntentionalElement

                    public SemanticObjectModel createForIntentionalElement(IntentionalElementView view, bool syncModelToView)
                    {
                        // controller or view are null or if the controller already has a model, then stop this methode
                        if (view == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (view.GetType() != typeof(IntentionalElementView))
                        {
                            SemanticObjectModel modelCreatedFromExistingView = ExecuteCorrespondingSemanticObjectCreationMethode(view, syncModelToView);
                            return modelCreatedFromExistingView;
                        }

                        // If no controller exists, then make one
                        if (view.FindCreateSetController() == null)
                            return null;

                        // If there is already a model return that one
                        if (view.Controller.FindSetModel() != null)
                            return (SemanticObjectModel)view.Controller.Model;
                        else
                        {
                            SemanticObjectModel newModel = new IntentionalElementModel(view.EntityID, view.SemanticNode, view.Category, view.NavObject);
                            AddSemanticObjectToIndex(newModel.ObjectType, newModel);
                            return newModel;
                        }
                    }

                    public SemanticObjectModel createForIntentionalElement(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject)
                    {
                        // Find existing MVC-components and try to build  a model using those components, if successed return that result
                        ObjectModel madeModelUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                        if (madeModelUsingExistingComponents != null)
                            if (madeModelUsingExistingComponents is SemanticObjectModel)
                            {
                                if (navObject != null)
                                    ((SemanticObjectModel)madeModelUsingExistingComponents).NavObject = navObject;
                                return (SemanticObjectModel)madeModelUsingExistingComponents;
                            }

                        // Check if current Category is correct, else search for the right one. If there still is no right category, then stop this methode
                        SMWCategory correspondingCategory = CheckForRightCategory(semanticNode, category);
                        if (correspondingCategory == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) != typeof(IntentionalElementModel))
                        {
                            SemanticObjectModel model = ExecuteCorrespondingSemanticObjectCreationMethode(entityID, semanticNode, category, navObject);
                            return model;
                        }

                        // Check if entityID is new, else generate new ID
                        //string id = entityID;
                    if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                        entityID = ObjectFactory.Instance.GenerateEntityID();

                        // Set ObjectType
                        ObjectTypeEnum type = ObjectTypeEnum.INTENTIONALELEMENT;

                        // semanticObjectModel to return
                        IntentionalElementModel newSemanticObjectModel = new IntentionalElementModel(entityID, semanticNode, category, navObject);

                        // Add to index
                        AddSemanticObjectToIndex(type, newSemanticObjectModel);

                        return newSemanticObjectModel;
                    }

                    public SemanticObjectModel createForIntentionalElement(INode semanticNode, SMWCategory category, NavObjectModel navObject)
                    {
                        return createForIntentionalElement(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject);
                    }

                        // creates a Activity

                    public SemanticObjectModel createForActivity(ActivityView view, bool syncModelToView)
                    {
                        // controller or view are null or if the controller already has a model, then stop this methode
                        if (view == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (view.GetType() != typeof(ActivityView))
                        {
                            SemanticObjectModel modelCreatedFromExistingView = ExecuteCorrespondingSemanticObjectCreationMethode(view, syncModelToView);
                            return modelCreatedFromExistingView;
                        }

                        // If no controller exists, then make one
                        if (view.FindCreateSetController() == null)
                            return null;

                        // If there is already a model return that one
                        if (view.Controller.FindSetModel() != null)
                            return (SemanticObjectModel)view.Controller.Model;
                        else
                        {
                            SemanticObjectModel newModel = new ActivityModel(view.EntityID, view.SemanticNode, view.Category, view.NavObject);
                            AddSemanticObjectToIndex(newModel.ObjectType, newModel);
                            return newModel;
                        }
                    }

                    public SemanticObjectModel createForActivity(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject)
                    {
                        // Find existing MVC-components and try to build  a model using those components, if successed return that result
                        ObjectModel madeModelUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                        if (madeModelUsingExistingComponents != null)
                            if (madeModelUsingExistingComponents is SemanticObjectModel)
                            {
                                if (navObject != null)
                                    ((SemanticObjectModel)madeModelUsingExistingComponents).NavObject = navObject;
                                return (SemanticObjectModel)madeModelUsingExistingComponents;
                            }

                        // Check if current Category is correct, else search for the right one. If there still is no right category, then stop this methode
                        SMWCategory correspondingCategory = CheckForRightCategory(semanticNode, category);
                        if (correspondingCategory == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) != typeof(ActivityModel))
                        {
                            SemanticObjectModel model = ExecuteCorrespondingSemanticObjectCreationMethode(entityID, semanticNode, category, navObject);
                            return model;
                        }

                        // Check if entityID is new, else generate new ID
                        //string id = entityID;
                        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                            entityID = ObjectFactory.Instance.GenerateEntityID();

                        // Set ObjectType
                        ObjectTypeEnum type = ObjectTypeEnum.ACTIVITY;

                        // semanticObjectModel to return
                        ActivityModel newSemanticObjectModel = new ActivityModel(entityID, semanticNode, category, navObject);

                        // Add to index
                        AddSemanticObjectToIndex(type, newSemanticObjectModel);

                        return newSemanticObjectModel;
                    }

                    public SemanticObjectModel createForActivity(INode semanticNode, SMWCategory category, NavObjectModel navObject)
                    {
                        return createForActivity(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject);
                    }

                    // creates a Actor

                    public SemanticObjectModel createForActor(ActorView view, bool syncModelToView)
                    {
                        // controller or view are null or if the controller already has a model, then stop this methode
                        if (view == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (view.GetType() != typeof(ActorView))
                        {
                            SemanticObjectModel modelCreatedFromExistingView = ExecuteCorrespondingSemanticObjectCreationMethode(view, syncModelToView);
                            return modelCreatedFromExistingView;
                        }

                        // If no controller exists, then make one
                        if (view.FindCreateSetController() == null)
                            return null;

                        // If there is already a model return that one
                        if (view.Controller.FindSetModel() != null)
                            return (SemanticObjectModel)view.Controller.Model;
                        else
                        {
                            SemanticObjectModel newModel = new ActorModel(view.EntityID, view.SemanticNode, view.Category, view.NavObject);
                            AddSemanticObjectToIndex(newModel.ObjectType, newModel);
                            return newModel;
                        }
                    }

                    public SemanticObjectModel createForActor(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject)
                    {
                        // Find existing MVC-components and try to build  a model using those components, if successed return that result
                        ObjectModel madeModelUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                        if (madeModelUsingExistingComponents != null)
                            if (madeModelUsingExistingComponents is SemanticObjectModel)
                            {
                                if (navObject != null)
                                    ((SemanticObjectModel)madeModelUsingExistingComponents).NavObject = navObject;
                                return (SemanticObjectModel)madeModelUsingExistingComponents;
                            }

                        // Check if current Category is correct, else search for the right one. If there still is no right category, then stop this methode
                        SMWCategory correspondingCategory = CheckForRightCategory(semanticNode, category);
                        if (correspondingCategory == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) != typeof(ActorModel))
                        {
                            SemanticObjectModel model = ExecuteCorrespondingSemanticObjectCreationMethode(entityID, semanticNode, category, navObject);
                            return model;
                        }

                        // Check if entityID is new, else generate new ID
                        //string id = entityID;
                    if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                        entityID = ObjectFactory.Instance.GenerateEntityID();

                        // Set ObjectType
                        ObjectTypeEnum type = ObjectTypeEnum.ACTOR;

                        // semanticObjectModel to return
                        SemanticObjectModel newSemanticObjectModel = new SMWObjectModel(entityID, semanticNode, category, navObject);

                        // Add to index
                        AddSemanticObjectToIndex(type, newSemanticObjectModel);

                        return newSemanticObjectModel;
                    }

                    public SemanticObjectModel createForActor(INode semanticNode, SMWCategory category, NavObjectModel navObject)
                    {
                        return createForActor(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject);
                    }

                    // creates a Practice

                    public SemanticObjectModel createForPractice(PracticeView view, bool syncModelToView)
                    {
                        // controller or view are null or if the controller already has a model, then stop this methode
                        if (view == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (view.GetType() != typeof(PracticeView))
                        {
                            SemanticObjectModel modelCreatedFromExistingView = ExecuteCorrespondingSemanticObjectCreationMethode(view, syncModelToView);
                            return modelCreatedFromExistingView;
                        }

                        // If no controller exists, then make one
                        if (view.FindCreateSetController() == null)
                            return null;

                        // If there is already a model return that one
                        if (view.Controller.FindSetModel() != null)
                            return (SemanticObjectModel)view.Controller.Model;
                        else
                        {
                            SemanticObjectModel newModel = new PracticeModel(view.EntityID, view.SemanticNode, view.Category, view.NavObject);
                            AddSemanticObjectToIndex(newModel.ObjectType, newModel);
                            return newModel;
                        }
                    }

                    public SemanticObjectModel createForPractice(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject)
                    {
                        // Find existing MVC-components and try to build  a model using those components, if successed return that result
                        ObjectModel madeModelUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                        if (madeModelUsingExistingComponents != null)
                            if (madeModelUsingExistingComponents is SemanticObjectModel)
                            {
                                if (navObject != null)
                                    ((SemanticObjectModel)madeModelUsingExistingComponents).NavObject = navObject;
                                return (SemanticObjectModel)madeModelUsingExistingComponents;
                            }
                        
                        // Check if current Category is correct, else search for the right one. If there still is no right category, then stop this methode
                        SMWCategory correspondingCategory = CheckForRightCategory(semanticNode, category);
                        if (correspondingCategory == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) != typeof(PracticeModel))
                        {
                            SemanticObjectModel model = ExecuteCorrespondingSemanticObjectCreationMethode(entityID, semanticNode, category, navObject);
                            return model;
                        }

                        // Check if entityID is new, else generate new ID
                        //string id = entityID;
                        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                            entityID = ObjectFactory.Instance.GenerateEntityID();

                        // Set ObjectType
                        ObjectTypeEnum type = ObjectTypeEnum.PRACTICE;

                        // semanticObjectModel to return
                        PracticeModel newSemanticObjectModel = new PracticeModel(entityID, semanticNode, category, navObject);

                        // Add to index
                        AddSemanticObjectToIndex(type, newSemanticObjectModel);

                        return newSemanticObjectModel;
                    }

                    public SemanticObjectModel createForPractice(INode semanticNode, SMWCategory category, NavObjectModel navObject)
                    {
                        return createForPractice(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject);
                    }


    //--------------------------------// Creation methodes for ObjectControl MVC components //--------------------------------//

        // Create a ObjectControl

        public ObjectControlModel createForObjectControl(ObjectControlView view, bool syncModelToView)
        {
            // controller or view are null or if the controller already has a model, then stop this methode
            if (view == null)
                return null;

            // Check if this is the right create methode, else execute the right one
            if (view.GetType() != typeof(ObjectControlView))
            {
                ObjectControlModel modelCreatedFromExistingView = ExecuteCorrespondingObjectControlCreationMethode(view, syncModelToView);
                return modelCreatedFromExistingView;
            }

            // If no controller exists, then make one
            if (view.FindCreateSetController() == null)
                return null;

            // If there is already a model return that one
            if (view.Controller.FindSetModel() != null)
                return (ObjectControlModel) view.Controller.Model;
            else
            {
                ObjectControlModel newModel = new ObjectControlModel(view, syncModelToView);
                AddObjectControlToIndex(ControlTypeEnum.OBJECTCONTROL, newModel);
                return newModel;
            }
        }

        public ObjectControlModel createForObjectControl(string entityID, ComplexObjectModel controlledSubject ,bool syncModelToView)
        {
            // Check for an existing NavObjectModel holding the same semanticModel, return that one if found
            ObjectControlModel controlModel = ObjectFactory.Instance.entityModels.Values.ToList().Where(m => m is ObjectControlModel).Cast<ObjectControlModel>().First(m => m.controlWielder == controlledSubject);
            if (controlModel != null)
                return controlModel;

            // Check if object exists
            if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                    entityID = ObjectFactory.Instance.GenerateEntityID();

        ObjectControlModel newModel = new ObjectControlModel(entityID, controlledSubject);

        AddObjectControlToIndex(ControlTypeEnum.OBJECTCONTROL, newModel);

        return newModel;
        }

        public ObjectControlModel createForObjectControl(ComplexObjectModel controlledSubject ,bool syncModelToView)
        {
            return createForObjectControl(ObjectFactory.Instance.GenerateEntityID(), controlledSubject, syncModelToView);
        }

        // Create a CameraControl

        public ObjectControlModel createForCameraControl(CameraControlView view, bool syncModelToView)
            {
                // controller or view are null or if the controller already has a model, then stop this methode
                if (view == null)
                    return null;

                // Check if this is the right create methode, else execute the right one
                if (view.GetType() != typeof(CameraControlView))
                {
                    ObjectControlModel modelCreatedFromExistingView = ExecuteCorrespondingObjectControlCreationMethode(view, syncModelToView);
                    return modelCreatedFromExistingView;
                }

                // If no controller exists, then make one
                if (view.FindCreateSetController() == null)
                    return null;

                // If there is already a model return that one
                if (view.Controller.FindSetModel() != null)
                    return (CameraControlModel)view.Controller.Model;
                else
                {
                    ObjectControlModel newModel = new CameraControlModel(view, syncModelToView);
                    AddObjectControlToIndex(ControlTypeEnum.CAMERACONTROL, newModel);
                    return newModel;
                }
            }

        public ObjectControlModel createForCameraControl(string entityID, ComplexObjectModel controlledSubject, bool syncModelToView)
        {
            // If no controlledSubject is given stop this method
            if (controlledSubject == null)
                return null;

            // Check for an existing NavObjectModel holding the same semanticModel, return that one if found
            ObjectControlModel controlModel = ObjectFactory.Instance.entityModels.Values.ToList().Where(m => m is ObjectControlModel).Cast<ObjectControlModel>().First(m => m.controlWielder == controlledSubject);
            if (controlModel != null)
                return controlModel;

            // Check if object exists
            if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                    entityID = ObjectFactory.Instance.GenerateEntityID();

        ObjectControlModel newModel = new CameraControlModel(entityID, controlledSubject);

        AddObjectControlToIndex(ControlTypeEnum.CAMERACONTROL, newModel);

        return newModel;
    }
    
        public ObjectControlModel createForCameraControl(ComplexObjectModel controlledSubject ,bool syncModelToView)
        {
            return createForCameraControl(ObjectFactory.Instance.GenerateEntityID(), controlledSubject, syncModelToView);
        }


    // Executes the right create methode for corresponding to the parameter "view" Type
    private ObjectModel ExecuteCorrespondingCreateMethode(ObjectView view, bool syncModelToView)
    {
        if (view.GetType() == typeof(PracticeView))
            return ObjectFactory.Instance.modelFactory.createForPractice((PracticeView)view, syncModelToView);

        else if (view.GetType() == typeof(ContextView))
            return ObjectFactory.Instance.modelFactory.createForContext((ContextView)view, syncModelToView);

        else if (view.GetType() == typeof(IntentionalElementView))
            return ObjectFactory.Instance.modelFactory.createForIntentionalElement((IntentionalElementView)view, syncModelToView);

        else if (view.GetType() == typeof(ActorView))
            return ObjectFactory.Instance.modelFactory.createForActor((ActorView)view, syncModelToView);

        else if (view.GetType() == typeof(ActivityView))
            return ObjectFactory.Instance.modelFactory.createForActivity((ActivityView)view, syncModelToView);

        else if (view.GetType() == typeof(SMWObjectView))
            return ObjectFactory.Instance.modelFactory.createForSMWObject((SMWObjectView)view, syncModelToView);

        else if (view.GetType() == typeof(SemanticObjectView))
            return ObjectFactory.Instance.modelFactory.createForSemanticObject((SemanticObjectView)view, syncModelToView);

        else if (view.GetType() == typeof(ComplexObjectView))
            return ObjectFactory.Instance.modelFactory.createForComplexObject((ComplexObjectView)view, syncModelToView);

        else if (view.GetType() == typeof(NavObjectView))
            return ObjectFactory.Instance.modelFactory.createForNavObject((NavObjectView)view, syncModelToView);

        else if (view.GetType() == typeof(CameraView))
            return ObjectFactory.Instance.modelFactory.createForCamera((CameraView)view, syncModelToView);
        else
            return null;
    }

    // Executes the right create methode for corresponding to the parameter "controller" Type
    private ObjectModel ExecuteCorrespondingCreateMethode(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject)
    {
        if (category.objectType == ObjectTypeEnum.ACTIVITY)
            return ObjectFactory.Instance.modelFactory.createForActivity(entityID, semanticNode, category, navObject);

        else if (category.objectType == ObjectTypeEnum.ACTOR)
            return ObjectFactory.Instance.modelFactory.createForActor(entityID, semanticNode, category, navObject);

        else if (category.objectType == ObjectTypeEnum.CONTEXT)
            return ObjectFactory.Instance.modelFactory.createForContext(entityID, semanticNode, category, navObject);

        else if (category.objectType == ObjectTypeEnum.PRACTICE)
            return ObjectFactory.Instance.modelFactory.createForPractice(entityID, semanticNode, category, navObject);

        else if (category.objectType == ObjectTypeEnum.INTENTIONALELEMENT)
            return ObjectFactory.Instance.modelFactory.createForIntentionalElement(entityID, semanticNode, category, navObject);

        else if (category.objectType == ObjectTypeEnum.SMWOBJECT)
            return ObjectFactory.Instance.modelFactory.createForSMWObject(entityID, semanticNode, category, navObject);

        else
            return null;
    }


    // Using the entityID there shall be asked for a existing View or Controller, which will be used to create a Model if possible
    private BaseModel CreateUsingFoundNonSemanticObjectMVCComponents(string entityID)
    {
        BaseModel existingModel = ObjectFactory.Instance.GetObjectModelByEntityID(entityID);
        if (existingModel != null)
            return existingModel;

        BaseView existingView = ObjectFactory.Instance.GetObjectViewByEnityID(entityID);
        if (existingView != null)
        {
            if (existingView is ObjectControlView)
                return createForObjectControl((ObjectControlView) existingView, false);
            else if (existingView is ObjectView)
                return createForObject((ObjectView) existingView, false);
            else
                return null;
        }

        return null;
    }

    // Using the semanticNode there shall be asked for a existing View or Controller, which will be used to create a Model if possible
    private SemanticObjectModel CreateUsingFoundSemanticMVCComponents(INode semanticNode)
    {
        SemanticObjectModel existingModel = ObjectFactory.Instance.GetSemanticObjectModelByNode(semanticNode);
        if (existingModel != null)
            return existingModel;

        SemanticObjectView existingView = ObjectFactory.Instance.GetSemanticObjectViewByNode(semanticNode);
        if (existingView != null)
        {
            if (existingView is SMWObjectView)
            {
                return createForSMWObject((SMWObjectView) existingView, false);
            }
            else
            {
                return createForSemanticObject(existingView, false);
            }
        }
            

        return null;
    }


    // Executes the right create methode for corresponding to the parameter "model" Type
    private ObjectModel ExecuteCorrespondingObjectCreationMethode(ObjectView existingView, bool syncModelToView)
    {
        // For semantic objects
        if (existingView is SemanticObjectView)
            return ExecuteCorrespondingSemanticObjectCreationMethode((SemanticObjectView)existingView, syncModelToView);

        // For Non-semantic objects
        else if (existingView.GetType() == typeof(CameraView))
            return ObjectFactory.Instance.modelFactory.createForCamera((CameraView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(ComplexObjectView))
            return ObjectFactory.Instance.modelFactory.createForComplexObject((ComplexObjectView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(NavObjectView))
            return ObjectFactory.Instance.modelFactory.createForNavObject((NavObjectView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(ObjectView))
            return ObjectFactory.Instance.modelFactory.createForObject((ObjectView)existingView, syncModelToView);

        else
            return null;
    }

    // Executes the right create methode for corresponding to the parameter "model" Type
    private ObjectControlModel ExecuteCorrespondingObjectControlCreationMethode(ObjectControlView existingView, bool syncModelToView)
    {
        if (existingView.GetType() == typeof(ObjectControlView))
            return ObjectFactory.Instance.modelFactory.createForObjectControl((ObjectControlView)existingView, syncModelToView);
        else if (existingView.GetType() == typeof(CameraControlView))
            return ObjectFactory.Instance.modelFactory.createForCameraControl((CameraControlView)existingView, syncModelToView);

        else
            return null;
    }

    // Executes the right create methode for corresponding to the parameter "model" Type
    private SemanticObjectModel ExecuteCorrespondingSemanticObjectCreationMethode(SemanticObjectView existingView, bool syncModelToView)
    {
        // For semantic objects
        if (existingView.GetType() == typeof(PracticeModel))
            return ObjectFactory.Instance.modelFactory.createForPractice((PracticeView)existingView, syncModelToView);

        else if (existingView.GetType() == typeof(ContextModel))
            return ObjectFactory.Instance.modelFactory.createForContext((ContextView)existingView, syncModelToView);

        else if (existingView.GetType() == typeof(IntentionalElementModel))
            return ObjectFactory.Instance.modelFactory.createForIntentionalElement((IntentionalElementView)existingView, syncModelToView);

        else if (existingView.GetType() == typeof(ActorModel))
            return ObjectFactory.Instance.modelFactory.createForActor((ActorView)existingView, syncModelToView);

        else if (existingView.GetType() == typeof(ActivityModel))
            return ObjectFactory.Instance.modelFactory.createForActivity((ActivityView)existingView, syncModelToView);

        else if (existingView.GetType() == typeof(SMWObjectModel))
            return ObjectFactory.Instance.modelFactory.createForSMWObject((SMWObjectView)existingView, syncModelToView);

        else if (existingView.GetType() == typeof(SemanticObjectView))
            return ObjectFactory.Instance.modelFactory.createForSemanticObject((SemanticObjectView)existingView, syncModelToView);

        else
            return null;
    }


    // Executes the right create methode for corresponding to the parameter "view" Type
    private SemanticObjectModel ExecuteCorrespondingSemanticObjectCreationMethode(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject)
    {
        // For semantic objects
        if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == typeof(PracticeModel))
            return ObjectFactory.Instance.modelFactory.createForPractice(entityID, semanticNode, category, navObject);

        else if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == typeof(ContextModel))
            return ObjectFactory.Instance.modelFactory.createForContext(entityID, semanticNode, category, navObject);

        else if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == typeof(IntentionalElementModel))
            return ObjectFactory.Instance.modelFactory.createForIntentionalElement(entityID, semanticNode, category, navObject);

        else if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == typeof(ActorModel))
            return ObjectFactory.Instance.modelFactory.createForActor(entityID, semanticNode, category, navObject);

        else if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == typeof(ActivityModel))
            return ObjectFactory.Instance.modelFactory.createForActivity(entityID, semanticNode, category, navObject);

        else if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == typeof(SMWObjectModel))
            return ObjectFactory.Instance.modelFactory.createForSMWObject(entityID, semanticNode, category, navObject);

        else if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == null)
            return ObjectFactory.Instance.modelFactory.createForSemanticObject(entityID, semanticNode, category, navObject);

        else
            return null;
    }


    private void AddObjectToIndex(ObjectTypeEnum objectType, ObjectModel model)
    {
        // Add controller to objectfactory index
        ObjectFactory.Instance.AddToIndex(model);

        //// Updates the GameManger index
        //if (GameManager.instance.modelsByEntityID.ContainsKey(objectType) != true)
        //    GameManager.instance.modelsByEntityID.Add(objectType, new Dictionary<string, ObjectModel>());

        //if (GameManager.instance.modelsByEntityID[objectType].ContainsKey(entityID) == true)
        //    GameManager.instance.modelsByEntityID[objectType][entityID] = model;
        //else GameManager.instance.modelsByEntityID[objectType].Add(entityID, model);

    }

    private void AddObjectControlToIndex(ControlTypeEnum controlType, ObjectControlModel model)
    {
        // Add controller to objectfactory index
        ObjectFactory.Instance.AddToIndex(model);

        //// Updates the GameManger index
        //if (GameManager.instance.controlModelsByEntityID.ContainsKey(controlType) != true)
        //    GameManager.instance.controlModelsByEntityID.Add(controlType, new Dictionary<string, ObjectControlModel>());

        //if (GameManager.instance.controlModelsByEntityID[controlType].ContainsKey(entityID) == true)
        //    GameManager.instance.controlModelsByEntityID[controlType][entityID] = model;
        //else GameManager.instance.controlModelsByEntityID[controlType].Add(entityID, model);
    }

    private void AddSemanticObjectToIndex(ObjectTypeEnum objectType, SemanticObjectModel model)
    {
        // Add controller to objectfactory index
        ObjectFactory.Instance.AddToIndex(model);

        //// Updates the GameManger index
        //if (GameManager.instance.modelsBySementicURI.ContainsKey(objectType) != true)
        //    GameManager.instance.modelsBySementicURI.Add(objectType, new Dictionary<string, SemanticObjectModel>());

        //if (GameManager.instance.modelsBySementicURI[objectType].ContainsKey(objectURI) == true)
        //    GameManager.instance.modelsBySementicURI[objectType][objectURI] = model;
        //else GameManager.instance.modelsBySementicURI[objectType].Add(objectURI, model);
    }


    // Checks for a specific instance INode if the category = the parameter category, else it returns the correct one
    private SMWCategory CheckForRightCategory(INode categoryInstanceOfIntressed, SMWCategory currentKnownCategory)
    {
        //Debug.Log("Finding category for instance -> " + categoryInstanceOfIntressed.ToString());

        // Graph
        IGraph g = categoryInstanceOfIntressed.Graph;

        // Triples of the intressed Node
        List <Triple> triplesThatIndicateType = g.GetTriplesWithSubjectPredicate(categoryInstanceOfIntressed, g.CreateUriNode(UriFactory.Create(g.NamespaceMap.GetNamespaceUri("rdf") + "type"))).ToList();

        //Debug.Log("foundTriples -> " + triplesThatIndicateType.Count);

        // Find triple object node that is a category node
        INode potentialCategoryNode = null;
            
        if(triplesThatIndicateType != null)
            if(triplesThatIndicateType.Count >= 1)
                potentialCategoryNode = triplesThatIndicateType.FirstOrDefault(t => SMWParser.Instance.GetCategoryBySemanticNode(t.Object) != null).Object;

        //Debug.Log(potentialCtaegoryNode.ToString());

        // Check
        if (potentialCategoryNode != null)
            return SMWParser.Instance.GetCategoryBySemanticNode(potentialCategoryNode);

        return null;

        //SMWCategory smwCategory = currentKnownCategory;

        //Debug.Log(smwCategory.GetAllInstanceTriples().Count);
        //foreach (INode node in smwCategory.GetAllInstanceTriples().Keys)
        //{
        //    Debug.Log(node.ToString());
        //    Debug.Log(smwCategory.GetAllInstanceTriples()[node]);
        //}

        //if (smwCategory.GetAllInstanceTriples().ContainsKey(categoryInstanceOfIntressed) == false)
        //    smwCategory = SMWParser.Instance.GetAllCategorySMWCategory(categoryInstanceOfIntressed.Graph).Find(c => c.GetAllInstanceTriples().ContainsKey(categoryInstanceOfIntressed) == true);
        
        
        //smwCategory = SMWParser.Instance.GetAllCategorySMWCategory(categoryInstanceOfIntressed.Graph).Find(c => c.GetSpecificInstanceTriples(categoryInstanceOfIntressed) != null || c.GetSpecificInstanceTriples(categoryInstanceOfIntressed).Count >= 1);
        
    }

}
