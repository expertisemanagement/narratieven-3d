﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using VDS.RDF;

/// <summary>
/// This Singleton class handles knows the Factories for creating of all MVC-components of objects. This class also handles the indexing of the MVC-components.
/// </summary>
public sealed class ObjectFactory {

    // A instance of the ObjectFactory as part of the Singleton pattern
    private static ObjectFactory instance = null;

    // A Multitread singleton duplication prevention object. 
    // This object is locked the first time the Instance() is called, Instance() does nothing when the object is locked.
    private static readonly object padlock = new object();

    // last used number for id when it's already taken
    private int lastExtendNumber = 0;

    // Savedata locations
    private readonly string nonSemanticObjectSavedata = ProjectManager.instance.currentProjectSubFolderSavedata + "/" + "NonSemanticObjects.dat";
    private readonly string SemanticObjectSavedata = ProjectManager.instance.currentProjectSubFolderSavedata + "/" + "SemanticObjects.dat";

    // EntityID index. 
    // Keys are entityID and values are MVC components
    public Dictionary<string, BaseView> entityViews = new Dictionary<string, BaseView>();
    public Dictionary<string, BaseModel> entityModels = new Dictionary<string, BaseModel>();
    public Dictionary<string, BaseController> entityControllers = new Dictionary<string, BaseController>();

    // ObjectURI index. 
    // Keys are ObjectURI and values are semantic MVC components
    public Dictionary<string, SemanticObjectView> semanticViews = new Dictionary<string, SemanticObjectView>();
    public Dictionary<string, SemanticObjectModel> semanticModels = new Dictionary<string, SemanticObjectModel>();
    public Dictionary<string, SemanticObjectController> semanticControllers = new Dictionary<string, SemanticObjectController>();

    // Factories for each of the MVC-components of the objects
    public ModelFactory modelFactory = ModelFactory.Instance;
    public ViewFactory viewFactory = ViewFactory.Instance;
    public ControllerFactory controllerFactory = ControllerFactory.Instance;

    // Constructor
    private ObjectFactory()
    {
    }

    // Returns this object's instance variable value, also creates a new ObjectFactory instance if the padlock is not locked. (Mutlitread security)
    public static ObjectFactory Instance
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {

                    // Test
                    //Debug.Log("new factory");

                    instance = new ObjectFactory();
                }
                return instance;
            }
        }
    }


    // Deserialize non-semanticobjects from a file
    public void LoadNonSemanticObjectsFromSaveFile()
    {
        // Set current savefile Location
        string fileLocation = nonSemanticObjectSavedata;

        // Check if file exists, else create it
        if (File.Exists(fileLocation) == false)
            File.Create(fileLocation);

        // Filestream to serialise to
        FileStream fs = new FileStream(fileLocation, FileMode.Open);

        // Serialize objects in list and convert it to a dictionary
        Dictionary<string, BaseModel> deserializedDictionary = new Dictionary<string, BaseModel>();

        foreach (BaseModel model in (List<BaseModel>)new BinaryFormatter().Deserialize(fs))
        {
            deserializedDictionary.Add(model.EntityID, model);
        }

        // Set this dictionary as
        entityModels = deserializedDictionary;
    }

    // Deserialize semanticobjects from a file
    public void LoadSemanticObjectFromSaveFile()
    {
        // Set current savefile Location
        string fileLocation = SemanticObjectSavedata;

        // Check if file exists, else create it
        if (File.Exists(fileLocation) == false)
            File.Create(fileLocation);

        // Filestream to serialise to
        FileStream fs = new FileStream(fileLocation, FileMode.Open);

        // Serialize objects in list and convert it to a dictionary
        Dictionary<string, SemanticObjectModel> deserializedDictionary = new Dictionary<string, SemanticObjectModel>();

        foreach (SemanticObjectModel model in (List<SemanticObjectModel>)new BinaryFormatter().Deserialize(fs))
        {
            deserializedDictionary.Add(model.EntityID, model);
        }

        // Set this dictionary as
        semanticModels = deserializedDictionary;
    }


    // Serialize non-semantic objects to file 
    public void AddNonSemanticObjectsToSaveFile(List<BaseModel> baseModels, FileMode fileMode)
    {
        // Set current savefile Location
        string fileLocation = nonSemanticObjectSavedata;

        // Check if file exists, else create it
        if (File.Exists(fileLocation) == false)
            File.Create(fileLocation);

        // Filestream to serialise to
        FileStream fs = new FileStream(fileLocation, fileMode);

        // Serialize
        new BinaryFormatter().Serialize(fs, baseModels);
    }

    // Serialize non-semantic objects to file 
    public void SaveAllNonSemanticObjectsToSaveFile()
    {
        AddNonSemanticObjectsToSaveFile(entityModels.Values.ToList(), FileMode.Create);
    }

    // Serialize semanticobjects to file 
    public void AddSemanticObjectsToSaveFile(List<SemanticObjectModel> semanticModels, FileMode fileMode)
    {
        // Set current savefile Location
        string fileLocation = SemanticObjectSavedata;

        // Check if file exists, else create it
        if (File.Exists(fileLocation) == false)
            File.Create(fileLocation);

        // Filestream to serialise to
        FileStream fs = new FileStream(fileLocation, fileMode);

        // Serialize
        new BinaryFormatter().Serialize(fs, semanticModels);
    }

    // Serialize semanticobjects to file 
    public void SaveAllSemanticObjectsToSaveFile()
    {
        AddSemanticObjectsToSaveFile(semanticModels.Values.ToList(), FileMode.Create);
    }


    // Check if entity exists, if so then return true  
    public bool CheckForExistingEntityID(string entityID)
    {
        bool existence = false;

        foreach (string id in entityViews.Keys)
            if (id == entityID) existence = true;

        foreach (string id in entityViews.Keys)
            if (id == entityID) existence = true;

        foreach (string id in entityViews.Keys)
            if (id == entityID) existence = true;

        return existence;
    }

    // Check if semanticobject exists, if so then return true 
    public bool CheckForExistingObjectURI(string objectURI)
    {
        bool existence = false;

        foreach (string uri in semanticModels.Keys)
            if(uri == objectURI) existence = true;

        return existence;
    }


    // Returns all indexed views
    public List<BaseView> GetAllViews()
    {
        List<BaseView> allFoundViews = new List<BaseView>();
        allFoundViews.AddRange(GetAllNonSemanticViews());
        allFoundViews.AddRange(GetAllSemanticViews().Cast<BaseView>().ToList());
        return allFoundViews;
    }

    // Returns all indexed views
    public List<BaseModel> GetAllModels()
    {
        List<BaseModel> allFoundModels = new List<BaseModel>();
        allFoundModels.AddRange(GetAllNonSemanticModels());
        allFoundModels.AddRange(GetAllSemanticModels().Cast<BaseModel>().ToList());
        return allFoundModels;
    }

    // Returns all indexed views
    public List<BaseController> GetAllControllers()
    {
        List<BaseController> allFoundControllers = new List<BaseController>();
        allFoundControllers.AddRange(GetAllNonSemanticControllers());
        allFoundControllers.AddRange(GetAllSemanticControllers().Cast<BaseController>().ToList());
        return allFoundControllers;
    }


    // Returns all indexed non-semantic views
    public List<BaseView> GetAllNonSemanticViews()
    {
        return entityViews.Values.ToList();
    }

    // Returns all indexed non-semantic models
    public List<BaseModel> GetAllNonSemanticModels()
    {
        return entityModels.Values.ToList();
    }

    // Returns all indexed non-semantic controller
    public List<BaseController> GetAllNonSemanticControllers()
    {
        return entityControllers.Values.ToList();
    }


    // Returns the Non-SemanticObjectView by it's entityID
    public BaseView GetObjectViewByEnityID(string entityID)
    {
        BaseView foundView = null;

        if (entityViews.ContainsKey(entityID) == true)
            foundView = entityViews[entityID];

        return foundView;
    }

    // Returns the Non-SemanticObjectModel by it's entityID
    public BaseModel GetObjectModelByEntityID(string entityID)
    {
        BaseModel foundModel = null;

        if (entityModels.ContainsKey(entityID) == true)
            foundModel = entityModels[entityID];

        return foundModel;
    }

    // Returns the Non-SemanticObjectController by it's entityID
    public BaseController GetObjectControllerByEntityID(string entityID)
    {
        BaseController foundController = null;

        if (entityControllers.ContainsKey(entityID) == true)
            foundController = entityControllers[entityID];

        return foundController;
    }


    // Returns all indexed semantic views
    public List<SemanticObjectView> GetAllSemanticViews()
    {
        return semanticViews.Values.ToList();
    }

    // Returns all indexed semantic models
    public List<SemanticObjectModel> GetAllSemanticModels()
    {
        return semanticModels.Values.ToList();
    }

    // Returns all indexed semantic models
    public List<SemanticObjectController> GetAllSemanticControllers()
    {
        return semanticControllers.Values.ToList();
    }

    // Returns the SemanticObjectView by it's objectURI
    public SemanticObjectView GetSemanticObjectViewByURI(string objectURI)
    {
        SemanticObjectView foundView = null;

        if (semanticViews.ContainsKey(objectURI) == true)
            foundView = semanticViews[objectURI];

        return foundView;
    }

    // Returns the SemanticObjectModel by it's objectURI
    public SemanticObjectModel GetSemanticObjectModelByURI(string objectURI)
    {
        SemanticObjectModel foundModel = null;

        if (semanticModels.ContainsKey(objectURI) == true)
            foundModel = semanticModels[objectURI];

        return foundModel;
    }

    // Returns the SemanticObjectController by it's objectURI
    public SemanticObjectController GetSemanticObjectControllerByURI(string objectURI)
    {
        SemanticObjectController foundController = null;

        if (semanticControllers.ContainsKey(objectURI) == true)
            foundController = semanticControllers[objectURI];

        return foundController;
    }


    // Returns the SemanticObjectView by it's semanticNode
    public SemanticObjectView GetSemanticObjectViewByNode(INode semanticNode)
    {
        if (semanticNode == null)
            return null;

        return GetSemanticObjectViewByURI(semanticNode.ToString());
    }

    // Returns the SemanticObjectModel by it's semanticNode
    public SemanticObjectModel GetSemanticObjectModelByNode(INode semanticNode)
    {
        if (semanticNode == null)
            return null;

        return GetSemanticObjectModelByURI(semanticNode.ToString());
    }

    // Returns the SemanticObjectController by it's semanticNode
    public SemanticObjectController GetSemanticObjectControllerByNode(INode semanticNode)
    {
        if (semanticNode == null)
            return null;

        return GetSemanticObjectControllerByURI(semanticNode.ToString());
    }

    public void DeleteView(ObjectView view)
    {
        if (view is SemanticObjectView)
            semanticViews.Remove(((SemanticObjectView)view).SemanticNode.ToString());

        if(view is ObjectView)
            semanticViews.Remove(view.EntityID);
    }

    public void DeleteController(ObjectController controller)
    {
        if (controller is SemanticObjectController)
            semanticViews.Remove(((SemanticObjectController)controller).FindSemanticNode().ToString());

        if (controller is ObjectController)
            semanticViews.Remove(controller.EntityID);
    }

    public void DeleteModel(ObjectModel model)
    {
        if (model is SemanticObjectModel)
            semanticViews.Remove(((SemanticObjectModel)model).SemanticNode.ToString());

        if (model is ObjectModel)
            semanticViews.Remove(model.EntityID);
    }


    // Adds a view to the index (true = succes, false = failed)
    public bool AddToIndex(BaseView view)
    {
        bool isSucces = false;

        if (view == null)
            return isSucces;

        if (view is SemanticObjectView)
        {
            if (((SemanticObjectView)view).ObjectURI != null && ((SemanticObjectView)view).ObjectURI != "")
            {
                semanticViews.Add(((SemanticObjectView)view).ObjectURI, ((SemanticObjectView)view));
                isSucces = true;
                return isSucces;
            }
        }
        else if (view is ObjectView)
        {
            if (((ObjectView)view).EntityID != null && ((ObjectView)view).EntityID != "")
            {
                entityViews.Add(((ObjectView)view).EntityID, ((ObjectView)view));
                isSucces = true;
                return isSucces;
            }
        }
        else if (view is ObjectControlView)
        {
            if (((ObjectControlView)view).EntityID != null && ((ObjectControlView)view).EntityID != "")
            {
                entityViews.Add(((ObjectControlView)view).EntityID, ((ObjectControlView)view));
                isSucces = true;
                return isSucces;
            }
        }
        else
        {
            isSucces = false;
        }

        return isSucces;

    }

    // Adds a model to the index (true = succes, false = failed)
    public bool AddToIndex(BaseModel model)
    {
        bool isSucces = false;

        if (model == null)
            return isSucces;

        if (model is SemanticObjectModel)
        {
            if (((SemanticObjectModel)model).ObjectURI != null && ((SemanticObjectModel)model).ObjectURI != "")
            {
                semanticModels.Add(((SemanticObjectModel)model).ObjectURI, ((SemanticObjectModel)model));
                isSucces = true;
                return isSucces;
            }
        }
        else if (model is ObjectModel)
        {
            if (((ObjectModel)model).EntityID != null && ((ObjectModel)model).EntityID != "")
            {
                entityModels.Add(((ObjectModel)model).EntityID, ((ObjectModel)model));
                isSucces = true;
                return isSucces;
            }
        }
        else if (model is ObjectControlModel)
        {
            if (((ObjectControlModel)model).EntityID != null && ((ObjectControlModel)model).EntityID != "")
            {
                entityModels.Add(((ObjectControlModel)model).EntityID, ((ObjectControlModel)model));
                isSucces = true;
                return isSucces;
            }
        }
        else
        {
            isSucces = false;
        }

        return isSucces;

    }

    // Adds a controller to the index (true = succes, false = failed)
    public bool AddToIndex(BaseController controller)
    {
        bool isSucces = false;

        if (controller == null)
            return isSucces;

        if (controller is SemanticObjectController)
        {
            if (((SemanticObjectController)controller).FindObjectURI() != null && ((SemanticObjectController)controller).FindObjectURI() != "")
            {
                semanticControllers.Add(((SemanticObjectController)controller).FindObjectURI(), ((SemanticObjectController)controller));
                isSucces = true;
                return isSucces;
            }
        }
        else if (controller is ObjectController)
        {
            if (((ObjectController)controller).FindEntityID() != null && ((ObjectController)controller).FindEntityID() != "")
            {
                entityControllers.Add(((ObjectController)controller).FindEntityID(), ((ObjectController)controller));
                isSucces = true;
                return isSucces;
            }
        }
        else if (controller is ObjectControlController)
        {
            if (((ObjectControlController)controller).FindEntityID() != null && ((ObjectControlController)controller).FindEntityID() != "")
            {
                entityControllers.Add(((ObjectControlController)controller).FindEntityID(), ((ObjectControlController)controller));
                isSucces = true;
                return isSucces;
            }
        }
        else
        {
            isSucces = false;
        }

        Debug.Log("succes");

        return isSucces;

    }


    // Removes a view from the index (true = succes, false = failed)
    public bool RemoveFromIndex(BaseView view)
    {
        bool isSucces = false;

        if (view == null)
            return isSucces;

        if (view is SemanticObjectView)
        {
            if (((SemanticObjectView)view).ObjectURI != null || ((SemanticObjectView)view).ObjectURI != "")
            {
                isSucces = semanticViews.Remove(((SemanticObjectView)view).ObjectURI);
                return isSucces;
            }
        }
        else if (view is ObjectView)
        {
            if (((ObjectView)view).EntityID != null || ((ObjectView)view).EntityID != "")
            {
                isSucces = semanticViews.Remove(((ObjectView)view).EntityID);
                return isSucces;
            }
        }
        else if (view is ObjectControlView)
        {
            if (((ObjectControlView)view).EntityID != null || ((ObjectControlView)view).EntityID != "")
            {
                isSucces = semanticViews.Remove(((ObjectControlView)view).EntityID);
                return isSucces;
            }
        }
        else
        {
            isSucces = false;
        }

        return isSucces;

    }

    // Removes a controller from the index (true = succes, false = failed)
    public bool RemoveFromIndex(BaseModel model)
    {
        bool isSucces = false;

        if (model == null)
            return isSucces;

        if (model is SemanticObjectModel)
        {
            if (((SemanticObjectModel)model).ObjectURI != null || ((SemanticObjectModel)model).ObjectURI != "")
            {
                isSucces = semanticModels.Remove(((SemanticObjectModel)model).ObjectURI);
                return isSucces;
            }
        }
        else if (model is ObjectModel)
        {
            if (((ObjectModel)model).EntityID != null || ((ObjectModel)model).EntityID != "")
            {
                isSucces = entityModels.Remove(((ObjectModel)model).EntityID);
                return isSucces;
            }
        }
        else if (model is ObjectControlModel)
        {
            if (((ObjectControlModel)model).EntityID != null || ((ObjectControlModel)model).EntityID != "")
            {
                isSucces = entityModels.Remove(((ObjectControlModel)model).EntityID);
                return isSucces;
            }
        }
        else
        {
            isSucces = false;
        }

        return isSucces;

    }

    // Removes a controller to the index (true = succes, false = failed)
    public bool RemoveFromIndex(BaseController controller)
    {
        bool isSucces = false;

        if (controller == null)
            return isSucces;

        if (controller is SemanticObjectController)
        {
            if (((SemanticObjectController)controller).FindObjectURI() != null || ((SemanticObjectController)controller).FindObjectURI() != "")
            {
                isSucces = semanticControllers.Remove(((SemanticObjectController)controller).FindObjectURI());
                return isSucces;
            }
        }
        else if (controller is ObjectController)
        {
            if (((ObjectController)controller).FindEntityID() != null || ((ObjectController)controller).FindEntityID() != "")
            {
                isSucces = entityControllers.Remove(((ObjectController)controller).FindEntityID());
                return isSucces;
            }
        }
        else if (controller is ObjectControlController)
        {
            if (((ObjectControlController)controller).FindEntityID() != null || ((ObjectControlController)controller).FindEntityID() != "")
            {
                isSucces = entityControllers.Remove(((ObjectControlController)controller).FindEntityID());
            }
        }
        else
        {
            isSucces = false;
        }

        return isSucces;

    }

    // Generate a new id using the total times ticked on the clock, if it already exists add some more end characters
    public string GenerateEntityID()
    {
        // thanks to https://stackoverflow.com/questions/15009423/way-to-generate-a-unique-number-that-does-not-repeat-in-a-reasonable-time

        lastExtendNumber = 0;

        long ticks = DateTime.Now.Ticks;
        byte[] bytes = BitConverter.GetBytes(ticks);
        string id = Convert.ToBase64String(bytes)
                                .Replace('+', '_')
                                .Replace('/', '-')
                                .TrimEnd('=');

        while (CheckForExistingEntityID(id + lastExtendNumber) == true)
            lastExtendNumber++;

        return (id + lastExtendNumber);
    }
}
