﻿public interface IViewFactory {

    // creates a baseView with a generated entitiyID
    IBaseView createBaseView();

    // creates a baseView with given entityID
    IBaseView createBaseView(string entityID);

    // creates a baseView with given controller
    IBaseView createBaseView(IBaseController controller);


    // creates a objectView with a generated entitiyID
    IObjectView createObjectView();

    // creates a objectView with given entityID
    IObjectView createObjectView(string entityID);

    // creates a objectView with given controller
    IObjectView createObjectView(IObjectController controller);


    // creates a objectView with a generated entitiyID
    IObjectControlView createObjectControlView();

    // creates a objectView with given entityID
    IObjectControlView createObjectControlView(string entityID);

    // creates a objectView with given controller
    IObjectControlView createObjectControlView(IObjectControlController controller);


    // creates a cameraView with a generated entitiyID
    ICameraControlView createCameraControlView();

    // creates a cameraView with given entityID
    ICameraControlView createCameraControlView(string entityID);

    // creates a cameraView with given controller
    ICameraControlView createCameraControlView(ICameraController controller);


    // creates a objectControl with a generated entitiyID
    IObjectView createComplexObjectView();

    // creates a objectControl with given entityID
    IObjectView createComplexObjectView(string entityID);

    // creates a objectControl with given controller
    IObjectView createComplexObjectView(IComplexObjectController controller);



}
