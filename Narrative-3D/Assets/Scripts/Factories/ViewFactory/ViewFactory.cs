﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object.Type;
using UnityEngine;
using System.Linq;
using ObjectControl.Type;
using VDS.RDF;

public class ViewFactory {

    // A instance of the ObjectFactory as part of the Singleton pattern
    private static ViewFactory instance = null;

    // A Multitread singleton duplication prevention object. 
    // This object is locked the first time the Instance() is called, Instance() does nothing when the object is locked.
    private static readonly object padlock = new object();

    // Constructor
    private ViewFactory()
    {
    }

    // Returns this object's instance variable value, also creates a new ObjectFactory instance if the padlock is not locked. (Mutlitread security)
    public static ViewFactory Instance
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new ViewFactory();
                }
                return instance;
            }
        }
    }

    // Create Object view

    public ObjectView createForObject(ObjectModel model, Transform parentTransform, bool syncViewToModel)
    {
        // controller or view are null or if the controller already has a model, then stop this methode
        if (model == null)
            return null;

        // Check if this is the right create methode, else execute the right one
        if (model.GetType() != typeof(ObjectModel))
        {
            ObjectView viewCreatedFromExistingView = ExecuteCorrespondingCreateMethode(model, parentTransform, syncViewToModel);
            return viewCreatedFromExistingView;
        }

        // If no controller exists, then make one
        if (model.FindCreateSetController() == null)
            return null;

        // If there is already a model return that one
        if (model.Controller.FindSetModel() != null)
            return (ObjectView)model.Controller.View;
        else
        {
            ObjectView newView = (ObjectView) NewUninitialisedObject(parentTransform, true).Initialise(model, syncViewToModel);
            AddObjectToIndex(newView.ObjectType, newView);

            return newView;
        }
            
    }

    public ObjectView createForObject(string entityID, Transform parentTransform)
    {
        // Check if object exists
        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
            entityID = ObjectFactory.Instance.GenerateEntityID();

        ObjectView newView = (ObjectView)NewUninitialisedObject(parentTransform, true).Initialise(entityID);
        AddObjectToIndex(newView.ObjectType, newView);

        return newView;
    }

    public ObjectView createForObject(Transform parentTransform)
    {
        return createForObject(ObjectFactory.Instance.GenerateEntityID(), parentTransform);
    }

        // Create ComplexObject view

        public ObjectView createForComplexObject(ComplexObjectModel model, Transform parentTransform, bool syncViewToModel)
        {
            // controller or view are null or if the controller already has a model, then stop this methode
            if (model == null)
                return null;

            // Check if this is the right create methode, else execute the right one
            if (model.GetType() != typeof(ComplexObjectModel))
            {
                ObjectView viewCreatedFromExistingView = ExecuteCorrespondingCreateMethode(model, parentTransform, syncViewToModel);
                return viewCreatedFromExistingView;
            }

            // If no controller exists, then make one
            if (model.FindCreateSetController() == null)
                return null;

            // If there is already a model return that one
            if (model.Controller.FindSetModel() != null)
                return (ObjectView)model.Controller.View;
            else
            {
                ComplexObjectView newView = (ComplexObjectView)NewUninitialisedComplexObject(parentTransform, true).Initialise(model, syncViewToModel);
                AddObjectToIndex(newView.ObjectType, newView); 

                return newView;
            }

        }

        public ObjectView createForComplexObject(string entityID, Transform parentTransform)
        {
            Debug.Log("Create Complex " + entityID + parentTransform);

            // Check if object exists
            if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                entityID = ObjectFactory.Instance.GenerateEntityID();

             Debug.Log("Create Complex  entityID " + entityID );

            ComplexObjectView newView = (ComplexObjectView) NewUninitialisedComplexObject(parentTransform, true).Initialise(entityID);

            Debug.Log("AddObjectToIndex " + newView.EntityID);
            AddObjectToIndex(newView.ObjectType, newView);

            return newView;
        }

        public ObjectView createForComplexObject(Transform parentTransform)
        {
            return createForComplexObject(ObjectFactory.Instance.GenerateEntityID(), parentTransform);
        }


        // Create ComplexObject view

        public ObjectView createForCamera(CameraModel model, Transform parentTransform, bool syncViewToModel)
        {
            // controller or view are null or if the controller already has a model, then stop this methode
            if (model == null)
                return null;

            // Check if this is the right create methode, else execute the right one
            if (model.GetType() != typeof(CameraModel))
            {
                ObjectView viewCreatedFromExistingView = ExecuteCorrespondingCreateMethode(model, parentTransform, syncViewToModel);
                return viewCreatedFromExistingView;
            }

            // If no controller exists, then make one
            if (model.FindCreateSetController() == null)
                return null;

            // If there is already a model return that one
            if (model.Controller.FindSetModel() != null)
                return (ObjectView) model.Controller.View;
            else
            {
                CameraView newView = (CameraView) NewUninitialisedCamera(parentTransform, true).Initialise(model, syncViewToModel);
                AddObjectToIndex(newView.ObjectType, newView); 

                return newView;
            }

        }

        public ObjectView createForCamera(string entityID, Transform parentTransform)
        {
            // Check if object exists
            if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                entityID = ObjectFactory.Instance.GenerateEntityID();

            CameraView newView = (CameraView) NewUninitialisedCamera(parentTransform, true).Initialise(entityID);
            AddObjectToIndex(newView.ObjectType, newView);

            return newView;
        }

        public ObjectView createForCamera(Transform parentTransform)
        {
            return createForComplexObject(ObjectFactory.Instance.GenerateEntityID(), parentTransform);
        }

    // Create NavObject view

    public ObjectView createForNavObject(NavObjectModel model, Transform parentTransform, bool syncViewToModel)
    {
        // controller or view are null or if the controller already has a model, then stop this methode
        if (model == null)
            return null;

        // Check if this is the right create methode, else execute the right one
        if (model.GetType() != typeof(NavObjectModel))
        {
            ObjectView viewCreatedFromExistingView = ExecuteCorrespondingCreateMethode(model, parentTransform, syncViewToModel);
            return viewCreatedFromExistingView;
        }

        // If no controller exists, then make one
        if (model.FindCreateSetController() == null)
            return null;

        // If there is already a model return that one
        if (model.Controller.FindSetModel() != null)
            return (ObjectView)model.Controller.View;
        else
        {
            NavObjectView newView = (NavObjectView) NewUninitialisedNavObject(parentTransform, true).Initialise(model, syncViewToModel);
            AddObjectToIndex(newView.ObjectType, newView);

            return newView;
        }

    }

    public ObjectView createForNavObject(string entityID, SemanticObjectModel semanticObjectModel, Transform parentTransform, bool isChildNode)
    {
        // Check if object exists
        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
            entityID = ObjectFactory.Instance.GenerateEntityID();

        NavObjectView newView = null;
        if(isChildNode == false)
            newView = (NavObjectView) NewUninitialisedNavObject(parentTransform, true).Initialise(entityID, semanticObjectModel);
        if (isChildNode == true)
            newView = (NavObjectView) NewUninitialisedNavObject(parentTransform, true).Initialise(entityID, semanticObjectModel);

        AddObjectToIndex(newView.ObjectType, newView);

        return newView;
    }

    public ObjectView createForNavObject(SemanticObjectModel semanticObjectModel, Transform parentTransform, bool isChildNode)
    {
        return createForNavObject(ObjectFactory.Instance.GenerateEntityID(), semanticObjectModel, parentTransform, isChildNode);
    }

    //--------------------------------// Creation methodes for Semantic objects MVC components //--------------------------------//


            // creates a SemanticObject

            public SemanticObjectView createForSemanticObject(SemanticObjectModel model, Transform parentTransform, bool syncModelToView)
            {
                // controller or view are null or if the controller already has a model, then stop this methode
                if (model == null)
                    return null;

                // Check if this is the right create methode, else execute the right one
                if (model.GetType() != typeof(SemanticObjectView))
                {
                    SemanticObjectView viewCreatedFromExistingModel = ExecuteCorrespondingSemanticObjectCreationMethode(model, parentTransform, syncModelToView);
                    return viewCreatedFromExistingModel;
                }

                // If no controller exists, then make one
                if (model.FindCreateSetController() == null)
                    return null;

                // If there is already a model return that one
                if (model.Controller.FindSetModel() != null)
                    return (SemanticObjectView) model.Controller.View;
                else
                {
                    SemanticObjectView newView = (SemanticObjectView) NewUninitialisedSemanticObject(parentTransform, true).Initialise(model.EntityID, model.SemanticNode, model.NavObject);
                    AddSemanticObjectToIndex(newView.ObjectType, newView);
                    return newView;
                }
            }

            public SemanticObjectView createForSemanticObject(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
            {
                // Find existing MVC-components and try to build  a model using those components, if successed return that result
                ObjectView madeViewUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                if (madeViewUsingExistingComponents != null)
                    if(madeViewUsingExistingComponents is SemanticObjectView)
                    {
                        if (navObject != null)
                            ((SemanticObjectView)madeViewUsingExistingComponents).NavObject = navObject;
                            return (SemanticObjectView) madeViewUsingExistingComponents;
                    }

                // If there is a category, call the other methode
                if (CheckForRightCategory(semanticNode, category) != null)
                    return createForSMWObject(entityID, semanticNode, category, navObject, parentTransform);

                // Check if entityID is new, else generate new ID
                //string id = entityID;
                if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                entityID = ObjectFactory.Instance.GenerateEntityID();
        
                // Set ObjectType
                ObjectTypeEnum type = ObjectTypeEnum.SEMANTICOBJECT;

                // semanticObjectModel to return
                SemanticObjectView newSemanticObjectView = (SemanticObjectView) NewUninitialisedSemanticObject(parentTransform, true).Initialise(entityID, semanticNode, navObject);
                
                // Add to index
                AddSemanticObjectToIndex(type, newSemanticObjectView);

                return newSemanticObjectView;
            }

            public SemanticObjectView createForSemanticObject(INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
            {
                return createForSemanticObject(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject, parentTransform);
            }
    
                // creates a SMWObject

                public SemanticObjectView createForSMWObject(SMWObjectModel model, Transform parentTransform, bool syncModelToView)
                {
                    // controller or view are null or if the controller already has a model, then stop this methode
                    if (model == null)
                        return null;

                    // Check if this is the right create methode, else execute the right one
                    if (model.GetType() != typeof(SMWObjectView))
                    {
                        SemanticObjectView viewCreatedFromExistingModel = ExecuteCorrespondingSemanticObjectCreationMethode(model, parentTransform, syncModelToView);
                        return viewCreatedFromExistingModel;
                    }

                    // If no controller exists, then make one
                    if (model.FindCreateSetController() == null)
                        return null;

                    // If there is already a model return that one
                    if (model.Controller.FindSetModel() != null)
                        return (SemanticObjectView) model.Controller.View;
                    else
                    {
                        SemanticObjectView newView = (SMWObjectView) NewUninitialisedSMWObject(parentTransform, true).Initialise(model.EntityID, model.SemanticNode, model.NavObject);
                        AddSemanticObjectToIndex(newView.ObjectType, newView);
                        return newView;
                    }
                }

                public SemanticObjectView createForSMWObject(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
                {
                    // Find existing MVC-components and try to build  a model using those components, if successed return that result
                    ObjectView madeViewUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                    if (madeViewUsingExistingComponents != null)
                        if(madeViewUsingExistingComponents is SemanticObjectView)
                        {
                            if (navObject != null)
                                ((SemanticObjectView)madeViewUsingExistingComponents).NavObject = navObject;
                                return (SemanticObjectView) madeViewUsingExistingComponents;
                        }

                    // Check if current Category is correct, else search for the right one. If there still is no right category, then stop this methode
                    SMWCategory correspondingCategory = CheckForRightCategory(semanticNode, category);
                    if (correspondingCategory == null)
                        return null;

                                        // Check if this is the right create methode, else execute the right one
                    if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) != typeof(SMWObjectModel))
                    {
                        SemanticObjectView view = ExecuteCorrespondingSemanticObjectCreationMethode(entityID, semanticNode, category, navObject, parentTransform);
                        return view;
                    }

                    // Check if entityID is new, else generate new ID
                    //string id = entityID;
                    if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                    entityID = ObjectFactory.Instance.GenerateEntityID();
        
                    // Set ObjectType
                    ObjectTypeEnum type = ObjectTypeEnum.SMWOBJECT;

                    // semanticObjectModel to return
                    SemanticObjectView newSemanticObjectView = (SemanticObjectView) NewUninitialisedSemanticObject(parentTransform, true).Initialise(entityID, semanticNode, navObject);
                
                    // Add to index
                    AddSemanticObjectToIndex(type, newSemanticObjectView);

                    return newSemanticObjectView;
                }

                public SemanticObjectView createForSMWObject(INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
                {
                    return createForSMWObject(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject, parentTransform);
                }

                    // creates a Context

                    public SemanticObjectView createForContext(ContextModel model, Transform parentTransform, bool syncModelToView)
                    {
                        // controller or view are null or if the controller already has a model, then stop this methode
                        if (model == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (model.GetType() != typeof(ContextModel))
                        {
                            SemanticObjectView viewCreatedFromExistingModel = ExecuteCorrespondingSemanticObjectCreationMethode(model, parentTransform, syncModelToView);
                            return viewCreatedFromExistingModel;
                        }

                        // If no controller exists, then make one
                        if (model.FindCreateSetController() == null)
                            return null;

                        // If there is already a model return that one
                        if (model.Controller.FindSetModel() != null)
                            return (SemanticObjectView)model.Controller.View;
                        else
                        {
                            SemanticObjectView newView = null;
                            if (model.ContextType == SMW.EMM.Context.ContextTypeEnum.ROLE)
                                newView = (SMWObjectView) NewUninitialisedContextObject(parentTransform, true, true).Initialise(model.EntityID, model.SemanticNode, model.NavObject);
                            else
                                newView = (SMWObjectView)NewUninitialisedContextObject(parentTransform, false, true).Initialise(model.EntityID, model.SemanticNode, model.NavObject);

                            AddSemanticObjectToIndex(newView.ObjectType, newView);
                            return newView;
                        }
                    }

                    public SemanticObjectView createForContext(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
                    {
                        // Find existing MVC-components and try to build  a model using those components, if successed return that result
                        ObjectView madeViewUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                        if (madeViewUsingExistingComponents != null)
                            if (madeViewUsingExistingComponents is SemanticObjectView)
                            {
                                if (navObject != null)
                                    ((SemanticObjectView)madeViewUsingExistingComponents).NavObject = navObject;
                                return (SemanticObjectView)madeViewUsingExistingComponents;
                            }

                        // Check if current Category is correct, else search for the right one. If there still is no right category, then stop this methode
                        SMWCategory correspondingCategory = CheckForRightCategory(semanticNode, category);
                        if (correspondingCategory == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) != typeof(ContextModel))
                        {
                            SemanticObjectView view = ExecuteCorrespondingSemanticObjectCreationMethode(entityID, semanticNode, category, navObject, parentTransform);
                            return view;
                        }

                        // Check if entityID is new, else generate new ID
                        //string id = entityID;
                        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                        entityID = ObjectFactory.Instance.GenerateEntityID();

                        // Set ObjectType
                        ObjectTypeEnum type = ObjectTypeEnum.CONTEXT;
        
                        SemanticObjectView newSemanticObjectView = null;
                        if (SMWParser.Instance.CheckIfContextNodeIsRole(semanticNode) == true)
                            newSemanticObjectView = (SMWObjectView)NewUninitialisedContextObject(parentTransform, true, true).Initialise(entityID, semanticNode, navObject);
                        else
                            newSemanticObjectView = (SMWObjectView)NewUninitialisedContextObject(parentTransform, false, true).Initialise(entityID, semanticNode, navObject);

                        // Add to index
                        AddSemanticObjectToIndex(type, newSemanticObjectView);

                        return newSemanticObjectView;
                    }

                    public SemanticObjectView createForContext(INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
                    {
                        return createForContext(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject, parentTransform);
                    }

                    // creates a Intentional Element

                    public SemanticObjectView createForIntentionalElement(IntentionalElementModel model, Transform parentTransform, bool syncModelToView)
                    {
                        // controller or view are null or if the controller already has a model, then stop this methode
                        if (model == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (model.GetType() != typeof(IntentionalElementModel))
                        {
                            SemanticObjectView viewCreatedFromExistingModel = ExecuteCorrespondingSemanticObjectCreationMethode(model, parentTransform, syncModelToView);
                            return viewCreatedFromExistingModel;
                        }

                        // If no controller exists, then make one
                        if (model.FindCreateSetController() == null)
                            return null;

                        // If there is already a model return that one
                        if (model.Controller.FindSetModel() != null)
                            return (SemanticObjectView)model.Controller.View;
                        else
                        {
                            SemanticObjectView newView = (SMWObjectView) NewUninitialisedIntentionalElementObject(parentTransform, true).Initialise(model.EntityID, model.SemanticNode, model.NavObject);
                            AddSemanticObjectToIndex(newView.ObjectType, newView);
                            return newView;
                        }
                    }

                    public SemanticObjectView createForIntentionalElement(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
                    {
                        // Find existing MVC-components and try to build  a model using those components, if successed return that result
                        ObjectView madeViewUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                        if (madeViewUsingExistingComponents != null)
                            if (madeViewUsingExistingComponents is SemanticObjectView)
                            {
                                if (navObject != null)
                                    ((SemanticObjectView)madeViewUsingExistingComponents).NavObject = navObject;
                                return (SemanticObjectView)madeViewUsingExistingComponents;
                            }

                        // Check if current Category is correct, else search for the right one. If there still is no right category, then stop this methode
                        SMWCategory correspondingCategory = CheckForRightCategory(semanticNode, category);
                        if (correspondingCategory == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) != typeof(IntentionalElementModel))
                        {
                            SemanticObjectView view = ExecuteCorrespondingSemanticObjectCreationMethode(entityID, semanticNode, category, navObject, parentTransform);
                            return view;
                        }

                        // Check if entityID is new, else generate new ID
                        //string id = entityID;
                        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                        entityID = ObjectFactory.Instance.GenerateEntityID();

                        // Set ObjectType
                        ObjectTypeEnum type = ObjectTypeEnum.INTENTIONALELEMENT;

                        // semanticObjectModel to return
                        SemanticObjectView newSemanticObjectView = (SemanticObjectView)NewUninitialisedIntentionalElementObject(parentTransform, true).Initialise(entityID, semanticNode, navObject);

                        // Add to index
                        AddSemanticObjectToIndex(type, newSemanticObjectView);

                        return newSemanticObjectView;
                    }

                    public SemanticObjectView createForIntentionalElement(INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
                    {
                        return createForIntentionalElement(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject, parentTransform);
                    }

                        // creates a Activity

                        public SemanticObjectView createForActivity(ActivityModel model, Transform parentTransform, bool syncModelToView)
                        {
                            // controller or view are null or if the controller already has a model, then stop this methode
                            if (model == null)
                                return null;

                            // Check if this is the right create methode, else execute the right one
                            if (model.GetType() != typeof(ActivityModel))
                            {
                                SemanticObjectView viewCreatedFromExistingModel = ExecuteCorrespondingSemanticObjectCreationMethode(model, parentTransform, syncModelToView);
                                return viewCreatedFromExistingModel;
                            }

                            // If no controller exists, then make one
                            if (model.FindCreateSetController() == null)
                                return null;

                            // If there is already a model return that one
                            if (model.Controller.FindSetModel() != null)
                                return (SemanticObjectView)model.Controller.View;
                            else
                            {
                                SemanticObjectView newView = (SMWObjectView) NewUninitialisedActivityObject(parentTransform, true).Initialise(model.EntityID, model.SemanticNode, model.NavObject);
                                AddSemanticObjectToIndex(newView.ObjectType, newView);
                                return newView;
                            }
                        }

                        public SemanticObjectView createForActivity(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
                        {
                            // Find existing MVC-components and try to build  a model using those components, if successed return that result
                            ObjectView madeViewUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                            if (madeViewUsingExistingComponents != null)
                                if (madeViewUsingExistingComponents is SemanticObjectView)
                                {
                                    if (navObject != null)
                                        ((SemanticObjectView)madeViewUsingExistingComponents).NavObject = navObject;
                                    return (SemanticObjectView)madeViewUsingExistingComponents;
                                }

                            // Check if current Category is correct, else search for the right one. If there still is no right category, then stop this methode
                            SMWCategory correspondingCategory = CheckForRightCategory(semanticNode, category);
                            if (correspondingCategory == null)
                                return null;

                            // Check if this is the right create methode, else execute the right one
                            if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) != typeof(ActivityModel))
                            {
                                SemanticObjectView view = ExecuteCorrespondingSemanticObjectCreationMethode(entityID, semanticNode, category, navObject, parentTransform);
                                return view;
                            }

                            // Check if entityID is new, else generate new ID
                            //string id = entityID;
                            if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                            entityID = ObjectFactory.Instance.GenerateEntityID();

                            // Set ObjectType
                            ObjectTypeEnum type = ObjectTypeEnum.ACTIVITY;

                            // semanticObjectModel to return
                            SemanticObjectView newSemanticObjectView = (SemanticObjectView)NewUninitialisedActivityObject(parentTransform, true).Initialise(entityID, semanticNode, navObject);

                            // Add to index
                            AddSemanticObjectToIndex(type, newSemanticObjectView);

                            return newSemanticObjectView;
                        }

                        public SemanticObjectView createForActivity(INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
                        {
                            return createForActivity(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject, parentTransform);
                        }

                        // creates a Actor

                        public SemanticObjectView createForActor(ActorModel model, Transform parentTransform, bool syncModelToView)
                        {
                            // controller or view are null or if the controller already has a model, then stop this methode
                            if (model == null)
                                return null;

                            // Check if this is the right create methode, else execute the right one
                            if (model.GetType() != typeof(ActivityModel))
                            {
                                SemanticObjectView viewCreatedFromExistingModel = ExecuteCorrespondingSemanticObjectCreationMethode(model, parentTransform, syncModelToView);
                                return viewCreatedFromExistingModel;
                            }

                            // If no controller exists, then make one
                            if (model.FindCreateSetController() == null)
                                return null;

                            // If there is already a model return that one
                            if (model.Controller.FindSetModel() != null)
                                return (SemanticObjectView)model.Controller.View;
                            else
                            {
                                SemanticObjectView newView = (SMWObjectView) NewUninitialisedActorObject(parentTransform, true).Initialise(model.EntityID, model.SemanticNode, model.NavObject);
                                AddSemanticObjectToIndex(newView.ObjectType, newView);
                                return newView;
                            }
                        }

                        public SemanticObjectView createForActor(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
                        {
                            // Find existing MVC-components and try to build  a model using those components, if successed return that result
                            ObjectView madeViewUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                            if (madeViewUsingExistingComponents != null)
                                if (madeViewUsingExistingComponents is SemanticObjectView)
                                {
                                    if (navObject != null)
                                        ((SemanticObjectView)madeViewUsingExistingComponents).NavObject = navObject;
                                    return (SemanticObjectView)madeViewUsingExistingComponents;
                                }

                            // Check if current Category is correct, else search for the right one. If there still is no right category, then stop this methode
                            SMWCategory correspondingCategory = CheckForRightCategory(semanticNode, category);
                            if (correspondingCategory == null)
                                return null;

                            // Check if this is the right create methode, else execute the right one
                            if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) != typeof(ActorModel))
                            {
                                SemanticObjectView view = ExecuteCorrespondingSemanticObjectCreationMethode(entityID, semanticNode, category, navObject, parentTransform);
                                return view;
                            }

                            // Check if entityID is new, else generate new ID
                            //string id = entityID;
                            if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                            entityID = ObjectFactory.Instance.GenerateEntityID();

                            // Set ObjectType
                            ObjectTypeEnum type = ObjectTypeEnum.ACTOR;

                            // semanticObjectModel to return
                            SemanticObjectView newSemanticObjectView = (SemanticObjectView)NewUninitialisedActorObject(parentTransform, true).Initialise(entityID, semanticNode, navObject);

                            // Add to index
                            AddSemanticObjectToIndex(type, newSemanticObjectView);

                            return newSemanticObjectView;
                        }

                        public SemanticObjectView createForActor(INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
                        {
                            return createForActor(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject, parentTransform);
                        }
    
                    // creates a Practice

                    public SemanticObjectView createForPractice(PracticeModel model, Transform parentTransform, bool syncModelToView)
                    {
                        // controller or view are null or if the controller already has a model, then stop this methode
                        if (model == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (model.GetType() != typeof(ActivityModel))
                        {
                            SemanticObjectView viewCreatedFromExistingModel = ExecuteCorrespondingSemanticObjectCreationMethode(model, parentTransform, syncModelToView);
                            return viewCreatedFromExistingModel;
                        }

                        // If no controller exists, then make one
                        if (model.FindCreateSetController() == null)
                            return null;

                        // If there is already a model return that one
                        if (model.Controller.FindSetModel() != null)
                            return (SemanticObjectView)model.Controller.View;
                        else
                        {
                            SemanticObjectView newView = (SMWObjectView) NewUninitialisedPracticeObject(parentTransform, true).Initialise(model.EntityID, model.SemanticNode, model.NavObject);
                            AddSemanticObjectToIndex(newView.ObjectType, newView);
                            return newView;
                        }
                    }

                    public SemanticObjectView createForPractice(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
                    {
                        // Find existing MVC-components and try to build  a model using those components, if successed return that result
                        ObjectView madeViewUsingExistingComponents = CreateUsingFoundSemanticMVCComponents(semanticNode);
                        if (madeViewUsingExistingComponents != null)
                            if (madeViewUsingExistingComponents is SemanticObjectView)
                            {
                                if (navObject != null)
                                    ((SemanticObjectView)madeViewUsingExistingComponents).NavObject = navObject;
                                return (SemanticObjectView)madeViewUsingExistingComponents;
                            }

                        // Check if current Category is correct, else search for the right one. If there still is no right category, then stop this methode
                        SMWCategory correspondingCategory = CheckForRightCategory(semanticNode, category);
                        if (correspondingCategory == null)
                            return null;

                        // Check if this is the right create methode, else execute the right one
                        if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) != typeof(PracticeModel))
                        {
                            SemanticObjectView view = ExecuteCorrespondingSemanticObjectCreationMethode(entityID, semanticNode, category, navObject, parentTransform);
                            return view;
                        }

                        // Check if entityID is new, else generate new ID
                        //string id = entityID;
                        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
                        entityID = ObjectFactory.Instance.GenerateEntityID();

                        // Set ObjectType
                        ObjectTypeEnum type = ObjectTypeEnum.PRACTICE;

                        // semanticObjectModel to return
                        SemanticObjectView newSemanticObjectView = (SemanticObjectView)NewUninitialisedPracticeObject(parentTransform, true).Initialise(entityID, semanticNode, navObject);

                        // Add to index
                        AddSemanticObjectToIndex(type, newSemanticObjectView);

                        return newSemanticObjectView;
                    }

                    public SemanticObjectView createForPractice(INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
                    {
                        return createForPractice(ObjectFactory.Instance.GenerateEntityID(), semanticNode, category, navObject, parentTransform);
                    }

    //--------------------------------// Creation methodes for ObjectControl MVC components //--------------------------------//

    // Create a ObjectControl

    public ObjectControlView createForObjectControl(ComplexObjectModel complexObject)
    {
        if (((ComplexObjectView)complexObject.Controller.FindSetView()) == true)
            return createForObjectControl(((ComplexObjectView)complexObject.Controller.FindSetView()));
        else
            return null;
    }

    public ObjectControlView createForObjectControl(ComplexObjectView complexObject)
    {
        ObjectControlView objectView = complexObject.GetComponent<ObjectControlView>();

        // Find existing view
        if (objectView != null)
        {
            // Check if it's linked with the model
            if (complexObject.Controller.Model != null)
                if (((ComplexObjectModel)complexObject.Controller.Model).ControlSettings != null)
                    if (((ComplexObjectModel)complexObject.Controller.Model).ControlSettings.Controller.View != objectView)
                        ((ComplexObjectModel)complexObject.Controller.Model).ControlSettings.Controller.View = objectView;

            AddObjectControlToIndex(ControlTypeEnum.OBJECTCONTROL, objectView);

            return objectView;
        }
        else
        {
            // Check if it's linked with the model
            if (complexObject.Controller.Model != null)
                if (((ComplexObjectModel)complexObject.Controller.Model).ControlSettings != null)
                {
                    if (((ComplexObjectModel)complexObject.Controller.Model).ControlSettings.Controller.View != null)
                        objectView = createForObjectControl(((ComplexObjectModel)complexObject.Controller.Model).ControlSettings.EntityID, complexObject);
                }
                else
                {
                    objectView = createForObjectControl(ObjectFactory.Instance.GenerateEntityID(), complexObject);
                }

            return objectView;
        }
    }

    public ObjectControlView createForObjectControl(ObjectControlModel model, bool syncViewToModel)
    {
        // controller or model are null or if the controller already has a view, then stop this methode
        if (model == null)
            return null;

        // If no controller exists, then make one
        if (model.Controller == null)
            ControllerFactory.Instance.createForObjectControl(model, false);

        // View to return
        ObjectControlView view = null;

        // If there is already a view return that one
        if (model.Controller.FindSetView() != null)
            view = ((ObjectControlView)model.Controller.FindSetView());

        // Create new one if no other exists and when all requirements are fulfilled
        else
            view = createForObjectControl(model.controlWielder);

        // Return a view created for a specific model
        return view;
    }

    //public ObjectControlView createForObjectControl(ObjectControlController controller, bool syncViewToModel)
    //{
    //    // controller or model are null or if the controller already has a view, then stop this methode
    //    if (controller == null || controller.model == null || controller.view != null)
    //        return null;

    //    // Set entityID
    //    string id = controller.model.entityID;

    //    // if the view exists use that one instead
    //    ObjectControlView newObjectView = null;

    //    // 
    //    if (controller.view != null)
    //    {
    //        AddObjectControlToIndex(ControlTypeEnum.OBJECTCONTROL, (ObjectControlView)controller.view);
    //        newObjectView = ((ObjectControlView)controller.view);
    //    }
    //    else if (ObjectFactory.Instance.entityViews.ContainsKey(id) == true)
    //    {
    //        AddObjectControlToIndex(ControlTypeEnum.OBJECTCONTROL, (ObjectControlView)controller.view);
    //        newObjectView = ((ObjectControlView)ObjectFactory.Instance.entityViews[id]);
    //    }

    //    // Return the view
    //    return newObjectView;
    //}

    public ObjectControlView createForObjectControl(string entityID, ComplexObjectView complexObject)
    {
        // Check if entityID is new, else generate new ID
        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
            entityID = ObjectFactory.Instance.GenerateEntityID();

        // Create new Object
        ObjectControlView newObjectView = complexObject.currentGameObject.AddComponent(typeof(ObjectControlView)) as ObjectControlView;
        newObjectView.Initialise(entityID);

        // Bind to model
        if (complexObject.ControlSettings != null)
            complexObject.ControlSettings.Controller.View = newObjectView;

        // Add to index
        AddObjectControlToIndex(ControlTypeEnum.OBJECTCONTROL, newObjectView);

        return newObjectView;
    }


    // Create a CameraControl

    public CameraControlView createForCameraControl(ComplexObjectModel complexObject)
    {
        if (((ComplexObjectView)complexObject.Controller.FindSetView()) == true)
            return createForCameraControl(((ComplexObjectView)complexObject.Controller.FindSetView()));
        else
            return null;
    }

    public CameraControlView createForCameraControl(ComplexObjectView complexObject)
    {
        CameraControlView objectView = complexObject.GetComponent<CameraControlView>();

        // Find existing view
        if (objectView != null)
        {
            // Check if it's linked with the model
            if (complexObject.Controller.Model != null)
                if (((ComplexObjectModel)complexObject.Controller.Model).ControlSettings != null)
                    if (((ComplexObjectModel)complexObject.Controller.Model).ControlSettings.Controller.View != objectView)
                        ((ComplexObjectModel)complexObject.Controller.Model).ControlSettings.Controller.View = objectView;

            AddObjectControlToIndex(ControlTypeEnum.CAMERACONTROL, objectView);

            return objectView;
        }
        else
        {
            // Check if it's linked with the model
            if (complexObject.Controller.Model != null)
                if (((ComplexObjectModel)complexObject.Controller.Model).ControlSettings != null)
                {
                    if (((ComplexObjectModel)complexObject.Controller.Model).ControlSettings.Controller.View != null)
                        objectView = createForCameraControl(((ComplexObjectModel)complexObject.Controller.Model).ControlSettings.EntityID, complexObject);
                }
                else
                {
                    objectView = createForCameraControl(ObjectFactory.Instance.GenerateEntityID(), complexObject);
                }

            return objectView;
        }
    }

    public CameraControlView createForCameraControl(CameraControlModel model, bool syncViewToModel)
    {
        // controller or model are null or if the controller already has a view, then stop this methode
        if (model == null)
            return null;

        // If no controller exists, then make one
        if (model.Controller == null)
            ControllerFactory.Instance.createForObjectControl(model, false);

        // View to return
        CameraControlView view = null;

        // If there is already a view return that one
        if (model.Controller.FindSetView() != null)
        {
            view = ((CameraControlView)model.Controller.FindSetView());
            AddObjectControlToIndex(ControlTypeEnum.CAMERACONTROL, view);
        }

        // Create new one if no other exists and when all requirements are fulfilled
        else
            view = createForCameraControl(model.controlWielder);

        // Return a view created for a specific model
        return view;
    }

    //public CameraControlView createForCameraControl(CameraControlController controller, bool syncViewToModel)
    //{
    //    // controller or model are null or if the controller already has a view, then stop this methode
    //    if (controller == null || controller.model == null || controller.view != null)
    //        return null;

    //    // Set entityID
    //    string id = controller.model.entityID;

    //    // if the view exists use that one instead
    //    CameraControlView newObjectView = null;

    //    // 
    //    if (controller.view != null)
    //    {
    //        AddObjectControlToIndex(ControlTypeEnum.CAMERACONTROL, (CameraControlView)controller.view);
    //        newObjectView = ((CameraControlView)controller.view);
    //    }
    //    else if (ObjectFactory.Instance.entityViews.ContainsKey(id) == true)
    //    {
    //        AddObjectControlToIndex(ControlTypeEnum.CAMERACONTROL, (CameraControlView)controller.view);
    //        newObjectView = ((CameraControlView)ObjectFactory.Instance.entityViews[id]);
    //    }

    //    // Return the view
    //    return newObjectView;
    //}

    public CameraControlView createForCameraControl(string entityID, ComplexObjectView complexObject)
    {
        // Check if entityID is new, else generate new ID
        if (ObjectFactory.Instance.CheckForExistingEntityID(entityID) == true)
            entityID = ObjectFactory.Instance.GenerateEntityID();

        // Create new Object
        CameraControlView newObjectView = complexObject.currentGameObject.AddComponent(typeof(CameraControlView)) as CameraControlView;
        newObjectView.Initialise(entityID);

        // Bind to model
        if (complexObject.ControlSettings != null)
            complexObject.ControlSettings.Controller.View = newObjectView;

        // Add to index
        AddObjectControlToIndex(ControlTypeEnum.CAMERACONTROL, newObjectView);

        return newObjectView;
    }


    private void AddObjectToIndex(ObjectTypeEnum objectType, ObjectView view)
    {
        // Add controller to objectfactory index
        ObjectFactory.Instance.AddToIndex(view);
    }

    private void AddObjectControlToIndex(ControlTypeEnum controlType, ObjectControlView view)
    {
        // Add controller to objectfactory index
        ObjectFactory.Instance.AddToIndex(view);
    }

    private void AddSemanticObjectToIndex(ObjectTypeEnum objectType, SemanticObjectView view)
    {
        // Add controller to objectfactory index
        ObjectFactory.Instance.AddToIndex(view);
    }


    // Executes the right create methode for corresponding to the parameter "view" Type
    private ObjectView ExecuteCorrespondingCreateMethode(ObjectModel model, Transform parentTransform, bool syncViewToModel)
    {
        if (model.GetType() == typeof(PracticeModel))
            return ObjectFactory.Instance.viewFactory.createForPractice((PracticeModel) model, parentTransform, syncViewToModel);

        else if (model.GetType() == typeof(ContextModel))
            return ObjectFactory.Instance.viewFactory.createForContext((ContextModel) model, parentTransform, syncViewToModel);

        else if (model.GetType() == typeof(IntentionalElementModel))
            return ObjectFactory.Instance.viewFactory.createForIntentionalElement((IntentionalElementModel) model, parentTransform, syncViewToModel);

        else if (model.GetType() == typeof(ActorModel))
            return ObjectFactory.Instance.viewFactory.createForActor((ActorModel) model, parentTransform, syncViewToModel);

        else if (model.GetType() == typeof(ActivityModel))
            return ObjectFactory.Instance.viewFactory.createForActivity((ActivityModel) model, parentTransform, syncViewToModel);

        else if (model.GetType() == typeof(SMWObjectModel))
            return ObjectFactory.Instance.viewFactory.createForSMWObject((SMWObjectModel) model, parentTransform, syncViewToModel);

        else if (model.GetType() == typeof(SemanticObjectModel))
            return ObjectFactory.Instance.viewFactory.createForSemanticObject((SemanticObjectModel) model, parentTransform, syncViewToModel);

        else if (model.GetType() == typeof(ComplexObjectModel))
            return ObjectFactory.Instance.viewFactory.createForComplexObject((ComplexObjectModel) model, parentTransform, syncViewToModel);

        else if (model.GetType() == typeof(NavObjectModel))
            return ObjectFactory.Instance.viewFactory.createForNavObject((NavObjectModel) model, parentTransform, syncViewToModel);

        else if (model.GetType() == typeof(CameraModel))
            return ObjectFactory.Instance.viewFactory.createForCamera((CameraModel) model, parentTransform, syncViewToModel);
        else
            return null;
    }

    // Executes the right create methode for corresponding to the parameter "controller" Type
    private ObjectView ExecuteCorrespondingCreateMethode(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
    {
        if (category.objectType == ObjectTypeEnum.ACTIVITY)
            return ObjectFactory.Instance.viewFactory.createForActivity(entityID, semanticNode, category, navObject, parentTransform);

        else if (category.objectType == ObjectTypeEnum.ACTOR)
            return ObjectFactory.Instance.viewFactory.createForActor(entityID, semanticNode, category, navObject, parentTransform);

        else if (category.objectType == ObjectTypeEnum.CONTEXT)
            return ObjectFactory.Instance.viewFactory.createForContext(entityID, semanticNode, category, navObject, parentTransform);

        else if (category.objectType == ObjectTypeEnum.PRACTICE)
            return ObjectFactory.Instance.viewFactory.createForPractice(entityID, semanticNode, category, navObject, parentTransform);

        else if (category.objectType == ObjectTypeEnum.INTENTIONALELEMENT)
            return ObjectFactory.Instance.viewFactory.createForIntentionalElement(entityID, semanticNode, category, navObject, parentTransform);

        else if (category.objectType == ObjectTypeEnum.SMWOBJECT)
            return ObjectFactory.Instance.viewFactory.createForSMWObject(entityID, semanticNode, category, navObject, parentTransform);

        else
            return null;
    }


    // Using the entityID there shall be asked for a existing View or Controller, which will be used to create a Model if possible
    private BaseView CreateUsingFoundNonSemanticObjectMVCComponents(string entityID)
    {
        BaseView existingView = ObjectFactory.Instance.GetObjectViewByEnityID(entityID);
        if (existingView != null)
            return existingView;

        BaseModel existingModel = ObjectFactory.Instance.GetObjectModelByEntityID(entityID);
        if (existingModel != null)
        {
            if (existingModel is ObjectControlModel)
                return createForObjectControl((ObjectControlModel) existingModel, false);
            else if (existingModel is ObjectModel)
                return createForObject((ObjectModel) existingModel, null, false);
            else
                return null;
        }

        return null;
    }

    // Using the semanticNode there shall be asked for a existing View or Controller, which will be used to create a Model if possible
    private SemanticObjectView CreateUsingFoundSemanticMVCComponents(INode semanticNode)
    {
        SemanticObjectView existingView = ObjectFactory.Instance.GetSemanticObjectViewByNode(semanticNode);
        if (existingView != null)
            return existingView;

        SemanticObjectModel existingModel = ObjectFactory.Instance.GetSemanticObjectModelByNode(semanticNode);
        if (existingModel != null)
        {
            if (existingModel is SMWObjectModel)
            {
                return createForSMWObject((SMWObjectModel) existingModel, null, false);
            }
            else
            {
                return createForSemanticObject(existingModel, null, false);
            }
        }
        return null;
    }


    // Executes the right create methode for corresponding to the parameter "model" Type
    private ObjectView ExecuteCorrespondingObjectCreationMethode(ObjectModel existingModel, Transform parentTransform, bool syncViewToModel)
    {
        // For semantic objects
        if (existingModel is SemanticObjectModel)
            return ExecuteCorrespondingSemanticObjectCreationMethode((SemanticObjectModel)existingModel, parentTransform, syncViewToModel);

        // For Non-semantic objects
        else if (existingModel.GetType() == typeof(CameraModel))
            return ObjectFactory.Instance.viewFactory.createForCamera((CameraModel)existingModel, parentTransform, syncViewToModel);
        else if (existingModel.GetType() == typeof(ComplexObjectModel))
            return ObjectFactory.Instance.viewFactory.createForComplexObject((ComplexObjectModel)existingModel, parentTransform, syncViewToModel);
        else if (existingModel.GetType() == typeof(NavObjectModel))
            return ObjectFactory.Instance.viewFactory.createForNavObject((NavObjectModel)existingModel, parentTransform, syncViewToModel);
        else if (existingModel.GetType() == typeof(ObjectModel))
            return ObjectFactory.Instance.viewFactory.createForObject((ObjectModel)existingModel, parentTransform, syncViewToModel);

        else
            return null;
    }

    // Executes the right create methode for corresponding to the parameter "model" Type
    private ObjectControlView ExecuteCorrespondingObjectControlCreationMethode(ObjectControlModel existingModel, bool syncViewToModel)
    {
        if (existingModel.GetType() == typeof(ObjectControlModel))
            return ObjectFactory.Instance.viewFactory.createForObjectControl((ObjectControlModel) existingModel, syncViewToModel);
        else if (existingModel.GetType() == typeof(CameraControlModel))
            return ObjectFactory.Instance.viewFactory.createForCameraControl((CameraControlModel) existingModel, syncViewToModel);

        else
            return null;
    }

    // Executes the right create methode for corresponding to the parameter "model" Type
    private SemanticObjectView ExecuteCorrespondingSemanticObjectCreationMethode(SemanticObjectModel existingModel, Transform parentTransform, bool syncViewToModel)
    {
        // For semantic objects
        if (existingModel.GetType() == typeof(PracticeModel))
            return ObjectFactory.Instance.viewFactory.createForPractice((PracticeModel) existingModel, parentTransform, syncViewToModel);

        else if (existingModel.GetType() == typeof(ContextModel))
            return ObjectFactory.Instance.viewFactory.createForContext((ContextModel) existingModel, parentTransform, syncViewToModel);

        else if (existingModel.GetType() == typeof(IntentionalElementModel))
            return ObjectFactory.Instance.viewFactory.createForIntentionalElement((IntentionalElementModel) existingModel, parentTransform, syncViewToModel);

        else if (existingModel.GetType() == typeof(ActorModel))
            return ObjectFactory.Instance.viewFactory.createForActor((ActorModel) existingModel, parentTransform, syncViewToModel);

        else if (existingModel.GetType() == typeof(ActivityModel))
            return ObjectFactory.Instance.viewFactory.createForActivity((ActivityModel) existingModel, parentTransform, syncViewToModel);

        else if (existingModel.GetType() == typeof(SMWObjectModel))
            return ObjectFactory.Instance.viewFactory.createForSMWObject((SMWObjectModel) existingModel, parentTransform, syncViewToModel);

        else if (existingModel.GetType() == typeof(SemanticObjectView))
            return ObjectFactory.Instance.viewFactory.createForSemanticObject((SemanticObjectModel) existingModel, parentTransform, syncViewToModel);

        else
            return null;
    }


    // Executes the right create methode for corresponding to the parameter "view" Type
    private SemanticObjectView ExecuteCorrespondingSemanticObjectCreationMethode(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navObject, Transform parentTransform)
    {
        // For semantic objects
        if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == typeof(PracticeModel))
            return ObjectFactory.Instance.viewFactory.createForPractice(entityID, semanticNode, category, navObject, parentTransform);

        else if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == typeof(ContextModel))
            return ObjectFactory.Instance.viewFactory.createForContext(entityID, semanticNode, category, navObject, parentTransform);

        else if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == typeof(IntentionalElementModel))
            return ObjectFactory.Instance.viewFactory.createForIntentionalElement(entityID, semanticNode, category, navObject, parentTransform);

        else if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == typeof(ActorModel))
            return ObjectFactory.Instance.viewFactory.createForActor(entityID, semanticNode, category, navObject, parentTransform);

        else if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == typeof(ActivityModel))
            return ObjectFactory.Instance.viewFactory.createForActivity(entityID, semanticNode, category, navObject, parentTransform);

        else if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == typeof(SMWObjectModel))
            return ObjectFactory.Instance.viewFactory.createForSMWObject(entityID, semanticNode, category, navObject, parentTransform);

        else if (SMWParser.Instance.GetObjectModelTypePerObjectTypes(category.objectType) == null)
            return ObjectFactory.Instance.viewFactory.createForSemanticObject(entityID, semanticNode, category, navObject, parentTransform);

        else
            return null;
    }


    // Create Object GameObject with View
    private ObjectView NewUninitialisedObject(Transform parentTransform, bool instantiateInWorldSpace)
    {
        var prefab = Resources.Load<GameObject>("Resources/Prefabs/Models/Default/Object");
        var instance = UnityEngine.Object.Instantiate(prefab, parentTransform, true);
        return instance.GetComponent<ObjectView>();
    }

    // Create ComplexObject GameObject with View
    private ComplexObjectView NewUninitialisedComplexObject(Transform parentTransform, bool instantiateInWorldSpace)
    {
        Transform prefab = Resources.Load<GameObject>("Prefabs/Models/Default/WorldDimension/Non-semantic/ComplexObject").transform;

        var instance = UnityEngine.Object.Instantiate(prefab.gameObject, parentTransform, true);
        return instance.GetComponent<ComplexObjectView>();
    }

    // Create Object GameObject with View
    private CameraView NewUninitialisedCamera(Transform parentTransform, bool instantiateInWorldSpace)
    {
        var prefab = Resources.Load<GameObject>("Prefabs/Models/Default/Camera");
        var instance = UnityEngine.Object.Instantiate(prefab, parentTransform, true);
        return instance.GetComponent<CameraView>();
    }

    // Create SemanticObject GameObject with View
    private SemanticObjectView NewUninitialisedSemanticObject(Transform parentTransform, bool instantiateInWorldSpace)
    {
        var prefab = Resources.Load<GameObject>("Prefabs/Models/Default/WorldDimension/Semantic/SemanticObject");
        var instance = UnityEngine.Object.Instantiate(prefab, parentTransform, true);
        return instance.GetComponent<SemanticObjectView>();
    }

    // Create SMWObject GameObject with View
    private SMWObjectView NewUninitialisedSMWObject(Transform parentTransform, bool instantiateInWorldSpace)
    {
        var prefab = Resources.Load<GameObject>("Prefabs/Models/Default/WorldDimension/Semantic/SMWObject");
        var instance = UnityEngine.Object.Instantiate(prefab, parentTransform, true);
        return instance.GetComponent<SMWObjectView>();
    }

    // Create PracticeObject GameObject with View
    private PracticeView NewUninitialisedPracticeObject(Transform parentTransform, bool instantiateInWorldSpace)
    {
        var prefab = Resources.Load<GameObject>("Prefabs/Models/Default/WorldDimension/Semantic/Practice");
        var instance = UnityEngine.Object.Instantiate(prefab, parentTransform, true);
        return instance.GetComponent<PracticeView>();
    }

    // Create IntentionalElement GameObject with View
    private IntentionalElementView NewUninitialisedIntentionalElementObject(Transform parentTransform, bool instantiateInWorldSpace)
    {
        var prefab = Resources.Load<GameObject>("Prefabs/Models/Default/WorldDimension/Semantic/IntentionalElement");
        var instance = UnityEngine.Object.Instantiate(prefab, parentTransform, true);
        return instance.GetComponent<IntentionalElementView>();
    }

    // Create SemanticObject GameObject with View
    private ActorView NewUninitialisedActorObject(Transform parentTransform, bool instantiateInWorldSpace)
    {
        var prefab = Resources.Load<GameObject>("Prefabs/Models/Default/WorldDimension/Semantic/IntentionalElement_Actor");
        var instance = UnityEngine.Object.Instantiate(prefab, parentTransform, true);
        return instance.GetComponent<ActorView>();
    }

    // Create ActivityObject GameObject with View
    private ActivityView NewUninitialisedActivityObject(Transform parentTransform, bool instantiateInWorldSpace)
    {
        var prefab = Resources.Load<GameObject>("Prefabs/Models/Default/WorldDimension/Semantic/IntentionalElement_Activity");
        var instance = UnityEngine.Object.Instantiate(prefab, parentTransform, true);
        return instance.GetComponent<ActivityView>();
    }

    // Create ContextObject GameObject with View
    private ContextView NewUninitialisedContextObject(Transform parentTransform, bool isRole, bool instantiateInWorldSpace)
    {
        string pathToPrefab = "Prefabs/Models/Default/WorldDimension/Semantic/Context";
        if (isRole == true)
            pathToPrefab = "Prefabs/Models/Default/WorldDimension/Semantic/Context_Role";

        var prefab = Resources.Load<GameObject>(pathToPrefab);
        var instance = UnityEngine.Object.Instantiate(prefab, parentTransform, instantiateInWorldSpace);
        return instance.GetComponent<ContextView>();
    }

    // Create NavObject GameObject with View
    private NavObjectView NewUninitialisedNavObject(Transform parentTransform, bool instantiateInWorldSpace)
    {
        var prefab = Resources.Load<GameObject>("Prefabs/Models/Default/NavDimension/NavObject");
        var instance = UnityEngine.Object.Instantiate(prefab, parentTransform, instantiateInWorldSpace);
        return instance.GetComponent<NavObjectView>();
    }

    // Create NavObjectCHild GameObject with View
    private NavObjectView NewUninitialisedNavObjectChild(Transform parentTransform, bool instantiateInWorldSpace)
    {
        var prefab = Resources.Load<GameObject>("Prefabs/Models/Default/NavDimension/NavObjectChild");
        var instance = UnityEngine.Object.Instantiate(prefab, parentTransform, instantiateInWorldSpace);
        return instance.GetComponent<NavObjectView>();
    }

    // Checks for a specific instance INode if the category = the parameter category, else it returns the correct one
    private SMWCategory CheckForRightCategory(INode categoryInstanceOfIntressed, SMWCategory currentKnownCategory)
    {
        SMWCategory smwCategory = currentKnownCategory;
        if (smwCategory.GetAllInstanceTriples().ContainsKey(categoryInstanceOfIntressed) == false)
            smwCategory = SMWParser.Instance.GetAllCategorySMWCategory(categoryInstanceOfIntressed.Graph).Find(c => c.GetSpecificInstanceTriples(categoryInstanceOfIntressed) != null || c.GetSpecificInstanceTriples(categoryInstanceOfIntressed).Count >= 1);

        // Return result
        return smwCategory;
    }

}
