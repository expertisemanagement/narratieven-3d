﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmotralMonobehaviour : MonoBehaviour {

    // Use this for pre-initialization
    protected virtual void Awake()
    {
        // Very important for preventing duplicates
        SetImmortalSceneSingleton();
    }

    // Prevent duplicates when scene switching  (This code removes duplicatres)
    private void SetImmortalSceneSingleton()
    {
        DontDestroyOnLoad(this);

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
    }
}
