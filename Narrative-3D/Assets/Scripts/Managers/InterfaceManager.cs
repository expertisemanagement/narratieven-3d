﻿using System;
using System.Linq;
using Interface.Element;
using UnityEngine;

/// <summary>
/// This is a Singleton Manager class for managing GUI elements.
/// </summary>
public class InterfaceManager : ImmotralMonobehaviour
{
    // Static instance of a class of this type
    [HideInInspector] public static InterfaceManager instance;

    // List of interfaceELements that are know, and there settings
    [SerializeField] public InterfaceElement[] interfaceElements;

    // List of interfaceElements that can not beallowed to start, because of interference
    [HideInInspector] private InterfaceElementEnum[] NotAllowedInterfaceElements;

    // Use this for pre-initialization (A Unity MonodevelopEvent)
    protected override void Awake()
    {
        // inheritted Awake executions
        base.Awake();

        setInterfaceManager();

        // Setup different GUI settings based on device
        switch (SystemInfo.deviceType)
        {
            case DeviceType.Desktop:
                break;
            case DeviceType.Console:
                break;
            case DeviceType.Handheld:
                break;
            case DeviceType.Unknown:
                Debug.Log("Device unkown");
                break;
        }
    }

    public void Start()
    {
    }

    public void Update()
    {

    }

    // Hides all GUI elements
    public void HideAllGUI()
    {
        foreach (InterfaceElement element in interfaceElements)
        {
            element.interfaceElementGameObject.SetActive(false);
        }
    }

    // Shows all GUI elements
    public void ShowAllGUI()
    {
        foreach (InterfaceElement element in interfaceElements)
        {
            element.interfaceElementGameObject.SetActive(true);
        }
    }

    // Zooms (moves forward) the minimap RenderTexture camera based in zoomfactor input. Zooms in when parameter > 0, zoom out when parameter < 0
    public void ZoomMiniMap(float zoomFactor)
    {
        int interfaceElementIndex = Array.FindIndex(interfaceElements, i => i.interfaceElement == InterfaceElementEnum.MINIMAP);

        if (interfaceElementIndex > 0)
        {
            

            interfaceElements[interfaceElementIndex].interfaceElementGameObject.transform.Translate(Vector3.forward * zoomFactor);
        }
    }

    // Zooms (moves forward) the minimap RenderTexture camera based in zoomfactor input. Zooms in when parameter > 0, zoom out when parameter < 0
    public void ZoomBigMap(float zoomFactor)
    {
        int interfaceElementIndex = Array.FindIndex(interfaceElements, i => i.interfaceElement == InterfaceElementEnum.BIGMAP);

        if (interfaceElementIndex > 0)
        {


            interfaceElements[interfaceElementIndex].interfaceElementGameObject.transform.Translate(Vector3.forward * zoomFactor);
        }
    }

    // Toggles the visiblity of the minimap, by letting the ToggleInterfaceElement() call the InterfaceController ToggleVisablity() function 
    public void ToggleMiniMap()
    {
        ToggleInterfaceElement(InterfaceElementEnum.MINIMAP);
    }

    // Toggles the visiblity of the bigmap, by letting the ToggleInterfaceElement() call the InterfaceController ToggleVisablity() function 
    public void ToggleBigMap()
    {
        ToggleInterfaceElement(InterfaceElementEnum.BIGMAP);
    }

    // Toggles the visiblity of the navigationbrowser, by letting the ToggleInterfaceElement() call the InterfaceController ToggleVisablity() function 
    public void ToggleNavBrowser()
    {
        ToggleInterfaceElement(InterfaceElementEnum.BROWSERNAVIGATION);
    }

    // Toggles the visiblity of the Projectbrowser, by letting the ToggleInterfaceElement() call the InterfaceController ToggleVisablity() function 
    public void ToggleProjectMenu()
    {
        ToggleInterfaceElement(InterfaceElementEnum.PROJECTBROWSER);
    }

    // Toggles the visiblity of the RecordingHUD, by letting the ToggleInterfaceElement() call the InterfaceController ToggleVisablity() function 
    public void ToggleRecordingHUDBrowser()
    {
        ToggleInterfaceElement(InterfaceElementEnum.RECRODINGHUD);
    }

    public void ToggleNarrativeMenu()
    {
        ToggleInterfaceElement(InterfaceElementEnum.PROTAGONISTBROWSER);
    }

    // Checks if an InterfaceController is available, if not adds a new InterfaceController
    private void HasInterfaceController(GameObject interfaceGameObject)
    {
        if (interfaceGameObject.GetComponent<InterfaceController>() == null || interfaceGameObject.GetComponent<InterfaceController>() == false)
            interfaceGameObject.AddComponent<InterfaceController>();
    }

    // Based on the InterfaceElementEnum
    private void ToggleInterfaceElement(InterfaceElementEnum interfaceElement)
    {
        GameObject interfaceGameObject = interfaceElements.ToList().FirstOrDefault(i => i.interfaceElement == interfaceElement).interfaceElementGameObject;

        if (interfaceGameObject != null)
        {
            interfaceGameObject.GetComponent<InterfaceController>().ToggleVisablity();
        }
    }
    
    // Set an instance of this object if none exists already
    private void setInterfaceManager()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }
}
