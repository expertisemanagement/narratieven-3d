﻿using Object.Type;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using VDS.RDF;
using System;
using System.Linq;

public class NavManager : InteractionManager
{
    // Static instance of a class of this type
    public static NavManager instance;
    public SMWObjectModel currentSemanticObject = null;
    public SMWObjectModel selectedSemanticObject = null;
    public GameObject navBrowser;
    public Stack<NavObjectModel> breadCrums;
    internal bool navIsLoaded;

    // Returns all the camera's in the world dimension
    public CameraView[] GetAllNavCameras()
    {
        return GameManager.instance.navDimensionTransform.GetComponentsInChildren<CameraView>();
    }
    
    // Enables all Camera Components from all the camera's in the world dimension
    public void EnableAllNavCameras()
    {
        foreach (CameraView cam in GetAllNavCameras())
        {
            cam.EnableCameraComponent();
        }
    }

    // Disables all Camera Components from all the camera's in the world dimension
    public void DisableAllNavCameras()
    {
        foreach (CameraView cam in GetAllNavCameras())
        {
            cam.DisableCameraComponent();
        }
    }

    // Use this for pre-initialization (A Unity MonodevelopEvent)
    protected override void Awake()
    {
        // inheritted Awake executions
        base.Awake();

        setNavManager();
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();


    }

    protected override void Update()
    {
        base.Update();
    }

    // Returns the GameObject holding a ObjectView of the object representing the world context (this game object holds all childs ObjectViews that are related) 
    public GameObject GetCurrentNavWorldParentObject()
    {
        return GameManager.instance.navDimensionTransform.transform.gameObject;
    }

    // 
    public void SetObjectViewAsNewParent(ObjectView objectView)
    {
        if (GetCurrentNavWorldParentObject() != objectView)
            RemoveNavWorldParentObject();
    }

    // Removes the parent object from the world and it's childs (it's childs first)
    public void RemoveNavWorldParentObject()
    {
        // First removes all his childs
        RemoveNavWorldParentObjectChilds();

        // Removes the parent object
        Destroy(GetCurrentNavWorldParentObject());
    }

    // Removes every child in the parentObject of the world (Used the MonoBehaviour DESTROY so ONLY WORKS in runtime)
    public void RemoveNavWorldParentObjectChilds()
    {
        foreach (Transform child in GetCurrentNavWorldParentObject().transform)
        {
            Destroy(child.gameObject);
        }
    }

    // Sets/Create the current SemanticObjectModel from which relevante nodes are shown
    public void SetNewCurrentNode(SemanticObjectModel semanticObject)
    {
        // If new semanticObject object is same as the current than stop this methode, without any further execution
        if (currentSemanticObject == semanticObject)
        {
            return;
        }

        // If new semanticObject object is null than load the base Contexts of the loaded Graphs
        else if (currentSemanticObject == null)
        {
            currentSemanticObject = null;
            LoadNavWorld(SMWParser.Instance.GetBaseInstancesOfObjectTypes(SMWParser.Instance.GetLoadedGraphs()[0], ObjectTypeEnum.CONTEXT).ToArray());
            return;
        }

        else if (semanticObject is SMWObjectModel)
        {
            currentSemanticObject = (SMWObjectModel)semanticObject;
            LoadNavWorld(currentSemanticObject);
        }
    }

    // Part of the initialisation
    private void setNavManager()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        //DontDestroyOnLoad(gameObject);
    }

    public bool LoadNavWorld(SMWObjectModel currentSMWObject)
    {
        // relevant Nodes
        List<SMWObjectModel> relevanteModels = new List<SMWObjectModel>();

        // Load in views based on SMWObjectModel type
        switch (currentSMWObject.ObjectType)
        {
            case ObjectTypeEnum.INTENTIONALELEMENT:
                relevanteModels.AddRange(((IntentionalElementModel)currentSMWObject).ConcernsThis);
                break;
            case ObjectTypeEnum.ACTOR:
                relevanteModels.AddRange(((ActorModel)currentSMWObject).ConcernsThis);
                break;
            case ObjectTypeEnum.ACTIVITY:
                relevanteModels.AddRange(((ActivityModel)currentSMWObject).ConcernsThis);
                break;
            case ObjectTypeEnum.CONTEXT:
                relevanteModels.AddRange(((ContextModel)currentSMWObject).RelatedActivities.Cast<SMWObjectModel>());
                relevanteModels.AddRange(((ContextModel)currentSMWObject).RelatedActors.Cast<SMWObjectModel>());
                relevanteModels.AddRange(((ContextModel)currentSMWObject).RelatedPractices.Cast<SMWObjectModel>());
                relevanteModels.AddRange(((ContextModel)currentSMWObject).RelatedRoles.Cast<SMWObjectModel>());
                relevanteModels.AddRange(((ContextModel)currentSMWObject).SubContexts);
                break;
            case ObjectTypeEnum.PRACTICE:
                foreach (Selects select in ((PracticeModel)currentSMWObject).Selections) relevanteModels.Add(select.GetSelectionLink());
                break;
            default:
                return false;
        }

        return LoadNavWorld(relevanteModels.ToArray());
    }

    private bool LoadNavWorld(SMWObjectModel[] relevantNodes)
    {
        foreach (SMWObjectModel semanticModel in relevantNodes)
        {
            if (semanticModel.NavObject == null)
                ObjectFactory.Instance.viewFactory.createForNavObject(semanticModel, GetCurrentNavWorldParentObject().transform, false);
        }

        return true;
    }
}
