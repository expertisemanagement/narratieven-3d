﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : ImmotralMonobehaviour
{
    // Static instance of a class of this type
    public static SoundManager instance;

    // Use this for pre-initialization (A Unity MonodevelopEvent)
    protected override void Awake()
    {
        // inheritted Awake executions
        base.Awake();

        setSoundManager();
    }

    private void setSoundManager()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

        //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        //DontDestroyOnLoad(gameObject);
    }

}
