﻿using Object.FSM;
using Object.Type;
using SMW.EMM.Context;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// This is a class for a for Managing the 3d representation of the ObjectView (and subclasses).
/// </summary>
public class WorldManager : ImmotralMonobehaviour
{
    // Static instance of a class of this type
    public static WorldManager instance;

    // GameObject of the contextBreadcrumbsDisplay
    public GameObject contextBreadcrumbsDisplay;

    // ObjectView of the object that is main parent of the world
    public ObjectView currentWorldParent = null;

    // Booleans indicating the status of the world
    public bool isShowingRelations = false;
    public bool isNarrativeMode = false;
    public bool isDesignedWorld = false;
    public bool isLoadedWorld = false;

    // GameObjects containing camera's (GameObjects with a Camera Component) in the world, by concept
    public GameObject WorldCamerasContainer;

    // GameObjects containing the EMont instances (GameObjects with SMWObjectViews Components), by concept
    public GameObject actorsAndRolls = null;
    public GameObject activities = null;
    public GameObject goals = null;
    public GameObject beliefs = null;
    public GameObject outcomes = null;
    public GameObject relations = null;
    public GameObject decor = null;
    public GameObject subContexts = null;

    Dictionary<Relation, IntentionalElementModel> ieRelations = new Dictionary<Relation, IntentionalElementModel>();
    
    // Methode called every frame (A Unity MonodevelopEvent)
    public void Update()
    {
        UpdateWorldLocationRefDisplay();
    }

    // Use this for pre-initialization (A Unity MonodevelopEvent)
    protected override void Awake()
    {
        // inheritted Awake executions
        base.Awake();

        setWorldManager();
    }

    // Returns all the camera's in the world dimension
    public CameraView[] GetAllWorldCameras()
    {
        return GameManager.instance.worldDimensionTransform.GetComponentsInChildren<CameraView>();
    }

    // Enables all Camera Components from all the camera's in the world dimension
    public void EnableAllWorldCameras()
    {
        foreach (CameraView cam in GetAllWorldCameras())
        {
            cam.EnableCameraComponent();
        }
    }

    // Disables all Camera Components from all the camera's in the world dimension
    public void DisableAllWorldCameras()
    {
        foreach (CameraView cam in GetAllWorldCameras())
        {
            cam.DisableCameraComponent();
        }
    }

    // Returns the GameObject holding a ObjectView of the object representing the world context (this game object holds all childs ObjectViews that are related) 
    public ObjectView GetCurrentWorldObject()
    {
        if (currentWorldParent == null)
        {
            int childindex = 0;
            
            while (childindex != GameManager.instance.worldDimensionTransform.transform.childCount)
            {
                Transform nextchild = GameManager.instance.worldDimensionTransform.transform.GetChild(childindex);

                // if no next child found stop the search
                if (nextchild == null)
                    childindex++;

                if (nextchild.GetComponent<ObjectView>() != null)
                {
                    currentWorldParent = nextchild.gameObject.GetComponent<ObjectView>();
                    break;
                }

                childindex++;
            }

        //foreach (SMWObjectView v in GameManager.instance.worldDimensionTransform.GetComponentsInChildren(typeof(ObjectView), true).ToList().Cast<ObjectView>().ToList())
        //{
        //    if (currentSMWObjectModel.FindCreateSetController().FindSetView() == v)
        //        return currentSMWObjectModel.FindCreateSetController().FindSetView().gameObject;
        //}

        //return GameManager.instance.worldDimensionTransform;
        }

        return currentWorldParent;

        //Debug.Log(currentParentWorldObject);

        //return currentParentWorldObject;
    }

    // Set the given view as the new world parent
    public bool SetObjectViewAsNewParent(ObjectView objectView)
    {
        if (GetCurrentWorldObject() != objectView)
        {
            // Place this Gameobject apart
            objectView.transform.parent = GameManager.instance.worldDimensionTransform.transform;

            // Remove World Parent Object
            RemoveWorldParentObjectChilds();

            // Set this as new parent
            currentWorldParent = objectView;
        }

        // Succes
        return true;
    }

    // Removes the parent object from the world and it's childs (it's childs first)
    public void RemoveWorldParentObject()
    {
        // First removes all his childs
        RemoveWorldParentObjectChilds();

        // Removes the parent object
        if(GetCurrentWorldObject() != null && GetCurrentWorldObject() != GameManager.instance.worldDimensionTransform)
            Destroy(GetCurrentWorldObject());
    }

    // Removes every child in the parentObject of the world (Used the MonoBehaviour DESTROY so ONLY WORKS in runtime)
    public bool RemoveWorldParentObjectChilds()
    {
        if (GetCurrentWorldObject() == null)
            return false;

        foreach (Transform child in GetCurrentWorldObject().transform)
            if(child != WorldCamerasContainer.transform)
                Destroy(child.gameObject);

        return true;
    }

    // Create a new world for for a given situation, or given protagonist. Deletes the current world.
    public bool CreateNewWorldAndWipeOldWorld(ComplexObjectModel protagonist, bool allowCreateNewProtagonist, SMWObjectModel situation, bool allowCreateNewSituation, bool narrativeMode, bool allowDesigned)
    {
        // Variable for holding the succes of this methode
        bool isSucces = false;

        // If there is no given protagonist or situation, then stop this methode and return false
        if (protagonist == null && situation == null)
            return isSucces;

        // Set the narrative, if not possible then stop this methode and return false
        if (!NarrativeManager.instance.SetupNewNarrative(protagonist, allowCreateNewProtagonist, situation, allowCreateNewSituation, true))
            return false;

        // Delete/Destroy the current world, if it exists
        if (currentWorldParent != null)
            DeleteCurrentWorldParentAndItsChildren();

        // Set the 3d world models
        if (allowDesigned == true) // FUTURE UPGRADE -> && ProjectManager.instance.GetDesignedLevelAssets() != null)
            isSucces = CreateAPredesignedWorld(situation, narrativeMode);
        else
            isSucces = CreateAGenerateWorld(situation, narrativeMode);

        // On a succes fix camera and activity visuals
        if (isSucces == true)
        {
            // Disable all word camera's to prevent unwanted render
            DisableAllWorldCameras();

            // Enable allowed camera's
            EnableAllowedCameras();

            // Updates visiable and possible activities
            UpdateActivitiyVisuals();
        }

        // Define if the world is loaded or not
        isLoadedWorld = isSucces;

        return isSucces;
    }

    // This enables the relevant cameras in the current world
    private static void EnableAllowedCameras()
    {
        if (PerspectiveManager.instance.GetCurrentCamera() != null)
        {
            PerspectiveManager.instance.GetCurrentCamera().GetComponent<CameraView>().enabled = true;
        }
        else
        {
            CameraView[] currentExistingCameras = WorldManager.instance.GetAllWorldCameras();
            if (currentExistingCameras != null && currentExistingCameras.Count() >= 1)
                currentExistingCameras[0].enabled = true;
        }
    }

    // Methode for generation of a non-designed worldmodel
    private bool CreateAGenerateWorld(SMWObjectModel smwObject, bool narrativeMode)
    {
        if (smwObject == null)
            return false;

        // smwObject View
        SMWObjectView view = (SMWObjectView)smwObject.FindCreateSetController().FindCreateSetView(GameManager.instance.worldDimensionTransform.gameObject.transform);
        
        // If still no view exists stop this methode
        if (view == null)
            return false;

        // Set the view as world parent
        SetObjectViewAsNewParent(view);

        // Load in views based on SMWObjectModel type
        switch (view.ObjectType)
        {
            case ObjectTypeEnum.INTENTIONALELEMENT:
                return false; 
            case ObjectTypeEnum.ACTOR:
                return false; 
            case ObjectTypeEnum.ACTIVITY:
                return false; 
            case ObjectTypeEnum.CONTEXT:
                return CreateAContextWorld((ContextModel)view.FindCreateSetController().FindCreateSetModel(), narrativeMode);
            case ObjectTypeEnum.PRACTICE:
                return false;
            default:
                return false;
        }
    }

    // Methode loading a designed worldmodel
    private bool CreateAPredesignedWorld(SMWObjectModel smwObject, bool narrativeMode)
    {
        // Check if there does exist a designed world for this SMWObject
        if (ProjectManager.instance.currentProjectWorldsNamesAndPaths.ContainsKey(smwObject.ObjectName) == true)
        {
            string pathToScene = "";

            if (narrativeMode == true)
                pathToScene = ProjectManager.instance.currentProjectWorldsNamesAndPaths + "NarrativeWorld/narrative.unity";
            else if (narrativeMode != false)
                pathToScene = ProjectManager.instance.currentProjectWorldsNamesAndPaths + "WikiWorld/wiki.unity";

            if (Directory.Exists(pathToScene) == true)
            {
                LoadDesignerAsyncScene(pathToScene);
                return true;
            }
            else
                return false;
        }
        else
        {
            // Return false because there is no existing project
            return false;
        }

    }

    // Updates visiable and possible activities, by checking actionmanager progress
    public void UpdateActivitiyVisuals()
    {
        foreach (RelationController rc in GameObject.FindObjectsOfType<RelationController>())
        {
            if (NarrativeManager.instance.GetExecutedActivities().Contains((ActivityModel)rc.relatedElement))
                rc.relatedElementIsExecuted = true;
            else
                rc.relatedElementIsExecuted = false;

            if (NarrativeManager.instance.GetExecutedActivities().Contains((ActivityModel)rc.subjectElement))
                rc.subjectElementIsExecuted = true;
            else
                rc.subjectElementIsExecuted = false;

            if (NarrativeManager.instance.GetAllowedActivities().Contains((ActivityModel)rc.relatedElement))
                rc.relatedElementAllowsExecution = true;
            else
                rc.relatedElementAllowsExecution = false;

            if (NarrativeManager.instance.GetAllowedActivities().Contains((ActivityModel)rc.subjectElement))
                rc.subjectElementAllowsExecution = true;
            else
                rc.subjectElementAllowsExecution = false;

            // Color activities
            if (rc.subjectElementIsExecuted)
                rc.subjectElement.FindCreateSetController().FindSetView().GetComponent<MeshRenderer>().material.color = new Color32(0, 255, 0, 255);

            if (rc.subjectElementAllowsExecution)
                rc.subjectElement.FindCreateSetController().FindSetView().GetComponent<MeshRenderer>().material.color = new Color32(0, 0, 255, 255);

            if (rc.relatedElementIsExecuted)
                rc.relatedElement.FindCreateSetController().FindSetView().GetComponent<MeshRenderer>().material.color = new Color32(0, 255, 0, 255);

            if (rc.relatedElementAllowsExecution)
                rc.relatedElement.FindCreateSetController().FindSetView().GetComponent<MeshRenderer>().material.color = new Color32(0, 0, 255, 255);
        }
    }

    // Toggle condition lines
    public void ToggleVisabilityOfTheConditionLines()
    {
        isShowingRelations = !isShowingRelations;
    }

    // Create a new world with a Context as the worldparent object 
    private bool CreateAContextWorld(ContextModel model, bool narrativeMode)
    {
        // Set the parentTransform en parentView of the world
        Transform parentTransform = GameManager.instance.worldDimensionTransform.transform;
        ContextView mainContextView = null;
        ContextModel mainContextModel = null;

        if (model.ContextType == ContextTypeEnum.ROLE)
            mainContextView = (ContextView)model.SuperContext.FindCreateSetController().FindCreateSetView(parentTransform);
        else
            mainContextView = (ContextView)model.FindCreateSetController().FindCreateSetView(parentTransform);

        parentTransform = mainContextView.transform;
        mainContextModel = (ContextModel)mainContextView.FindCreateSetController().FindCreateSetModel();

        // If the current context is a superContext of this context then add this context as a child of the currentContext
        if (mainContextView == null)
            return false;

        // Remove the meshRenderer and Collider from the parent View
        if (mainContextView.GetComponent<Collider>() != null) mainContextView.GetComponent<Collider>().enabled = false;
        if (mainContextView.GetComponent<Renderer>() != null) mainContextView.GetComponent<Renderer>().enabled = false;

        // Create/Set intentional Elements view contatainers (as GameObjects)
        this.CreateNewIntentionalElementGameObjectContainers(mainContextView);

        // Fill intentional element containers (with SMWObjectModels)
        int XSpawnLocation, ZSpawnLocation;
        this.CreateTheModelsForeachIntentionalElementContainer(model, mainContextModel, out XSpawnLocation, out ZSpawnLocation);

        // Create relations for the 
        this.CreateNewRelationObjectsForTheIntentionalElements(model, mainContextModel);

        // Fill decor container
        this.CreateAFloorOfTilesForAllIntentionalElementsInTheWorld(XSpawnLocation, ZSpawnLocation);

        // Set protagonist as existing actor or roll, else create placeholder no-semantic role protagonist
        this.CreateAndSetRandomControllableProtagonist(parentTransform, mainContextModel);

        // Return succes!
        return true;
    }

    // Creates a floor for the intentional Elements in the world
    private void CreateAFloorOfTilesForAllIntentionalElementsInTheWorld(int XSpawnLocation, int ZSpawnLocation)
    {
        int amountOfTilesOnX = XSpawnLocation + 20;
        int amountOfTilesOnZ = ZSpawnLocation + 20;
        int currentTileXLocation = 0;
        int currentTileZLocation = 0;

        while (currentTileXLocation < amountOfTilesOnX)
        {
            while (currentTileZLocation < amountOfTilesOnZ)
            {
                var prefab = Resources.Load<GameObject>("Prefabs/Models/Default/WorldDimension/Non-semantic/Decor/Tile_Dubbelsided_Bottom_1x1");
                var instance = Instantiate(prefab, decor.transform, true);
                instance.transform.position = new Vector3(currentTileXLocation - 5, -1, currentTileZLocation - 5);

                currentTileZLocation++;
            }

            currentTileZLocation = 0;
            currentTileXLocation++;
        }
    }

    // Create 3d models (the View-components) for each intentional elements containers of this context
    private void CreateTheModelsForeachIntentionalElementContainer(ContextModel model, ContextModel mainContextModel, out int XSpawnLocation, out int ZSpawnLocation)
    {
        Debug.Log("Fill Intentional Element containers");

        XSpawnLocation = 0;
        ZSpawnLocation = 0;

        foreach (ActorModel actor in model.RelatedActors)
        {
            BaseView actorView = actor.FindCreateSetController().FindCreateSetView(actorsAndRolls.transform);
            actorView.transform.position = new Vector3(XSpawnLocation, instance.transform.position.y, ZSpawnLocation);
            ((ComplexObjectModel)actorView.FindCreateSetController().FindCreateSetModel()).MachineState = ObjectFSMEnum.PLAYERCONTROLLED;
            XSpawnLocation++;
        }

        foreach (ContextModel role in mainContextModel.RelatedRoles)
        {
            BaseView roleView = role.FindCreateSetController().FindCreateSetView(actorsAndRolls.transform);
            roleView.transform.position = new Vector3(XSpawnLocation, instance.transform.position.y, ZSpawnLocation);
            ((ComplexObjectModel)roleView.FindCreateSetController().FindCreateSetModel()).MachineState = ObjectFSMEnum.PLAYERCONTROLLED;
            XSpawnLocation++;

            foreach (ActivityModel activity in role.RelatedActivities)
            {
                BaseView activityView = activity.FindCreateSetController().FindCreateSetView(activities.transform);
                activityView.transform.position = new Vector3(XSpawnLocation, instance.transform.position.y, ZSpawnLocation);
                XSpawnLocation++;
            }

            foreach (IntentionalElementModel goal in role.RelatedActivities)
            {
                BaseView goalView = goal.FindCreateSetController().FindCreateSetView(activities.transform);
                goalView.transform.position = new Vector3(XSpawnLocation, instance.transform.position.y, ZSpawnLocation);
                XSpawnLocation++;
            }

            foreach (IntentionalElementModel belief in role.RelatedActivities)
            {
                BaseView beliefView = belief.FindCreateSetController().FindCreateSetView(activities.transform);
                beliefView.transform.position = new Vector3(XSpawnLocation, instance.transform.position.y, ZSpawnLocation);
                XSpawnLocation++;
            }

            foreach (IntentionalElementModel outcome in role.RelatedActivities)
            {
                BaseView outcomeView = outcome.FindCreateSetController().FindCreateSetView(activities.transform);
                outcomeView.transform.position = new Vector3(XSpawnLocation, instance.transform.position.y, ZSpawnLocation);
                XSpawnLocation++;
            }

            ZSpawnLocation += 5;
        }
    }

    // Creates realtion objects for all intentional elements in the current Context
    private void CreateNewRelationObjectsForTheIntentionalElements(ContextModel model, ContextModel mainContextModel)
    {
        this.ieRelations = new Dictionary<Relation, IntentionalElementModel>();

        foreach (ActorModel actor in model.RelatedActors)
        {
            foreach (Contributes con in actor.Contributes)
                if (!ieRelations.ContainsKey(con))
                    ieRelations.Add(con, actor);

            foreach (Dependency dep in actor.Depends)
                if (!ieRelations.ContainsKey(dep))
                    ieRelations.Add(dep, actor);
        }

        foreach (ContextModel role in mainContextModel.RelatedRoles)
        {
            foreach (ActivityModel activity in role.RelatedActivities)
            {
                foreach (Connects con in activity.Connects)
                    if (!ieRelations.ContainsKey(con))
                        ieRelations.Add(con, activity);

                foreach (Contributes con in activity.Contributes)
                    if (!ieRelations.ContainsKey(con))
                        ieRelations.Add(con, activity);

                foreach (Dependency dep in activity.Depends)
                    if (!ieRelations.ContainsKey(dep))
                        ieRelations.Add(dep, activity);
            }

            foreach (IntentionalElementModel goal in role.RelatedActivities)
            {
                foreach (Contributes con in goal.Contributes)
                    if (!ieRelations.ContainsKey(con))
                        ieRelations.Add(con, goal);

                foreach (Dependency dep in goal.Depends)
                    if (!ieRelations.ContainsKey(dep))
                        ieRelations.Add(dep, goal);
            }

            foreach (IntentionalElementModel belief in role.RelatedActivities)
            {
                foreach (Contributes con in belief.Contributes)
                    if (!ieRelations.ContainsKey(con))
                        ieRelations.Add(con, belief);

                foreach (Dependency dep in belief.Depends)
                    if (!ieRelations.ContainsKey(dep))
                        ieRelations.Add(dep, belief);
            }

            foreach (IntentionalElementModel outcome in role.RelatedActivities)
            {
                foreach (Contributes con in outcome.Contributes)
                    if (!ieRelations.ContainsKey(con))
                        ieRelations.Add(con, outcome);

                foreach (Dependency dep in outcome.Depends)
                    if (!ieRelations.ContainsKey(dep))
                        ieRelations.Add(dep, outcome);
            }
        }

        foreach (Relation relation in ieRelations.Keys)
        {
            var prefab = Resources.Load<GameObject>("Prefabs/Models/Default/WorldDimension/Semantic/Relation");
            var instance = Instantiate(prefab, relations.transform, true);
            instance.GetComponent<RelationController>().InitialiseRelationController((IntentionalElementView)ieRelations[relation].FindCreateSetController().FindSetView(), relation);
        }
    }

    // Creates intentional element containers for a context
    private void CreateNewIntentionalElementGameObjectContainers(ContextView mainContextView)
    {
        Debug.Log("Generate Main Context Intentional Element containers");

        this.actorsAndRolls = new GameObject("ACTORS/ROLES");
        actorsAndRolls.transform.parent = mainContextView.transform;

        this.activities = new GameObject("ACTIVITIES");
        activities.transform.parent = mainContextView.transform;

        this.goals = new GameObject("GOALS");
        goals.transform.parent = mainContextView.transform;

        this.beliefs = new GameObject("BELIEFS");
        beliefs.transform.parent = mainContextView.transform;

        this.outcomes = new GameObject("OUTCOMES");
        outcomes.transform.parent = mainContextView.transform;

        this.relations = new GameObject("RELATIONS");
        relations.transform.parent = mainContextView.transform;

        this.decor = new GameObject("DECOR");
        decor.transform.parent = mainContextView.transform;

        // Set subcontexts view contatainer (as GameObjects)

        this.subContexts = new GameObject("SUBCONTEXTS");
        subContexts.transform.parent = mainContextView.transform;
    }

    // Destroys the current parent worldview and it's childs
    private void DeleteCurrentWorldParentAndItsChildren()
    {
        Destroy(currentWorldParent.GetComponentInChildren<MonoBehaviour>());
        Destroy(currentWorldParent.gameObject);
        currentWorldParent = null;
    }

    // Looksup all models from all Roles and Actors of a mainContextModel
    private void CreateAndSetRandomControllableProtagonist(Transform parentTransformForCreationOfnewprotagonist, ContextModel mainContextModel)
    {
        List<ComplexObjectModel> allModelsFromCurrentExisistingActorsAndRoles = new List<ComplexObjectModel>();

        foreach (ComplexObjectView actorOrRoleView in actorsAndRolls.GetComponentsInChildren<ComplexObjectView>())
        {
            if (actorOrRoleView.GetType() != typeof(CameraView))
                allModelsFromCurrentExisistingActorsAndRoles.Add((ComplexObjectModel)actorOrRoleView.FindCreateSetController().FindCreateSetModel());
        }

        if (allModelsFromCurrentExisistingActorsAndRoles.Contains(NarrativeManager.instance.GetProtagonist()) == true)
            PerspectiveManager.instance.SetPerspectiveToProtagonist(true, true);
        else
            PerspectiveManager.instance.SetNewPerspective(CreateNonSemanticCharacter(parentTransformForCreationOfnewprotagonist), false, true, true);

        // Update the contextBreadcrumbsDisplay
        string protagonist = "";
        if (PerspectiveManager.instance.GetCurrentSubject() != null && PerspectiveManager.instance.GetCurrentSubject().GetComponent<ComplexObjectView>() != null)
        {
            if (PerspectiveManager.instance.GetCurrentSubject().GetComponent<ComplexObjectView>() is SemanticObjectView)
                protagonist = ((SemanticObjectModel)PerspectiveManager.instance.GetCurrentSubject().GetComponent<ComplexObjectView>().FindSetController().FindCreateSetModel()).ObjectName;
            else
                protagonist = PerspectiveManager.instance.GetCurrentSubject().GetComponent<ComplexObjectView>().name;
        }
    }

    // Creates a placeholder character
    private ComplexObjectView CreateNonSemanticCharacter(Transform parentTransform)
    {
        return (ComplexObjectView) ObjectFactory.Instance.viewFactory.createForComplexObject(parentTransform);
    }

    // Load the new scene in the background
    private IEnumerator LoadDesignerAsyncScene(string pathToScene)
    {
        // The Application loads the Scene in the background at the same time as the current Scene.
        //This is particularly good for creating loading screens. You could also load the scene by build //number.
        Debug.Log("Trying to load: " +  pathToScene);

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(pathToScene);

        //Wait until the last operation fully loads to return anything
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    // Updates the textual information of the WorldLocationRefDisplay (which shows the situation and protagonist)
    private void UpdateWorldLocationRefDisplay()
    {
        ComplexObjectModel currentWorldParentModel = null;
        string contextName = "";
        string protagonist = "";

        if (currentWorldParent != null && currentWorldParent.FindCreateSetController().FindCreateSetModel() != null)
            currentWorldParentModel = (ComplexObjectModel)currentWorldParent.FindCreateSetController().FindCreateSetModel();

        if (currentWorldParentModel != null)
        {
            contextName = currentWorldParent.name;

            Debug.Log(currentWorldParentModel.GetType().ToString());

            if (currentWorldParentModel is SMWObjectModel)
                contextName = ((SMWObjectModel)currentWorldParent.FindCreateSetController().FindCreateSetModel()).ObjectName;
        }

        if (PerspectiveManager.instance.GetCurrentSubject() != null)
        {
            protagonist = PerspectiveManager.instance.GetCurrentSubject().GetComponent<ComplexObjectView>().name;

            if (PerspectiveManager.instance.GetCurrentSubject().GetComponent<ComplexObjectView>().FindSetController().FindCreateSetModel() is SMWObjectModel)
                protagonist = ((SMWObjectModel)PerspectiveManager.instance.GetCurrentSubject().GetComponent<ComplexObjectView>().FindSetController().FindCreateSetModel()).ObjectName;
        }

        contextBreadcrumbsDisplay.GetComponentInChildren<Text>().text = contextName + " / " + protagonist;
    }

    // part of initialisation
    private void setWorldManager()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
    }

}
