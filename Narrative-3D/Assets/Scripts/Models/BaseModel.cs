﻿using System;

/// <summary>
/// This is the base Model class.
/// </summary>
public class BaseModel : IBaseModel
{
    // Constructor
    public BaseModel(string entityID)
    {
        this.b_entityID = entityID;
    }

    // Constructor
    public BaseModel(BaseView existingView, bool syncViewToModel)
    {
        LinkViewToModel(existingView);

        if (syncViewToModel == true)
            Controller.View.SyncViewToModel();
        else
            this.b_entityID = ((BaseView)Controller.View).EntityID;
    }

    // Unique indentifier for a object
    protected string b_entityID;
    public string EntityID
    {
        get { return b_entityID; }
        set
        {
            // Only if the backing field changes
            if (b_entityID != value)
            {
                // Set new value
                b_entityID = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new EntityIDChangedEventArgs();
                OnEntityIDChanged(this, eventArgs);
            }
        }
    }
    
    // Controller for sending events and syncing data based on the Model
    protected BaseController b_controller;
    public BaseController Controller
    {
        get
        {
            return b_controller;
        }
        set
        {
            // Only if the backing field changes
            if (b_controller != value)
            {
                // Set new value
                b_controller = value;

                // Set this model as it's model
                this.Controller.Model = this;
            }
        }
    }

    // Finds for an existing controller, else creates one!
    public virtual BaseController FindSetController()
    {
        if (b_controller != null)
            return b_controller;
        else
            return null; // [Defined in the subclasses] Search for existing or even create one 
    }

    // Finds for an existing controller, else creates one
    public virtual BaseController FindCreateSetController()
    {
        // Looks up for the existence of a view with the same entityID
        if (FindSetController() != null)
            return FindSetController();

        // Not possible yet
        return null;
    }

    // Links the given view to the model (returns false if failed)
    protected bool LinkViewToModel(BaseView view)
    {
        // If there is a view and a controller for this model link them.
        if (view != null && this.FindSetController() != null)
        {
            view.Controller = this.Controller;
            this.Controller.View = view;
            return true;
        }
        else
        {
            // Try to find/create a controller for the view and link those, else return false
            if (view.FindCreateSetController() != null)
            {
                this.Controller = view.Controller;
                this.Controller.View = view;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    // Dispatch EventHandlers for controller
    public event EventHandler<EntityIDChangedEventArgs> OnEntityIDChanged = (sender, e) => { };

    // Dispatch EventHandlers for controller
    //public event EventHandler<ControllerChangedEventArgs> OnControllerChanged = (sender, e) => { };

}
