﻿using System;

// EventHandlers for for objectID changes
public class EntityIDChangedEventArgs : EventArgs { }

// EventHandlers for controller changes
//public class ControllerChangedEventArgs : EventArgs { }

public interface IBaseModel
{
    // Unique indentifier for a object
    string EntityID { get; set; }

    // Controller for sending events and syncing data based on the Model
    BaseController Controller { get; set; }


    //Dispatch EventHandlers for objectID changes
    event EventHandler<EntityIDChangedEventArgs> OnEntityIDChanged;

    // Dispatch EventHandlers for controller changes
    //event EventHandler<ControllerChangedEventArgs> OnControllerChanged;

}
