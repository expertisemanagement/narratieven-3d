﻿using Object.FSM;
using Object.Type;

public class CameraModel : ComplexObjectModel, ICameraModel
{
    // Constructor
    public CameraModel(string entityID) : base(entityID)
    {
        this.b_entityID = entityID;
        this.b_objectType = ObjectTypeEnum.CAMERA;
    }

    // Constructor
    public CameraModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
        if (syncViewToModel == false)
        {
            b_objectType = ((CameraView)Controller.View).ObjectType;
        }
    }

    // Toggle controllablity
    public bool ControllableCamera
    {
        get { return this.Controllable; }
        set
        {
            this.Controllable = value;
        }
    }

    // Controll settings for defining possible movement and there input axes
    public CameraControlModel CameraControlSettings
    {
        get
        {
            if (ControlSettings != null && ControlSettings.GetType() == typeof(CameraControlModel))
                return (CameraControlModel)ControlSettings;
            else
                return null;
        }
        set
        {
            this.ControlSettings = value;
        }
    }
}
