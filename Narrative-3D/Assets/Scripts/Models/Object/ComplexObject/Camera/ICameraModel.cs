﻿internal interface ICameraModel
{
    // Toggle controllablity
    bool ControllableCamera { get; set; }

    // Controll settings for defining possible movement and there input axes
    CameraControlModel CameraControlSettings { get; set; }
}