﻿using Object.FSM;
using Object.Type;
using System;

public class ComplexObjectModel : ObjectModel, IComplexObjectModel
{
    // Constructor
    public ComplexObjectModel(string entityID) : base(entityID)
    {
    }

    // Constructor
    public ComplexObjectModel(string entityID, ObjectTypeEnum objectType) : base(entityID, objectType)
    {
        if (objectType == ObjectTypeEnum.ACTOR)
        {
            b_machineState = ObjectFSMEnum.AIINTERACTIVE;
            b_controllable = true;
        }
    }

    // Constructor
    public ComplexObjectModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
    }

    public string b_customName;
    public string CustomName
    {
        get { return b_customName; }
        set { b_customName = value; }
    }

    // Toggle controllablity
    private bool b_controllable = false;
    public bool Controllable
    {
        get { return b_controllable; }
        set
        {
            // Only if the backing field changes
            if (b_controllable != value)
            {
                // Set new value
                b_controllable = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ControllabilityChangedEventArgs();
                OnControllabilityChanged(this, eventArgs);
            }
        }
    }

    // Controll settings for defining possible movement and there input axes
    private ObjectControlModel b_controlSettings;
    public ObjectControlModel ControlSettings
    {
        get
        {
            if (b_controlSettings != null)
                return b_controlSettings;

            if (this.Controller != null && this.Controller.View != null)
                if(((ComplexObjectView)this.Controller.View).ControlSettings != null)
                    b_controlSettings = ((ComplexObjectView)this.Controller.View).ControlSettings;

            return b_controlSettings;
        }
        set
        {
            // Only if the backing field changes
            if (b_controlSettings != value)
            {
                // Set new value
                b_controlSettings = value;
                
                // Dispatch the deticated "changed" event
                var eventArgs = new ControlSettingsChangedEventArgs();
                OnControlSettingsChanged(this, eventArgs);
            }
        }
    }

    // field for object interaction (interaction state & interaction feedback)
    private ObjectFSMEnum b_machineState = ObjectFSMEnum.STATICINTERACTIVE;
    public ObjectFSMEnum MachineState
    {

        get { return b_machineState; }
        set
        {
            // Only if the backing field changes
            if (b_machineState != value)
            {
                // Set new value
                b_machineState = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new MachineStateChangedEventArgs();
                OnMachineStateChanged(this, eventArgs);
            }
        }
    }
    
    // Dispatch EventHandlers for objectControlSettings
    public event EventHandler<ControllabilityChangedEventArgs> OnControllabilityChanged = (sender, e) => { };
    public event EventHandler<ControlSettingsChangedEventArgs> OnControlSettingsChanged = (sender, e) => { };

    // Dispatch EventHandler for object's machine
    public event EventHandler<MachineStateChangedEventArgs> OnMachineStateChanged = (sender, e) => { };
}
