﻿using Object.FSM;
using System;


// EventHandlers when the object's machinestate changes
public class MachineStateChangedEventArgs : EventArgs { }

// EventHandlers for the objectControlSettings
public class ControllabilityChangedEventArgs : EventArgs { }
public class ControlSettingsChangedEventArgs : EventArgs { }

public interface IComplexObjectModel : IObjectModel {

    // Toggle controllablity
    bool Controllable { get; set; }

    // Controll settings for defining possible movement and there input axes
    ObjectControlModel ControlSettings { get; set; }

    // field for object interaction (interaction state & interaction feedback)
    ObjectFSMEnum MachineState { get; set; }

    // Dispatch EventHandlers for object's machine and GUI state changes
    event EventHandler<MachineStateChangedEventArgs> OnMachineStateChanged;

    // Dispatch EventHandlers for objectControlSettings
    event EventHandler<ControllabilityChangedEventArgs> OnControllabilityChanged;
    event EventHandler<ControlSettingsChangedEventArgs> OnControlSettingsChanged;

}
