﻿using System;
using VDS.RDF;

// Dispatched objectType changes
//public class ObjectTypeURIChangedEventArgs : EventArgs { }

// Dispatched sementic node/URI changes
public class SemanticNodeChangedEventArgs : EventArgs { }
public class ObjectURIChangedEventArgs : EventArgs { }

// Dispatched when the navobject's changes
public class NavObjectChangedEventArgs : EventArgs { }

public interface ISemanticObjectModel : IObjectModel
{
    // The type of object
    //string objectTypeURI { get; set; }

    // Ref to resource node (dotNetRDF "INode" object) from the semantic graph (dotNetRDF "IGraph" object) representation of the RDF (SMW export)
    INode SemanticNode { get; set; }

    // Semantic object ID 
    string ObjectURI { get; set; }
    
    // Navigation object
    NavObjectModel NavObject { get; set; }

    // Dispatched objectType changes
    //event EventHandler<ObjectTypeURIChangedEventArgs> OnObjectTypeURIChanged;

    // Dispatch Semantic refferences changes
    event EventHandler<SemanticNodeChangedEventArgs> OnSemanticNodeChanged;
    event EventHandler<ObjectURIChangedEventArgs> OnObjectURIChanged;

    // Dispatch Object Scene placement (navigation) changes
    event EventHandler<NavObjectChangedEventArgs> OnNavObjectChanged;

}
