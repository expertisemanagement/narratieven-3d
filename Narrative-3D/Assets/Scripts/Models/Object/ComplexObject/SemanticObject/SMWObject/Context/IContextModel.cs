﻿using SMW.EMM.Context;
using System;
using VDS.RDF;

// Dispatched SMW basic pageform information changes
public class SuperContextChangedEventArgs : EventArgs { }
public class SubContextsChangedEventArgs : EventArgs { }
public class ContextTypeChangedEventArgs : EventArgs { }
public class DescriptionChangedEventArgs : EventArgs { }

public interface IContextModel : ISMWObjectModel
{
    // Structured text consist out of existing paragraphheadings en alineas:
    
    INode SuperContextNode { get; set; }

    ContextTypeEnum ContextType { get; set; }

    string Description { get; set; }

    // Dispatch EventHandlers for SWM basic pageform information changes
    event EventHandler<SuperContextChangedEventArgs> OnSuperContextChanged;
    event EventHandler<SubContextsChangedEventArgs> OnSubContextsChanged;
    event EventHandler<ContextTypeChangedEventArgs> OnContextTypeChanged;
    event EventHandler<DescriptionChangedEventArgs> OnDescriptionChanged;

}
