﻿using System;

// Dispatched EventHandlers for category
public class CategoryChangedEventArgs : EventArgs { }

// Dispatched SMW basic pageform information changes
public class ParagraphHeadNLChangedEventArgs : EventArgs { }
public class ParagraphHeadENChangedEventArgs : EventArgs { }
public class AlineasChangedEventArgs : EventArgs { }
public class ImagaryChangedEventArgs : EventArgs { }
public class FreeTextChangedEventArgs : EventArgs { }
public class SummeryChangedEventArgs : EventArgs { }

public interface ISMWObjectModel : ISemanticObjectModel
{
    // Structured text consist out of existing paragraphheadings en alineas:
    
    string ParagraphHeadNL { get; set; }
    
    string ParagraphHeadEN { get; set; }

    Struct_SMW_Alinea[] Alineas { get; set; }

    Struct_SMW_Imagary[] Imagary { get; set; }

    string FreeText { get; set; }

    string Summery { get; set; }

    // Dispatched EventHandlers for category
    event EventHandler<CategoryChangedEventArgs> OnCategoryChanged;

    // Dispatch EventHandlers for SWM basic pageform information changes
    event EventHandler<ParagraphHeadNLChangedEventArgs> OnParagraphHeadNLChanged;
    event EventHandler<ParagraphHeadENChangedEventArgs> OnParagraphHeadENChanged;
    event EventHandler<AlineasChangedEventArgs> OnAlineasChanged;
    event EventHandler<ImagaryChangedEventArgs> OnImagaryChanged;
    event EventHandler<FreeTextChangedEventArgs> OnFreeTextChanged;
    event EventHandler<SummeryChangedEventArgs> OnSummeryChanged;

}
