﻿using System;

// Eventhandler when the selections changes
public class ConsumesChangedEventArgs : EventArgs { }
public class ConsumesThisChangedEventArgs : EventArgs { }
public class ProducesChangedEventArgs : EventArgs { }
public class ProducesThisChangedEventArgs : EventArgs { }
public class ConnectsChangedEventArgs : EventArgs { }

public interface IActivityModel : IIntentionalElementModel
{
    IntentionalElementModel[] Consumes { get; }

    IntentionalElementModel[] Produces { get; }

    Connects[] Connects { get; }

    // Dispatch eventhandler when the ...
    event EventHandler<ConsumesChangedEventArgs> OnConsumesChanged;
    event EventHandler<ConsumesThisChangedEventArgs> OnConsumesThisChanged;
    event EventHandler<ProducesChangedEventArgs> OnProducesChanged;
    event EventHandler<ProducesThisChangedEventArgs> OnProducesThisChanged;
    event EventHandler<ConnectsChangedEventArgs> OnConnectsChanged;
}
