﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameChangedEventArgs : EventArgs { }
public class AddressChangedEventArgs : EventArgs { }
public class HouseNumberChangedEventArgs : EventArgs { }
public class ZipcodeChangedEventArgs : EventArgs { }
public class LocationChangedEventArgs : EventArgs { }
public class PhoneChangedEventArgs : EventArgs { }
public class EmailChangedEventArgs : EventArgs { }
public class WebsiteChangedEventArgs : EventArgs { }
public class InfoChangedEventArgs : EventArgs { }

public interface IActorModel : IIntentionalElementModel
{
    string name { get; set; }

    string Address { get; set; }

    string HouseNumber { get; set; }

    string Zipcode { get; set; }

    string Location { get; set; }

    string Phone { get; set; }

    string Email { get; set; }

    string Website { get; set; }

    string Info { get; set; }

    event EventHandler<NameChangedEventArgs> OnNameChanged;
    event EventHandler<AddressChangedEventArgs> OnAddressChanged;
    event EventHandler<HouseNumberChangedEventArgs> OnHouseNumberChanged;
    event EventHandler<ZipcodeChangedEventArgs> OnZipcodeChanged;
    event EventHandler<LocationChangedEventArgs> OnLocationChanged;
    event EventHandler<PhoneChangedEventArgs> OnPhoneChanged;
    event EventHandler<EmailChangedEventArgs> OnEmailChanged;
    event EventHandler<WebsiteChangedEventArgs> OnWebsiteChanged;
    event EventHandler<InfoChangedEventArgs> OnInfoChanged;
}
