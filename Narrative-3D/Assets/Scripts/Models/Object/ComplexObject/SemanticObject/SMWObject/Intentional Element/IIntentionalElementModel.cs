﻿using SMW.Relation.IntetionalELement;
using System;
using VDS.RDF;

// Eventhandler when the selections changes
public class ContextChangedEventArgs : EventArgs { }
public class IntentionalElementTypeChangedEventArgs : EventArgs { }
public class ElementCompositionTypeChangedEventArgs : EventArgs { }
public class DecompositionTypeChangedEventArgs : EventArgs { }
public class PartOfChangedEventArgs : EventArgs { }
public class PartOfThisChangedEventArgs : EventArgs { }
public class InstanceOfChangedEventArgs : EventArgs { }
public class InstanceOfThisChangedEventArgs : EventArgs { }
public class ConcernsChangedEventArgs : EventArgs { }
public class ConcernsThisChangedEventArgs : EventArgs { }
public class ContributesChangedEventArgs : EventArgs { }
public class DependsChangedEventArgs : EventArgs { }

public interface IIntentionalElementModel : ISMWObjectModel {

    ContextModel Context { get; set; }
    ElementCompositionTypeEnum DecompositionType { get; set; }

    INode[] PartOfNodes { get; set; }
    IntentionalElementModel[] PartOf { get; }

    INode[] InstanceOfNodes { get; set; }
    IntentionalElementModel[] InstanceOf { get; }

    INode[] ConcernsNodes { get; set; }
    IntentionalElementModel[] Concerns { get; }

    Contributes[] Contributes { get; set; }
    Dependency[] Depends { get; set; }

    // Dispatch eventhandler when the ...
    event EventHandler<ContextChangedEventArgs> OnContextChanged;
    event EventHandler<IntentionalElementTypeChangedEventArgs> OnIntentionalElementTypeChanged;
    event EventHandler<ElementCompositionTypeChangedEventArgs> OnElementCompositionTypeChanged;
    event EventHandler<DecompositionTypeChangedEventArgs> OnDecompositionTypeChanged;
    event EventHandler<PartOfChangedEventArgs> OnPartOfChanged;
    event EventHandler<PartOfThisChangedEventArgs> OnPartOfThisChanged;
    event EventHandler<InstanceOfChangedEventArgs> OnInstanceOfChanged;
    event EventHandler<InstanceOfThisChangedEventArgs> OnInstanceOfThisChanged;
    event EventHandler<ConcernsChangedEventArgs> OnConcernsChanged;
    event EventHandler<ContributesChangedEventArgs> OnContributesChanged;
    event EventHandler<DependsChangedEventArgs> OnDependsChanged;
}
