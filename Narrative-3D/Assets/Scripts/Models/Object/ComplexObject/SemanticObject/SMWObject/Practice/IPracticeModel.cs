﻿using SMW.Relation.Practice;
using System;
using System.Collections.Generic;

// Eventhandler when the selections changes
public class PracticeContextChangedEventArgs : EventArgs { }
public class PracticeTypeChangedEventArgs : EventArgs { }
public class SelectionChangedEventArgs : EventArgs { }

public interface IPracticeModel : ISMWObjectModel {

    ContextModel Context { get; set; }

    PracticeTypeEnum PracticeType { get; set; }

    Selects[] Selections { get; set; }

    // Dispatch eventhandler when the selections changes
    event EventHandler<PracticeContextChangedEventArgs> OnPracticeContextChanged;
    event EventHandler<PracticeTypeChangedEventArgs> OnPracticeTypeChanged;
    event EventHandler<SelectionChangedEventArgs> OnSelectionsChanged;

}
