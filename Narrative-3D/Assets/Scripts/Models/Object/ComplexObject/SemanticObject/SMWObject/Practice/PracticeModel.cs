﻿using Object.Type;
using SMW.Relation.Practice;
using System;
using System.Linq;
using VDS.RDF;

public class PracticeModel : SMWObjectModel, IPracticeModel {

    // Constructor
    public PracticeModel(string entityID, INode semanticNode) : base(entityID, semanticNode)
    {
    }

    // Constructor
    public PracticeModel(string entityID, INode semanticNode, SMWCategory smwCategory, NavObjectModel navObject) : base(entityID, semanticNode, smwCategory, navObject)
    {
    }

    // Constructor
    public PracticeModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
    }

    // Constructor
    public PracticeModel(BaseView view, bool syncViewToModel, SMWCategory smwCategory, NavObjectModel navObject) : base(view, syncViewToModel, navObject)
    {
    }


    private ContextModel b_context;
    public ContextModel Context
    {
        get
        {
            if (b_context == null)
            {

                Triple[] relevantTriples = TripleData.Where(t => t.Predicate.ToString() == SMWParser.Instance.GetGraphPropertyURI(SemanticNode.Graph) + "Context").ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                    b_context = null;
                else 
                    b_context = ((ContextModel)SMWParser.Instance.GetCategoryByName(SemanticNode.Graph, "Context").GetSpecificInstanceModel(relevantTriples[0].Object, true)); //OLD -> GameManager.instance.modelsBySementicURI[objectType][relevantTriples[0].Subject.ToString()]);
            }

            return b_context;
        }
        set
        {
            // Only if the backing field changes
            if (b_context != value)
            {
                // Set new value
                b_context = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new PracticeContextChangedEventArgs();
                OnPracticeContextChanged(this, eventArgs);
            }
        }
    }

    private PracticeTypeEnum b_practiceType = PracticeTypeEnum.UNASSIGNED;
    public PracticeTypeEnum PracticeType
    {
        get
        {
            if (b_practiceType == PracticeTypeEnum.UNASSIGNED)
            {

                Triple[] relevantTriples = TripleData.Where(t => t.Predicate.ToString() == SemanticNode.Graph.NamespaceMap.GetNamespaceUri("rdfs") + "Practice_type").ToArray();

                if (relevantTriples == null || relevantTriples.Count() <= 0)
                {
                    b_practiceType = PracticeTypeEnum.UNASSIGNED;
                }
                else
                {
                    switch (SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(relevantTriples[0].Object).ToUpper())
                    {
                        case "UNASSIGNED":
                            b_practiceType = PracticeTypeEnum.UNASSIGNED;
                            break;
                        case "PRACTICE":
                            b_practiceType = PracticeTypeEnum.PRACTICE;
                            break;
                        case "EXPERIENCE":
                            b_practiceType = PracticeTypeEnum.EXPERIENCE;
                            break;
                        default:
                            b_practiceType = PracticeTypeEnum.UNASSIGNED;
                            break;
                    }
                }
            }

            return b_practiceType;
        }
        set
        {
            // Only if the backing field changes
            if (b_practiceType != value)
            {
                // Set new value
                b_practiceType = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new PracticeTypeChangedEventArgs();
                OnPracticeTypeChanged(this, eventArgs);
            }
        }
    }

    private Selects[] b_selections;
    public Selects[] Selections
    {
        get
        {
            if (b_selections == null)
                return SMWParser.Instance.GetSelectsForPracticeNode(SemanticNode).ToArray();
            else
                return b_selections;
        }
        set
        {
            // Only if the backing field changes
            if (b_selections != value)
            {
                // Set new value
                b_selections = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new SelectionChangedEventArgs();
                OnSelectionsChanged(this, eventArgs);
            }
        }
    }

    // Reset tripleDataCaches, by emptying the caches for each tripledata related variable for this object
    protected override void ResetTripleData()
    {
        // Wipe all know cach data and request data (this triggers new caching)

        base.ResetTripleData();

        this.b_context = null;
        this.b_context = this.Context;

        this.b_practiceType = PracticeTypeEnum.UNASSIGNED;
        this.b_practiceType = this.PracticeType;

        this.b_selections = null;
        this.b_selections = this.Selections;
    }

    // Returns all smeantic models that have a relation with this model
    public override SemanticObjectModel GetRelatedSemanticModels()
    {
        return null; // no current relations can be defined
    }

    // Dispatch eventhandler when the selections changes
    public event EventHandler<PracticeContextChangedEventArgs> OnPracticeContextChanged = (sender, e) => { };
    public event EventHandler<PracticeTypeChangedEventArgs> OnPracticeTypeChanged = (sender, e) => { };
    public event EventHandler<SelectionChangedEventArgs> OnSelectionsChanged = (sender, e) => { };

}
