﻿using System;
using System.Collections.Generic;
using VDS.RDF;
using System.Linq;
using Object.Type;
using UnityEngine;

public class SemanticObjectModel : ComplexObjectModel, ISemanticObjectModel
{
    // Constructor
    public SemanticObjectModel(string entityID, INode semanticNode) : base(entityID)
    {
        this.b_semanticNode = semanticNode;
    }

    // Constructor
    public SemanticObjectModel(string entityID, INode semanticNode, NavObjectModel navObject) : base(entityID)
    {
        this.b_semanticNode = semanticNode;
        this.b_navObject = navObject;
    }

    // Constructor
    public SemanticObjectModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
        if (syncViewToModel == false)
            this.b_semanticNode = ((SemanticObjectView)Controller.View).SemanticNode;
    }

    // Constructor
    public SemanticObjectModel(BaseView view, bool syncViewToModel, NavObjectModel navObject) : base(view, syncViewToModel)
    {
        if (syncViewToModel == false)
        {
            this.b_semanticNode = ((SemanticObjectView)Controller.View).SemanticNode;
            this.b_navObject = navObject;
        }
    }
    
    // List of triple holding the data
    protected List<Triple> b_tripleData = new List<Triple>();
    public virtual List<Triple> TripleData
    {
        get
        {
            if (b_tripleData == null || b_tripleData.Count <= 0)
            {
                b_tripleData = SemanticNode.Graph.GetTriplesWithSubject(SemanticNode).ToList();

                // Set all cached tripledata to null
                ResetTripleData();
            }

            return b_tripleData;
        }
        set
        {
            b_tripleData = value;

            // Set all cached tripledata to null
            ResetTripleData();
        }
    }

    // Ref to resource node (dotNetRDF "INode" object) from the semantic graph (dotNetRDF "IGraph" object) representation of the RDF (SMW export)
    //private string b_objectTypeURI;
    //public string objectTypeURI
    //{
    //    get { return b_objectTypeURI; }
    //    set
    //    {
    //        // Only if the backing field changes
    //        if (b_objectTypeURI != value)
    //        {
    //            // Set new value
    //            b_objectTypeURI = value;

    //            // Dispatch the deticated "changed" event
    //            var eventArgs = new ObjectTypeURIChangedEventArgs();
    //            OnObjectTypeURIChanged(this, eventArgs);
    //        }
    //    }
    //}

    // Ref to resource node (dotNetRDF "INode" object) from the semantic graph (dotNetRDF "IGraph" object) representation of the RDF (SMW export)
    private INode b_semanticNode;
    public INode SemanticNode
    {
        get
        {
            if (b_semanticNode != null)
            {
                return b_semanticNode;
            }
            else if (TripleData[0] != null)
            {
                SemanticNode = TripleData[0].Subject;
                return b_semanticNode;
            }
            else
            {
                return null;
            }

        }
        set
        {
            // Only if the backing field changes
            if (b_semanticNode != value)
            {
                // Set new value
                b_semanticNode = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new SemanticNodeChangedEventArgs();
                OnSemanticNodeChanged(this, eventArgs);
            }
        }
    }

    // Semantic object ID 
    private string b_objectURI;
    public string ObjectURI
    {
        get
        {
            if (b_objectURI != null)
            {
                return b_objectURI;
            }
            else if (SemanticNode != null)
            {
                ObjectURI = SemanticNode.ToString();
                return b_objectURI;
            }
            else
                return null;
        }
        set
        {
            // Only if the backing field changes
            if (b_objectURI != value)
            {
                // Set new value
                b_objectURI = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new ObjectURIChangedEventArgs();
                OnObjectURIChanged(this, eventArgs);
            }
        }
    }

    // Returns the name of the node (category uri shall be removed)
    public string ObjectName
    {
        get
        {
            return SMWParser.Instance.RemoveUriPrefixFromNodeValueEnding(SemanticNode);
        }
    }

    // Object's position in the world
    protected NavObjectModel b_navObject;
    public NavObjectModel NavObject
    {
        get { return b_navObject; }
        set
        {
            // Only if the backing field changes
            if (b_navObject != value)
            {
                // Set new value
                b_navObject = value;

                // Dispatch the deticated "changed" event
                var eventArgs = new NavObjectChangedEventArgs();
                OnNavObjectChanged(this, eventArgs);
            }
        }
    }

    // Reset tripleDataCaches, by emptying the caches for each tripledata related variable for this object
    protected virtual void ResetTripleData()
    {
        // No triple related data yet definted
    }

    // Returns all smeantic models that have a relation with this model
    public virtual SemanticObjectModel GetRelatedSemanticModels()
    {
        return null; // no current relations can be defined
    }

    // Dispatch EventHandlers for Semantic refferences changes
    public event EventHandler<SemanticNodeChangedEventArgs> OnSemanticNodeChanged = (sender, e) => { };
    public event EventHandler<ObjectURIChangedEventArgs> OnObjectURIChanged = (sender, e) => { };

    // Dispatch EventHandlers for navobject Scene placement (navigation) changes
    public event EventHandler<NavObjectChangedEventArgs> OnNavObjectChanged = (sender, e) => { };

}
