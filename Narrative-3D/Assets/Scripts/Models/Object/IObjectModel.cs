﻿using System;
using UnityEngine;
using Object.Type;
using Object.GUIstate;
using System.Collections.Generic;

// EventHandlers for when the objects parent or childs changes
public class ParentsChangedEventArgs : EventArgs { }
public class ChildsChangedEventArgs : EventArgs { }

// EventHandlers when the object's position/rotation/scale/state changes in the 
public class PositionChangedEventArgs : EventArgs { }
public class RotationChangedEventArgs : EventArgs { }
public class ScaleChangedEventArgs : EventArgs { }

// EventHandlers when the object's Type- /GUI state changes
public class TypeChangedEventArgs : EventArgs { }
public class GUIStateChangedEventArgs : EventArgs { }

public interface IObjectModel : IBaseModel {

    // Parent ObjectModels
    List<ObjectModel> Parents { get; set; }

    // List of Children ObjectModels
    List<ObjectModel> Childs { get; set; }


    // Object's position in the scene
    Vector3 Position { get; set; }

    // Object's rotation in the scene
    Quaternion Rotation { get; set; }

    // Object's scale in the scene
    Vector3 Scale { get; set; }


    // field that defines the objects Type
    ObjectTypeEnum ObjectType { get; set; }

    // object guiState (for visual feedback on selcing in the navigation or showing it is being focessed) 
    ObjectGUIStateEnum GUIState { get; set; }


    // Dispatch EventHandlers Parent & Child node(s) refferences changes
    event EventHandler<ParentsChangedEventArgs> OnParentsChanged;
    event EventHandler<ChildsChangedEventArgs> OnChildsChanged;

    // Dispatch EventHandlers for object Scene placement () changes
    event EventHandler<PositionChangedEventArgs> OnPositionChanged;
    event EventHandler<RotationChangedEventArgs> OnRotationChanged;
    event EventHandler<ScaleChangedEventArgs> OnScaleChanged;

    // Dispatch EventHandlers for object's GUI state changes
    event EventHandler<TypeChangedEventArgs> OnTypeChanged;
    event EventHandler<GUIStateChangedEventArgs> OnGUIStateChanged;

}
