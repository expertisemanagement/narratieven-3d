﻿using System;

// EventHandlers when the object's machinestate changes
public class SemanticObjectChangedEventArgs : EventArgs { }

public interface INavObjectModel : IObjectModel {

    SemanticObjectModel semanticObject { get; set; }

    // Dispatch EventHandlers for semanticObjectChanged
    event EventHandler<SemanticObjectChangedEventArgs> OnSemanticObjectChanged;

}
