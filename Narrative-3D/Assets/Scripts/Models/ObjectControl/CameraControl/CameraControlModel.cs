﻿using Object.CameraObject.ActionInput;
using Object.CameraZoomInput;
using Object.Movement;
using Object.MovementInput;
using SCamera.State;
using SCamera.Type;
using System;
using System.Collections.Generic;

public class CameraControlModel : ObjectControlModel, ICameraControlModel
{
    // Constructor
    public CameraControlModel(string entityID, ComplexObjectModel complexObject) : base(entityID, complexObject)
    {
    }

    // Constructor
    public CameraControlModel(BaseView view, bool syncViewToModel) : base(view, syncViewToModel)
    {
    }

    /* controls settings */

    // Defines the type of camera. A.k.a it's usage
        public CameraTypeEnum b_cameraType = CameraTypeEnum.FIRSTPERSON;
        public CameraTypeEnum cameraType
                {
            get { return b_cameraType; }
            set
            {
                // Only if the backing field changes
                if (b_cameraType != value)
                {
                // Set new value
                b_cameraType = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new CameraTypeChangedEventArgs();
                    OnCameraTypeChanged(this, eventArgs);
                }
            }
        }

        // Defines the state of camera. A.k.a it's usage
        public CameraStateEnum b_cameraState = CameraStateEnum.MANUAL;
        public CameraStateEnum cameraState
                {
            get { return b_cameraState; }
            set
            {
                // Only if the backing field changes
                if (b_cameraState != value)
                {
                // Set new value
                b_cameraState = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new CameraStateChangedEventArgs();
                    OnCameraStateChanged(this, eventArgs);
                }
            }
        }

        // An Array of "ObjectMovmentInput" object, that define default inputKey for every possible cameraMovement 
        private CameraActionInput[] b_defaultCameraActionInput;
        public CameraActionInput[] defaultCameraActionInput
        {
            get
            {
                if (b_defaultCameraActionInput == null)
                {
                    b_defaultCameraActionInput = new CameraActionInput[7];

                    b_defaultCameraActionInput[0] = new CameraActionInput();
                    b_defaultCameraActionInput[0].actionInput = CameraActionInputEnum.ZOOMING;
                    b_defaultCameraActionInput[0].inputKeyAxes = new List<string>();
                    b_defaultCameraActionInput[0].inputKeyAxes.Add("Depth_Zoom");

                    return b_defaultCameraActionInput;
                }
                else
                {
                    return b_defaultCameraActionInput;
                }
            }
            set
            {
                // Only if the backing field changes
                if (b_defaultCameraActionInput != value)
                {
                    // Set new value
                    b_defaultCameraActionInput = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new DefaultCameraActionInputChangedEventArgs();
                    OnDefaultCameraActionInputChanged(this, eventArgs);
                }
            }
        }

        // An Array of "ObjectMovmentInput" object, that define custom inputKey for every possible cameraMovement 
        private CameraActionInput[] b_customCameraActionInput;
        public CameraActionInput[] customCameraActionInput
        {
            get { return b_customCameraActionInput; }
            set
            {
                // Only if the backing field changes
                if (b_customCameraActionInput != value)
                {
                // Set new value
                b_customCameraActionInput = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new CustomCameraActionInputChangedEventArgs();
                    OnCustomCameraActionInputChanged(this, eventArgs);
                }
            }
        }

        // An Array of "ObjectMovmentInput" object, that all allowed inputKey for every possible cameraMovement of the current camera ( = defaultCameraActionInput/customCameraMovementInput containing only for cameraMovement)
        private CameraActionInput[] b_availableCameraActionInput;
        public CameraActionInput[] availableCameraActionInput
        {
            get { return b_availableCameraActionInput; }
            set
            {
                // Only if the backing field changes
                if (b_availableCameraActionInput != value)
                {
                // Set new value
                b_availableCameraActionInput = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new AvailableCameraActionInputChangedEventArgs();
                    OnAvailableCameraActionInputChanged(this, eventArgs);
                }
            }
        }

        // Defines the not-posible actions of the object
        private CameraActionInputEnum[] b_actionRestrictions;
        public CameraActionInputEnum[] actionRestrictions
        {
            get { return b_actionRestrictions; }
            set
            {
                // Only if the backing field changes
                if (b_actionRestrictions != value)
                {
                    // Set new value
                    b_actionRestrictions = value;

                     // Dispatch the deticated "changed" event
                    var eventArgs = new CameraActionRestrictionsChangedEventArgs();
                    OnCameraActionRestrictionsChanged(this, eventArgs);
                }
            }
        }

    /* Zoom */

        // Zoom speed
        public float b_zoomSpeed = 1.0F;
        public float zoomSpeed
        {
            get { return b_zoomSpeed; }
            set
            {
                // Only if the backing field changes
                if (b_zoomSpeed != value)
                {
                // Set new value
                b_zoomSpeed = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new ZoomSpeedChangedEventArgs();
                    OnZoomSpeedChanged(this, eventArgs);
                }
            }
        }

        //Toggle max. zoom ristriction        
        public float b_startingZoomFactor = 0.0F;
        public float startingZoomFactor
        {
            get { return b_startingZoomFactor; }
            set
            {
                // Only if the backing field changes
                if (b_startingZoomFactor != value)
                {
                    // Set new value
                    b_startingZoomFactor = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new StartingZoomFactorChangedEventArgs();
                    OnStartingZoomFactorChanged(this, eventArgs);
                }
            }
        }


        // Starting zoomfactor
        public bool b_maxZoomRestricted = false;
        public bool maxZoomRestricted
        {
            get { return b_maxZoomRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_maxZoomRestricted != value)
                {
                // Set new value
                b_maxZoomRestricted = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new MaxZoomRestrictedChangedEventArgs();
                    OnMaxZoomRestrictedChanged(this, eventArgs);
                }
            }
        }

        // Max. Zoom factor 
        public float b_maxZoomFactor = 100.0F;
        public float maxZoomFactor
        {
            get { return b_maxZoomFactor; }
            set
            {
                // Only if the backing field changes
                if (b_maxZoomFactor != value)
                {
                // Set new value
                b_maxZoomFactor = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new MaxZoomRestrictedChangedEventArgs();
                    OnMaxZoomRestrictedChanged(this, eventArgs);
                }
            }
        }
        

        //Toggle min. zoom ristriction
        public bool b_minZoomRestricted = false;
        public bool minZoomRestricted
        {
            get { return b_minZoomRestricted; }
            set
            {
                // Only if the backing field changes
                if (b_minZoomRestricted != value)
                {
                // Set new value
                b_minZoomRestricted = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new MinZoomRestrictedChangedEventArgs();
                OnMinZoomRestrictedChanged(this, eventArgs);
                }
            }
        }

        // Min. Zoom factor
        public float b_minZoomFactor = -100.0F;
        public float minZoomFactor
        {
            get { return b_minZoomFactor; }
            set
            {
                // Only if the backing field changes
                if (b_minZoomFactor != value)
                {
                // Set new value
                b_minZoomFactor = value;

                    // Dispatch the deticated "changed" event
                    var eventArgs = new MinZoomFactorChangedEventArgs();
                    OnMinZoomFactorChanged(this, eventArgs);
                }
            }
        }

        protected override void SetDefaultMovementInput()
        {
            b_defaultObjectMovementInput = new ObjectMovementInput[1];

            int arrayIndex = 0;

            //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.MOVELATERAL;
            //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Horizontal_Move");

            //arrayIndex++;

            //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.MOVEVERTICAL;
            //b_defaultObjectMovementInputarrayIndex1].inputKeyAxes = new List<string>();
            //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Vertical_Move");

            //arrayIndex++;

            //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEAROUNDAZIMUTHAL;
            //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Horizontal_RotateAround");

            //arrayIndex++;

            // b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEAROUNDPOLAR;
            //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Vertical_RotateAround");

            //arrayIndex++;

            //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEYAW;
            //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Horizontal_Rotate");

            //arrayIndex++;

            b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEPITCH;
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Vertical_Rotate");

            arrayIndex++;

            //b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            //b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            //b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.MOVELONGITUDINAL;
            //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            //b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Depth_Move");

            arrayIndex++;
    }


    //protected override ObjectMovementInput[] GetDefaultObjectMovementInput()
    //{
    //    ObjectMovementInput[] newDefaultObjectMovementInput = new ObjectMovementInput[7];

    //    defaultObjectMovementInput[0] = new ObjectMovementInput();
    //    defaultObjectMovementInput[0].moveByForce = false;
    //    defaultObjectMovementInput[0].movement = ObjectMovementEnum.MOVELATERAL;
    //    defaultObjectMovementInput[0].inputKeyAxes = new List<string>();
    //    defaultObjectMovementInput[0].inputKeyAxes.Add("Horizontal_Move");

    //    defaultObjectMovementInput[1] = new ObjectMovementInput();
    //    defaultObjectMovementInput[1].moveByForce = false;
    //    defaultObjectMovementInput[1].movement = ObjectMovementEnum.MOVEVERTICAL;
    //    defaultObjectMovementInput[1].inputKeyAxes = new List<string>();
    //    defaultObjectMovementInput[1].inputKeyAxes.Add("Vertical_Move");

    //    defaultObjectMovementInput[2] = new ObjectMovementInput();
    //    defaultObjectMovementInput[2].moveByForce = false;
    //    defaultObjectMovementInput[2].movement = ObjectMovementEnum.ROTATEAROUNDAZIMUTHAL;
    //    defaultObjectMovementInput[2].inputKeyAxes = new List<string>();
    //    defaultObjectMovementInput[2].inputKeyAxes.Add("Horizontal_RotateAround");

    //    defaultObjectMovementInput[3] = new ObjectMovementInput();
    //    defaultObjectMovementInput[3].moveByForce = false;
    //    defaultObjectMovementInput[3].movement = ObjectMovementEnum.ROTATEAROUNDPOLAR;
    //    defaultObjectMovementInput[3].inputKeyAxes = new List<string>();
    //    defaultObjectMovementInput[3].inputKeyAxes.Add("Vertical_RotateAround");

    //    defaultObjectMovementInput[4] = new ObjectMovementInput();
    //    defaultObjectMovementInput[4].moveByForce = false;
    //    defaultObjectMovementInput[4].movement = ObjectMovementEnum.ROTATEYAW;
    //    defaultObjectMovementInput[4].inputKeyAxes = new List<string>();
    //    defaultObjectMovementInput[4].inputKeyAxes.Add("Horizontal_Rotate");

    //    defaultObjectMovementInput[5] = new ObjectMovementInput();
    //    defaultObjectMovementInput[5].moveByForce = false;
    //    defaultObjectMovementInput[5].movement = ObjectMovementEnum.ROTATEPITCH;
    //    defaultObjectMovementInput[5].inputKeyAxes = new List<string>();
    //    defaultObjectMovementInput[5].inputKeyAxes.Add("Vertical_Rotate");

    //    defaultObjectMovementInput[6] = new ObjectMovementInput();
    //    defaultObjectMovementInput[6].moveByForce = false;
    //    defaultObjectMovementInput[6].movement = ObjectMovementEnum.MOVELONGITUDINAL;
    //    defaultObjectMovementInput[6].inputKeyAxes = new List<string>();
    //    defaultObjectMovementInput[6].inputKeyAxes.Add("Depth_Move");

    //    return newDefaultObjectMovementInput;
    //}

    // Dispatch EventHandlers for when the camera type or state changes
    public event EventHandler<CameraTypeChangedEventArgs> OnCameraTypeChanged = (sender, e) => { };
    public event EventHandler<CameraStateChangedEventArgs> OnCameraStateChanged = (sender, e) => { };

    // Dispatch EventHandlers for camera zoomingcontrol changes
    public event EventHandler<DefaultCameraActionInputChangedEventArgs> OnDefaultCameraActionInputChanged = (sender, e) => { };
    public event EventHandler<CustomCameraActionInputChangedEventArgs> OnCustomCameraActionInputChanged = (sender, e) => { };
    public event EventHandler<AvailableCameraActionInputChangedEventArgs> OnAvailableCameraActionInputChanged = (sender, e) => { };
    public event EventHandler<CameraActionRestrictionsChangedEventArgs> OnCameraActionRestrictionsChanged = (sender, e) => { };

    // Dispatch EventHandlers for camera zoomingcontrol changes
    public event EventHandler<ZoomSpeedChangedEventArgs> OnZoomSpeedChanged = (sender, e) => { };
    public event EventHandler<StartingZoomFactorChangedEventArgs> OnStartingZoomFactorChanged = (sender, e) => { };
    public event EventHandler<MaxZoomRestrictedChangedEventArgs> OnMaxZoomRestrictedChanged = (sender, e) => { };
    public event EventHandler<MaxZoomFactorChangedEventArgs> OnMaxZoomFactorChanged = (sender, e) => { };
    public event EventHandler<MinZoomRestrictedChangedEventArgs> OnMinZoomRestrictedChanged = (sender, e) => { };
    public event EventHandler<MinZoomFactorChangedEventArgs> OnMinZoomFactorChanged = (sender, e) => { };
}
