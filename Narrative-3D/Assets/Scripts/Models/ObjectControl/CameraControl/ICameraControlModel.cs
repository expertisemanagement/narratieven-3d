﻿using System;

// Dispatch EventHandlers for when the camera type or state changes
public class CameraTypeChangedEventArgs : EventArgs { }
public class CameraStateChangedEventArgs : EventArgs { }

// Dispatch EventHandlers for camera actionsInput
public class DefaultCameraActionInputChangedEventArgs : EventArgs { }
public class CustomCameraActionInputChangedEventArgs : EventArgs { }
public class AvailableCameraActionInputChangedEventArgs : EventArgs { }
public class CameraActionRestrictionsChangedEventArgs : EventArgs { }

// Dispatch EventHandlers for camera zoomingcontrol changes
public class ZoomSpeedChangedEventArgs : EventArgs { }
public class StartingZoomFactorChangedEventArgs : EventArgs { }
public class MaxZoomRestrictedChangedEventArgs : EventArgs { }
public class MaxZoomFactorChangedEventArgs : EventArgs { }
public class MinZoomRestrictedChangedEventArgs : EventArgs { }
public class MinZoomFactorChangedEventArgs : EventArgs { }

public interface ICameraControlModel : IObjectControlModel {

    // Dispatch EventHandlers for when the camera type or state changes
    event EventHandler<CameraTypeChangedEventArgs> OnCameraTypeChanged;
    event EventHandler<CameraStateChangedEventArgs> OnCameraStateChanged;

    // Dispatch EventHandlers for camera actionsInput
    event EventHandler<DefaultCameraActionInputChangedEventArgs> OnDefaultCameraActionInputChanged;
    event EventHandler<CustomCameraActionInputChangedEventArgs> OnCustomCameraActionInputChanged;
    event EventHandler<AvailableCameraActionInputChangedEventArgs> OnAvailableCameraActionInputChanged;
    event EventHandler<CameraActionRestrictionsChangedEventArgs> OnCameraActionRestrictionsChanged;

    // Dispatch EventHandlers for camera zoomingcontrol changes
    event EventHandler<ZoomSpeedChangedEventArgs> OnZoomSpeedChanged;
    event EventHandler<StartingZoomFactorChangedEventArgs> OnStartingZoomFactorChanged;
    event EventHandler<MaxZoomRestrictedChangedEventArgs> OnMaxZoomRestrictedChanged;
    event EventHandler<MaxZoomFactorChangedEventArgs> OnMaxZoomFactorChanged;
    event EventHandler<MinZoomRestrictedChangedEventArgs> OnMinZoomRestrictedChanged;
    event EventHandler<MinZoomFactorChangedEventArgs> OnMinZoomFactorChanged;

}
