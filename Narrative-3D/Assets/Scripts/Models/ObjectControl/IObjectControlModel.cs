﻿using Object.ControlSetup;
using Object.Movement;
using Object.MovementInput;
using Object.MovementRestriction;
using System;

// Dispatch EventHandler for controls settings changes
public class DefaultObjectMovementInputChangedEventArgs : EventArgs { }
public class CustomObjectMovementInputChangedEventArgs : EventArgs { }
public class AvailableObjectMovementInputChangedEventArgs : EventArgs { }

public class ControlSetupChangedEventArgs : EventArgs { }
public class MovementChangedEventArgs : EventArgs { }
public class MovementRestrictionsChangedEventArgs : EventArgs { }

// Dispatch EventHandler for axial Movement changes
public class LongitudinalMovementSpeedChangedEventArgs : EventArgs { }
public class StartingDistanceLongitudinalMovementChangedEventArgs : EventArgs { }
public class MaxLongitudinalMoveDistanceRestrictedChangedEventArgs : EventArgs { }
public class MaxLongitudinalMoveDistanceChangedEventArgs : EventArgs { }
public class MinLongitudinalMoveDistanceRestrictedChangedEventArgs : EventArgs { }
public class b_minLongitudinalMoveDistanceChangedEventArgs : EventArgs { }

public class VerticalMovementSpeedChangedEventArgs : EventArgs { }
public class StartingDistanceVerticalMovementChangedEventArgs : EventArgs { }
public class MaxVerticalMoveDistanceRestrictedChangedEventArgs : EventArgs { }
public class MaxVerticalMoveDistanceChangedEventArgs : EventArgs { }
public class MinVerticalMoveDistanceRestrictedChangedEventArgs : EventArgs { }
public class MinVerticalMoveDistanceChangedEventArgs : EventArgs { }

public class LateralMovementSpeedChangedEventArgs : EventArgs { }
public class StartingDistanceLateralMovementChangedEventArgs : EventArgs { }
public class MaxLateralMoveDistanceRestrictedChangedEventArgs : EventArgs { }
public class MaxLateralMoveDistanceChangedEventArgs : EventArgs { }
public class MinLateralMoveDistanceRestrictedChangedEventArgs : EventArgs { }
public class MinLateralMoveDistanceChangedEventArgs : EventArgs { }

// Dispatch EventHandler for radial movement/Rotation changes
public class PitchRotationSpeedChangedEventArgs : EventArgs { }
public class PitchRotationAngleRestrictedChangedEventArgs : EventArgs { }
public class StartingPitchRotationAngleChangedEventArgs : EventArgs { }
public class MaxPitchNegRotationAngleChangedEventArgs : EventArgs { }
public class MaxPitchPosRotationAngleChangedEventArgs : EventArgs { }

public class YawRotationSpeedChangedEventArgs : EventArgs { }
public class YawRotationAngleRestrictedChangedEventArgs : EventArgs { }
public class StartingYawRotationAngleChangedEventArgs : EventArgs { }
public class MaxYawNegRotationAngleChangedEventArgs : EventArgs { }
public class MaxYawPosRotationAngleChangedEventArgs : EventArgs { }

public class RollRotationSpeedChangedEventArgs : EventArgs { }
public class RollRotationAngleRestrictedChangedEventArgs : EventArgs { }
public class StartingRollRotationAngleChangedEventArgs : EventArgs { }
public class MaxRollNegRotationAngleChangedEventArgs : EventArgs { }
public class MaxRollPosRotationAngleChangedEventArgs : EventArgs { }

// Dispatch EventHandler for radial movement around/Rotation around the subject changes
public class RadiusChangeSpeedChangedEventArgs : EventArgs { }
public class RadiusRestrictedChangedEventArgs : EventArgs { }
public class MaxRadiusExpansionChangedEventArgs : EventArgs { }
public class MinRadiusFromCenterChangedEventArgs : EventArgs { } 

// Dispatch the deticated "changed" event
public class PolarAngleCoordinatesChangeSpeedChangedEventArgs : EventArgs { }
public class PolarRotationAroundAngleRestrictedChangedEventArgs : EventArgs { }
public class StartingPolarRotationAroundAngleChangedEventArgs : EventArgs { }
public class MaxPolarNegRotationAroundAngleChangedEventArgs : EventArgs { }
public class MaxPolarPosRotationAroundAngleChangedEventArgs : EventArgs { }

public class AzimuthalAngleCoordinatesChangeSpeedChangedEventArgs : EventArgs { }
public class AzimuthalRotationAroundAngleRestrictedChangedEventArgs : EventArgs { }
public class StartingAzimuthalRotationAroundAngleChangedEventArgs : EventArgs { }
public class MaxAzimuthalNegRotationAroundAngleChangedEventArgs : EventArgs { }
public class MaxAzimuthalPosRotationAroundAngleChangedEventArgs : EventArgs { }

// Dispatch EventHandler for interaction and control targets changes 
public class SubjectChangedEventArgs : EventArgs { }
public class DedicatedObjectChangedEventArgs : EventArgs { }
public class TargetChangedEventArgs : EventArgs { }

public interface IObjectControlModel : IBaseModel
{

     /* controls settings */
    
        // An Array of "ObjectMovmentInput" object, that define default inputKey for every possible cameraMovement 
        ObjectMovementInput[] defaultObjectMovementInput { get; set; }

        // An Array of "ObjectMovmentInput" object, that define custom inputKey for every possible cameraMovement 
        ObjectMovementInput[] customObjectMovementInput { get; set; }

        // An Array of "ObjectMovmentInput" object, that all allowed inputKey for every possible cameraMovement of the current camera ( = defaultObjectMovementInput/customCameraMovementInput containing only for cameraMovement)
        ObjectMovementInput[] availableObjectMovementInput { get; set; }

        // Defines the objects input controls and the movement they control
        ControlSetupEnum controlSetup { get; set; }

        // Defines the posible movement of the object
        ObjectMovementEnum[] movement { get; set; }

        // Defines the not-posible movement of the object
        ObjectMovementRestrictionEnum[] movementRestrictions { get; set; }

    /* Axial Movement */

        // Longitudinal movement speed
        float longitudinalMovementSpeed { get; set; }

        // Starting location longitudinal distance
        float startingDistanceLongitudinalMovement { get; set; }

        // Toggle max. lateral movement ristriction
        bool maxLongitudinalMoveDistanceRestricted { get; set; }

        // Max. lateral move distance
        float maxLongitudinalMoveDistance { get; set; }

        //Toggle min. lateral movement ristriction
        bool minLongitudinalMoveDistanceRestricted { get; set; }

        // Min. lateral move distance
        float minLongitudinalMoveDistance { get; set; }


        // Vertical movement speed
        float verticalMovementSpeed { get; set; }

        // Starting location vertical distance
        float startingDistanceVerticalMovement { get; set; }

        // Toggle max. vertical movement ristriction
        bool maxVerticalMoveDistanceRestricted { get; set; }

        // Max. vertical move distance
        float maxVerticalMoveDistance { get; set; }

        //Toggle min. vertical movement ristriction
        bool minVerticalMoveDistanceRestricted  { get; set; }

        // Min. vertical move distance
        float minVerticalMoveDistance  { get; set; }


        // Lateral  movement speed
        float lateralMovementSpeed { get; set; }

        // Starting location lateral distance
        float startingDistanceLateralMovement { get; set; }

        // Toggle max. vertical movement ristriction
        bool maxLateralMoveDistanceRestricted { get; set; }

        // Max. vertical move distance
        float maxLateralMoveDistance { get; set; }

        //Toggle min. vertical movement ristriction
        bool minLateralMoveDistanceRestricted { get; set; }

        // Min. vertical move distance
        float minLateralMoveDistance { get; set; }


    /* Radial movement/Rotation on this object. Based on aircraft principal axes pitch (x-axes rotation), yaw (y-axes rotation), roll (z-axes rotation) */

        // Pitch rotation around speed 
        float pitchRotationSpeed { get; set; }

        // Toggle max. pitch rotation angle ristriction
        bool pitchRotationAngleRestricted { get; set; }

        // Starting pitch rotation angle
        float startingPitchRotationAngle { get; set; }

        // Max. backward pitch rotation angle
        float maxPitchNegRotationAngle { get; set; }

        // Max. forward pitch rotation angle
        float maxPitchPosRotationAngle  { get; set; }

        // Yaw rotation speed 
        float yawRotationSpeed { get; set; }

        // Toggle max. yaw angle rotation around  ristriction
        bool yawRotationAngleRestricted  { get; set; }

        // Starting yaw rotation angle
        float startingYawRotationAngle { get; set; }

        // Max. down angle rotation around 
        float maxYawNegRotationAngle { get; set; }

        // Max. up angle rotation around 
        float maxYawPosRotationAngle { get; set; }


        // Roll rotation speed 
        float rollRotationSpeed { get; set; }

        // Toggle max. roll rotation angle ristriction
        bool rollRotationAngleRestricted { get; set; }

        // Starting roll rotation angle
        float startingRollRotationAngle { get; set; }

        // Max. left roll rotation angle 
        float maxRollNegRotationAngle  { get; set; }

        // Max. right roll rotation angle  
        float maxRollPosRotationAngle { get; set; }

    /* Radial movement around/Rotation around subject. Based on the spherical coordinate system: radius (r), polar angle θ(theta), and azimuthal angle φ(phi). */

        // Radial distance rotation change speed 
        float radiusChangeSpeed  { get; set; }

        // Toggle max. horizontal rotation around angle ristriction
        bool radiusRestricted { get; set; }

        // Max. radial distance around expansion from starting coordinate
        float maxRadiusExpansion { get; set; }

        // Min. radial distance around from objects from center coordinate
        float minRadiusFromCenter { get; set; }


        // Polar angle rotation around speed (This is horizontal or as in geography "latitude")
        float polarAngleCoordinatesChangeSpeed { get; set; }

        // Toggle max. polar angle rotation around ristriction
        bool polarRotationAroundAngleRestricted { get; set; }

        // Starting angle rotation around polar angle
        float startingPolarRotationAroundAngle { get; set; }

        // Max. Down rotation around polar angle
        float maxPolarNegRotationAroundAngle { get; set; }

        // Max. Up rotation around polar angle
        float maxPolarPosRotationAroundAngle { get; set; }


        // Azimuthal rotation around (This is vertical or as in geography "longtitude")
        float azimuthalAngleCoordinatesChangeSpeed { get; set; }

        // Toggle max. azimuthal rotation around angle ristriction
        bool azimuthalRotationAroundAngleRestricted { get; set; }

        // Starting angle rotation around azimuthal angle
        float startingAzimuthalRotationAroundAngle { get; set; }

        // Max. left rotation around azimuthal angle
        float maxAzimuthalNegRotationAroundAngle { get; set; }

        // Max. right rotation around azimuthal angle
        float maxAzimuthalPosRotationAroundAngle { get; set; }


    /* Targets for interaction and control */

        // Object that holds the controlView
        ComplexObjectModel controlWielder { get; set; }

        // Indicates the GameObject which controls this object
        ObjectModel subject { get; set; }

        // Indicates the currently controlled object
        ObjectModel dedicatedObject { get; set; }

        // Backing field for the interactable object of intressed
        ObjectModel target { get; set; }

    // Dispatch EventHandler for controls settings changes
    event EventHandler<DefaultObjectMovementInputChangedEventArgs> OnDefaultObjectMovementInputChanged;
    event EventHandler<CustomObjectMovementInputChangedEventArgs> OnCustomObjectMovementInputChanged;
    event EventHandler<AvailableObjectMovementInputChangedEventArgs> OnAvailableObjectMovementInputChanged;

    event EventHandler<ControlSetupChangedEventArgs> OnControlSetupChanged;
    event EventHandler<MovementChangedEventArgs> OnMovementChanged;
    event EventHandler<MovementRestrictionsChangedEventArgs> OnMovementRestrictionsChanged;

    // Dispatch EventHandler for axial Movement changes
    event EventHandler<LongitudinalMovementSpeedChangedEventArgs> OnLongitudinalMovementSpeedChanged;
    event EventHandler<StartingDistanceLongitudinalMovementChangedEventArgs> OnStartingDistanceLongitudinalMovementChanged;
    event EventHandler<MaxLongitudinalMoveDistanceRestrictedChangedEventArgs> OnMaxLongitudinalMoveDistanceRestrictedChanged;
    event EventHandler<MaxLongitudinalMoveDistanceChangedEventArgs> OnMaxLongitudinalMoveDistanceChanged;
    event EventHandler<MinLongitudinalMoveDistanceRestrictedChangedEventArgs> OnMinLongitudinalMoveDistanceRestrictedChanged;
    event EventHandler<b_minLongitudinalMoveDistanceChangedEventArgs> OnMinLongitudinalMoveDistanceChanged;

    event EventHandler<VerticalMovementSpeedChangedEventArgs> OnVerticalMovementSpeedChanged;
    event EventHandler<StartingDistanceVerticalMovementChangedEventArgs> OnStartingDistanceVerticalMovementChanged;
    event EventHandler<MaxVerticalMoveDistanceRestrictedChangedEventArgs> OnMaxVerticalMoveDistanceRestrictedChanged;
    event EventHandler<MaxVerticalMoveDistanceChangedEventArgs> OnMaxVerticalMoveDistanceChanged;
    event EventHandler<MinVerticalMoveDistanceRestrictedChangedEventArgs> OnMinVerticalMoveDistanceRestrictedChanged;
    event EventHandler<MinVerticalMoveDistanceChangedEventArgs> OnMinVerticalMoveDistanceChanged;

    event EventHandler<LateralMovementSpeedChangedEventArgs> OnLateralMovementSpeedChanged;
    event EventHandler<StartingDistanceLateralMovementChangedEventArgs> OnStartingDistanceLateralMovementChanged;
    event EventHandler<MaxLateralMoveDistanceRestrictedChangedEventArgs> OnMaxLateralMoveDistanceRestrictedChanged;
    event EventHandler<MaxLateralMoveDistanceChangedEventArgs> OnMaxLateralMoveDistanceChanged;
    event EventHandler<MinLateralMoveDistanceRestrictedChangedEventArgs> OnMinLateralMoveDistanceRestrictedChanged;
    event EventHandler<MinLateralMoveDistanceChangedEventArgs> OnMinLateralMoveDistanceChanged;

    // Dispatch EventHandler for radial movement/Rotation changes
    event EventHandler<PitchRotationSpeedChangedEventArgs> OnPitchRotationSpeedChanged;
    event EventHandler<PitchRotationAngleRestrictedChangedEventArgs> OnPitchRotationAngleRestrictedChanged;
    event EventHandler<StartingPitchRotationAngleChangedEventArgs> OnStartingPitchRotationAngleChanged;
    event EventHandler<MaxPitchNegRotationAngleChangedEventArgs> OnMaxPitchNegRotationAngleChanged;
    event EventHandler<MaxPitchPosRotationAngleChangedEventArgs> OnMaxPitchPosRotationAngleChanged;

    event EventHandler<YawRotationSpeedChangedEventArgs> OnYawRotationSpeedChanged;
    event EventHandler<YawRotationAngleRestrictedChangedEventArgs> OnYawRotationAngleRestrictedChanged;
    event EventHandler<StartingYawRotationAngleChangedEventArgs> OnStartingYawRotationAngleChanged;
    event EventHandler<MaxYawNegRotationAngleChangedEventArgs> OnMaxYawNegRotationAngleChanged;
    event EventHandler<MaxYawPosRotationAngleChangedEventArgs> OnMaxYawPosRotationAngleChanged;

    event EventHandler<RollRotationSpeedChangedEventArgs> OnRollRotationSpeedChanged;
    event EventHandler<RollRotationAngleRestrictedChangedEventArgs> OnRollRotationAngleRestrictedChanged;
    event EventHandler<StartingRollRotationAngleChangedEventArgs> OnStartingRollRotationAngleChanged;
    event EventHandler<MaxRollNegRotationAngleChangedEventArgs> OnMaxRollNegRotationAngleChanged;
    event EventHandler<MaxRollPosRotationAngleChangedEventArgs> OnMaxRollPosRotationAngleChanged;

    // Dispatch EventHandler for radial movement around/Rotation around the subject changes
    event EventHandler<RadiusChangeSpeedChangedEventArgs> OnRadiusChangeSpeedChanged;
    event EventHandler<RadiusRestrictedChangedEventArgs> OnRadiusRestrictedChanged;
    event EventHandler<MaxRadiusExpansionChangedEventArgs> OnMaxRadiusExpansionChanged;
    event EventHandler<MinRadiusFromCenterChangedEventArgs> OnMinRadiusFromCenterChanged; // Dispatch the deticated "changed" event

    event EventHandler<PolarAngleCoordinatesChangeSpeedChangedEventArgs> OnPolarAngleCoordinatesChangeSpeedChanged;
    event EventHandler<PolarRotationAroundAngleRestrictedChangedEventArgs> OnPolarRotationAroundAngleRestrictedChanged;
    event EventHandler<StartingPolarRotationAroundAngleChangedEventArgs> OnStartingPolarRotationAroundAngleChanged;
    event EventHandler<MaxPolarNegRotationAroundAngleChangedEventArgs> OnMaxPolarNegRotationAroundAngleChanged;
    event EventHandler<MaxPolarPosRotationAroundAngleChangedEventArgs> OnMaxPolarPosRotationAroundAngleChanged;

    event EventHandler<AzimuthalAngleCoordinatesChangeSpeedChangedEventArgs> OnAzimuthalAngleCoordinatesChangeSpeedChanged;
    event EventHandler<AzimuthalRotationAroundAngleRestrictedChangedEventArgs> OnAzimuthalRotationAroundAngleRestrictedChanged;
    event EventHandler<StartingAzimuthalRotationAroundAngleChangedEventArgs> OnStartingAzimuthalRotationAroundAngleChanged;
    event EventHandler<MaxAzimuthalNegRotationAroundAngleChangedEventArgs> OnMaxAzimuthalNegRotationAroundAngleChanged;
    event EventHandler<MaxAzimuthalPosRotationAroundAngleChangedEventArgs> OnMaxAzimuthalPosRotationAroundAngleChanged;

    // Dispatch EventHandler for interaction and control targets changes 
    event EventHandler<SubjectChangedEventArgs> OnSubjectChanged;
    event EventHandler<DedicatedObjectChangedEventArgs> OnDedicatedObjectChanged;
    event EventHandler<TargetChangedEventArgs> OnTargetChanged;
}
