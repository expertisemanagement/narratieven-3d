﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VDS.RDF;
using Object.Type;
using SMW.EMM.Context;
using System.Linq;

/// <summary>
/// This is a class for a for the NarrativeMenu. 
/// This Menu allows the user to choose which semantic Grarphs, context and roles should be used in the world.
/// </summary>
public class NarrativeMenu : InterfaceController
{
    // GameObject of the Step 2 - Dropdown for selecting a specific Graph or set of Graphs
    [Header("Dropdown for the selection of a Graphs")]
    [Tooltip("Dropdown for selecting a Graph to use")]
    [SerializeField] public GameObject b_stepOneDropdown;
    [HideInInspector] private GameObject stepOneDropdown
    {
        get
        {
            if (b_stepOneDropdown != null) return b_stepOneDropdown;
            else return gameObject.transform.Find("StepOne").Find("Dropdown").gameObject;
        }
        set { stepOneDropdown = value; }
    }

    // GameObject of the Step 2 - Dropdown for selecting a specific context
    [Header("Dropdown for the selection of a context")]
    [Tooltip("Dropdown for selecting a context to simulate")]
    [SerializeField] public GameObject b_stepTwoDropdown;
    [HideInInspector] private GameObject stepTwoDropdown
    {
        get
        {
            if (b_stepTwoDropdown != null) return b_stepTwoDropdown;
            else return gameObject.transform.Find("StepTwo").Find("Dropdown").gameObject;
        }
        set { stepTwoDropdown = value; }
    }

    // GameObject of the Step 3 - Dropdown for selecting a specific role
    [Header("Dropdown for the selection of a role")]
    [Tooltip("Dropdown for selecting role to follow")]
    [SerializeField] public GameObject b_stepThreeDropdown;
    [HideInInspector] private GameObject stepThreeDropdown
    {
        get
        {
            if (b_stepThreeDropdown != null) return b_stepThreeDropdown;
            else return gameObject.transform.Find("StepThree").Find("Dropdown").gameObject;
        }
        set { stepTwoDropdown = value; }
    }

    // GameObject of the Step 4 - Startbutton
    [Header("Dropdown for the selection of a role")]
    [Tooltip("Dropdown for selecting a role to follow")]
    [SerializeField] public GameObject b_stepFourButton;
    [HideInInspector] private GameObject stepFourButton
    {
        get
        {
            if (b_stepFourButton != null) return b_stepFourButton;
            else return gameObject.transform.Find("StepFour").Find("Button").gameObject;
        }
        set { stepFourButton = value; }
    }

    // Loaded Graphs for the b_stepOneDropdown
    private List<IGraph> listGraphs = new List<IGraph>();

    // Existing Contexts for the b_stepTwoDropdown
    private List<SMWObjectModel> smwObjectModels = new List<SMWObjectModel>();

    // List of all possible Actors/Rolls for the b_stepThreeDropdown
    private List<SMWObjectModel> knowCharacters = new List<SMWObjectModel>();

    // Updates all Graph choices and choices that depend on the chosen graph
    public void UpdateAllMenuChoices()
    {
        SetGraphChoicesDropdown();
    }

    // Toggle interaction state of the start/initiate button. This will enable when the required variables for levelloading are compleet.
    public void ToggleStartButton()
    {
        if (stepOneDropdown.GetComponent<Dropdown>().options.Count > 0 && stepTwoDropdown.GetComponent<Dropdown>().options.Count > 0 && stepThreeDropdown.GetComponent<Dropdown>().options.Count > 0)
            stepFourButton.GetComponent<Button>().interactable = true;
        else
            stepFourButton.GetComponent<Button>().interactable = false;
    }

    // Refreshes the Graph options
    public void OnGraphDropdownValueChanged()
    {
        SetContextDropdown();
    }

    // Refreshes the Context options
    public void OnContextDropdownValueChanged()
    {
        SetProtagonistDropdown();
    }

    // Refreshes the Role options
    public void OnProtagonistDropdownValueChanged()
    {
        ToggleStartButton();
    }

    // Set the world and protagonist and disable hide this menu
    public void OnStartButtonClick()
    {
        // Get step 2 choice (= the Context)
        SMWObjectModel worldSMWObject = smwObjectModels.Find(smw => smw.ObjectName == stepTwoDropdown.GetComponent<Dropdown>().options[stepTwoDropdown.GetComponent<Dropdown>().value].text);

        // Get step 3 choice (= the protagonist)
        SMWObjectModel protagonist = knowCharacters.Find(c => c.ObjectName == stepThreeDropdown.GetComponent<Dropdown>().options[stepThreeDropdown.GetComponent<Dropdown>().value].text);

        // load world
        WorldManager.instance.CreateNewWorldAndWipeOldWorld(protagonist, false, worldSMWObject, false, true, false);

        // Hide this interface
        this.ToggleVisablity();
    }

    // Unity event for when this object enables
    public void OnEnable()
    {
        UpdateAllMenuChoices();
    }

    // Sets the options of the project dorpdown
    private void SetGraphChoicesDropdown()
    {
        //  Clear the current dropdown list
        stepOneDropdown.GetComponent<Dropdown>().ClearOptions();

        // Create a list of save names and add these to the options 
        List<string> graphNames = new List<string>();

        listGraphs = SMWParser.Instance.GetLoadedGraphs();
        foreach (IGraph g in listGraphs)
            graphNames.Add(g.ToString());

        if (graphNames.Count <= 0)
        {
            stepOneDropdown.GetComponent<Dropdown>().interactable = false;
            graphNames.Add("No Graphs found");
        }
        else
        {
            stepOneDropdown.GetComponent<Dropdown>().interactable = true;
            graphNames.Add("All Graphs");
        }

        stepOneDropdown.GetComponent<Dropdown>().AddOptions(graphNames);

        // Update other choices from next step
        SetContextDropdown();
    }

    // Sets the options of the project dorpdown
    private void SetContextDropdown()
    {
        //  Clear the current dropdown list
        stepTwoDropdown.GetComponent<Dropdown>().ClearOptions();

        // Create a list of save names and add these to the options 
        List<SMWObjectModel> listOfSMWObjectModels = new List<SMWObjectModel>();
        string stepTwoChoice = stepOneDropdown.GetComponent<Dropdown>().options[stepOneDropdown.GetComponent<Dropdown>().value].text;
        if (stepTwoChoice != "All Graphs")
        {
            smwObjectModels = SMWParser.Instance.GetObjectTypeBaseCategory(listGraphs.Find(g => g.ToString() == stepTwoChoice), ObjectTypeEnum.CONTEXT).GetAllnstanceModelsFromThisCategoryAndSubCategoriesUnOrdered();
        }
        else
        {
            foreach (IGraph g in listGraphs)
                if (listGraphs.Contains(g) == true)
                    smwObjectModels = SMWParser.Instance.GetObjectTypeBaseCategory(listGraphs[listGraphs.IndexOf(g)], ObjectTypeEnum.CONTEXT).GetAllnstanceModelsFromThisCategoryAndSubCategoriesUnOrdered();
        }


        List<string> listSMWObjectNames = new List<string>();
        foreach (SMWObjectModel smwObjectModel in smwObjectModels)
            listSMWObjectNames.Add(smwObjectModel.ObjectName);

        if (listSMWObjectNames.Count <= 0)
        {
            stepTwoDropdown.GetComponent<Dropdown>().interactable = false;
            listSMWObjectNames.Add("No Contexts found");
        }
        else
        {
            stepTwoDropdown.GetComponent<Dropdown>().interactable = true;
        }

        stepTwoDropdown.GetComponent<Dropdown>().AddOptions(listSMWObjectNames);

        // Update other choices from next step
        SetProtagonistDropdown();
    }

    // Sets the options of the project dorpdown
    private void SetProtagonistDropdown()
    {
        //  Clear the current dropdown list
        stepThreeDropdown.GetComponent<Dropdown>().ClearOptions();

        // Step Two choice
        string stepOneChoice = stepOneDropdown.GetComponent<Dropdown>().options[stepOneDropdown.GetComponent<Dropdown>().value].text;

        knowCharacters = new List<SMWObjectModel>();

        if (stepOneChoice != "All Graphs")
        {
            // Gather Actors
            knowCharacters.AddRange(SMWParser.Instance.GetAllInstancesByObjectType(listGraphs.Find(g => g.ToString() == stepOneChoice), ObjectTypeEnum.ACTOR, true));

            // Gather Rolls
            foreach (SMWObjectModel potentialRoll in SMWParser.Instance.GetAllInstancesByObjectType(listGraphs.Find(g => g.ToString() == stepOneChoice), ObjectTypeEnum.CONTEXT, true))
            {
                if (potentialRoll is ContextModel)
                    if (((ContextModel)potentialRoll).ContextType == ContextTypeEnum.ROLE)
                        knowCharacters.Add(potentialRoll);
            }
        }
        else
        {
            foreach (IGraph g in listGraphs)
                if (listGraphs.Contains(g) == true)
                {
                    // Gather Actors
                    knowCharacters.AddRange(SMWParser.Instance.GetAllInstancesByObjectType(listGraphs[listGraphs.IndexOf(g)], ObjectTypeEnum.ACTOR, true));

                    // Gather Rolls
                    foreach (SMWObjectModel potentialRoll in SMWParser.Instance.GetAllInstancesByObjectType(listGraphs[listGraphs.IndexOf(g)], ObjectTypeEnum.CONTEXT, true))
                    {
                        if (potentialRoll is ContextModel)
                        {
                            Debug.Log(((ContextModel)potentialRoll).ContextType);

                            if (((ContextModel)potentialRoll).ContextType == ContextTypeEnum.ROLE)
                                knowCharacters.Add(potentialRoll);
                        }
                    }
                }
        }

        // Step two choice
        SMWObjectModel stepTwoChoiceObject = smwObjectModels.Find(smw => smw.ObjectName == stepTwoDropdown.GetComponent<Dropdown>().options[stepTwoDropdown.GetComponent<Dropdown>().value].text);



        // Limit the found Actors and Rolls to only the selected Context
        List<ActorModel> actorsFromSelectedContext = knowCharacters.FindAll(smw => smw.GetType() == typeof(ActorModel)).Cast<ActorModel>().ToList().FindAll(a => a.Context == stepTwoChoiceObject).ToList();

        List<ContextModel> rollsFromSelectedContext = knowCharacters.FindAll(smw => smw.GetType() == typeof(ContextModel)).Cast<ContextModel>().ToList().FindAll(a => a.SuperContext == stepTwoChoiceObject).ToList();

        knowCharacters.Clear();
        knowCharacters.AddRange(actorsFromSelectedContext.Cast<SMWObjectModel>().ToList());
        knowCharacters.AddRange(rollsFromSelectedContext.Cast<SMWObjectModel>().ToList());

        // Create a list of save names and add these to the options 
        List<string> listProtagonistNames = new List<string>();

        foreach (SMWObjectModel smwObjectModel in knowCharacters)
            listProtagonistNames.Add(smwObjectModel.ObjectName);

        if (listProtagonistNames.Count <= 0)
        {
            stepThreeDropdown.GetComponent<Dropdown>().interactable = false;
            listProtagonistNames.Add("No Actors/Rolls found");
        }
        else
        {
            stepThreeDropdown.GetComponent<Dropdown>().interactable = true;
        }

        stepThreeDropdown.GetComponent<Dropdown>().AddOptions(listProtagonistNames);

        // Update button from next step
        ToggleStartButton();
    }
}