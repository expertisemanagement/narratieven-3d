﻿using Object.FSM;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This is a class for a for the ObjectInteractionMenu for an interactable object. 
/// This Menu allows the user the execution of several interactions.
/// </summary>
public class ObjectInteractionMenu : MonoBehaviour {

    [Header("Objects that are part of the interaction")]

    // The interacting object (protagonist)
    [Tooltip("The interacting object")]
    [SerializeField] private GameObject subject;

    // The object that can be interacted
    [Tooltip("The object that can be interacted")]
    [SerializeField] public GameObject directObject;

    // The object used for interaction
    [Tooltip("Dropdown for selecting a Graph to use")]
    [SerializeField] private GameObject indirectObject;

    // The canvas component for the menu
    [Tooltip("The canvas component for the UI-elements of the menu")]
    [SerializeField] public Canvas menuCanvas;

    // The text component for the menutitle
    [Tooltip("The menu title")]
    [SerializeField] public Text titleText;

    // The button component for the interactionButton
    [Tooltip("The interact buttun")]
    [SerializeField] public Button interactButton;

    // The button component for the spectateButton
    [Tooltip("The inspect buttun")]
    [SerializeField] public Button spectateButton;

    // The button component for the diveInToButton
    [Tooltip("The dive into buttun")]
    [SerializeField] public Button diveIntoButton;

    // (Custom) Constructor for this monobehaviour subclass 
    public ObjectInteractionMenu Initialise(GameObject subject, GameObject directObject, GameObject indirectObject)
    {
        this.subject = subject;
        this.directObject = directObject;
        this.indirectObject = indirectObject;

        return this;
    }

    // Executed when the object gets enabled
    public void OnEnable()
    {
        if (subject != null && menuCanvas != null && titleText != null && interactButton != null && spectateButton != null && diveIntoButton != null)
        {
            // Set all UI-elements of this menu to active
            SetActiveStateOfTheMenuUIComponents(true);

            // Set the canvas of the menu visable for the current camera of the current camera of the perspective
            menuCanvas.worldCamera = PerspectiveManager.instance.GetCurrentCamera().GetComponent<Camera>();

            // Update the canvas information
            UpdateCanvasUIComponentInformation();
        }
        else
        {
            // Disable this menu (this component)
            this.enabled = false;
        }
    }

    // Executed when the object gets disabled
    public void OnDisable()
    {
        // Set all UI-elements of this menu to inactive
        SetActiveStateOfTheMenuUIComponents(false);
    }

    // Update the canvas information
    private void UpdateCanvasUIComponentInformation()
    {
        if (subject != null)
        {
            interactButton.interactable = false;
            spectateButton.interactable = false;
            diveIntoButton.interactable = false;

            ObjectView foundObjectView = directObject.GetComponent<ObjectView>();

            if (foundObjectView is ComplexObjectView)
            {
                if (((ComplexObjectView)foundObjectView).MachineState != ObjectFSMEnum.STATICNONINTERACTIVE || ((ComplexObjectView)foundObjectView).MachineState != ObjectFSMEnum.HIDDEN || ((ComplexObjectView)foundObjectView).MachineState != ObjectFSMEnum.AINONINTERACTIVE)
                    interactButton.interactable = true;

                if (subject.GetComponent<SemanticObjectView>())
                {
                    titleText.text = ((SemanticObjectModel)directObject.GetComponent<SemanticObjectView>().FindSetController().FindCreateSetModel()).ObjectName;

                    diveIntoButton.interactable = true;

                    spectateButton.interactable = true;
                }
            }
            else
            {
                titleText.text = directObject.name;
            }

            RotateCanvasToFaceSubject();
        }



    }

    // Execute subject interaction with the directedobject
    public void InteractDirectObjectWithSubject()
    {
        ObjectView foundObjectView = directObject.GetComponent<ObjectView>();

        if (foundObjectView is ActivityView && NarrativeManager.instance.RegisterExcetutedAction((ActivityModel)foundObjectView.FindCreateSetController().FindCreateSetModel()) == true)
            {
                WorldManager.instance.UpdateActivitiyVisuals();

                ActivityModel nextActivity = (ActivityModel) GameObject.FindObjectsOfType<RelationController>().ToList().FindAll(rc => rc.subjectElement == (IntentionalElementModel)foundObjectView.FindCreateSetController().FindCreateSetModel()).ToList().Find(rc => rc.relatedElement is ActivityModel && rc.relatedElementAllowsExecution).relatedElement;

                if (nextActivity != null && nextActivity.Context != NarrativeManager.instance.GetProtagonist())
                {
                    this.gameObject.transform.SetParent(null);

                    PerspectiveManager.instance.SetNewPerspective((ContextView) nextActivity.Context.FindCreateSetController().FindSetView(), false, true, true);
                    Destroy(this.gameObject);
                    return;
                }

                WorldManager.instance.UpdateActivitiyVisuals();
            }

        this.enabled = false;
    }

    // Execute spectatermode for the directedobject
    public void SpectateDirectObject()
    {
        ComplexObjectView foundObjectView = subject.GetComponent<ComplexObjectView>();
        if(foundObjectView != null)
            PerspectiveManager.instance.SetNewPerspective(foundObjectView, true, false, false);

        this.enabled = false;
    }

    // Execute dive into directedobject (loads a world featuring onformation about this object)
    public void DiveInToDirectObject()
    {
        SMWObjectView foundObjectView = subject.GetComponent<SMWObjectView>();

        if (foundObjectView != null)
            WorldManager.instance.CreateNewWorldAndWipeOldWorld(NarrativeManager.instance.GetProtagonist(), false, (SMWObjectModel) foundObjectView.FindCreateSetController().FindCreateSetModel(), false, false, true);

        this.enabled = false;
    }

    // Rotates the canvas of this menu to face the subject of the interactionMenu
    private void RotateCanvasToFaceSubject()
    {
        if (directObject != null)
        {
            // Set location to stand between the subject and the directObject
            this.gameObject.transform.position = (subject.transform.position + directObject.transform.position) / 2;
            transform.LookAt(directObject.transform.position);
        }
    }

    // Set all states of the menu UI-components to active (=true) or inactive (=false)
    private void SetActiveStateOfTheMenuUIComponents(bool isActive)
    {
        if(menuCanvas != null)
            menuCanvas.gameObject.SetActive(isActive);

        if (titleText != null)
            titleText.gameObject.SetActive(isActive);

        if (interactButton != null)
            interactButton.gameObject.SetActive(isActive);

        if (spectateButton != null)
            spectateButton.gameObject.SetActive(isActive);

        if (diveIntoButton != null)
            diveIntoButton.gameObject.SetActive(isActive);
    }
}
