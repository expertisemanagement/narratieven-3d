﻿using UnityEngine;

/// <summary>
/// This is the abstract View class for Unity Components
/// </summary>
public abstract class BaseView : MonoBehaviourEventForwarder, IBaseView
{
    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public virtual BaseView Initialise(string id)
    {
        // Set id
        this.b_entityID = id;

        // Initialise (, also checks for a controller and fixes it)
        FindSetController();

        // Return this view
        return this;
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public virtual BaseView Initialise(BaseModel model, bool syncToModel)
    {
        // Initialise with out the sync
        InitialiseNoSync(model);

        // sync to view if "false", else if "true" sync to model
        if (syncToModel == false)
            SyncModelToView();
        else
        {
            b_entityID = Controller.FindEntityID();
            SyncViewToModel();
        }

        // Return this view
        return this;
    }

    // View events (+ MonoBehaviour events coming from the inherited class)
    // Yet only only inherited MonoBehaviour events

    // The GameObject holding the component (Not registered in Model)
    [HideInInspector] public GameObject currentGameObject
    {
        get { return this.transform.gameObject; }
    }

    // Unique indentifier for a object
    [HideInInspector] public string b_entityID;
    [HideInInspector] public virtual string EntityID
    {
        get
        {
            return b_entityID;
        }
        set
        {
            b_entityID = value;
        }
    }

    // Controller for sending events and syncing data based on the Model
    protected BaseController b_controller;
    public BaseController Controller
    {
        get
        {
            return b_controller;
        }
        set
        {
            // Only if the backing field changes
            if (b_controller != value)
            {
                // Set new value
                b_controller = value;

                // Set this model as it's model
                this.Controller.View = this;
            }
        }
    }

    // Methode for syncing Model data to the View
    public virtual void SyncModelToView()
    {
        Controller.SyncModelToView();
    }

    // Methode for syncing View data to the Model
    public virtual void SyncViewToModel()
    {
        // Nothing to sync
    }

    // Finds for an existing controller, else creates one
    public virtual BaseController FindSetController()
    {
        if (b_controller != null)
            return b_controller;
        else
            return null; // [Defined in the subclasses] Search for existing or even create one 
    }
    
    // Finds for an existing controller, else creates one
    public virtual BaseController FindCreateSetController()
    {
        // Not possible in this type
        Debug.Log("Not possible to create a controller");
        return null;
    }

    // Links the given view to the model (returns false if failed)
    protected bool LinkModelToView(BaseModel model)
    {
        // If there is a model and a controller for this model link them.
        if (model != null && this.FindSetController() != null)
        {
            model.Controller = this.Controller;
            this.Controller.Model = model;
            return true;
        }
        else
        {
            // Try to find/create a controller for the model and link those, else return false
            if (model.FindCreateSetController() != null)
            {
                this.Controller = model.Controller;
                this.Controller.Model = model;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    // Initialisation without syncing the data from the view to the model
    protected virtual void InitialiseNoSync(BaseModel model)
    {
        LinkModelToView(model);
    }


}
