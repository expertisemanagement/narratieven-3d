﻿using UnityEngine;

public interface IBaseView
{   
    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    BaseView Initialise(string id);

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    BaseView Initialise(BaseModel model, bool syncToModel);


    // The GameObject holding the component (Not registered in Model)
    GameObject currentGameObject { get; }

    // Unique indentifier for a object
    string EntityID { get; }

    // Controller for sending events and syncing data based on the Model
    BaseController Controller { get; set; }


    // Methode for syncing Model data to the View
    void SyncModelToView();

    // Methode for syncing View data to the Model
    void SyncViewToModel();

}