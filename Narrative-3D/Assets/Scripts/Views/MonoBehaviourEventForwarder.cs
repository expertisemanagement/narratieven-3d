﻿using UnityEngine;

/**
 * Utility component to add to game objects whose events you want forwarded from Unity's message
 * system to standard C# events. Handles all events as of Unity 4.6.1.
 * @author Jackson Dunstan - http://jacksondunstan.com/articles/2922
 */
public class MonoBehaviourEventForwarder : MonoBehaviour
{
    public delegate void EventHandler0();
    public delegate void EventHandler1<TParam>(TParam param);
    public delegate void EventHandler2<TParam1, TParam2>(TParam1 param1, TParam2 param2);

    public event EventHandler0 AwakeEvent = () => { };
    public event EventHandler0 FixedUpdateEvent = () => { };
    public event EventHandler0 LateUpdateEvent = () => { };
    public event EventHandler1<int> OnAnimatorIKEvent = layerIndex => { };
    public event EventHandler0 OnAnimatorMoveEvent = () => { };
    public event EventHandler1<bool> OnApplicationFocusEvent = focusStatus => { };
    public event EventHandler1<bool> OnApplicationPauseEvent = pauseStatus => { };
    public event EventHandler0 OnApplicationQuitEvent = () => { };
    public event EventHandler2<float[], int> OnAudioFilterReadEvent = (data, channels) => { };
    public event EventHandler0 OnBecameInvisibleEvent = () => { };
    public event EventHandler0 OnBecameVisibleEvent = () => { };
    public event EventHandler1<Collision> OnCollisionEnterEvent = collision => { };
    public event EventHandler1<Collision2D> OnCollisionEnter2DEvent = collision => { };
    public event EventHandler1<Collision> OnCollisionExitEvent = collision => { };
    public event EventHandler1<Collision2D> OnCollisionExit2DEvent = collision => { };
    public event EventHandler1<Collision> OnCollisionStayEvent = collision => { };
    public event EventHandler1<Collision2D> OnCollisionStay2DEvent = collision => { };
    public event EventHandler0 OnConnectedToServerEvent = () => { };
    public event EventHandler1<ControllerColliderHit> OnControllerColliderHitEvent = hit => { };
    public event EventHandler0 OnDestroyEvent = () => { };
    public event EventHandler0 OnDisableEvent = () => { };
    public event EventHandler1<NetworkDisconnection> OnDisconnectedFromServerEvent = info => { };
    public event EventHandler0 OnDrawGizmosEvent = () => { };
    public event EventHandler0 OnDrawGizmosSelectedEvent = () => { };
    public event EventHandler0 OnEnableEvent = () => { };
    public event EventHandler1<NetworkConnectionError> OnFailedToConnectEvent = error => { };
    public event EventHandler1<NetworkConnectionError> OnFailedToConnectToMasterServerEvent = error => { };
    public event EventHandler0 OnGUIEvent = () => { };
    public event EventHandler1<float> OnJointBreakEvent = breakForce => { };
    //public event EventHandler1<int> OnLevelWasLoadedEvent = level => { };
    public event EventHandler1<MasterServerEvent> OnMasterServerEventEvent = msEvent => { };
    public event EventHandler0 OnMouseDownEvent = () => { };
    public event EventHandler0 OnMouseDragEvent = () => { };
    public event EventHandler0 OnMouseEnterEvent = () => { };
    public event EventHandler0 OnMouseExitEvent = () => { };
    public event EventHandler0 OnMouseOverEvent = () => { };
    public event EventHandler0 OnMouseUpEvent = () => { };
    public event EventHandler0 OnMouseUpAsButtonEvent = () => { };
    public event EventHandler1<NetworkMessageInfo> OnNetworkInstantiateEvent = info => { };
    public event EventHandler1<GameObject> OnParticleCollisionEvent = other => { };
    public event EventHandler1<NetworkPlayer> OnPlayerConnectedEvent = player => { };
    public event EventHandler1<NetworkPlayer> OnPlayerDisconnectedEvent = player => { };
    public event EventHandler0 OnPostRenderEvent = () => { };
    public event EventHandler0 OnPreCullEvent = () => { };
    public event EventHandler0 OnPreRenderEvent = () => { };
    public event EventHandler2<RenderTexture, RenderTexture> OnRenderImageEvent = (src, dest) => { };
    public event EventHandler0 OnRenderObjectEvent = () => { };
    public event EventHandler2<BitStream, NetworkMessageInfo> OnSerializeNetworkViewEvent = (stream, info) => { };
    public event EventHandler0 OnServerInitializedEvent = () => { };
    public event EventHandler1<Collider> OnTriggerEnterEvent = other => { };
    public event EventHandler1<Collider2D> OnTriggerEnter2DEvent = other => { };
    public event EventHandler1<Collider> OnTriggerExitEvent = other => { };
    public event EventHandler1<Collider2D> OnTriggerExit2DEvent = other => { };
    public event EventHandler1<Collider> OnTriggerStayEvent = other => { };
    public event EventHandler1<Collider2D> OnTriggerStay2DEvent = other => { };
    public event EventHandler0 OnValidateEvent = () => { };
    public event EventHandler0 OnWillRenderObjectEvent = () => { };
    public event EventHandler0 ResetEvent = () => { };
    public event EventHandler0 StartEvent = () => { };
    public event EventHandler0 UpdateEvent = () => { };

    public virtual void Awake()
    {
        AwakeEvent();
    }

    public virtual void FixedUpdate()
    {
        FixedUpdateEvent();
    }

    public virtual void LateUpdate()
    {
        LateUpdateEvent();
    }

    public virtual void OnAnimatorIK(int layerIndex)
    {
        OnAnimatorIKEvent(layerIndex);
    }

    public virtual void OnAnimatorMove()
    {
        OnAnimatorMoveEvent();
    }

    public virtual void OnApplicationFocus(bool focusStatus)
    {
        OnApplicationFocusEvent(focusStatus);
    }

    public virtual void OnApplicationPause(bool pauseStatus)
    {
        OnApplicationPauseEvent(pauseStatus);
    }

    public virtual void OnApplicationQuit()
    {
        OnApplicationQuitEvent();
    }

    public virtual void OnAudioFilterRead(float[] data, int channels)
    {
        OnAudioFilterReadEvent(data, channels);
    }

    public virtual void OnBecameInvisible()
    {
        OnBecameInvisibleEvent();
    }

    public virtual void OnBecameVisible()
    {
        OnBecameVisibleEvent();
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        OnCollisionEnterEvent(collision);
    }

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        OnCollisionEnter2DEvent(collision);
    }

    public virtual void OnCollisionExit(Collision collision)
    {
        OnCollisionExitEvent(collision);
    }

    public virtual void OnCollisionExit2D(Collision2D collision)
    {
        OnCollisionExit2DEvent(collision);
    }

    public virtual void OnCollisionStay(Collision collision)
    {
        OnCollisionStayEvent(collision);
    }

    public virtual void OnCollisionStay2D(Collision2D collision)
    {
        OnCollisionStay2DEvent(collision);
    }

    public virtual void OnConnectedToServer()
    {
        OnConnectedToServerEvent();
    }

    public virtual void OnControllerColliderHit(ControllerColliderHit hit)
    {
        OnControllerColliderHitEvent(hit);
    }

    public virtual void OnDestroy()
    {
        OnDestroyEvent();
    }

    public virtual void OnDisable()
    {
        OnDisableEvent();
    }

    public virtual void OnDisconnectedFromServer(NetworkDisconnection info)
    {
        OnDisconnectedFromServerEvent(info);
    }

    public virtual void OnDrawGizmos()
    {
        OnDrawGizmosEvent();
    }

    public virtual void OnDrawGizmosSelected()
    {
        OnDrawGizmosSelectedEvent();
    }

    public virtual void OnEnable()
    {
        OnEnableEvent();
    }

    public virtual void OnFailedToConnect(NetworkConnectionError error)
    {
        OnFailedToConnectEvent(error);
    }

    public virtual void OnFailedToConnectToMasterServer(NetworkConnectionError error)
    {
        OnFailedToConnectToMasterServerEvent(error);
    }

    public virtual void OnGUI()
    {
        OnGUIEvent();
    }

    public virtual void OnJointBreak(float breakForce)
    {
        OnJointBreakEvent(breakForce);
    }

    //public virtual void OnLevelWasLoaded(int level)
    //{
    //    OnLevelWasLoadedEvent(level);
    //}

    public virtual void OnMasterServerEvent(MasterServerEvent msEvent)
    {
        OnMasterServerEventEvent(msEvent);
    }

    public virtual void OnMouseDown()
    {
        OnMouseDownEvent();
    }

    public virtual void OnMouseDrag()
    {
        OnMouseDragEvent();
    }

    public virtual void OnMouseEnter()
    {
        OnMouseEnterEvent();
    }

    public virtual void OnMouseExit()
    {
        OnMouseExitEvent();
    }

    public virtual void OnMouseOver()
    {
        OnMouseOverEvent();
    }

    public virtual void OnMouseUp()
    {
        OnMouseUpEvent();
    }

    public virtual void OnMouseUpAsButton()
    {
        OnMouseUpAsButtonEvent();
    }

    public virtual void OnNetworkInstantiate(NetworkMessageInfo info)
    {
        OnNetworkInstantiateEvent(info);
    }

    public virtual void OnParticleCollision(GameObject other)
    {
        OnParticleCollisionEvent(other);
    }

    public virtual void OnPlayerConnected(NetworkPlayer player)
    {
        OnPlayerConnectedEvent(player);
    }

    public virtual void OnPlayerDisconnected(NetworkPlayer player)
    {
        OnPlayerDisconnectedEvent(player);
    }

    public virtual void OnPostRender()
    {
        OnPostRenderEvent();
    }

    public virtual void OnPreCull()
    {
        OnPreCullEvent();
    }

    public virtual void OnPreRender()
    {
        OnPreRenderEvent();
    }

    //public virtual void OnRenderImage(RenderTexture src, RenderTexture dest)
    //{
    //    OnRenderImageEvent(src, dest);
    //}

    public virtual void OnRenderObject()
    {
        OnRenderObjectEvent();
    }

    public virtual void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        OnSerializeNetworkViewEvent(stream, info);
    }

    public virtual void OnServerInitialized()
    {
        OnServerInitializedEvent();
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        OnTriggerEnterEvent(other);
    }

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        OnTriggerEnter2DEvent(other);
    }

    public virtual void OnTriggerExit(Collider other)
    {
        OnTriggerExitEvent(other);
    }

    public virtual void OnTriggerExit2D(Collider2D other)
    {
        OnTriggerExit2DEvent(other);
    }

    public virtual void OnTriggerStay(Collider other)
    {
        OnTriggerStayEvent(other);
    }

    public virtual void OnTriggerStay2D(Collider2D other)
    {
        OnTriggerStay2DEvent(other);
    }

    public virtual void OnValidate()
    {
        OnValidateEvent();
    }

    public virtual void OnWillRenderObject()
    {
        OnWillRenderObjectEvent();
    }

    public virtual void Reset()
    {
        ResetEvent();
    }

    public virtual void Start()
    {
        StartEvent();
    }

    public virtual void Update()
    {
        UpdateEvent();
    }
}