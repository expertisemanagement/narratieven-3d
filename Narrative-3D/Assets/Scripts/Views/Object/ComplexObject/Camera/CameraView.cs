﻿using Object.Type;
using UnityEngine;

public class CameraView : ComplexObjectView, ICameraView
{
    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string id)
    {
        // Excecute base initialisation
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, syncToModel);

    }

    // Toggle controllablity
    [SerializeField] public bool controllableCamera
    {
        get
        {
            if (Controller != null) return ((CameraModel)Controller.Model).ControllableCamera; else return b_controllable;
        }
        set
        {
            b_controllable = value;
        }
    }

    [SerializeField] public CameraControlModel cameraControlSettings
    {
        get
        {
            if (ControlSettings != null && ControlSettings.GetType() == typeof(CameraControlModel))
                return (CameraControlModel)ControlSettings;
            else
                return null;
        }
        set
        {
            b_controlSettings = value;
        }
    }

    public bool EnableCameraComponent()
    {
        if (this.GetComponent<Camera>() != null)
        {
            this.GetComponent<Camera>().enabled = true;

            // Return succes
            return true;
        }
        else
        {
            // Return failed
            return false;
        }
    }

    public bool DisableCameraComponent()
    {
        if (this.GetComponent<Camera>() != null)
        {
            this.GetComponent<Camera>().enabled = false;

            // Return succes
            return true;
        }
        else
        {
            // Return failed
            return false;
        }
    }

    // Methode for syncing View data to the Model
    public override void SyncViewToModel()
    {
        base.SyncViewToModel();

        // No special syncing methodes needed, because ComplexObjectController variables are used

    }

    // (No initialisation part of the) sort of constructor for the monobehaviour 
    // Initialise a controller datasource without auto sync
    protected override void InitialiseNoSync(BaseModel model)
    {
        // Initialiase this controller
        base.InitialiseNoSync(model);
    }

}
