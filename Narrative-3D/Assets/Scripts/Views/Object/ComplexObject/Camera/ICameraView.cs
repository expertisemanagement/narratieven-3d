﻿﻿public interface ICameraView : IComplexObjectView
{
    // Toggle controllablity
    bool controllableCamera { get; set; }

    // Controll settings for defining possible movement and there input axes
    CameraControlModel cameraControlSettings { get; set; }

}