﻿using UnityEngine;
using Object.Type;
using Object.FSM;

/// <summary>
/// This is the base View class for an ComplexObject. 
/// A ComplexObject is an object that can move.
/// </summary>
public class ComplexObjectView : ObjectView, IComplexObjectView
{
    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string id)
    {
        // Excecute base initialisation
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string id, ObjectTypeEnum objectType)
    {
        if (objectType == ObjectTypeEnum.ACTOR)
        {
            b_machineState = ObjectFSMEnum.AIINTERACTIVE;
            b_controllable = true;
        }

        // Excecute base initialisation
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, syncToModel);

    }

    // Toggle controllablity
    [SerializeField] public bool b_controllable;
    [HideInInspector] public virtual bool Controllable
    {
        get
        {
            if (Controller != null) return ((ComplexObjectModel)Controller.Model).Controllable; else return b_controllable;
        }
        set
        {
            b_controllable = value;
        }
    }

    // Controll settings for defining possible movement and there input axes
    [SerializeField] public virtual ObjectControlModel b_controlSettings
    {
        get
        {
            if (gameObject.GetComponent<ObjectControlView>() != null)
                return (ObjectControlModel)gameObject.GetComponent<ObjectControlView>().FindCreateSetController().FindCreateSetModel();
            else if (((ComplexObjectModel)this.FindSetController().FindSetModel()).ControlSettings != null)
                return ((ComplexObjectModel)this.FindSetController().FindSetModel()).ControlSettings;
            else
                return null;
        }
        set { }
    }
    [HideInInspector] public virtual ObjectControlModel ControlSettings
    {
        get
        {
            return b_controlSettings;
        }
        set
        {
            // Update controlView
            if (b_controlSettings != value)
            {
                // Destroy current control
                Destroy(((ObjectControlView)b_controlSettings.FindSetController().FindSetView()));
                
                // Create/add new controlView
                ObjectFactory.Instance.viewFactory.createForObjectControl(value, this.currentGameObject);
            }

            b_controlSettings = value;
            
            
        }
    }


    // In world position (location)
    [HideInInspector] public override Vector3 b_position
    {
        get
        {
            return transform.position;
        }
        set
        {
            transform.position = value;
        }
    }
    [HideInInspector] public override Vector3 Position
    {
        get
        {
            if (Controller != null) return ((NavObjectModel)Controller.Model).Position;
            else return b_position;
        }
        set
        {
            b_position = value;
        }
    }

    // In world rotation
    [HideInInspector] public override Quaternion b_rotation
    {
        get
        {
            return transform.rotation;
        }
        set
        {
            transform.rotation = value;
        }
    }
    [HideInInspector] public override Quaternion Rotation
    {
        get
        {
            if (Controller != null) return ((NavObjectModel)Controller.Model).Rotation;
            else return b_rotation;
        }
        set
        {
            transform.rotation = value;
        }
    }

    // In world scale
    [HideInInspector] public override Vector3 b_scale
    {
        get
        {
            return transform.localScale;
        }
        set
        {
            transform.localScale = value;
        }
    }
    [HideInInspector] public override Vector3 Scale
    {
        get
        {
            if (Controller != null) return ((ComplexObjectModel)Controller.Model).Scale;
            else return b_scale;
        }
        set
        {
            b_scale = value;
        }
    }


    // Machine state for defining it's interaction within the world
    [HideInInspector] public ObjectFSMEnum b_machineState = ObjectFSMEnum.STATICINTERACTIVE;
    [HideInInspector] public virtual ObjectFSMEnum MachineState
    {
        get
        {
            if (Controller != null) return ((ComplexObjectModel)Controller.Model).MachineState;
            else return b_machineState;
        }
        set
        {
            b_machineState = value;
        }
    }

    // Methode for syncing View data to the Model
    public override void SyncViewToModel()
    {
        base.SyncViewToModel();

        ((ComplexObjectController)Controller).SyncControllabilityViewToModel(b_controllable);
        ((ComplexObjectController)Controller).SyncControlSettingsViewToModel(b_controlSettings);
        ((ComplexObjectController)Controller).SyncMachineStateViewToModel(b_machineState);
    }
}
