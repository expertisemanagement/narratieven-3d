﻿using Object.FSM;

public interface IComplexObjectView : IObjectView
{

    // Toggle controllablity
    bool Controllable { get; set; }

    // Controll settings for defining possible movement and there input axes
    ObjectControlModel ControlSettings { get; set; }

    // Machine state for defining it's interaction within the world
    ObjectFSMEnum MachineState { get; set; }

}
