﻿using VDS.RDF;

public interface ISemanticObjectView :  IObjectView
{
    // RDF Source sementic node, for indentification
    INode SemanticNode { get; set; }

    // Object URI coming from RDF Source sementicNode, for indentification
    string ObjectURI { get; set; }

    // Controller for sending events and syncing data based on the Model
    NavObjectModel NavObject { get; set; }
}
