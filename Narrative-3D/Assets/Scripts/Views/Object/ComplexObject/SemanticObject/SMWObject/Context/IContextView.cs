﻿using SMW.EMM.Context;
using VDS.RDF;

public interface IContextView
{
    ContextTypeEnum contextType { get; }
}