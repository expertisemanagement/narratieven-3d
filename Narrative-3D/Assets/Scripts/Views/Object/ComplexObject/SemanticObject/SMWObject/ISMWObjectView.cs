﻿using System;

public interface ISMWObjectView : ISemanticObjectView
{
    // Structured text consist out of existing paragraphheadings en alineas:
    
    string ParagraphHeadNL { get; }
    
    string ParagraphHeadEN { get; }

    Struct_SMW_Alinea[] Alineas { get; }

    Struct_SMW_Imagary[] Imagary { get; }

    string FreeText { get; }

    string Summery { get; }

    string Description { get; }

}
