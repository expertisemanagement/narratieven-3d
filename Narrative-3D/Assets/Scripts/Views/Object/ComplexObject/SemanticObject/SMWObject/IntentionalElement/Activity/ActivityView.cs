﻿using Object.Type;
using UnityEngine;
using VDS.RDF;

public class ActivityView : IntentionalElementView, IActivityView
{
    // [DON'T USE!] Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string id)
    {
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string entityID, INode semanticNode)
    {
        // Excecute base initialisation
        return base.Initialise(entityID, semanticNode);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navModel)
    {
        // Excecute base initialisation
        return base.Initialise(entityID, semanticNode, category, navModel);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, syncToModel);

    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, NavObjectModel navModel, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, navModel, syncToModel);

    }

    // (No initialisation part of the) sort of constructor for the monobehaviour 
    // Initialise a controller datasource without auto sync
    protected override void InitialiseNoSync(BaseModel model)
    {
        // Initialiase this controller
        base.InitialiseNoSync(model);
    }
}
