﻿using Object.Type;
using VDS.RDF;

public class ActorView : IntentionalElementView, IActorView
{
    // [DON'T USE!] Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string id)
    {
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string entityID, INode semanticNode)
    {
        // Excecute base initialisation
        return base.Initialise(entityID, semanticNode);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navModel)
    {
        // Excecute base initialisation
        return base.Initialise(entityID, semanticNode, category, navModel);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, syncToModel);

    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, NavObjectModel navModel, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, navModel, syncToModel);

    }

    public new string name
    {
        get
        {
            return ((ActorModel)Controller.Model).name;
        }
    }

    public string address
    {
        get
        {
            return ((ActorModel)Controller.Model).Address;
        }
    }

    public string houseNumber
    {
        get
        {
            return ((ActorModel)Controller.Model).HouseNumber;
        }
    }

    public string zipcode
    {
        get
        {
            return ((ActorModel)Controller.Model).Zipcode;
        }
    }

    public string location
    {
        get
        {
            return ((ActorModel)Controller.Model).Location;
        }
    }

    public string phone
    {
        get
        {
            return ((ActorModel)Controller.Model).Phone;
        }
    }

    public string email
    {
        get
        {
            return ((ActorModel)Controller.Model).Email;
        }
    }

    public string website
    {
        get
        {
            return ((ActorModel)Controller.Model).Website;
        }
    }

    public string info
    {
        get
        {
            return ((ActorModel)Controller.Model).Info;
        }
    }

    // (No initialisation part of the) sort of constructor for the monobehaviour 
    // Initialise a controller datasource without auto sync
    protected override void InitialiseNoSync(BaseModel model)
    {
        // Initialiase this controller
        base.InitialiseNoSync(model);
    }
}
