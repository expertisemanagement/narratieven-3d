﻿using System;

public interface IActorView : IIntentionalElementView
{
    string name { get; }

    string address { get; }

    string houseNumber { get; }

    string zipcode { get; }

    string location { get; }

    string phone { get; }

    string email { get; }

    string website { get; }

    string info { get; }
}
