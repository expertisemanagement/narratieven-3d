﻿using SMW.Relation.IntetionalELement;

public interface IIntentionalElementView : ISMWObjectView {

    ContextModel Context { get; }

    ElementCompositionTypeEnum CompositionType { get; }

    IntentionalElementModel[] PartOf { get; }

    IntentionalElementModel[] InstanceOf { get; }

    IntentionalElementModel[] Concerns { get; }

    Contributes[] Contributes { get; }

    Dependency[] Depends { get; }
   
}
