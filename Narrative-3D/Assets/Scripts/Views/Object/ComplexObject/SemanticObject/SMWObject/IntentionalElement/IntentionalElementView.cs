﻿using Object.Type;
using SMW.Relation.IntetionalELement;
using VDS.RDF;

public class IntentionalElementView : SMWObjectView, IIntentionalElementView
{
    // [DON'T USE!] Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string id)
    {
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string entityID, INode semanticNode)
    {
        // Excecute base initialisation
        return base.Initialise(entityID, semanticNode);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string entityID, INode semanticNode, SMWCategory category, NavObjectModel navModel)
    {
        // Excecute base initialisation
        return base.Initialise(entityID, semanticNode, category, navModel);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, syncToModel);

    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, NavObjectModel navModel, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, navModel, syncToModel);

    }


    public ContextModel Context
    {
        get
        {
            return ((IntentionalElementModel)Controller.Model).Context;
        }
    }

    public ElementCompositionTypeEnum CompositionType
    {
        get
        {
            return ((IntentionalElementModel)Controller.Model).DecompositionType; 
        }
    }

    public IntentionalElementModel[] PartOf
    {
        get
        {
            return ((IntentionalElementModel)Controller.Model).PartOf; 
        }
    }

    public IntentionalElementModel[] InstanceOf
    {
        get
        {
            return ((IntentionalElementModel)Controller.Model).InstanceOf; 
        }
    }

    public IntentionalElementModel[] Concerns
    {
        get
        {
            return ((IntentionalElementModel)Controller.Model).Concerns; 
        }
    }

    public Contributes[] Contributes
    {
        get
        {
            return ((IntentionalElementModel)Controller.Model).Contributes; 
        }
    }

    public Dependency[] Depends
    {
        get
        {
            return ((IntentionalElementModel)Controller.Model).Depends;
        }
    }

    // (No initialisation part of the) sort of constructor for the monobehaviour 
    // Initialise a controller datasource without auto sync
    protected override void InitialiseNoSync(BaseModel model)
    {
        // Initialiase this controller
        base.InitialiseNoSync(model);
    }

}
