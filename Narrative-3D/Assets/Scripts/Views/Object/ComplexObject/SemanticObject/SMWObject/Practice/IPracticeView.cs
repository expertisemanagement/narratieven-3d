﻿using SMW.Relation.Practice;
using System.Collections.Generic;

public interface IPracticeView : ISMWObjectView {

    ContextModel Context { get; }

    PracticeTypeEnum PacticeType { get; }

    Selects[] Selections { get; }

}
