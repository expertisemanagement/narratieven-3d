﻿using Object.Type;
using System;
using UnityEngine;
using VDS.RDF;

public class SMWObjectView : SemanticObjectView, ISMWObjectView
{
    // [DON'T USE!] Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string id)
    {
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string entityID, INode semanticNode)
    {
        // Excecute base initialisation
        return base.Initialise(entityID, semanticNode);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public virtual BaseView Initialise(string entityID, INode semanticNode, SMWCategory category,  NavObjectModel navModel)
    {
        b_category = category;

        // Excecute base initialisation
        return base.Initialise(entityID, semanticNode, navModel);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, syncToModel);

    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, NavObjectModel navModel, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, navModel, syncToModel);

    }

    // RDF Source sementic node, for indentification
    [SerializeField] public SMWCategory b_category;
    [HideInInspector] public virtual SMWCategory Category
    {
        get
        {
            if (Controller != null) return ((SMWObjectModel)Controller.Model).Category;
            else return b_category;
        }

        set
        {
            b_category = value;
        }
    }

    // Structured text consist out of existing paragraphheadings en alineas:

    public string ParagraphHeadNL
    {
        get {
            if (Controller != null) return ((SMWObjectModel)Controller.Model).ParagraphHeadNL; else return null;
        }
    }

    public string ParagraphHeadEN
    {
        get
        {
            if (Controller != null) return ((SMWObjectModel)Controller.Model).ParagraphHeadEN; else return null;
        }
    }
    
    public Struct_SMW_Alinea[] Alineas
    {
        get
        {
            if (Controller != null) return ((SMWObjectModel)Controller.Model).Alineas; else return null;
        }
    }

    public Struct_SMW_Imagary[] Imagary
    {
        get
        {
            if (Controller != null) return ((SMWObjectModel)Controller.Model).Imagary; else return null;
        }
    }

    public string FreeText
    {
        get
        {
            if (Controller != null) return ((SMWObjectModel)Controller.Model).FreeText; else return null;
        }
    }

    public string Summery
    {
        get
        {
            if (Controller != null) return ((SMWObjectModel)Controller.Model).Summery; else return null;
        }
    }

    public string Description
    {
        get
        {
            return ((ContextModel)Controller.Model).Description;
        }
    }

    // Methode for syncing View data to the Model
    public override void SyncViewToModel()
    {
        base.SyncViewToModel();

        ((SMWObjectController)Controller).SyncCategoryViewToModel(b_category);
    }

    // (No initialisation part of the) sort of constructor for the monobehaviour 
    // Initialise a controller datasource without auto sync
    protected override void InitialiseNoSync(BaseModel model)
    {
        // Initialiase this controller
        base.InitialiseNoSync(model);
    }

}
