﻿using UnityEngine;
using VDS.RDF;

/// <summary>
/// This is the base View class for a SemanticObject. A SemanticObject is an object that can be controlled and is defined in a Semantic Graph.
/// </summary>
public class SemanticObjectView : ComplexObjectView, ISemanticObjectView
{
    // [DON'T USE!] Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string id)
    {
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public virtual BaseView Initialise(string entityID, INode semanticNode)
    {
        // Set semanticURI
        this.b_semanticNode = semanticNode;
        this.b_objectURI = this.SemanticNode.ToString();
        this.b_navObject = NavObject;

        // Excecute base initialisation
        return base.Initialise(entityID);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public virtual BaseView Initialise(string entityID, INode semanticNode, NavObjectModel navModel)
    {
       this.b_navObject = NavObject;        
        
        // Excecute base initialisation
        return Initialise(entityID, semanticNode);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, syncToModel);

    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public virtual BaseView Initialise(BaseModel model, NavObjectModel navModel, bool syncToModel)
    {
        this.b_navObject = NavObject;

        // Excecute base initialisation
        return Initialise(model, syncToModel);

    }


    // RDF Source sementic node, for indentification
    [SerializeField] public INode b_semanticNode;
    [HideInInspector] public virtual INode SemanticNode
    {
        get
        {
            if (Controller != null) return ((SemanticObjectModel)Controller.Model).SemanticNode;
            else return b_semanticNode;
        }

        set
        {
            b_semanticNode = value;
        }
    }

    // Object URI coming from RDF Source sementicNode, for indentification
    [SerializeField] public string b_objectURI;
    [HideInInspector] public virtual string ObjectURI
    {
        get
        {
            if (Controller != null) return ((SemanticObjectModel) Controller.Model).ObjectURI;
            else return b_objectURI;
        }
        set
        {
            b_objectURI = value;
        }
    }

    // Controller for sending events and syncing data based on the Model
    [HideInInspector] public NavObjectModel b_navObject;
    [HideInInspector] public virtual NavObjectModel NavObject
    {
        get
        {
            return b_navObject;
        }

        set
        {
            b_navObject = value;
        }
    }


    // Methode for syncing View data to the Model
    public override void SyncViewToModel()
    {
        base.SyncViewToModel();

        ((ISemanticObjectController)Controller).SyncSemanticNodeViewToModel(b_semanticNode);
        ((ISemanticObjectController)Controller).SyncObjectURIViewToModel(b_objectURI);
        ((ISemanticObjectController)Controller).SyncNavObjectViewToModel(b_navObject);
    } 

    // (No initialisation part of the) sort of constructor for the monobehaviour 
    // Initialise a controller datasource without auto sync
    protected override void InitialiseNoSync(BaseModel model)
    {
        // Initialiase this controller
        base.InitialiseNoSync(model);
    }


}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
