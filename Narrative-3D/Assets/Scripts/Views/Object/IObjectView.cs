﻿using System;
using Object.GUIstate;
using Object.Type;
using UnityEngine;

// Dispatch Physical events
public class ObjectClickedEventArgs : EventArgs { }
public class ObjectMovedEventArgs : EventArgs { }
public class ObjectLookedAtEventArgs : EventArgs
{
    public GameObject subject;
    public GameObject indirectObject;

    public ObjectLookedAtEventArgs(GameObject subject, GameObject indirectObject)
    {
        this.subject = subject;
        this.indirectObject = indirectObject;
    }
}
public class ObjectInteractedEventArgs : EventArgs
{
    public GameObject subject;
    public GameObject indirectObject;

    public ObjectInteractedEventArgs(GameObject subject, GameObject indirectObject)
    {
        this.subject = subject;
        this.indirectObject = indirectObject;
    }
}


/// <summary>
/// This is the interface View class for an Object.
/// </summary>
public interface IObjectView : IBaseView
{
    // Dispatched when the object is clicked
    event EventHandler<ObjectClickedEventArgs> OnClicked;

    // Dispatched when the object is collided with
    event EventHandler<ObjectLookedAtEventArgs> OnLookedAt;

    // Dispatched when the object is interacted with
    event EventHandler<ObjectInteractedEventArgs> OnInteracted;

    // Dispatched when the object is moving
    event EventHandler<ObjectMovedEventArgs> OnMoved;


    // Set the objects position in the navigation
    Vector3 Position { get; set; }

    // Set the objects rotation in the navigation
    Quaternion Rotation { get; set; }

    // Set the objects size in the navigation
    Vector3 Scale { get; set; }


    // Set the object type
    ObjectTypeEnum ObjectType { get; set; }

    // Set the object type
    ObjectGUIStateEnum GuiState { get; set; }


    // Executes event that happen when being clicked
    void Click();

    // Executes events that will move the object
    void Move();

    // Executes events when behind watched/focussed/hovered/LookedAt upon
    void LookedAt(GameObject subject, GameObject directObject);

    // Executes events when interacting with something
    void Interact(GameObject subject, GameObject directObject);

}