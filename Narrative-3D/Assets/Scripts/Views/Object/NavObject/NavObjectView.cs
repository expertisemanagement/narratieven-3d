﻿using UnityEngine;
using Object.GUIstate;

/// <summary>
/// This is the base View class for a NavObject. A NavObject is an object that represents a SemanticObject in the Droste-effect navigation.
/// </summary>
public class NavObjectView : ObjectView, INavObjectView
{
    // [DON'T USE!] Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string id)
    {
       return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public virtual BaseView Initialise(string id, SemanticObjectModel semanticObjectModel)
    {
        // Set semanticObjectModel
        this.b_semanticObject = semanticObjectModel;

        // Excecute base initialisation
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, syncToModel);

    }

    // In world position (location)
    [HideInInspector] public override Vector3 b_position
    {
        get
        {
            return transform.position;
        }
        set
        {
            transform.position = value;
        }
    }
    [HideInInspector] public override Vector3 Position
    {
        get
        {
            if (Controller != null) return ((ObjectModel)Controller.Model).Position; else return b_position;
        }
        set
        {
            b_position = value;
        }
    }

    // In world rotation
    [HideInInspector] public override Quaternion b_rotation
    {
        get
        {
            return transform.rotation;
        }
        set
        {
            transform.rotation = value;
        }
    }
    [HideInInspector] public override Quaternion Rotation
    {
        get
        {
            if (Controller != null) return ((ObjectModel)Controller.Model).Rotation; else return b_rotation;
        }
        set
        {
            transform.rotation = value;
        }
    }

    // In world scale
    [HideInInspector] public override Vector3 b_scale
    {
        get
        {
            return transform.localScale;
        }
        set
        {
            transform.localScale = value;
        }
    }
    [HideInInspector] public override Vector3 Scale
    {
        get
        {
            if (Controller != null) return ((ObjectModel)Controller.Model).Scale; else return b_scale;
        }
        set
        {
            b_scale = value;
        }
    }

    // Controller for sending events and syncing data based on the Model
    [HideInInspector] public SemanticObjectModel b_semanticObject;
    [HideInInspector] public virtual SemanticObjectModel semanticObject
    {
        get
        {
            if (Controller != null) return ((NavObjectModel)Controller.Model).SemanticObject; else return b_semanticObject;
        }

        set
        {
            b_semanticObject = value;
        }
    }

    // Object GUI state for defining it's visualising it's interactionstate within the (navigation)world
    [HideInInspector] public override ObjectGUIStateEnum GuiState
    {
        get
        {
            if (Controller != null) return ((ObjectModel)Controller.Model).GUIState; else return b_guiState;
        }
        set
        {
            b_guiState = value;
        }
    }

    // Methode for syncing View data to the Model
    public override void SyncViewToModel()
    {
        base.SyncViewToModel();

        ((NavObjectModel)this.Controller.Model).SemanticObject = semanticObject;
    }

    // (No initialisation part of the) sort of constructor for the monobehaviour 
    // Initialise a controller datasource without auto sync
    protected override void InitialiseNoSync(BaseModel model)
    {
        // Initialiase this controller
        base.InitialiseNoSync(model);
    }
}
