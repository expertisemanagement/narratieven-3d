﻿using System;
using UnityEngine;
using Object.Type;
using Object.GUIstate;

/// <summary>
/// This is the View class for an Object.
/// </summary>
public class ObjectView : BaseView, IObjectView
{
    // If this variable is true, than the object is designed by a designer
    [SerializeField] public bool designed = false;

    // Return the entityID. If none exists then generate it
    [HideInInspector] public override string  EntityID
    {
        get
        {
            if (b_entityID == null || b_entityID == "" && designed == true)
                b_entityID = ObjectFactory.Instance.GenerateEntityID();

            return b_entityID;
        }
        set
        {
            b_entityID = value;
        }
    }

    // In world position (in sync with the Transform component)
    [HideInInspector] public virtual Vector3 b_position
    {
        get
        {
            return transform.position;
        }
        set
        {
            transform.position = value;
        }
    }
    [HideInInspector] public virtual Vector3 Position
    {
        get
        {
            if (Controller != null) return ((ObjectModel)Controller.Model).Position;
            else return b_position;
        }
        set
        {
            b_position = value;
        }
    }

    // In world rotation (in sync with the Transform component)
    [HideInInspector] public virtual Quaternion b_rotation
    {
        get
        {
            return transform.rotation;
        }
        set
        {
            transform.rotation = value;
        }
    }
    [HideInInspector] public virtual Quaternion Rotation
    {
        get
        {
            if (Controller != null) return ((ObjectModel)Controller.Model).Rotation;
            else return b_rotation;
        }
        set
        {
            transform.rotation = value;
        }
    }

    // In world scale (in sync with the Transform component)
    [HideInInspector] public virtual Vector3 b_scale
    {
        get
        {
            return transform.localScale;
        }
        set
        {
            transform.localScale = value;
        }
    }
    [HideInInspector] public virtual Vector3 Scale
    {
        get
        {
            if (Controller != null) return ((ObjectModel)Controller.Model).Scale; else return b_scale;
        }
        set
        {
            b_scale = value;
        }
    }
    
    // Object type for defining it's use and meaning within the world
    [SerializeField] public ObjectTypeEnum b_objectType = ObjectTypeEnum.UNASSIGNED;
    [HideInInspector] public virtual ObjectTypeEnum ObjectType
    {
        get
        {
            if (FindSetController() != null) return ((ObjectModel)Controller.Model).ObjectType;
            else return b_objectType;
        }
        set
        {
            b_objectType = value;
        }
    }

    // Object GUI state for defining it's visualising it's interactionstate within the world
    [SerializeField] public ObjectGUIStateEnum b_guiState = ObjectGUIStateEnum.INACTIVE;
    [HideInInspector] public virtual ObjectGUIStateEnum GuiState
    {
        get
        {
            if (Controller != null) return ((ObjectModel)Controller.Model).GUIState;
            else return b_guiState;
        }
        set
        {
            b_guiState = value;
        }
    }


    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string id)
    {
        // Set objectType
        SetDefaultObjectTypeForTheRightType();

        // Excecute base initialisation
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public virtual BaseView Initialise(string id, ObjectTypeEnum objectType)
    {
        // Set objectType
        SetDefaultObjectTypeForTheRightType();

        // Set objectType
        if (objectType != ObjectTypeEnum.UNASSIGNED)
            b_objectType = objectType;

        // Excecute base initialisation
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, syncToModel);
    }
    

    // Methode for syncing View data to the Model
    public override void SyncViewToModel()
    {
        base.SyncViewToModel();

        ((ObjectController)this.Controller).SyncPositionViewToModel(b_position);
        ((ObjectController)this.Controller).SyncRotationViewToModel(b_rotation);
        ((ObjectController)this.Controller).SyncScaleViewToModel(b_scale);

        ((ObjectController)this.Controller).SyncObjectTypeViewToModel(b_objectType);
        ((ObjectController)this.Controller).SyncGUIStateViewToModel(b_guiState);
    }


    // Executes when Destroy(this.gameobject) is being called
    public override void OnDestroy()
    {
        // On View destruction, also remove it from the index
        if (this is SemanticObjectView)
        {
            ObjectFactory.Instance.RemoveFromIndex(FindSetController());
            ObjectFactory.Instance.RemoveFromIndex(this);
        }
        else
        {
            ObjectFactory.Instance.RemoveFromIndex(FindCreateSetController().FindSetModel());
            ObjectFactory.Instance.RemoveFromIndex(FindSetController());
            ObjectFactory.Instance.RemoveFromIndex(this);
        }
    }


    // Executes event that happen when being clicked
    public void Click()
    {
        //Do something... then Dispatch to event

        // Dispatch the 'on clicked' event
        var eventArgs = new ObjectClickedEventArgs();
        OnClicked(this, eventArgs);
    }

    // Executes events that will move the object
    public void Move()
    {
        //Do something... then Dispatch to event

        // Dispatch the 'on move' event
        var eventArgs = new ObjectMovedEventArgs();
        OnMoved(this, eventArgs);
    }

    // Executes events when behind watched/focussed/hovered/LookedAt upon
    public void LookedAt(GameObject subject, GameObject indirectObject)
    {
        //Do something... then Dispatch to event

        // Dispatch the 'on move' event
        var eventArgs = new ObjectLookedAtEventArgs(subject, indirectObject);
        OnLookedAt(this, eventArgs);
    }

    // Executes events when interacting with something
    public void Interact(GameObject subject, GameObject indirectObject)
    {
        //Do something... then Dispatch to event

        // Dispatch the 'on move' event
        var eventArgs = new ObjectInteractedEventArgs(subject, indirectObject);
        OnInteracted(this, eventArgs);
    }


    // Finds for an existing controller
    public sealed override BaseController FindSetController()
    {
        if (b_controller == null)
        {
            if (this is SemanticObjectView)
            {
                // Looks up for the existence of a controller with the same objectURI
                SemanticObjectController semanticController = ObjectFactory.Instance.GetSemanticObjectControllerByURI(((SemanticObjectView)this).ObjectURI);

                if (semanticController != null)
                {
                    this.Controller = semanticController;
                    return Controller;
                }
                else
                    return null;
            }

            else
            {
                // Looks up for the existence of a controller with the same objectURI
                BaseController controller = ObjectFactory.Instance.GetObjectControllerByEntityID(EntityID);

                if (controller != null)
                {
                    this.Controller = controller;
                    return controller;
                }
                else
                    return null;
            }
        }
        else
        {
            return b_controller;
        }
    }

    // Finds for an existing controller, else creates one
    public override BaseController FindCreateSetController()
    {

        // Looks up for the existence of a view with the same entityID
        if (FindSetController() != null)
            return FindSetController();

        else if (this is SemanticObjectView)
        {
            this.Controller = ObjectFactory.Instance.controllerFactory.createForSemanticObject(((SemanticObjectView)this), false);

            return this.Controller;
        }

        else
        {
            this.Controller = ObjectFactory.Instance.controllerFactory.createForObject(this, false);
            return this.Controller;
        }
    }


    // (No initialisation part of the) sort of constructor for the monobehaviour. Initialise a controller datasource without auto sync
    protected override void InitialiseNoSync(BaseModel model)
    {
        // Initialiase this controller
        base.InitialiseNoSync(model);
    }
    
    // Sets the default ObjectType for this object
    protected void SetDefaultObjectTypeForTheRightType()
    {
        if (this.GetType() == typeof(PracticeView))
            this.b_objectType = ObjectTypeEnum.PRACTICE;

        else if (this.GetType() == typeof(ContextView))
            this.b_objectType = ObjectTypeEnum.CONTEXT;

        else if (this.GetType() == typeof(IntentionalElementView))
            this.b_objectType = ObjectTypeEnum.INTENTIONALELEMENT;

        else if (this.GetType() == typeof(ActorView))
            this.b_objectType = ObjectTypeEnum.ACTOR;

        else if (this.GetType() == typeof(ActivityView))
            this.b_objectType = ObjectTypeEnum.ACTIVITY;

        else if (this.GetType() == typeof(SMWObjectView))
            this.b_objectType = ObjectTypeEnum.SMWOBJECT;

        else if (this.GetType() == typeof(SemanticObjectView))
            this.b_objectType = ObjectTypeEnum.SEMANTICOBJECT;

        else if (this.GetType() == typeof(ComplexObjectView))
            this.b_objectType = ObjectTypeEnum.COMPLEXOBJECT;

        else if (this.GetType() == typeof(NavObjectView))
            this.b_objectType = ObjectTypeEnum.NAVOBJECT;

        else if (this.GetType() == typeof(CameraView))
            this.b_objectType = ObjectTypeEnum.CAMERA;
        else
            this.b_objectType = ObjectTypeEnum.UNASSIGNED;
    }


    // View events (+ MonoBehaviour events coming from the inherited class)
    public event EventHandler<ObjectClickedEventArgs> OnClicked = (sender, e) => { };
    public event EventHandler<ObjectMovedEventArgs> OnMoved = (sender, e) => { };
    public event EventHandler<ObjectLookedAtEventArgs> OnLookedAt = (sender, e) => { };
    public event EventHandler<ObjectInteractedEventArgs> OnInteracted = (sender, e) => { };
}
