﻿using Object.CameraObject.ActionInput;
using Object.CameraZoomInput;
using ObjectControl.Type;
using SCamera.State;
using SCamera.Type;
using System.Collections.Generic;
using UnityEngine;

public class CameraControlView : ObjectControlView, ICameraControlView
{

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string id)
    {
        // Excecute base initialisation
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public override BaseView Initialise(BaseModel model, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, syncToModel);

    }

    // Preinitialisation
    public override void Awake()
    {
        if (b_defaultCameraActionInput == null)
        {
            defaultCameraActionInput = new CameraActionInput[7];

            defaultCameraActionInput[0] = new CameraActionInput();
            defaultCameraActionInput[0].actionInput = CameraActionInputEnum.ZOOMING;
            defaultCameraActionInput[0].inputKeyAxes = new List<string>();
            defaultCameraActionInput[0].inputKeyAxes.Add("Depth_Zoom");
        }

        base.Awake();
    }

    /* controls settings */

    // Defines the type of camera. A.k.a it's usage
        [SerializeField] public CameraTypeEnum b_cameraType = CameraTypeEnum.FIRSTPERSON;
        [HideInInspector] public CameraTypeEnum cameraType
        {
            get
            {
                if (Controller != null) return ((CameraControlModel)Controller.Model).cameraType; else return b_cameraType;
            }
            set
            {
                b_cameraType = value;
            }
        }

        // Defines the state of camera. A.k.a it's usage
        [SerializeField] public CameraStateEnum b_cameraState = CameraStateEnum.MANUAL;
        [HideInInspector] public CameraStateEnum cameraState
        {
            get
            {
                if (Controller != null) return ((CameraControlModel)Controller.Model).cameraState; else return b_cameraState;
            }
            set
            {
                b_cameraState = value;
            }
        }

        // An Array of "ObjectMovmentInput" object, that define default inputKey for every possible cameraMovement 
        [SerializeField] public CameraActionInput[] b_defaultCameraActionInput;
        [HideInInspector] public CameraActionInput[] defaultCameraActionInput
        {
            get
            {
                if (Controller != null) return ((CameraControlModel)Controller.Model).defaultCameraActionInput; else return b_defaultCameraActionInput;
            }
            set
            {
                b_defaultCameraActionInput = value;
            }
        }

        // An Array of "ObjectMovmentInput" object, that define custom inputKey for every possible cameraMovement 
        [SerializeField] public CameraActionInput[] b_customCameraActionInput;
        [HideInInspector] public CameraActionInput[] customCameraActionInput
        {
            get
            {
                if (Controller != null) return ((CameraControlModel)Controller.Model).customCameraActionInput; else return b_customCameraActionInput;
            }
            set
            {
                b_customCameraActionInput = value;
            }
        }

        // An Array of "ObjectMovmentInput" object, that all allowed inputKey for every possible cameraMovement of the current camera ( = defaultCameraActionInput/customCameraMovementInput containing only for cameraMovement)
        [HideInInspector] private CameraActionInput[] b_availableCameraActionInput;
        [HideInInspector] public CameraActionInput[] availableCameraActionInput
        {
            get
            {
                if (Controller != null) return ((CameraControlModel)Controller.Model).availableCameraActionInput; else return b_availableCameraActionInput;
            }
            set
            {
                b_availableCameraActionInput = value;
            }
        }

        // Defines the not-posible actions of the object
        [SerializeField] public  CameraActionInputEnum[] b_actionRestrictions;
        [HideInInspector] public CameraActionInputEnum[] actionRestrictions
        {
            get
            {
                if (Controller != null) return ((CameraControlModel)Controller.Model).actionRestrictions; else return b_actionRestrictions;
            }
            set
            {
                b_actionRestrictions = value;
            }
        }

    /* Zoom */

        // Zoom speed
        public float b_zoomSpeed = 1.0F;
        public float zoomSpeed
        {
            get
            {
                if (Controller != null) return ((CameraControlModel)Controller.Model).zoomSpeed; else return b_zoomSpeed;
            }
            set
            {
                b_zoomSpeed = value;
            }
        }

        //Toggle max. zoom ristriction        
        public float b_startingZoomFactor = 0.0F;
        public float startingZoomFactor

        {
            get
            {
                if (Controller != null) return ((CameraControlModel)Controller.Model).startingZoomFactor; else return b_startingZoomFactor;
            }
            set
            {
                b_startingZoomFactor = value;
            }
        }


        // Starting zoomfactor
        public bool b_maxZoomRestricted = false;
        public bool maxZoomRestricted
        {
            get
            {
                if (Controller != null) return ((CameraControlModel)Controller.Model).maxZoomRestricted; else return b_maxZoomRestricted;
            }
            set
            {
                b_maxZoomRestricted = value;
            }
        }

        // Max. Zoom factor 
        public float b_maxZoomFactor = 100.0F;
        public float maxZoomFactor
        {
            get
            {
                if (Controller != null) return ((CameraControlModel)Controller.Model).maxZoomFactor; else return b_maxZoomFactor;
            }
            set
            {
                b_maxZoomFactor = value;
            }
        }


        //Toggle min. zoom ristriction
        public bool b_minZoomRestricted = false;
        public bool minZoomRestricted
        {
            get
            {
                if (Controller != null) return ((CameraControlModel)Controller.Model).minZoomRestricted; else return b_minZoomRestricted;
            }
            set
            {
                b_minZoomRestricted = value;
            }
        }

        // Min. Zoom factor
        public float b_minZoomFactor = -100.0F;
        public float minZoomFactor
        {
            get
            {
                if (Controller != null) return ((CameraControlModel)Controller.Model).minZoomFactor; else return b_minZoomFactor;
            }
            set
            {
                b_minZoomFactor = value;
            }
        }

    // (No initialisation part of the) sort of constructor for the monobehaviour 
    // Initialise a controller datasource without auto sync
    protected override void InitialiseNoSync(BaseModel model)
    {
        // Initialiase this controller
        base.InitialiseNoSync(model);
    }
}
