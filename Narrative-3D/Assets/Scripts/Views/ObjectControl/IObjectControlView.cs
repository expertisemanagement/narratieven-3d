﻿using Object.ControlSetup;
using Object.Movement;
using Object.MovementInput;
using Object.MovementRestriction;

public interface IObjectControlView
{

     /* controls settings */
    
        // An Array of "ObjectMovmentInput" object, that define default inputKey for every possible cameraMovement 
        ObjectMovementInput[] defaultObjectMovementInput { get; set; }

        // An Array of "ObjectMovmentInput" object, that define custom inputKey for every possible cameraMovement 
        ObjectMovementInput[] customObjectMovementInput { get; set; }

        // An Array of "ObjectMovmentInput" object, that all allowed inputKey for every possible cameraMovement of the current camera ( = defaultObjectMovementInput/customCameraMovementInput containing only for cameraMovement)
        ObjectMovementInput[] availableObjectMovementInput { get; set; }

        // Defines the objects input controls and the movement they control
        ControlSetupEnum controlSetup { get; set; }

        // Defines the posible movement of the object
        ObjectMovementEnum[] movement { get; set; }

        // Defines the not-posible movement of the object
        ObjectMovementRestrictionEnum[] movementRestrictions { get; set; }

    /* Axial Movement */

        // Longitudinal movement speed
        float longitudinalMovementSpeed { get; set; }

        // Starting location longitudinal distance
        float startingDistanceLongitudinalMovement { get; set; }

        // Toggle max. lateral movement ristriction
        bool maxLongitudinalMoveDistanceRestricted { get; set; }

        // Max. lateral move distance
        float maxLongitudinalMoveDistance { get; set; }

        //Toggle min. lateral movement ristriction
        bool minLongitudinalMoveDistanceRestricted { get; set; }

        // Min. lateral move distance
        float minLongitudinalMoveDistance { get; set; }


        // Vertical movement speed
        float verticalMovementSpeed { get; set; }

        // Starting location vertical distance
        float startingDistanceVerticalMovement { get; set; }

        // Toggle max. vertical movement ristriction
        bool maxVerticalMoveDistanceRestricted { get; set; }

        // Max. vertical move distance
        float maxVerticalMoveDistance { get; set; }

        //Toggle min. vertical movement ristriction
        bool minVerticalMoveDistanceRestricted  { get; set; }

        // Min. vertical move distance
        float minVerticalMoveDistance  { get; set; }


        // Lateral  movement speed
        float lateralMovementSpeed { get; set; }

        // Starting location lateral distance
        float startingDistanceLateralMovement { get; set; }

        // Toggle max. vertical movement ristriction
        bool maxLateralMoveDistanceRestricted { get; set; }

        // Max. vertical move distance
        float maxLateralMoveDistance { get; set; }

        //Toggle min. vertical movement ristriction
        bool minLateralMoveDistanceRestricted { get; set; }

        // Min. vertical move distance
        float minLateralMoveDistance { get; set; }


    /* Radial movement/Rotation on this object. Based on aircraft principal axes pitch (x-axes rotation), yaw (y-axes rotation), roll (z-axes rotation) */

        // Pitch rotation around speed 
        float pitchRotationSpeed { get; set; }

        // Toggle max. pitch rotation angle ristriction
        bool pitchRotationAngleRestricted { get; set; }

        // Starting pitch rotation angle
        float startingPitchRotationAngle { get; set; }

        // Max. backward pitch rotation angle
        float maxPitchNegRotationAngle { get; set; }

        // Max. forward pitch rotation angle
        float maxPitchPosRotationAngle  { get; set; }

        // Yaw rotation speed 
        float yawRotationSpeed { get; set; }

        // Toggle max. yaw angle rotation around  ristriction
        bool yawRotationAngleRestricted  { get; set; }

        // Starting yaw rotation angle
        float startingYawRotationAngle { get; set; }

        // Max. down angle rotation around 
        float maxYawNegRotationAngle { get; set; }

        // Max. up angle rotation around 
        float maxYawPosRotationAngle { get; set; }


        // Roll rotation speed 
        float rollRotationSpeed { get; set; }

        // Toggle max. roll rotation angle ristriction
        bool rollRotationAngleRestricted { get; set; }

        // Starting roll rotation angle
        float startingRollRotationAngle { get; set; }

        // Max. left roll rotation angle 
        float maxRollNegRotationAngle  { get; set; }

        // Max. right roll rotation angle  
        float maxRollPosRotationAngle { get; set; }

    /* Radial movement around/Rotation around subject. Based on the spherical coordinate system: radius (r), polar angle θ(theta), and azimuthal angle φ(phi). */

        // Radial distance rotation change speed 
        float radiusChangeSpeed  { get; set; }

        // Toggle max. horizontal rotation around angle ristriction
        bool radiusRestricted { get; set; }

        // Max. radial distance around expansion from starting coordinate
        float maxRadiusExpansion { get; set; }

        // Min. radial distance around from objects from center coordinate
        float minRadiusFromCenter { get; set; }


        // Polar angle rotation around speed (This is horizontal or as in geography "latitude")
        float polarAngleCoordinatesChangeSpeed { get; set; }

        // Toggle max. polar angle rotation around ristriction
        bool polarRotationAroundAngleRestricted { get; set; }

        // Starting angle rotation around polar angle
        float startingPolarRotationAroundAngle { get; set; }

        // Max. Down rotation around polar angle
        float maxPolarNegRotationAroundAngle { get; set; }

        // Max. Up rotation around polar angle
        float maxPolarPosRotationAroundAngle { get; set; }


        // Azimuthal rotation around (This is vertical or as in geography "longtitude")
        float azimuthalAngleCoordinatesChangeSpeed { get; set; }

        // Toggle max. azimuthal rotation around angle ristriction
        bool azimuthalRotationAroundAngleRestricted { get; set; }

        // Starting angle rotation around azimuthal angle
        float startingAzimuthalRotationAroundAngle { get; set; }

        // Max. left rotation around azimuthal angle
        float maxAzimuthalNegRotationAroundAngle { get; set; }

        // Max. right rotation around azimuthal angle
        float maxAzimuthalPosRotationAroundAngle { get; set; }


    /* Targets for interaction and control */

        // Indicates the GameObject which controls this object
        ComplexObjectView subject { get; set; }

        // Indicates the currently controlled object
        ComplexObjectView dedicatedObject { get; set; }

        // Backing field for the interactable object of intressed
        ComplexObjectView target { get; set; }

}
