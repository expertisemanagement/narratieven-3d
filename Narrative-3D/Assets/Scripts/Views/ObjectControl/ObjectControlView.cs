﻿using Object.ControlSetup;
using Object.Movement;
using Object.MovementInput;
using Object.MovementRestriction;
using ObjectControl.Type;
using System.Collections.Generic;
using UnityEngine;

public class ObjectControlView : BaseView, IObjectControlView
{
    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public override BaseView Initialise(string id)
    {
        // Excecute base initialisation
        return base.Initialise(id);
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource, by adding a ref to a controller found by a search on search on all controllers using objectID
    public virtual BaseView Initialise(string id, ComplexObjectView complexObject)
    {
        // Excecute base initialisation
        Initialise(id);

        // Remove existing view
        if (complexObject.GetComponent<ObjectControlView>() != null && complexObject.GetComponent<ObjectControlView>() != this)
        {
            Destroy(complexObject.GetComponent<ObjectControlView>());

            // Set this as new ControlView
            complexObject.ControlSettings.Controller.View = this;
        }

        // Return this view
        return this;
    }

    // Sort of constructor for the monobehaviour 
    // Initialise a controller datasource and auto sync to View (=false)  or Model (=true)
    public virtual BaseView Initialise(ObjectControlModel model, bool syncToModel)
    {
        // Excecute base initialisation
        return base.Initialise(model, syncToModel);
    }

    [HideInInspector]
    public override string EntityID
    {
        get
        {
            if (b_entityID == null || b_entityID == "")
                b_entityID = ObjectFactory.Instance.GenerateEntityID();

            return b_entityID;
        }
        set
        {
            b_entityID = value;
        }
    }

    // Preinitialisation
    public override void Awake()
    {
        if (b_defaultObjectMovementInput == null)
        {
            b_defaultObjectMovementInput = new ObjectMovementInput[7];

            int arrayIndex = 0;

            b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.MOVELATERAL;
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Horizontal_Move");

            arrayIndex++;

            b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.MOVEVERTICAL;
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Vertical_Move");

            arrayIndex++;

            b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEAROUNDAZIMUTHAL;
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Horizontal_RotateAround");

            arrayIndex++;

            b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEAROUNDPOLAR;
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Vertical_RotateAround");

            arrayIndex++;

            b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEYAW;
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Horizontal_Rotate");

            arrayIndex++;

            b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.ROTATEPITCH;
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Vertical_Rotate");

            arrayIndex++;

            b_defaultObjectMovementInput[arrayIndex] = new ObjectMovementInput();
            b_defaultObjectMovementInput[arrayIndex].moveByForce = false;
            b_defaultObjectMovementInput[arrayIndex].movement = ObjectMovementEnum.MOVELONGITUDINAL;
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes = new List<string>();
            b_defaultObjectMovementInput[arrayIndex].inputKeyAxes.Add("Depth_Move");

            arrayIndex++;
        }

        base.Awake();
    }

    // Returns the controlType
    public ControlTypeEnum GetControlType()
    {
        if (this.GetType() == typeof(CameraControlModel))
            return ControlTypeEnum.CAMERACONTROL;
        else
            return ControlTypeEnum.OBJECTCONTROL;

    }

    /* controls settings */

        // Controller for sending events and syncing data based on the Model
        [HideInInspector] private ObjectMovementInput[] b_defaultObjectMovementInput;
        [HideInInspector] public virtual ObjectMovementInput[] defaultObjectMovementInput
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).defaultObjectMovementInput; else return b_defaultObjectMovementInput;
            }
            set { b_defaultObjectMovementInput = value; }
        }

        // An Array of "ObjectMovmentInput" object, that define custom inputKey for every possible cameraMovement 
        [SerializeField] public ObjectMovementInput[] b_customObjectMovementInput;
        [HideInInspector] public virtual ObjectMovementInput[] customObjectMovementInput
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).customObjectMovementInput; else return b_customObjectMovementInput;
            }
            set
            {
                b_defaultObjectMovementInput = value;
            }
        }

        // An Array of "ObjectMovmentInput" object, that all allowed inputKey for every possible cameraMovement of the current camera ( = defaultObjectMovementInput/customCameraMovementInput containing only for cameraMovement)
        [HideInInspector] private ObjectMovementInput[] b_availableObjectMovementInput;
        [HideInInspector] public virtual ObjectMovementInput[] availableObjectMovementInput
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).availableObjectMovementInput; else return b_availableObjectMovementInput;
            }
            set
            {
                b_availableObjectMovementInput = value;
            }
        }

        // Defines the objects input controls and the movement they control
        [SerializeField] public ControlSetupEnum b_controlSetup = ControlSetupEnum.DEFAULT;
        [HideInInspector] public virtual ControlSetupEnum controlSetup
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).controlSetup; else return b_controlSetup;
            }
            set
            {
                b_controlSetup = value;
            }
        }

        // Defines the posible movement of the object
        [SerializeField] public ObjectMovementEnum[] b_movement;
        [HideInInspector] public ObjectMovementEnum[] movement
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).movement; else return b_movement;
            }
            set
            {
                b_movement = value;
            }
        }

        // Defines the not-posible movement of the object
        [SerializeField] public ObjectMovementRestrictionEnum[] b_movementRestrictions;
        [HideInInspector] public ObjectMovementRestrictionEnum[] movementRestrictions
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).movementRestrictions; else return b_movementRestrictions;
            }
            set
            {
                b_movementRestrictions = value;
            }
        }

    /* Axial Movement */

        // Longitudinal movement speed
        [SerializeField] public float b_longitudinalMovementSpeed = 1.0F;
        [HideInInspector] public virtual float longitudinalMovementSpeed
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).longitudinalMovementSpeed; else return b_longitudinalMovementSpeed;
            }
            set
            {
                b_longitudinalMovementSpeed = value;
            }
        }

        // Starting location longitudinal distance
        [SerializeField] public float b_startingDistanceLongitudinalMovement = 0.0F;
        [HideInInspector] public virtual float startingDistanceLongitudinalMovement
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).startingDistanceLongitudinalMovement; else return b_startingDistanceLongitudinalMovement;
            }
            set
            {
                b_startingDistanceLongitudinalMovement = value;
            }
        }
        
        // Toggle max. lateral movement ristriction
        [SerializeField] public bool b_maxLongitudinalMoveDistanceRestricted = false;       
        [HideInInspector] public virtual bool maxLongitudinalMoveDistanceRestricted
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxLongitudinalMoveDistanceRestricted; else return b_maxLongitudinalMoveDistanceRestricted;
            }
            set
            {
                b_maxLongitudinalMoveDistanceRestricted = value;
            }
        }
    
        // Max. lateral move distance
        [SerializeField] public float b_maxLongitudinalMoveDistance = 100.0F;
        [HideInInspector] public virtual float maxLongitudinalMoveDistance
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxLongitudinalMoveDistance; else return b_maxLongitudinalMoveDistance;
            }
            set
            {
                b_maxLongitudinalMoveDistance = value;
            }
        }

        //Toggle min. lateral movement ristriction
        [SerializeField] public bool b_minLongitudinalMoveDistanceRestricted = false;
        [HideInInspector] public virtual bool minLongitudinalMoveDistanceRestricted
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).minLongitudinalMoveDistanceRestricted; else return b_minLongitudinalMoveDistanceRestricted;
            }
            set
            {
                b_minLongitudinalMoveDistanceRestricted = value;
            }
        }

        // Min. lateral move distance
        [SerializeField] public float b_minLongitudinalMoveDistance = -100.0F;
        [HideInInspector] public virtual float minLongitudinalMoveDistance
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).minLongitudinalMoveDistance; else return b_minLongitudinalMoveDistance;
            }
            set
            {
                b_minLongitudinalMoveDistance = value;
            }
        }


        // Vertical movement speed
        [SerializeField] public float b_verticalMovementSpeed = 1.0F;
        [HideInInspector] public virtual float verticalMovementSpeed
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).verticalMovementSpeed; else return b_verticalMovementSpeed;
            }
            set
            {
                b_verticalMovementSpeed = value;
            }
        }

        // Starting location vertical distance
        [SerializeField] public float b_startingDistanceVerticalMovement = 0.0F;
        [HideInInspector] public virtual float startingDistanceVerticalMovement
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).startingDistanceVerticalMovement; else return b_startingDistanceVerticalMovement;
            }
            set
            {
                b_startingDistanceVerticalMovement = value;
            }
        }

        // Toggle max. vertical movement ristriction
        [SerializeField] public bool b_maxVerticalMoveDistanceRestricted = false;
        [HideInInspector] public virtual bool maxVerticalMoveDistanceRestricted
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxVerticalMoveDistanceRestricted; else return b_maxVerticalMoveDistanceRestricted;
            }
            set
            {
                b_maxVerticalMoveDistanceRestricted = value;
            }
        }

        // Max. vertical move distance
        [SerializeField] public float b_maxVerticalMoveDistance = 100.0F;
        [HideInInspector] public virtual float maxVerticalMoveDistance
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxVerticalMoveDistance; else return b_maxVerticalMoveDistance;
            }
            set
            {
                b_maxVerticalMoveDistance = value;
            }
        }

        //Toggle min. vertical movement ristriction
        [SerializeField] public bool b_minVerticalMoveDistanceRestricted = false;
        [HideInInspector] public virtual bool minVerticalMoveDistanceRestricted
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).minVerticalMoveDistanceRestricted; else return b_minVerticalMoveDistanceRestricted;
            }
            set
            {
                b_minVerticalMoveDistanceRestricted = value;
            }
        }

        // Min. vertical move distance
        [SerializeField] public float b_minVerticalMoveDistance = -100.0F;
        [HideInInspector] public virtual float minVerticalMoveDistance
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).minVerticalMoveDistance; else return b_minVerticalMoveDistance;
            }
            set
            {
                b_minVerticalMoveDistance = value;
            }
        }


        // Lateral  movement speed
        [SerializeField] public float b_lateralMovementSpeed = 1.0F;
        [HideInInspector] public virtual float lateralMovementSpeed
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).lateralMovementSpeed; else return b_lateralMovementSpeed;
            }
            set
            {
                b_lateralMovementSpeed = value;
            }
        }

        // Starting location lateral distance
        [SerializeField] public float b_startingDistanceLateralMovement = 0.0F;
        [HideInInspector] public virtual float startingDistanceLateralMovement
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).startingDistanceLateralMovement; else return b_startingDistanceLateralMovement;
            }
            set
            {
                b_startingDistanceLateralMovement = value;
            }
        }

        // Toggle max. vertical movement ristriction
        [SerializeField] public bool b_maxLateralMoveDistanceRestricted = false;
        [HideInInspector] public virtual bool maxLateralMoveDistanceRestricted
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxLateralMoveDistanceRestricted; else return b_maxLateralMoveDistanceRestricted;
            }
            set
            {
                b_maxLateralMoveDistanceRestricted = value;
            }
        }

        // Max. vertical move distance
        [SerializeField] public float b_maxLateralMoveDistance = 100.0F;
        [HideInInspector] public virtual float maxLateralMoveDistance
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxLateralMoveDistance; else return b_maxLateralMoveDistance;
            }
            set
            {
                b_maxLateralMoveDistance = value;
            }
        }

        //Toggle min. vertical movement ristriction
        [SerializeField] public bool b_minLateralMoveDistanceRestricted = false;
        [HideInInspector] public virtual bool minLateralMoveDistanceRestricted
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).minLateralMoveDistanceRestricted; else return b_minLateralMoveDistanceRestricted;
            }
            set
            {
                b_minLateralMoveDistanceRestricted = value;
            }
        }

        // Min. vertical move distance
        [SerializeField] public float b_minLateralMoveDistance = -100.0F;
        [HideInInspector] public virtual float minLateralMoveDistance
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).minLateralMoveDistance; else return b_minLateralMoveDistance;
            }
            set
            {
                b_minLateralMoveDistance = value;
            }
        }


    /* Radial movement/Rotation on this object. Based on aircraft principal axes pitch (x-axes rotation), yaw (y-axes rotation), roll (z-axes rotation) */

        // Pitch rotation around speed 
        [SerializeField] public float b_pitchRotationSpeed = 1.0F;
        [HideInInspector] public virtual float pitchRotationSpeed
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).pitchRotationSpeed; else return b_pitchRotationSpeed;
            }
            set
            {
                b_pitchRotationSpeed = value;
            }
        }

        // Toggle max. pitch rotation angle ristriction
        [SerializeField] public bool b_pitchRotationAngleRestricted = false;
        [HideInInspector] public virtual bool pitchRotationAngleRestricted
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).pitchRotationAngleRestricted; else return b_pitchRotationAngleRestricted;
            }
            set
            {
                b_pitchRotationAngleRestricted = value;
            }
        }

        // Starting pitch rotation angle
        [SerializeField] public float b_startingPitchRotationAngle = 0.0F;
        [HideInInspector] public virtual float startingPitchRotationAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).startingPitchRotationAngle; else return b_startingPitchRotationAngle;
            }
            set
            {
                b_startingPitchRotationAngle = value;
            }
        }

        // Max. backward pitch rotation angle
        [SerializeField] public float b_maxPitchNegRotationAngle = 180.0F;
        [HideInInspector] public virtual float maxPitchNegRotationAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxPitchNegRotationAngle; else return b_maxPitchNegRotationAngle;
            }
            set
            {
                b_maxPitchNegRotationAngle = value;
            }
        }

        // Max. forward pitch rotation angle
        [SerializeField] public float b_maxPitchPosRotationAngle = 180.0F;
        [HideInInspector] public virtual float maxPitchPosRotationAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxPitchPosRotationAngle; else return b_maxPitchPosRotationAngle;
            }
            set
            {
                b_maxPitchPosRotationAngle = value;
            }
        }


        // Yaw rotation speed 
        [SerializeField] public float b_yawRotationSpeed = 1.0F;
        [HideInInspector] public virtual float yawRotationSpeed
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).yawRotationSpeed; else return b_yawRotationSpeed;
            }
            set
            {
                b_yawRotationSpeed = value;
            }
        }

        // Toggle max. yaw angle rotation around  ristriction
        [SerializeField] public bool b_yawRotationAngleRestricted = false;
        [HideInInspector] public virtual bool yawRotationAngleRestricted
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).yawRotationAngleRestricted; else return b_yawRotationAngleRestricted;
            }
            set
            {
                b_yawRotationAngleRestricted = value;
            }
        }

        // Starting yaw rotation angle
        [SerializeField] public float b_startingYawRotationAngle = 0.0F;
        [HideInInspector] public virtual float startingYawRotationAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).startingYawRotationAngle; else return b_startingYawRotationAngle;
            }
            set
            {
                b_startingYawRotationAngle = value;
            }
        }

        // Max. down angle rotation around 
        [SerializeField] public float b_maxYawNegRotationAngle = 180.0F;
        [HideInInspector] public virtual float maxYawNegRotationAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxYawNegRotationAngle; else return b_maxYawNegRotationAngle;
            }
            set
            {
                b_maxYawNegRotationAngle = value;
            }
        }

        // Max. up angle rotation around 
        [SerializeField] public float b_maxYawPosRotationAngle = 180.0F;
        [HideInInspector] public virtual float maxYawPosRotationAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxYawPosRotationAngle; else return b_maxYawPosRotationAngle;
            }
            set
            {
                b_maxYawPosRotationAngle = value;
            }
        }
    

        // Roll rotation speed 
        [SerializeField] public float b_rollRotationSpeed = 1.0F;
        [HideInInspector] public virtual float rollRotationSpeed
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).rollRotationSpeed; else return b_rollRotationSpeed;
            }
            set
            {
                b_rollRotationSpeed = value;
            }
        }

        // Toggle max. roll rotation angle ristriction
        [SerializeField] public bool b_rollRotationAngleRestricted = false;
        [HideInInspector] public virtual bool rollRotationAngleRestricted
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).rollRotationAngleRestricted; else return b_rollRotationAngleRestricted;
            }
            set
            {
                b_rollRotationAngleRestricted = value;
            }
        }

        // Starting roll rotation angle
        [SerializeField] public float b_startingRollRotationAngle = 0.0F;
        [HideInInspector] public virtual float startingRollRotationAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).startingRollRotationAngle; else return b_startingRollRotationAngle;
            }
            set
            {
                b_startingRollRotationAngle = value;
            }
        }

        // Max. left roll rotation angle 
        [SerializeField] public float b_maxRollNegRotationAngle = 180.0F;
        [HideInInspector] public virtual float maxRollNegRotationAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxRollNegRotationAngle; else return b_maxRollNegRotationAngle;
            }
            set
            {
                b_maxRollNegRotationAngle = value;
            }
        }

        // Max. right roll rotation angle  
        [SerializeField] public float b_maxRollPosRotationAngle = 180.0F;
        [HideInInspector] public virtual float maxRollPosRotationAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxRollPosRotationAngle; else return b_maxRollPosRotationAngle;
            }
            set
            {
                b_maxRollPosRotationAngle = value;
            }
        }

    /* Radial movement around/Rotation around subject. Based on the spherical coordinate system: radius (r), polar angle θ(theta), and azimuthal angle φ(phi). */

        // Radial distance rotation change speed 
        [SerializeField] public float b_radiusChangeSpeed = 1.0F;
        [HideInInspector] public virtual float radiusChangeSpeed
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).radiusChangeSpeed; else return b_radiusChangeSpeed;
            }
            set
            {
                b_radiusChangeSpeed = value;
            }
        }

        // Toggle max. horizontal rotation around angle ristriction
        [SerializeField] public bool b_radiusRestricted = false;
        [HideInInspector] public virtual bool radiusRestricted
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).radiusRestricted; else return b_radiusRestricted;
            }
            set
            {
                b_radiusRestricted = value;
            }
        }

        // Max. radial distance around expansion from starting coordinate
        [SerializeField] public float b_maxRadiusExpansion = 100.0F;
        [HideInInspector] public virtual float maxRadiusExpansion
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxRadiusExpansion; else return b_maxRadiusExpansion;
            }
            set
            {
                b_maxRadiusExpansion = value;
            }
        }

        // Min. radial distance around from objects from center coordinate
        [SerializeField] public float b_minRadiusFromCenter = 0F;
        [HideInInspector] public virtual float minRadiusFromCenter
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).minRadiusFromCenter; else return b_minRadiusFromCenter;
            }
            set
            {
                b_minRadiusFromCenter = value;
            }
        }
  

        // Polar angle rotation around speed (This is horizontal or as in geography "latitude")
        [SerializeField] public float b_polarAngleCoordinatesChangeSpeed = 1.0F;
        [HideInInspector] public virtual float polarAngleCoordinatesChangeSpeed
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).polarAngleCoordinatesChangeSpeed; else return b_polarAngleCoordinatesChangeSpeed;
            }
            set
            {
                b_polarAngleCoordinatesChangeSpeed = value;
            }
        }

        // Toggle max. polar angle rotation around ristriction
        [SerializeField] public bool b_polarRotationAroundAngleRestricted = false;
        [HideInInspector] public virtual bool polarRotationAroundAngleRestricted
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).polarRotationAroundAngleRestricted; else return b_polarRotationAroundAngleRestricted;
            }
            set
            {
                b_polarRotationAroundAngleRestricted = value;
            }
        }

        // Starting angle rotation around polar angle
        [SerializeField] public float b_startingPolarRotationAroundAngle = 0.0F;
        [HideInInspector] public virtual float startingPolarRotationAroundAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).startingPolarRotationAroundAngle; else return b_startingPolarRotationAroundAngle;
            }
            set
            {
                b_startingPolarRotationAroundAngle = value;
            }
        }

        // Max. Down rotation around polar angle
        [SerializeField] public float b_maxPolarNegRotationAroundAngle = 180.0F;
        [HideInInspector] public virtual float maxPolarNegRotationAroundAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxPolarNegRotationAroundAngle; else return b_maxPolarNegRotationAroundAngle;
            }
            set
            {
                b_maxPolarNegRotationAroundAngle = value;
            }
        }

        // Max. Up rotation around polar angle
        [SerializeField] public float b_maxPolarPosRotationAroundAngle = 180.0F;
        [HideInInspector] public virtual float maxPolarPosRotationAroundAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxPolarPosRotationAroundAngle; else return b_maxPolarPosRotationAroundAngle;
            }
            set
            {
                b_maxPolarPosRotationAroundAngle = value;
            }
        }


        // Azimuthal rotation around (This is vertical or as in geography "longtitude")
        [SerializeField] public float b_azimuthalAngleCoordinatesChangeSpeed = 1.0F;
        [HideInInspector] public virtual float azimuthalAngleCoordinatesChangeSpeed
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).azimuthalAngleCoordinatesChangeSpeed; else return b_azimuthalAngleCoordinatesChangeSpeed;
            }
            set
            {
                b_azimuthalAngleCoordinatesChangeSpeed = value;
            }
        }

        // Toggle max. azimuthal rotation around angle ristriction
        [SerializeField] public bool b_azimuthalRotationAroundAngleRestricted = false;
        [HideInInspector] public virtual bool azimuthalRotationAroundAngleRestricted
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).azimuthalRotationAroundAngleRestricted; else return b_azimuthalRotationAroundAngleRestricted;
            }
            set
            {
                b_azimuthalRotationAroundAngleRestricted = value;
            }
        }

        // Starting angle rotation around azimuthal angle
        [SerializeField] public float b_startingAzimuthalRotationAroundAngle = 0.0F;
        [HideInInspector] public virtual float startingAzimuthalRotationAroundAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).startingAzimuthalRotationAroundAngle; else return b_startingAzimuthalRotationAroundAngle;
            }
            set
            {
                b_startingAzimuthalRotationAroundAngle = value;
            }
        }

        // Max. left rotation around azimuthal angle
        [SerializeField] public float b_maxAzimuthalNegRotationAroundAngle = 180.0F;
        [HideInInspector] public virtual float maxAzimuthalNegRotationAroundAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxAzimuthalNegRotationAroundAngle; else return b_maxAzimuthalNegRotationAroundAngle;
            }
            set
            {
                b_maxAzimuthalNegRotationAroundAngle = value;
            }
        }

        // Max. right rotation around azimuthal angle
        [SerializeField] public float b_maxAzimuthalPosRotationAroundAngle = 180.0F;
        [HideInInspector] public virtual float maxAzimuthalPosRotationAroundAngle
        {
            get
            {
                if (Controller != null) return ((ObjectControlModel)Controller.Model).maxAzimuthalPosRotationAroundAngle; else return b_maxAzimuthalPosRotationAroundAngle;
            }
            set
            {
                b_maxAzimuthalPosRotationAroundAngle = value;
            }
        }


    /* Targets for interaction and control */

        // Indicates the GameObject which controls this object
        [SerializeField] public GameObject b_subject;
        [HideInInspector] public virtual ComplexObjectView subject
        {
            get
            {
                if (Controller != null) return (ComplexObjectView) ((ObjectControlModel)Controller.Model).subject.Controller.View; else return b_subject.GetComponent<ComplexObjectView>();
            }
            set
            {
                b_subject = value.currentGameObject;
            }
        }

        // Indicates the currently controlled object
        [SerializeField] public GameObject b_dedicatedObject;
        [HideInInspector] public virtual ComplexObjectView dedicatedObject
        {
            get
            {
                if (Controller != null) return (ComplexObjectView)((ObjectControlModel)Controller.Model).dedicatedObject.Controller.View; else return b_subject.GetComponent<ComplexObjectView>();
            }
            set
            {
                b_dedicatedObject = value.currentGameObject;
            }
        }

        // Backing field for the interactable object of intressed
        [SerializeField] public GameObject b_target;
        [HideInInspector] public virtual ComplexObjectView target
        {
            get
            {
                if (Controller != null) return (ComplexObjectView)((ObjectControlModel)Controller.Model).target.Controller.View; else return b_subject.GetComponent<ComplexObjectView>();
            }
            set
            {
                b_target = value.currentGameObject;
            }
        }

    // Methode for syncing View data to the Model
    public override void SyncViewToModel()
    {
        base.SyncViewToModel();

        ((ObjectControlController)this.Controller).SyncDefaultObjectMovementInputViewToModel(b_defaultObjectMovementInput);
        ((ObjectControlController)this.Controller).SyncCustomObjectMovementInputViewToModel(b_customObjectMovementInput);
        ((ObjectControlController)this.Controller).SyncAvailableObjectMovementInputViewToModel(b_availableObjectMovementInput);

        ((ObjectControlController)this.Controller).SyncControlSetupViewToModel(b_controlSetup);
        ((ObjectControlController)this.Controller).SyncMovementViewToModel(b_movement);
        ((ObjectControlController)this.Controller).SyncMovementRestrictionsViewToModel(b_movementRestrictions);

        ((ObjectControlController)this.Controller).SyncLongitudinalMovementSpeedViewToModel(b_longitudinalMovementSpeed);
        ((ObjectControlController)this.Controller).SyncStartingDistanceLongitudinalMovementViewToModel(b_startingDistanceLongitudinalMovement);
        ((ObjectControlController)this.Controller).SyncMaxLongitudinalMoveDistanceRestrictedViewToModel(b_maxLongitudinalMoveDistanceRestricted);
        ((ObjectControlController)this.Controller).SyncMaxLongitudinalMoveDistanceViewToModel(b_maxLongitudinalMoveDistance);
        ((ObjectControlController)this.Controller).SyncMinLongitudinalMoveDistanceRestrictedViewToModel(b_minLongitudinalMoveDistanceRestricted);
        ((ObjectControlController)this.Controller).SyncMinLongitudinalMoveDistanceViewToModel(b_minLongitudinalMoveDistance);

        ((ObjectControlController)this.Controller).SyncVerticalMovementSpeedViewToModel(b_verticalMovementSpeed);
        ((ObjectControlController)this.Controller).SyncStartingDistanceVerticalMovementViewToModel(b_startingDistanceVerticalMovement);
        ((ObjectControlController)this.Controller).SyncMaxVerticalMoveDistanceRestrictedViewToModel(b_maxVerticalMoveDistanceRestricted);
        ((ObjectControlController)this.Controller).SyncMaxVerticalMoveDistanceViewToModel(b_maxVerticalMoveDistance);
        ((ObjectControlController)this.Controller).SyncMinVerticalMoveDistanceRestrictedViewToModel(b_minVerticalMoveDistanceRestricted);
        ((ObjectControlController)this.Controller).SyncMinVerticalMoveDistanceViewToModel(b_minVerticalMoveDistance);

        ((ObjectControlController)this.Controller).SyncLateralMovementSpeedViewToModel(b_lateralMovementSpeed);
        ((ObjectControlController)this.Controller).SyncStartingDistanceLateralMovementViewToModel(b_startingDistanceLateralMovement);
        ((ObjectControlController)this.Controller).SyncMaxLateralMoveDistanceRestrictedViewToModel(b_maxLateralMoveDistanceRestricted);
        ((ObjectControlController)this.Controller).SyncMaxLateralMoveDistanceViewToModel(b_maxLateralMoveDistance);
        ((ObjectControlController)this.Controller).SyncMinLateralMoveDistanceRestrictedViewToModel(b_minLateralMoveDistanceRestricted);
        ((ObjectControlController)this.Controller).SyncMinLateralMoveDistanceViewToModel(b_minLateralMoveDistance);

        ((ObjectControlController)this.Controller).SyncPitchRotationSpeedViewToModel(b_pitchRotationSpeed);
        ((ObjectControlController)this.Controller).SyncPitchRotationAngleRestrictedViewToModel(b_pitchRotationAngleRestricted);
        ((ObjectControlController)this.Controller).SyncStartingPitchRotationAngleViewToModel(b_startingPitchRotationAngle);
        ((ObjectControlController)this.Controller).SyncMaxPitchNegRotationAngleViewToModel(b_maxPitchNegRotationAngle);
        ((ObjectControlController)this.Controller).SyncMaxPitchPosRotationAngleViewToModel(b_maxPitchPosRotationAngle);

        ((ObjectControlController)this.Controller).SyncYawRotationSpeedViewToModel(b_yawRotationSpeed);
        ((ObjectControlController)this.Controller).SyncYawRotationAngleRestrictedViewToModel(b_yawRotationAngleRestricted);
        ((ObjectControlController)this.Controller).SyncStartingYawRotationAngleViewToModel(b_startingYawRotationAngle);
        ((ObjectControlController)this.Controller).SyncMaxYawNegRotationAngleViewToModel(b_maxYawNegRotationAngle);
        ((ObjectControlController)this.Controller).SyncMaxYawPosRotationAngleViewToModel(b_maxYawPosRotationAngle);

        ((ObjectControlController)this.Controller).SyncRollRotationSpeedViewToModel(b_rollRotationSpeed);
        ((ObjectControlController)this.Controller).SyncRollRotationAngleRestrictedViewToModel(b_rollRotationAngleRestricted);
        ((ObjectControlController)this.Controller).SyncStartingRollRotationAngleViewToModel(b_startingRollRotationAngle);
        ((ObjectControlController)this.Controller).SyncMaxRollNegRotationAngleViewToModel(b_maxRollNegRotationAngle);
        ((ObjectControlController)this.Controller).SyncMaxRollPosRotationAngleViewToModel(b_maxRollPosRotationAngle);

        ((ObjectControlController)this.Controller).SyncRadiusChangeSpeedViewToModel(b_radiusChangeSpeed);
        ((ObjectControlController)this.Controller).SyncRadiusRestrictedViewToModel(b_radiusRestricted);
        ((ObjectControlController)this.Controller).SyncMaxRadiusExpansionViewToModel(b_maxRadiusExpansion);
        ((ObjectControlController)this.Controller).SyncMinRadiusFromCenterViewToModel(b_minRadiusFromCenter);

        ((ObjectControlController)this.Controller).SyncPolarAngleCoordinatesChangeSpeedViewToModel(b_polarAngleCoordinatesChangeSpeed);
        ((ObjectControlController)this.Controller).SyncPolarRotationAroundAngleRestrictedViewToModel(b_polarRotationAroundAngleRestricted);
        ((ObjectControlController)this.Controller).SyncStartingPolarRotationAroundAngleViewToModel(b_startingPolarRotationAroundAngle);
        ((ObjectControlController)this.Controller).SyncMaxPolarNegRotationAroundAngleViewToModel(b_maxPolarNegRotationAroundAngle);
        ((ObjectControlController)this.Controller).SyncMaxPolarPosRotationAroundAngleViewToModel(b_maxPolarPosRotationAroundAngle);

        ((ObjectControlController)this.Controller).SyncAzimuthalAngleCoordinatesChangeSpeedViewToModel(b_azimuthalAngleCoordinatesChangeSpeed);
        ((ObjectControlController)this.Controller).SyncAzimuthalRotationAroundAngleRestrictedViewToModel(b_azimuthalRotationAroundAngleRestricted);
        ((ObjectControlController)this.Controller).SyncStartingAzimuthalRotationAroundAngleViewToModel(b_startingAzimuthalRotationAroundAngle);
        ((ObjectControlController)this.Controller).SyncMaxAzimuthalNegRotationAroundAngleViewToModel(b_maxAzimuthalNegRotationAroundAngle);
        ((ObjectControlController)this.Controller).SyncMaxAzimuthalPosRotationAroundAngleViewToModel(b_maxAzimuthalPosRotationAroundAngle);

        ((ObjectControlController)this.Controller).SyncSubjectViewToModel(subject);
        ((ObjectControlController)this.Controller).SyncDedicatedObjectViewToModel(dedicatedObject);
        ((ObjectControlController)this.Controller).SyncTargetViewToModel(target); 



    }

    // Finds for an existing controller
    public sealed override BaseController FindSetController()
    {
        if (b_controller == null)
        {
            if (this is CameraControlView)
            {
                // Looks up for the existence of a controller with the same objectURI
                CameraControlController cameraControlController = (CameraControlController)ObjectFactory.Instance.GetObjectControllerByEntityID(EntityID);

                if (cameraControlController != null)
                {
                    this.Controller = cameraControlController;
                    return Controller;
                }
                else
                    return null;
            }

            else
            {
                // Looks up for the existence of a controller with the same objectURI
                ObjectControlController objectControlController = (ObjectControlController)ObjectFactory.Instance.GetObjectControllerByEntityID(EntityID);

                if (objectControlController != null)
                {
                    this.Controller = objectControlController;
                    return Controller;
                }
                else
                    return null;
            }
        }
        else
        {
            return b_controller;
        }
    }

    // Finds for an existing controller, else creates one
    public override BaseController FindCreateSetController()
    {

        // Looks up for the existence of a view with the same entityID
        if (FindSetController() != null)
            return FindSetController();

        else if (this is CameraControlView)
        {
            this.Controller = ObjectFactory.Instance.controllerFactory.createForCameraControl(((CameraControlView)this), false);
            return this.Controller;
        }

        else
        {
            this.Controller = ObjectFactory.Instance.controllerFactory.createForObjectControl(this, false);
            return this.Controller;
        }
    }

    public override void OnDestroy()
    {
        ObjectFactory.Instance.RemoveFromIndex(FindCreateSetController().FindSetModel());
        ObjectFactory.Instance.RemoveFromIndex(FindSetController());
        ObjectFactory.Instance.RemoveFromIndex(this);
    }

    // (No initialisation part of the) sort of constructor for the monobehaviour 
    // Initialise a controller datasource without auto sync
    protected override void InitialiseNoSync(BaseModel model)
    {
        // Initialiase this controller
        base.InitialiseNoSync(model);

        // find and set model (doesn't create one)
        this.Controller.FindSetModel();
    }
}
