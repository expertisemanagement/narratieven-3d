﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.IO;
using VDS.RDF;
using Object.Type;

public class LoadEMontNarrativesTest {

    public string testSceneName = "_Main";
    public bool testSceneLoaded = false;
    public bool projectIsLoaded = false;
    public bool protagonistIsLoaded = false;

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator LoadMainSceneTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        LoadScene(testSceneName);

        yield return null;

        LoadDefaultProject();

        Assert.IsTrue(testSceneLoaded == true, "The following scene could not beloaded: " + testSceneName);
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator ExistingAccessibleActionMangerTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        string mainSceneName = testSceneName;

        this.LoadScene(mainSceneName);

        yield return null;

        bool isSucces = false;

        if (NarrativeManager.instance != null)
            isSucces = true;

        Assert.IsTrue(isSucces == true, "The ActionManager does not exist, or is not accessible");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator ExistingAccessibleGameMangerTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        string mainSceneName = testSceneName;

        this.LoadScene(mainSceneName);

        yield return null;

        bool isSucces = false;

        if (GameManager.instance != null)
            isSucces = true;

        Assert.IsTrue(isSucces == true, "The GameManager does not exist, or is not accessible");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator ExistingAccessibleInterfaceMangerTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        string mainSceneName = testSceneName;

        this.LoadScene(mainSceneName);

        yield return null;

        bool isSucces = false;

        if (InterfaceManager.instance != null)
            isSucces = true;

        Assert.IsTrue(isSucces == true, "The InterfaceManager does not exist, or is not accessible");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator ExistingAccessibleNavManagerTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        string mainSceneName = testSceneName;

        this.LoadScene(mainSceneName);

        yield return null;

        bool isSucces = false;

        if (NavManager.instance != null)
            isSucces = true;

        Assert.IsTrue(isSucces == true, "The NavManager does not exist, or is not accessible");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator ExistingAccessiblePerspectiveManagerTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        string mainSceneName = testSceneName;

        this.LoadScene(mainSceneName);

        yield return null;

        bool isSucces = false;

        if (PerspectiveManager.instance != null)
            isSucces = true;

        Assert.IsTrue(isSucces == true, "The PerspectiveManager does not exist, or is not accessible");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator ExistingAccessibleProjectManagerTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        string mainSceneName = testSceneName;

        this.LoadScene(mainSceneName);

        yield return null;

        bool isSucces = false;

        if (ProjectManager.instance != null)
            isSucces = true;

        Assert.IsTrue(isSucces == true, "The ProjectManager does not exist, or is not accessible");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator ExistingAccessibleSoundManagerTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        string mainSceneName = testSceneName;

        this.LoadScene(mainSceneName);

        yield return null;

        bool isSucces = false;

        if (SoundManager.instance != null)
            isSucces = true;

        Assert.IsTrue(isSucces == true, "The SoundManager does not exist, or is not accessible");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator ExistingAccessibleWorldManagerTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        string mainSceneName = testSceneName;

        this.LoadScene(mainSceneName);

        yield return null;

        bool isSucces = false;

        if (WorldManager.instance != null)
            isSucces = true;

        Assert.IsTrue(isSucces == true, "The WorldManager does not exist, or is not accessible");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator ExistingAccessibleSMWParserTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        string mainSceneName = testSceneName;

        this.LoadScene(mainSceneName);

        yield return null;

        bool isSucces = false;

        if (SMWParser.Instance != null)
            isSucces = true;

        Assert.IsTrue(isSucces == true, "The SMWParser does not exist, or is not accessible");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator FindProjectsUsingTheProjectManagerTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        LoadScene(testSceneName);

        yield return null;

        List<string> listProjectNames = new List<string>();

        foreach (string projectName in ProjectManager.instance.projectNamesAndPaths.Keys)
            if (projectName != ProjectManager.instance.assetBundleFolderName)
                listProjectNames.Add(Path.GetFileName(projectName));

        Assert.IsTrue(listProjectNames.Count >= 0, "Not a single project has been found!");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator LoadProjectFromProjectBrowserTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        LoadScene(testSceneName);

        yield return null;

        Assert.IsTrue(LoadDefaultProject() == true, "Not possible to select a project");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator LoadContextFromProtagonistBrowserTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        string failMessage = "";

        LoadScene(testSceneName);

        yield return null;

        LoadDefaultProject();

        bool isSucces = false;

        if (WorldManager.instance.currentWorldParent == null)
            failMessage = "No world parent View set";

        if (SMWParser.Instance.GetLoadedGraphs().Count > 1)
            isSucces = true;
        else
            failMessage = "No loaded Graphs!";

        Assert.IsTrue(LoadDefaultSituation() == true, failMessage);
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator LoadRelationControllerForIntentionalElementViewsTest()
    {
        // Use the Assert class to test conditions.
        // yield to skip a frame

        string failMessage = "";

        LoadScene(testSceneName);

        yield return null;

        LoadDefaultProject();
        LoadDefaultSituation();

        IntentionalElementView[] allIntentionalElements = GameObject.FindObjectsOfType<IntentionalElementView>();
        List<Relation> ieRelations = new List<Relation>();

        foreach (IntentionalElementView ieView in allIntentionalElements)
        {
            IntentionalElementModel ieModel = ((IntentionalElementModel)ieView.FindCreateSetController().FindCreateSetModel());
            
            foreach (Contributes cont in ieModel.Contributes)
            {
                if (ieRelations.Contains(cont) == false)
                    ieRelations.Add(cont);
            }

            foreach (Dependency dep in ieModel.Depends)
            {
                if (ieRelations.Contains(dep) == false)
                    ieRelations.Add(dep);
            }

            if (ieModel is ActivityModel)
            {
                foreach (Connects con in ((ActivityModel)ieModel).Connects)
                {
                    if (ieRelations.Contains(con) == false)
                        ieRelations.Add(con);
                }
            }
        }

        RelationController[] relationControllers = GameObject.FindObjectsOfType<RelationController>();
        
        Assert.AreEqual(ieRelations.Count, relationControllers.Length, "There are a unequal amount of relations and relationcontrollers");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator LoadRdfInAsGraph()
    {
        LoadScene(testSceneName);

        yield return null;

        LoadDefaultProject();
        LoadDefaultSituation();

        Assert.IsTrue(SMWParser.Instance.GetLoadedGraphs().Count >= 1, "No graphs loaded!");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator LoadEMontCategoriesFromGraphsNodesAsSMWCategories()
    {
        LoadScene(testSceneName);

        yield return null;

        LoadDefaultProject();
        LoadDefaultSituation();

        bool isSucces = false;

        IGraph eMontGraphToUse = null;

        if (SMWParser.Instance.GetLoadedGraphs().Count >= 1)
            eMontGraphToUse = SMWParser.Instance.GetLoadedGraphs()[0];

        List<SMWCategory> foundSMWCategories = SMWParser.Instance.GetAllCategorySMWCategory(eMontGraphToUse);

        if (eMontGraphToUse != null)
            if (foundSMWCategories != null && foundSMWCategories.Count >= 1)
                isSucces = true;

        Assert.IsTrue(isSucces, "No Models found!");
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator LoadEMontInstanceFromGraphsNodesAsModels()
    {
        LoadScene(testSceneName);

        yield return null;

        LoadDefaultProject();
        LoadDefaultSituation();

        bool isSucces = false;

        IGraph eMontGraphToUse = ((SemanticObjectModel)GameObject.FindObjectOfType<SemanticObjectView>().FindCreateSetController().FindCreateSetModel()).SemanticNode.Graph;

        List<SMWCategory> foundSMWCategories = SMWParser.Instance.GetAllCategorySMWCategory(eMontGraphToUse);
        List<SMWObjectModel> foundIntentionalElements = foundSMWCategories.Find(smwC => smwC.objectType == ObjectTypeEnum.ACTIVITY).GetAllnstanceModelsFromThisCategoryAndSubCategoriesUnOrdered();//SMWParser.Instance.GetAllInstancesByObjectType(eMontGraphToUse, ObjectTypeEnum.INTENTIONALELEMENT, true);
        List<SMWObjectModel> foundContexts = foundSMWCategories.Find(smwC => smwC.objectType == ObjectTypeEnum.CONTEXT).GetAllnstanceModelsFromThisCategoryAndSubCategoriesUnOrdered();//SMWParser.Instance.GetAllInstancesByObjectType(eMontGraphToUse, ObjectTypeEnum.CONTEXT, true);

        if (eMontGraphToUse != null)
            if (foundSMWCategories != null && foundSMWCategories.Count >= 1)
                if (foundIntentionalElements.Count >= 1 && foundContexts.Count >= 1)
                    isSucces = true;

        Assert.IsTrue(isSucces, "No Models found!");
    }
    
    // Loads the main scene of the world. Returns true is secceeds.
    private bool LoadScene(string sceneName)
    {
        if (!testSceneLoaded)
        {
            SceneManager.LoadScene(sceneName);
            testSceneLoaded = true;
        }

        if (SceneManager.GetSceneByName(sceneName) != null)
            testSceneLoaded = true;
        else
            testSceneLoaded = false;

        return testSceneLoaded;
    }

    // Loads the main scene of the world. Returns true is secceeds.
    private bool LoadDefaultProject()
    {
        if (!projectIsLoaded)
        {
            GameObject.FindObjectOfType<ProjectMenu>().OnStartButtonClick();
            projectIsLoaded = true;
        }

        return projectIsLoaded;
    }

    // Loads the main scene of the world. Returns true is secceeds.
    private bool LoadDefaultSituation()
    {
        if (!protagonistIsLoaded)
        {
            GameObject.FindObjectOfType<NarrativeMenu>().OnStartButtonClick();
            protagonistIsLoaded = true;
        }

        if (PerspectiveManager.instance.GetCurrentSubject() == null)
            protagonistIsLoaded = false;

        return protagonistIsLoaded;
    }
}
