# "Narratieve 3D"-project README

# 1. About

This project features a Unity project, with builds, for visualising narratives into a interactive 3D experience. This product is a result of a research for the Expertise and Valorisation Management (EVM) team from the HZ - University of Applied Sciences.

# 2. Functional design

Located at:

- (Dutch language) [https://bitbucket.org/expertisemanagement/narratieven-3d/wiki/Functioneel%20ontwerp](https://bitbucket.org/expertisemanagement/narratieven-3d/wiki/Functioneel%20ontwerp)

# 3. Technical design

Located at:

- (Dutch language) [https://bitbucket.org/expertisemanagement/narratieven-3d/wiki/Technisch%20ontwerp](https://bitbucket.org/expertisemanagement/narratieven-3d/wiki/Technisch%20ontwerp)



# 4. Contributors

* L.S. Voncken - Software developer (2017-2018)
